//
//  KBSystemSettingsViewController.swift
//  Driver
//
//  Created by George on 2018-01-15.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class KBSystemSettingsViewController: KBViewController {
    let systemSettingsVM = KBSystemSettingsVM()
    
    @IBOutlet weak var tableView: UITableView!
    class var controller:KBSystemSettingsViewController{
        let controller = UIStoryboard.init(name: "SystemSettings", bundle: nil).instantiateViewController(withIdentifier: "KBSystemSettingsViewController") as! KBSystemSettingsViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 56
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension KBSystemSettingsViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 1:
            return 2
        default:
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0){
        var cell = tableView.dequeueReusableCell(withIdentifier: GDSwitchTableViewCell.cellIdentifier) as? GDSwitchTableViewCell
        if(cell == nil){
            cell = GDSwitchTableViewCell.cell()
        }
        cell?.delegate = self
        if(indexPath.row == 0){
            cell?.textLabel?.text = NSLocalizedString("Drop Shadow Effects", comment: "")
            cell?.switchView.isOn = self.systemSettingsVM.isShadowEnabled
            cell?.tag = 1
        }else if(indexPath.row == 1){
            cell?.textLabel?.text = NSLocalizedString("Show Traffic", comment: "")
            cell?.switchView.isOn = self.systemSettingsVM.isDisplayingTrafficOnMap
            cell?.tag = 2
        }
        else if(indexPath.row == 2){
            cell?.textLabel?.text = NSLocalizedString("Data Saver", comment: "")
            cell?.switchView.isOn = self.systemSettingsVM.isSavingData
            cell?.tag = 4
            }
            return cell!
        }
        else if indexPath.section == 1{
            if(indexPath.row == 0){
                var cell = tableView.dequeueReusableCell(withIdentifier: GDSwitchTableViewCell.cellIdentifier) as? GDSwitchTableViewCell
                if(cell == nil){
                    cell = GDSwitchTableViewCell.cell()
                }
                cell?.textLabel?.text = NSLocalizedString("New Reservation Popup Window", comment: "")
                cell?.switchView.isOn = self.systemSettingsVM.isShadowEnabled
                cell?.tag = 3
                return cell!
            }
            else{
                var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
                if(cell == nil){
                    cell = GDLabelTableViewCell.cell()
                }
                cell?.lbl_Title.text = NSLocalizedString("New Reservation Popup Pause Time", comment: "")
                cell?.lbl_Content.text = "\(systemSettingsVM.lengthOfNewReservationPopupMinutes) " + NSLocalizedString("Minutes", comment: "")
                return cell!
            }
        }
        else {
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section==1 && indexPath.row==1){
            presentPopupTimeSelectorVC()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func reloadTemporaryPopupTime(){
        tableView.reloadRows(at: [IndexPath(row: 1, section: 1)], with: UITableViewRowAnimation.automatic)
    }
    
    func presentPopupTimeSelectorVC(){
        let controller = UIStoryboard(name: "SystemSettings", bundle: nil).instantiateViewController(withIdentifier: "PopupTimeSelectorVC") as! PopupTimeSelectorVC
        controller.parentVC = self
        controller.modalPresentationStyle = .overCurrentContext
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalTransitionStyle = .coverVertical
        self.present(controller, animated: true, completion: {})
    }
    
}
extension KBSystemSettingsViewController : GDSwitchTableViewCellDelegate{
    func GDSwitchTableViewCellValueChanged(_ sender: GDSwitchTableViewCell) {
        if(sender.tag == 1){
            self.systemSettingsVM.isShadowEnabled = sender.switchView.isOn
        }else if(sender.tag == 2){
            self.systemSettingsVM.isDisplayingTrafficOnMap = sender.switchView.isOn
        }
        else if(sender.tag == 3){
            self.systemSettingsVM.isDisplayingNewReservationPopup = sender.switchView.isOn
        }
        else if(sender.tag == 4){
            self.systemSettingsVM.isSavingData = sender.switchView.isOn
            if(sender.switchView.isOn){
                toggleTrafficSwitch(false)
            }
//            self.tableView.reloadData()
        }
    }
    
    func toggleTrafficSwitch(_ toggle:Bool){
       let cell = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! GDSwitchTableViewCell
        cell.switchView.setOn(toggle, animated: true)
    }
}

