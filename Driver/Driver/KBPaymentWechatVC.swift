//
//  KBPaymentWechatVC.swift
//  Driver
//
//  Created by Jack Du on 2018-06-01.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//


class KBPaymentWechatVC : UIViewController{
    
    @IBOutlet var backgroundView: GDView!
    override func viewDidLoad() {
        super.viewDidLoad()
        KBBillManager.shared.getWechatpayOrderInfo(
            start: ({}),
            success: { orderInfo in
                print(orderInfo.partnerId)
                print(orderInfo.prepayId)
                print(orderInfo.sign)
                print(orderInfo.package)
                print(orderInfo.nonceStr)
                print(orderInfo.timeStamp)



               WXApi.send(orderInfo)
                
        },
            failure: {error in
                self.showError(error.message)
                Timer.scheduledTimer(timeInterval: 2, target: self, selector: .dismissView, userInfo: nil, repeats: false)
        })
        NotificationCenter.addObserver(self,
                                       action: .returnFromWechat,
                                       name: NSNotification.Name.UIApplicationDidBecomeActive);
        
    }
    
    @objc func returnFromWechat(){
        print("Return from wechat")
        DispatchQueue.main.async {
            self.showLoading("")
        }
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: .dismissView, userInfo: nil, repeats: false)
    }
    
    @objc func dismissSelf(){
        self.dismissLoading()
        self.navigationController?.popToRootViewController(animated: true)
    }
}

fileprivate extension Selector{
    static let dismissView = #selector(KBPaymentWechatVC.dismissSelf)
    static let returnFromWechat = #selector(KBPaymentWechatVC.returnFromWechat)

    
}
