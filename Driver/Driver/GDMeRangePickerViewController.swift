//
//  GDMeRangePickerViewController.swift
//  Driver
//
//  Created by ZY on 2017-05-20.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import GoogleMaps

class KBOrderRangePickerViewController: KBViewController, UITableViewDelegate, UITableViewDataSource {
    let range_Values = [KBDriver.Range_5,
                        KBDriver.Range_8,
                        KBDriver.Range_12,
                        KBDriver.Range_NoLimit]
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapContainerView: UIView!
    let accountVM = KBAccountVM()
    let mapView:GDMapView = GDMapView.view()
    let longitudePerKM = 0.0137
    var orderRangeCricle:GMSCircle?
    class func controller() -> KBOrderRangePickerViewController{
        let controller = UIStoryboard(name: "OrderRangePicker", bundle: nil)
            .instantiateViewController(withIdentifier: "KBOrderRangePickerViewController") as! KBOrderRangePickerViewController
        return controller
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = UINavigationItem.LargeTitleDisplayMode.never
        }
        self.automaticallyAdjustsScrollViewInsets = false
        self.title = NSLocalizedString("Service Area", comment: "Area of where the riders can order this driver")
        self.mapView.delegate = self
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.mapContainerView.addSubview(self.mapView)
        if let superview = self.mapView.superview{
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1.0,
                                                       constant: 0))
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.trailing,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.trailing,
                                                       multiplier: 1.0,
                                                       constant: 0))
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1.0,
                                                       constant: 0))
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.bottom,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.bottom,
                                                       multiplier: 1.0,
                                                       constant: 0))
        }
        self.mapView.mapView.isMyLocationEnabled = true
        self.mapView.mapView.settings.rotateGestures = false
        self.mapView.mapView.settings.tiltGestures = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateMap()
        self.updateOrderRangeMask()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    func updateMap(){
        if let account = self.accountVM.account{
            var defaultRange = Double(account.range)
            if(defaultRange == 0){
                defaultRange = 7.0
            }
            if let userCurrentLocation = account.lastLocation(){
                let coordinate_TopLeft = CLLocationCoordinate2D(latitude: userCurrentLocation.coordinate.latitude,
                                                                longitude: userCurrentLocation.coordinate.longitude - defaultRange * self.longitudePerKM * 2)
                let coordinate_BotRight = CLLocationCoordinate2D(latitude: userCurrentLocation.coordinate.latitude,
                                                                 longitude: userCurrentLocation.coordinate.longitude + defaultRange * self.longitudePerKM * 2)
                let bounds = GMSCoordinateBounds(coordinate: coordinate_TopLeft,
                                                 coordinate: coordinate_BotRight)
                self.mapView.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 0.0))
            }
        }
    }
    func updateOrderRangeMask(){
        if let account = KBAccountManager.shared.getAccount(){
            if let location = account.lastLocation(){
                if(account.range > 0){
                    if(self.orderRangeCricle == nil){
                        self.orderRangeCricle = GMSCircle(position: location.coordinate, radius: (Double)(account.range * 1000))
                    }
                    self.orderRangeCricle?.radius = (Double)(account.range * 1000)
                    self.orderRangeCricle?.position = location.coordinate
                    self.orderRangeCricle?.fillColor = UIColor(red: 58.0/255.0, green: 167.0/255.0, blue: 87.0/255.0, alpha: 0.2)
                    self.orderRangeCricle?.strokeWidth = 0
                    self.orderRangeCricle?.map = self.mapView.mapView
                    return;
                }
            }
        }
        if(self.orderRangeCricle != nil){
            self.orderRangeCricle?.map = nil
        }
    }
    // MARK: - UITableViewDelegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.range_Values.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentRangeValue = self.range_Values[indexPath.row]
        let currentRangeDisplayText = KBDriver.rangeDisplayText(currentRangeValue)
        var isSelected = false
        if let account = KBAccountManager.shared.getAccount(){
            isSelected = (currentRangeValue == account.range)
        }
        let cellIdentifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellIdentifier)
        }
        cell?.textLabel?.text = currentRangeDisplayText
        cell?.accessoryType = (isSelected == true) ? UITableViewCellAccessoryType.checkmark : UITableViewCellAccessoryType.none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let currentRangeValue = self.range_Values[indexPath.row]
        let driverData:[String:String] = [KBDriver.Key_Range : String(currentRangeValue)]
        self.accountVM.updateAccount(data: driverData,
                                     start: {
                                        self.showLoading("")
        }, success: { (driver:KBDriver) in
            self.updateMap()
            self.updateOrderRangeMask()
            self.tableView.reloadData()
            self.dismissLoading()
        }) { (error:NSError) in
            self.showError(error.message)
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("Please choose the range for the Service Area", comment: "")
    }
}
