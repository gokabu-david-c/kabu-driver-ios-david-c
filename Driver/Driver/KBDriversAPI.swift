//
//  KBDriversAPIHelper.swift
//  Driver
//
//  Created by Kabu on 2017-09-27.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class KBDriversAPI: KBAPI {
    static func downloadDrivers(start aStart:() -> (),
                                success aSuccess:@escaping(_ drivers:[KBDriver], _ range:Double, _ location:CLLocationCoordinate2D?) -> (),
                                failure aFailure:@escaping(_ error:NSError) -> ()){
        aStart()
        let url = URL_Drivers()
        let manager = AFHTTPSessionManager.newInstance()
        manager.get(url,
                    parameters: nil,
                    progress: nil,
                    success: { (task, responseObject) in
                        let r = GDResponse.init(json: responseObject as! [String : Any])
                        if(r.isOK() == false){
                            let message = r.message ?? NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        var result:[KBDriver] = []
                        if let dataArray = r.data as? [Any]{
                            for currentObject in dataArray{
                                if let currentObject = currentObject as? Dictionary<String, Any>{
                                    let currentDriver = KBDriver.init(json: currentObject)
                                    result.append(currentDriver)
                                    
                                }
                            }
                        }
                        var range = 0.0;
                        var location:CLLocationCoordinate2D?
                        if(r.meta != nil){
                            if let meta = r.meta as? Dictionary<String, AnyObject?>{
                                if let rangeInR = meta["range"] as? Double{
                                    range = rangeInR
                                }
                                if let locationInR = meta["location"] as? String{
                                    location = locationInR.toCLLocation()?.coordinate
                                }
                            }
                        }
                        aSuccess(result, range, location)
        }) { (task, error) in
            aFailure(NSError.Error_UnknownError())
        }
    }
    static func downloadDriver(driverID aDriverID:Int,
                               start aStart:() -> (),
                               success aSuccess:@escaping(_ driver:KBDriver) -> (),
                               failure aFailure:@escaping(_ error:NSError) -> ()){
        
        aStart()
        let url = URL_DownloadDriver(driverID: aDriverID)
        let manager = AFHTTPSessionManager.newInstance()
        manager.get(url,
                    parameters: nil,
                    progress: nil,
                    success: { (task:URLSessionDataTask, responseObject:Any?) in
                        let r = GDResponse.init(json: responseObject as! [String : Any])
                        if(r.isOK() == false){
                            if(r.isInvalidLogin()){
                                let message = r.message ?? NSLocalizedString("Please Login again", comment: "")
                                aFailure(NSError.Error_InvalidLogin(message: message))
                            }
                            else{
                                let message = r.message ?? NSLocalizedString("An Unknown Error has occurred", comment: "")
                                aFailure(NSError.Error(message: message))
                            }
                            return
                        }
                        let driver = KBDriver.init(json: r.data as! [String : Any])
                        aSuccess(driver)
        }) { (task, error) in
            aFailure(NSError.Error_UnknownError())
        }
    }
    static func updateDriver(driverID aDriverID:Int,
                      driverData aDriverData:[String:String],
                      start aStart:() -> (),
                      success aSuccess:@escaping (_ driver:KBDriver)->(),
                      failure aFailure:@escaping (_ error:NSError)->()) {
        if(aDriverID == 0){
            return
        }
        if(aDriverData.count == 0){
            return
        }
        aStart()

        let url = KBDriversAPI.URL_UpdateDriver(driverID: aDriverID)
        let manager = AFHTTPSessionManager.newInstance()
        manager.post(url,
                     parameters: aDriverData,
                     progress: nil,
                     success: { (task:URLSessionDataTask, responseObject:Any?) in
                        
                        let r = GDResponse.init(json: responseObject as! [String : Any])
                        if(r.isOK() == false){
                            let message = r.message ?? NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        if(r.data != nil){
                            if(r.data is [String:Any]){
                                let driver = KBDriver.init(json: r.data as! [String : Any])
                                aSuccess(driver)
                                return
                            }
                        }
                        aFailure(NSError.Error_UnknownError())
        }) { (task:URLSessionDataTask?, error:Error) in
            aFailure(NSError.Error_UnknownError())
        }
    }
    

}
