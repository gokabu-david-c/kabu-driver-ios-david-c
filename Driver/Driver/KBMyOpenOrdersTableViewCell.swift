//
//  KBMyOpenOrdersTableViewCell.swift
//  Driver
//
//  Created by Kabu on 2017-11-20.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBMyOpenOrdersTableViewCell: UITableViewCell {
    weak var delegate:AnyObject?
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var headerView: UIView!
    //@IBOutlet weak var orderTypeImageView: UIImageView!
    @IBOutlet weak var orderTypeLabel: UILabel!
    @IBOutlet weak var reservationPickupTimeLabel: UILabel!
    @IBOutlet weak var fromAddressLabel: UILabel!
    @IBOutlet weak var toAddressLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var ridersCountLabel: UILabel!
    @IBOutlet weak var riderContactPhoneLabel: UILabel!
    @IBOutlet weak var riderNoteLabel: UILabel!
    var orderNumber:String?
    private var countdownTimer:Timer?
    let openOrderVM = KBOpenOrdersVM()
    class func cell() -> KBMyOpenOrdersTableViewCell{
        let cell = Bundle.main.loadNibNamed("KBMyOpenOrdersTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! KBMyOpenOrdersTableViewCell
        return cell
    }
    class var reuseIdentifier:String{
        return "KBMyOpenOrdersTableViewCell"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.shadowView.clipsToBounds = true
        self.shadowView.layer.cornerRadius = 8
        self.containerView.clipsToBounds = true
        self.containerView.layer.cornerRadius = 8
        self.headerView.clipsToBounds = true
        self.containerView.addObserver(self,
                                       forKeyPath: "bounds",
                                       options: .old,
                                       context: nil)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        if(highlighted == true){
            UIView.animate(withDuration: 0.2, animations: {
                self.shadowView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.shadowView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
    }
    override func prepareForReuse() {
        self.orderNumber = nil
        self.resetCell()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.updateShadow()
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(object as? UIView === self.containerView && keyPath == "bounds"){
            self.updateShadow()
        }
    }
    func updateShadow(){
        let shadowPath = UIBezierPath(roundedRect: CGRect.init(x: 0, y: 0, width: self.shadowView.bounds.size.width, height: self.shadowView.bounds.size.height), cornerRadius: 8.0)
        self.shadowView.dropShadow(color: .black, opacity: 0.3, offSet: CGSize.zero, radius: 6, scale: true, path:shadowPath)
    }
    func setOrder(orderNumber aOrderNumber:String){
        self.orderNumber = aOrderNumber
        self.updateCell()
        self.startTimer()
    }
    func updateCell(){
        if(self.orderNumber == nil || self.orderNumber?.count == 0){
            self.resetCell()
            return
        }
        let order = self.openOrderVM.getOrder(orderNumber: self.orderNumber!)
        if(order == nil){
            self.resetCell()
            return
        }
        if(order!.isReservation == true){
            self.headerView.backgroundColor = UIColor.ReservationOrder
            //self.orderTypeImageView.image = UIImage.image(UIColor.ReservationOrder, size: CGSize(width: 1, height: 1))
            self.orderTypeLabel.text = NSLocalizedString("Go Now (Reserved)", comment: "")
            if(self.shouldShowCountdown() == true){
                let countdownSeconds = order!.reservationPickupTime - Int64(Date().timeIntervalSince1970)
                if(countdownSeconds > 0){
                    let hours = countdownSeconds / 3600
                    let minutes = countdownSeconds % 3600 / 60
                    let seconds = countdownSeconds % 3600 % 60
                    let timeAttributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey.font  : UIFont.systemFont(ofSize: 19.0, weight: .thin),
                                                           NSAttributedStringKey.foregroundColor : UIColor.white]
                    let hoursString = (hours < 10) ? NSAttributedString(string: "0" + String(hours), attributes: timeAttributes) : NSAttributedString(string: String(hours), attributes: timeAttributes)
                    let minuteString = (minutes < 10) ? NSAttributedString(string: ":0" + String(minutes), attributes: timeAttributes) : NSAttributedString(string: ":" + String(minutes), attributes: timeAttributes)
                    let secondsString = (seconds < 10) ? NSAttributedString(string: ":0" + String(seconds), attributes: timeAttributes) : NSAttributedString(string: ":" + String(seconds), attributes: timeAttributes)
                    let title = NSAttributedString(string:NSLocalizedString("Countdown", comment: "") + " ", attributes: timeAttributes)
                    let countdownString = NSMutableAttributedString()
                    countdownString.append(title)
                    countdownString.append(hoursString)
                    countdownString.append(minuteString)
                    countdownString.append(secondsString)
                    self.reservationPickupTimeLabel.attributedText = countdownString
                }else{
                    let countdownAttributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 17.0, weight: .thin)]
                    let countdownString = NSAttributedString(string: NSLocalizedString("In Progress", comment: ""), attributes: countdownAttributes)
                    self.reservationPickupTimeLabel.attributedText = countdownString
                }
            }else{
                let reservationDate = Date.ToDate(order!.reservationPickupTime)
                if(reservationDate.isToday){
                    self.reservationPickupTimeLabel.text = NSLocalizedString("Today", comment: "") + " " + reservationDate.toString("HH:mm")
                    self.reservationPickupTimeLabel.font = UIFont.systemFont(ofSize: 17)
                }else if(reservationDate.isTomorrow){
                    self.reservationPickupTimeLabel.text = NSLocalizedString("Tomorrow", comment: "") + " " + reservationDate.toString("HH:mm")
                    self.reservationPickupTimeLabel.font = UIFont.systemFont(ofSize: 17)
                }else{
                    let attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15.0, weight: .thin),
                                                       NSAttributedStringKey.foregroundColor : UIColor.white]
                    //let weekdayName = NSAttributedString(string:Date.ToDate(aOrder.reservationPickupTime).toString(" | EEEE | "))
                    let timeString = NSAttributedString(string:Date.ToDate(order!.reservationPickupTime).toString("  HH:mm"), attributes: attributes)
                    let dateString = NSAttributedString(string:Date.ToDate(order!.reservationPickupTime).toString("yyyy-MM-dd"), attributes: attributes)
                    let pickupTimeString = NSMutableAttributedString()
                    pickupTimeString.append(dateString)
                    pickupTimeString.append(timeString)
                    //pickupTimeString.append(weekdayName)
                    self.reservationPickupTimeLabel.attributedText = pickupTimeString
                }
            }
        }else{
            self.headerView.backgroundColor = UIColor.InstantOrder
            //self.orderTypeImageView.image = UIImage.image(UIColor.InstantOrder, size: CGSize(width: 1, height: 1))
            self.orderTypeLabel.text = NSLocalizedString("Go Now", comment: "")
            let countdownAttributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15.0, weight: .thin),
                                                        NSAttributedStringKey.foregroundColor : UIColor.white]
            let countdownString = NSAttributedString(string: "", attributes: countdownAttributes)
            self.reservationPickupTimeLabel.attributedText = countdownString
        }
        self.fromAddressLabel.text = order?.startAddress ?? ""
        self.toAddressLabel.text = order?.destinationAddress ?? ""
        let dollarSignAttributes = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15.0, weight: .bold),
                                    NSAttributedStringKey.foregroundColor : UIColor.black]
        let dollarSignString = NSAttributedString(string: "$", attributes:  dollarSignAttributes)
        let totalValueString = NSAttributedString(string:" " + String(Int(order!.total)))
        let totalString = NSMutableAttributedString()
        totalString.append(dollarSignString)
        totalString.append(totalValueString)
        self.priceLabel.attributedText = totalString
        //self.priceLabel.text = "$" + String(Int(order!.total))
        let riderCountIconAttachment:NSTextAttachment = NSTextAttachment()
        riderCountIconAttachment.image = UIImage(named: "ic-people-4.png")
        let riderCountIconString:NSAttributedString = NSAttributedString(attachment: riderCountIconAttachment)
        let riderCountTextString = NSAttributedString(string:" " + String(order!.numberOfPassengers))
        let ridersCountString = NSMutableAttributedString()
        ridersCountString.append(riderCountIconString)
        ridersCountString.append(riderCountTextString)
        self.ridersCountLabel.attributedText = ridersCountString
        let phoneIconAttachment:NSTextAttachment = NSTextAttachment()
        phoneIconAttachment.image = UIImage(named: "ic-phone-3.png")
        let phoneIconString:NSAttributedString = NSAttributedString(attachment: phoneIconAttachment)
        let phoneTextString = NSAttributedString(string:" " + order!.contactPhone!.toPhoneFormat())
        let phoneString = NSMutableAttributedString()
        phoneString.append(phoneIconString)
        phoneString.append(phoneTextString)
        self.riderContactPhoneLabel.attributedText = phoneString
        self.riderNoteLabel.text = ((order!.customerNote.count == 0) ? "无" : order!.customerNote)
    }
    func resetCell(){
        self.delegate = nil
        //self.orderTypeImageView.image = nil
        self.headerView.backgroundColor = UIColor.InstantOrder
        self.orderTypeLabel.text = ""
        self.reservationPickupTimeLabel.text = ""
        self.fromAddressLabel.text = ""
        self.toAddressLabel.text = ""
        self.priceLabel.text = ""
        self.ridersCountLabel.text = ""
        self.riderContactPhoneLabel.text = ""
        self.riderNoteLabel.text = ""
    }
    func startTimer(){
        if(self.countdownTimer != nil){
            return
        }
        self.countdownTimer = Timer.scheduledTimer(withTimeInterval: 0.1,
                                                   repeats: true,
                                                   block: { (timer:Timer) in
                                                    self.updateCell()
        })
    }
    func shouldShowCountdown() -> Bool{
        let order = self.openOrderVM.getOrder(orderNumber: self.orderNumber!)
        if(order?.isReservation == false){
            return false
        }
        let countdownSeconds = order!.reservationPickupTime - Int64(Date().timeIntervalSince1970)
        if(countdownSeconds >= 3600 * 3){
            return false
        }
        return true
    }
    @IBAction func phoneButtonTapped(_ sender: Any) {
        self.delegate?.KBMyOpenOrdersTableViewCellDidTapPhone(self)
    }
}
@objc protocol KBMyOpenOrdersTableViewCellDelegate : class {
    @objc func KBMyOpenOrdersTableViewCellDidTapPhone(_ sender:KBMyOpenOrdersTableViewCell)
}
