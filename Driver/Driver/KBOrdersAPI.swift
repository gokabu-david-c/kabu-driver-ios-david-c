//
//  KBOrdersAPI.swift
//  Driver
//
//  Created by Kabu on 2017-11-17.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class KBOrdersAPI: KBAPI {
    static func downloadOpenOrders(driverID aDriverID:Int,
                                   start aStart:() -> (),
                                   success aSuccess:@escaping(_ orders:[KBOrder]) -> (),
                                   failure aFailure:@escaping(_ error:NSError) -> ()){
        aStart()
        let url = URL_GetOpenOrdersForDriver(driverID: aDriverID)
        let manager = AFHTTPSessionManager.newInstance()
        manager.get(url,
                    parameters: nil,
                    progress: nil,
                    success: { (task, responseObject) in
                        let r = GDResponse.init(json: responseObject as! [String : Any])
                        if(r.isOK() == false){
                            let message = r.message ??  NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        var result:[KBOrder] = []
                        if let items = r.data as? [Dictionary<String, Any>]{
                            for currentItem in items{
                                let order = KBOrder(currentItem)
                                result.append(order)
                            }
                        }
                        aSuccess(result)
        }) { (task, error) in
            aFailure(NSError.Error_UnknownError())
        }
    }
    static func downloadClosedOrders(startDate aStartDate:String,
                                     endDate aEndDate:String,
                                     pageIndex aPageIndex:Int,
                                     itemsPerPage aItemsPerPage:Int,
                                     start aStart:() -> (),
                                     success aSuccess:@escaping(_ orders:[KBOrder], _ count:Int, _ total:Float, _ commission:Float) -> (),
                                     failure aFailure:@escaping(_ error:NSError) -> ()){
        aStart()
        let url = URL_GetClosedOrdersForDriver(startDate: aStartDate,
                                               endDate: aEndDate,
                                               pageIndex: aPageIndex,
                                               itemsPerPage: aItemsPerPage)
        let manager = AFHTTPSessionManager.newInstance()
        manager.get(url,
                    parameters: nil,
                    progress: nil,
                    success: { (task, responseObject) in
                        let r = GDResponse.init(json: responseObject as! [String : Any])
                        if(r.isOK() == false){
                            let message = r.message ??  NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        var result:[KBOrder] = []
                        if let items = r.data as? [Dictionary<String, Any>]{
                            for currentItem in items{
                                let order = KBOrder(currentItem)
                                result.append(order)
                            }
                        }
                        var count = 0
                        var total:Float = 0
                        var commission:Float = 0
                        if(r.meta != nil){
                            if let meta = r.meta as? Dictionary<String, AnyObject?>{
                                if let countInR = meta["count"] as? Int{
                                    count = countInR
                                }
                                if let totalInR = meta["total"] as? Double{
                                    total = Float(totalInR)
                                }
                                if let commissionInR = meta["commission"] as? Double{
                                    commission = Float(commissionInR)
                                }
                            }
                        }
                        aSuccess(result, count, total, commission)
        }) { (task, error) in
            aFailure(NSError.Error_UnknownError())
        }
    }
    static func downloadReservationPool(start aStart:() -> (),
                                        success aSuccess:@escaping(_ orders:[KBOrder]) -> (),
                                        failure aFailure:@escaping(_ error:NSError) -> ()){
        aStart()
        let url = URL_GetReservationPool
        let manager = AFHTTPSessionManager.newInstance()
        manager.get(url,
                    parameters: nil,
                    progress: nil,
                    success: { (task, responseObject) in
                        let r = GDResponse.init(json: responseObject as! [String : Any])
                        if(r.isOK() == false){
                            let message = r.message ??  NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        var result:[KBOrder] = []
                        if let items = r.data as? [Dictionary<String, Any>]{
                            for currentItem in items{
                                let order = KBOrder(currentItem)
                                result.append(order)
                            }
                        }
                        aSuccess(result)
        }) { (task, error) in
            aFailure(NSError.Error_UnknownError())
        }
    }
    static func updateOrder(orderNumber aOrderNumber:String,
                            orderData aOrderData:[String:String],
                            start aStart:(() -> ())?,
                            success aSuccess:@escaping(_ order:KBOrder) -> (),
                            failure aFailure:@escaping(_ error:NSError) -> ()) {
        if(aOrderData.count == 0){
            aFailure(NSError.Error(message: NSLocalizedString("Please enter data", comment: "")))
            return
        }
        aStart?()
        let url = URL_UpdateOrder(orderNumber: aOrderNumber)
        let manager = AFHTTPSessionManager.newInstance()
        manager.post(url,
                     parameters: aOrderData,
                     progress: nil,
                     success: { (task:URLSessionDataTask, responseObject:Any?) in
                        var response : [String:Any]?
                        if let responseDict = responseObject as? [String:Any]{
                            response = responseDict
                        }
                        if(response == nil){
                            let message = NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        let r = GDResponse.init(json: response!)
                        if(r.isOK() == false){
                            let message = r.message ??  NSLocalizedString("An Unknown Error has occurred", comment: "")
                            print(message)
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        var data : [String:Any]?
                        if let dataDict = r.data as? [String:Any]{
                            data = dataDict
                        }
                        if(data == nil){
                            let message = r.message ??  NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        let order = KBOrder.init(data!)
                        aSuccess(order)
        }) { (task:URLSessionDataTask?, error:Error) in
            print("Order failed " + error.localizedDescription)
            aFailure(NSError.Error_UnknownError())
        }
    }
}
