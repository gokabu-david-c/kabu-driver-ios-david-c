//
//  GDLocationManager.swift
//  Driver
//
//  Created by ZY on 2017-04-18.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDLocationManager: NSObject, CLLocationManagerDelegate {
    static let shared = GDLocationManager()
    var currentLocation:CLLocation?
    var prevLocation:CLLocation?
    var currentHeading:CLHeading?
    var isUpdatingLocation = false
    var isUpdatingHeading = false
    private let locationManager = CLLocationManager()
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = 10
    }
    func start() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestAlwaysAuthorization()
            if(self.locationManager.delegate !== self){
                self.locationManager.delegate = self
            }
            if(self.isUpdatingLocation == false){
                self.locationManager.distanceFilter = 10
                self.locationManager.startUpdatingLocation()
                self.isUpdatingLocation = true
            }
            if(self.isUpdatingHeading == false){
                self.isUpdatingHeading = true
                self.locationManager.headingFilter = 5
                self.locationManager.startUpdatingHeading()
            }
        }
    }
    func stop() {
        if(self.isUpdatingLocation){
            self.locationManager.stopUpdatingLocation()
            self.isUpdatingLocation = false
        }
        if(self.isUpdatingHeading){
            self.locationManager.stopUpdatingHeading()
            self.isUpdatingHeading = false
        }
        self.locationManager.delegate = nil
    }
    //MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(locations.count > 0){
            let newLocation = locations[0]
            self.prevLocation = self.currentLocation
            self.currentLocation = newLocation
            print(String(format:"Location updated:%f,%f", (currentLocation?.coordinate.latitude)!, (currentLocation?.coordinate.longitude)!))
            NotificationCenter.default.post(name: NSNotification.Name.GD_Location_LocationUpdated,
                                            object: nil,
                                            userInfo: [NSNotification.Key_UserInfo_Message:locations])
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        self.currentHeading = newHeading
        NotificationCenter.default.post(name: NSNotification.Name.GD_Location_HeadingUpdated,
                                        object: nil,
                                        userInfo: [NSNotification.Key_UserInfo_Message:newHeading])
    }
    /*func movedDistanceSinceLastLocationInKM() -> Float {
        if(self.currentLocation == nil
        || self.prevLocation == nil){
            return 0
        }
        let movedDistance = Float((self.currentLocation?.distance(from: self.prevLocation!))!)
        let movedDistanceInKM = movedDistance / 1000.0
        return movedDistanceInKM
    }*/
}
