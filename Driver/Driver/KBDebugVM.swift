//
//  KBDebugVM.swift
//  Driver
//
//  Created by George on 2018-01-17.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class KBDebugVM: NSObject {
    override init() {
        super.init()
        NotificationCenter.addObserver(self,
                                       action: #selector(performAppModeChanged),
                                       name: NSNotification.Name.KB_Debug_AppModeChanged)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    var appModeChanged:(() -> ())?
    var appMode:String{
        set{
            GDAppManager.appMode = newValue
        }
        get{
            return GDAppManager.appMode
        }
    }
    var isDevMode:Bool{
        return GDAppManager.isDevMode
    }
    var isDisplayingTrafficOnMap:Bool{
        set{
            GDAppManager.isDisplayingTrafficOnMap = newValue
        }get{
            return GDAppManager.isDisplayingTrafficOnMap
        }
    }
    @objc func performAppModeChanged(){
        self.appModeChanged?()
        if let account = KBAccountManager.shared.account{
            KBMeAPI.logout(uid: account.id,
                           userAccessToken: account.userAccessToken,
                           message: "",
                           startCallback: {
                            
            }, successCallback: {
                
            }, failureCallback: { (error:NSError) in
                
            })
        }
    }
}
