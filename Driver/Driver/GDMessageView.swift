//
//  GDMessageView.swift
//  Driver
//
//  Created by ZY on 2017-05-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDMessageView: UIView {

    
    @IBOutlet weak var messageLabel: UILabel!
    class func view(_ message:String) -> GDMessageView{
        
        let view = Bundle.main.loadNibNamed("GDMessageView",
                                            owner: self,
                                            options: nil)?.first as! GDMessageView
        
        return view
        
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func showMessage(_ message:String) {
        
        self.messageLabel.text = message
        
    }

}
