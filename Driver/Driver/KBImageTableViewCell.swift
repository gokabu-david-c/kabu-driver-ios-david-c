//
//  KBImageTableViewCell.swift
//  Driver
//
//  Created by Kabu on 2017-11-24.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBImageTableViewCell: UITableViewCell {
    @IBOutlet var iconImageView: GDImageView!
    @IBOutlet var titleLabel: UILabel!
    class func cell() -> KBImageTableViewCell{
        let cell = Bundle.main.loadNibNamed("KBImageTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! KBImageTableViewCell
        return cell
    }
    class var reuseIdentifier:String{
        return "KBImageTableViewCell"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.iconImageView.cornerRadius = 20
        self.iconImageView.clipsToBounds = true
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.iconImageView.image = nil
        self.titleLabel.text = ""
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.iconImageView.cornerRadius = self.iconImageView.frame.size.width * 0.5
    }
}
