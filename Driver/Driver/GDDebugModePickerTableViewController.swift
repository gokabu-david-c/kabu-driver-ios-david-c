//
//  GDDebugModePickerTableViewController.swift
//  Driver
//
//  Created by ZY on 2017-06-16.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDDebugModePickerTableViewController: UITableViewController {
    var debugVM = KBDebugVM()
    class func controller() -> GDDebugModePickerTableViewController{
        let controller = GDDebugModePickerTableViewController.init(style: UITableViewStyle.grouped)
        return controller;
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "模式"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadVM(){
        self.debugVM.appModeChanged = {
            
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if(cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.value1,
                                   reuseIdentifier: "cell")
        }
        
        cell?.accessoryType = UITableViewCellAccessoryType.none
        
        if(indexPath.row == 0){
            cell?.textLabel?.text = "生产模式"
            if(GDAppManager.isDevMode == false){
                cell?.accessoryType = UITableViewCellAccessoryType.checkmark
            }
        }else if(indexPath.row == 1){
            cell?.textLabel?.text = "开发模式"
            if(GDAppManager.isDevMode == true){
                cell?.accessoryType = UITableViewCellAccessoryType.checkmark
            }
        }
        return cell!
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        /*
        if(indexPath.row == 0)
        {
            GDAppManager.setAppMode(GDAppManager.Mode_App_Production)
        }
        else if(indexPath.row == 1)
        {
            GDAppManager.setAppMode(GDAppManager.Mode_App_Development)
        }
        
        self.tableView.reloadData()
        
        self.navigationController?.popViewController(animated: true)*/
        let alert = UIAlertController(title: "切换模式将退出登录 请确认",
                                      message: nil,
                                      preferredStyle: UIAlertControllerStyle.actionSheet);
        let action_Logout = UIAlertAction(title: "退出登录", style: UIAlertActionStyle.destructive, handler: { action in
            if(indexPath.row == 0){
                self.debugVM.appMode = GDAppManager.Mode_App_Production
            }else if(indexPath.row == 1){
                self.debugVM.appMode = GDAppManager.Mode_App_Development
            }
        })
        alert.addAction(action_Logout)
        let defaultAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert,
                     animated: true,
                     completion: nil)
        return
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
