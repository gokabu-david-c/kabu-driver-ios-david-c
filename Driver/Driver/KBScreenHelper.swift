//
//  KBScreenManager.swift
//  Driver
//
//  Created by ZY on 2017-07-14.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class KBScreenHelper{
    static let shared = KBScreenHelper()
    var shouldForceReload = false
    
    init(){
        NotificationCenter.addObserver(self,
                                       action:#selector(KBScreenHelper.applicationDidBecomeActive),
                                       name: NSNotification.Name.UIApplicationDidBecomeActive)
        
        
        NotificationCenter.addObserver(self,
                                       action: #selector(KBScreenHelper.showLoginViewController(_:)),
                                       name: NSNotification.Name.GD_Me_LoggedOut)
        
        
    }
    @objc func applicationDidBecomeActive(){
        self.updateScreen()
    }
    
    @objc func updateScreen(){
        if(KBAccountManager.shared.isLoggedIn() == false){
            KBScreenHelper.shared.showLoginViewController(nil)
            return
        }
        self.showMainController()
    }
    @objc func showLoginViewController(_ notification:NSNotification?) {
        DispatchQueue.main.async {
            if let window = UIApplication.shared.delegate?.window{
                if window?.rootViewController is GDLoginNavigationController{
                    return
                }
                var currentController:UIViewController?
                if let controller = window?.rootViewController{
                    currentController = controller
                }
                let loginController = GDLoginNavigationController.controller()
                window?.rootViewController = loginController
                UIView.transition(with: window!,
                                  duration: 0.2,
                                  options: UIViewAnimationOptions.transitionCrossDissolve,
                                  animations: nil) { (complete) in
                                    if(notification != nil){
                                        if let message = notification?.message(){
                                            if(message.count > 0){
                                                loginController.showError(message,
                                                                          UIViewControllerLoadingType.UIAlert)
                                            }
                                        }
                                    }
//                                    currentController?.dismiss(animated: false, completion: {
//                                        currentController?.removeFromParentViewController()
//                                        print(CFGetRetainCount(currentController))
//                                    })
                }
            }
        }
    }
    func showMainController() {
        DispatchQueue.main.async {
            /*if(self.loginController != nil){
                self.loginController!.removeFromParentViewController()
                self.loginController = nil
            }*/
            let window = UIApplication.shared.delegate?.window!!
            if(!self.shouldForceReload){
                if window?.rootViewController is KBMainTabBarController{
                    return
                }
            }
            let controller = KBMainTabBarController.controller() //GDMainContainerViewController()
            window?.rootViewController = controller
            UIView.transition(with: window!,
                              duration: 0.2,
                              options: UIViewAnimationOptions.transitionCrossDissolve,
                              animations: nil,
                              completion: nil)
            self.shouldForceReload = false
        }
    }
    func showClearController(){
        let window = UIApplication.shared.delegate?.window!!
        window?.rootViewController = nil
        let controller = UIViewController()
        window?.rootViewController = controller
        UIView.transition(with: window!,
                          duration: 0.2,
                          options: UIViewAnimationOptions.transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }
}
