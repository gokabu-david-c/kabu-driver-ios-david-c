//
//  KBMyCurrentOrderNavigationController.swift
//  Driver
//
//  Created by ZY on 2017-07-14.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBOpenOrderMapNavigationController: UINavigationController {
    var orderNumber:String = ""
    class func controller(orderNumber aOrderNumber:String) -> KBOpenOrderMapNavigationController {
        let controller = UIStoryboard(name: "OpenOrderMap", bundle: nil).instantiateViewController(withIdentifier: "KBOpenOrderMapNavigationController") as! KBOpenOrderMapNavigationController
        controller.orderNumber = aOrderNumber
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.childController()?.orderNumber = self.orderNumber
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func childController() -> KBOpenOrderMapViewController? {
        return self.childViewControllers.first as? KBOpenOrderMapViewController
    }
}
