//
//  GDTabBarController.swift
//  Driver
//
//  Created by ZY on 2017-05-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDTabBarController: UITabBarController {
    var shouldDoApplicationBackFromBackground = false

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(performApplicationDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(turnOnShouldDoApplicationBackFromBackground), name: .UIApplicationWillResignActive, object: nil)
    }


    
    override func viewDidDisappear(_ animated: Bool) {

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func turnOnShouldDoApplicationBackFromBackground(){
        shouldDoApplicationBackFromBackground = true
    }
    
    @objc func turnOffShouldDoApplicationBackFromBackground(){
        shouldDoApplicationBackFromBackground = false
    }
    
    @objc func performApplicationDidBecomeActive() {
        if(shouldDoApplicationBackFromBackground){
            turnOffShouldDoApplicationBackFromBackground()
            applicationDidBecomeActive()
        }

    }

    @objc func applicationDidBecomeActive(){
        
    }
    
    deinit {
                NotificationCenter.default.removeObserver(self)

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
