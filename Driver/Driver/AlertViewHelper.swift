//
//  AlertViewHelper.swift
//  Driver
//
//  Created by Jack Du on 2018-04-17.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

class AlertViewHelper: NSObject {

    
    
    func makeRaceOrderAlert(order:KBOrder, okAction:Selector) -> UIAlertController{
        let alert = UIAlertController(title: NSLocalizedString("You are about to Accept the following Reservation Order", comment: ""),
                                      message: "",
                                      preferredStyle: UIAlertControllerStyle.alert)
        let paragraphStyle = NSMutableParagraphStyle()//alert.value(forKey: "attributedMessage") as! NSMutableParagraphStyle
        paragraphStyle.alignment = NSTextAlignment.left
        paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        paragraphStyle.lineSpacing = 1.5
        let attributes = [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 14)]//NSParagraphStyleAttributeName:paragraphStyle
        let pickupTimeIconAttachment:NSTextAttachment = NSTextAttachment()
        pickupTimeIconAttachment.image = UIImage(named: "ic-time-1.png")
        let picupTimeIconString:NSAttributedString = NSAttributedString(attachment: pickupTimeIconAttachment)
        let newLine = NSAttributedString(string:"\n", attributes: attributes)
        let weekdayName = NSAttributedString(string:Date.ToDate(order.reservationPickupTime).toString(" | EEEE | "), attributes:attributes)
        let timeString = NSAttributedString(string:Date.ToDate(order.reservationPickupTime).toString(" HH:mm"), attributes: attributes)
        let dateString = NSAttributedString(string:Date.ToDate(order.reservationPickupTime).toString("yyyy-MM-dd"), attributes: attributes)
        let startAddressIconAttachment:NSTextAttachment = NSTextAttachment()
        startAddressIconAttachment.image = UIImage(named: "ic-location-10.png")
        let startAddressIconString:NSAttributedString = NSAttributedString(attachment: startAddressIconAttachment)
        let startAddressTextString = NSAttributedString(string:" " + order.startAddress!, attributes: attributes)
        let alertMesageString = NSMutableAttributedString()
        alertMesageString.append(newLine)
        alertMesageString.append(picupTimeIconString)
        alertMesageString.append(timeString)
        alertMesageString.append(weekdayName)
        alertMesageString.append(dateString)
        alertMesageString.append(newLine)
        alertMesageString.append(startAddressIconString)
        alertMesageString.append(startAddressTextString)
        let riderCountIconAttachment:NSTextAttachment = NSTextAttachment()
        riderCountIconAttachment.image = UIImage(named: "ic-people-4.png")
        let riderCountIconString:NSAttributedString = NSAttributedString(attachment: riderCountIconAttachment)
        let riderCountTextString = NSAttributedString(string:"  " + String(order.numberOfPassengers))
        alertMesageString.append(newLine)
        alertMesageString.append(riderCountIconString)
        alertMesageString.append(riderCountTextString)
        if(order.customerNote.replacingOccurrences(of: " ", with: "").count != 0){
            alertMesageString.append(newLine)
            let customerNoteString = NSAttributedString(string:"备注: " + order.customerNote, attributes: attributes)
            alertMesageString.append(customerNoteString)
        }
        alert.setValue(alertMesageString, forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "取消",
                                      style: UIAlertActionStyle.cancel,
                                      handler: { (action) in
                                        
        }))
        let OKAction = UIAlertAction(title: "确认",
                                     style: UIAlertActionStyle.default,
                                     handler: { (action) in
                                        
        })
        alert.addAction(OKAction)
        alert.preferredAction = OKAction
        return alert
    }
    
    func makeRaceOrderTooManyAlert(_ target: NSObject, okAction: Selector, cancelAction: Selector) -> UIAlertController{
        let alert = UIAlertController(title: "有超过五个定单",
                                      message: "关掉所有？",
                                      preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "取消",
                                      style: UIAlertActionStyle.cancel,
                                      handler: { (action) in
                                      target.perform(cancelAction)
        }))
        let OKAction = UIAlertAction(title: "确认",
                                     style: UIAlertActionStyle.default,
                                     handler: { (action) in
                                     target.perform(okAction)
        })
        alert.addAction(OKAction)
        alert.preferredAction = OKAction
        return alert
    }
    
    func dismissPopupTemporary(_ target: NSObject, okAction: Selector) -> UIAlertController{
        let alert = UIAlertController(title: "暂停视窗",
                                      message: "\(KBSystemSettingsVM().lengthOfNewReservationPopupMinutes) 分种",
                                      preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "取消",
                                      style: UIAlertActionStyle.cancel,
                                      handler: { (action) in
                                        
        }))
        let OKAction = UIAlertAction(title: "确认",
                                     style: UIAlertActionStyle.default,
                                     handler: { (action) in
                                        target.perform(okAction)
        })
        alert.addAction(OKAction)
        alert.preferredAction = OKAction
        return alert
    }
    
}
