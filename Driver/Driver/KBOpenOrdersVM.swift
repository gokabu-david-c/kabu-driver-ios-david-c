//
//  KBOpenOrdersVM.swift
//  Driver
//
//  Created by Kabu on 2017-11-20.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBOpenOrdersVM: NSObject {
    var isDownloadingOrder:Bool{
        didSet{
            if(isDownloadingOrder != oldValue){
                self.downloadingStatusChanged?()
            }
        }
    }
    var openOrderStatusMessage:String?{
        didSet{
            if(self.openOrderStatusMessage == ""){
                self.openOrderStatusMessage = nil
            }
        }
    }
    var lastUpdateTime:Date?{
        return KBOpenOrdersManager.shared.openOrdersLastUpdateTime
    }
    override init() {
        self.isDownloadingOrder = false
        super.init()
        NotificationCenter.addObserver(self,
                                       action: #selector(performOpenOrdersNewOrder),
                                       name: NSNotification.Name.KB_OpenOrders_NewOrder)
        NotificationCenter.addObserver(self,
                                       action: #selector(performOpenOrdersNewInstantOrder),
                                       name: NSNotification.Name.KB_OpenOrders_NewInstantOrder)
        NotificationCenter.addObserver(self,
                                       action: #selector(performOpenOrdersNewReservationOrder),
                                       name: NSNotification.Name.KB_OpenOrders_NewReservationOrder)
        NotificationCenter.addObserver(self,
                                       action: #selector(performOpenOrdersChanged),
                                       name: NSNotification.Name.KB_OpenOrders_Changed)
        NotificationCenter.addObserver(self,
                                       action: #selector(performOpenOrdersDataChanged),
                                       name: NSNotification.Name.KB_OpenOrders_Data_Changed)
        NotificationCenter.addObserver(self,
                                       action: #selector(performOpenOrdersLastUpdateTimeChanged),
                                       name: NSNotification.Name.KB_OpenOrders_Last_Update_Time_Changed)
        NotificationCenter.addObserver(self,
                                       action: #selector(performOpenOrdersDownloadingStatusChanged),
                                       name: NSNotification.Name.KB_OpenOrders_Downloading_Status_Changed)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    var lastUpdateTimeChanged:(() -> ())?
    var openOrdersChanged:(() -> ())?
    var newOrderAdded:(() -> ())?
    var newInstantOrderAdded:(() -> ())?
    var newReservationOrderAdded:(() -> ())?
    var openOrdersDataChanged:(() -> ())?
    var downloadingStatusChanged:(() -> ())?
    var openOrders:[KBOrder] {
        return KBOpenOrdersManager.shared.openOrders
    }
    var instantOrders:[KBOrder]{
        var result = [KBOrder]()
        for currentOrder in self.openOrders{
            if(currentOrder.isReservation == false){
                result.append(currentOrder)
            }
        }
        return result
    }
    var reservationOrders:[KBOrder]{
        var result = [KBOrder]()
        for currentOrder in self.openOrders {
            if(currentOrder.isReservation){
                result.append(currentOrder)
            }
        }
        return result
    }
    @objc private func performOpenOrdersChanged(){
        self.openOrdersChanged?()
    }
    @objc private func performOpenOrdersNewOrder(){
        self.newOrderAdded?()
    }
    @objc private func performOpenOrdersNewInstantOrder(){
        self.newInstantOrderAdded?()
    }
    @objc private func performOpenOrdersNewReservationOrder(){
        self.newReservationOrderAdded?()
    }
    @objc private func performOpenOrdersDataChanged(){
        self.openOrdersDataChanged?()
    }
    @objc private func performOpenOrdersLastUpdateTimeChanged(){
        self.lastUpdateTimeChanged?()
    }
    @objc private func performOpenOrdersDownloadingStatusChanged(){
        self.isDownloadingOrder = KBOpenOrdersManager.shared.isDownloadingOpenOrders
    }
    func hasReservation() -> Bool{
        for currentOrder in self.openOrders {
            if(currentOrder.isReservation){
                return true
            }
        }
        return false
    }
    func downloadOpenOrders(start aStart:(() -> ())?,
                            success aSuccess:@escaping(() -> ()),
                            failure aFailure:@escaping((_ aError:NSError) -> ())){
        KBOpenOrdersManager.shared.downloadOpenOrders(start: {
            self.openOrderStatusMessage = nil
            aStart?()
        }, success: {
            self.openOrderStatusMessage = nil
            aSuccess()
        }) { (error:NSError) in
            self.openOrderStatusMessage = error.message
            aFailure(error)
        }
    }
    func containsOrder(orderNumber aOrderNumber:String) -> Bool{
        if(self.openOrders.count == 0){
            return false
        }
        for currentOrder in self.openOrders {
            if(currentOrder.orderNumber == aOrderNumber){
                return true
            }
        }
        return false
    }
    func getOrder(orderNumber aOrderNumber:String) -> KBOrder?{
        for currentOrder in self.openOrders{
            if currentOrder.orderNumber == aOrderNumber{
                return currentOrder
            }
        }
        return nil
    }
    func updateOrder(orderNumber aOrderNumber:String,
                     orderData aOrderData:[String:String],
                     startCallback aStartCallback:(() -> ())?,
                     successCallback aSuccessCallback:@escaping((_ order:KBOrder) -> ()),
                     failureCallback aFailureCallback:@escaping((_ error:NSError) -> ())){
        KBOrdersAPI.updateOrder(orderNumber: aOrderNumber,
                                orderData: aOrderData,
                                start: {
                                    aStartCallback?()
        }, success: { (order:KBOrder) in
            aSuccessCallback(order)
        }) { (error:NSError) in
            aFailureCallback(error)
        }
    }
}
