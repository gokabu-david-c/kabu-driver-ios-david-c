//
//  AFHTTPSessionManagerExtension.swift
//  Driver
//
//  Created by ZY on 2017-07-10.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

extension AFHTTPSessionManager{
    static func newInstance() -> AFHTTPSessionManager {
        let manager = AFHTTPSessionManager()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.securityPolicy.validatesDomainName = false
        manager.securityPolicy.allowInvalidCertificates = true
        if let account = KBAccountManager.shared.getAccount(){
            let cookie = "uid=" + String(account.id) + ";useraccesstoken=" + account.userAccessToken + ";"
            manager.requestSerializer.setValue(cookie, forHTTPHeaderField: "Cookie")
            manager.requestSerializer.setValue(String(account.id),
                                               forHTTPHeaderField: "uid")
            manager.requestSerializer.setValue(account.userAccessToken,
                                               forHTTPHeaderField: "useraccesstoken")
        }
        manager.requestSerializer.setValue("iOS",
                                           forHTTPHeaderField: "App-Device")
        manager.requestSerializer.setValue(KBDeviceHelper.getSystemVersion(),
                                           forHTTPHeaderField: "OS-Version")
        manager.requestSerializer.setValue(GDAppManager.getVersion(),
                                           forHTTPHeaderField: "App-Version")
        manager.requestSerializer.setValue(GDAppManager.getBuild(),
                                           forHTTPHeaderField: "App-Build")
        manager.requestSerializer.setValue(KBConfig.appAccessToken,
                                           forHTTPHeaderField: "appaccesstoken")
        return manager
    }
}
