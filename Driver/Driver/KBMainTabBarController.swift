//
//  KBMainTabBarController.swift
//  Driver
//
//  Created by ZY on 2017-05-20.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import UserNotifications
import Whisper

class KBMainTabBarController: GDTabBarController {
    var mainTabbarVM = KBMainTabbarVM()
    var enablePushNotificationController:GDEnablePushNotificationViewController?
    var enableLocationServiceController:GDEnableLocationServiceViewController?
    var payBillController:GDPureWebViewNavigationController?
    let openOrdersVM = KBOpenOrdersVM()
    let accountVM = KBAccountVM()
    let reservationPoolVM = KBReservationPoolVM()
    let systemVM = KBSystemSettingsVM()
    let reachabilityVM = KBReachabilityVM()
    var noNetworkMessage:Murmur?
 
    //var updateOnlineStatusViewTimer:Timer?
    class func controller() -> KBMainTabBarController {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "KBMainTabBarController") as! KBMainTabBarController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self

        self.loadVM()
        self.openOrdersVM.openOrdersChanged = self.openOrderChanged
        self.openOrdersVM.newOrderAdded = self.newOrderAdded
        self.openOrdersVM.newInstantOrderAdded = self.newInstantOrderAdded
        self.openOrdersVM.newReservationOrderAdded = self.newReservationOrderAdded
        self.registerNotifications()
        self.loadReachability()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateOnlineStatusTabbarColor()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.checkUnpaidBills()

    }
    @objc override func applicationDidBecomeActive() {
        if(self.accountVM.account?.waiverAgreeTime == nil || self.accountVM.account?.waiverAgreeTime == ""){
            self.showWaiver()
        }else{
            self.checkUnpaidBills()
            self.checkLocationService()
            self.checkPushNotification()
            self.showCurrentOrder()
            print("did become active")
        }
    }
    func loadVM() {
        self.mainTabbarVM.accountOnlineStatusChanged = {
            self.updateOnlineStatusTabbarColor()
        }
    }
    func registerNotifications(){
        NotificationCenter.addObserver(self,
                                       action: #selector(checkNewReservationOrders),
                                       name: NSNotification.Name.KB_ReservationPool_OrderChanged);
    }
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        //NotificationCenter.default.removeObserver(self)
        print(CFGetRetainCount(self))
        super.dismiss(animated: flag, completion: completion)
    }
}
//reachability
extension KBMainTabBarController{
    func loadReachability(){
        self.reachabilityVM.connected = {
            Whisper.hide()
            self.noNetworkMessage = nil
        }
        self.reachabilityVM.disconnected = {
            if(self.noNetworkMessage == nil){
                self.noNetworkMessage = Murmur.init(title: NSLocalizedString("Not connected to the internet", comment: ""),
                                                    backgroundColor: .Danger,
                                                    titleColor: UIColor.white,
                                                    font: UIFont.systemFont(ofSize: 15),
                                                    action: nil)
                Whisper.show(whistle: self.noNetworkMessage!,
                             action: WhistleAction.present)
            }
        }
    }
}
//current order
extension KBMainTabBarController{
    func openOrderChanged(){
        self.updateOpenOrderBadge()
    }
    func newOrderAdded(){
        self.updateOpenOrderBadge()
        self.showCurrentOrder()
    }
    func newInstantOrderAdded(){
        self.updateOpenOrderBadge()
        self.showCurrentOrder()
        
    }
    func newReservationOrderAdded(){
        self.updateOpenOrderBadge()
        if(self.openOrdersVM.instantOrders.count > 0){
            return
        }
        self.showCurrentOrder()
    }
    @objc func checkNewReservationOrders(){
        let newReservedOrders = self.reservationPoolVM.getNewOrders()
        let openOrders = self.openOrdersVM.openOrders
        if let isOnline = accountVM.account?.is_online{
            if(newReservedOrders.count>0 && systemVM.isDisplayingNewReservationPopup && isOnline && openOrders.count<1){
                self.showNewOrderView(newOrders: newReservedOrders)
            }
        }
    }
    
    func showCurrentOrder(){

        if(openOrdersVM.openOrders.count == 0){
                return
            }
            let order = openOrdersVM.openOrders.first
            if order?.isReservation == false{
                let navController = KBOpenOrderMapNavigationController.controller(orderNumber: order?.orderNumber ?? "")
                self.present(navController, animated: true, completion: nil)
                return
            }
            if order?.isReservation == true
                && (order?.reservationPickupTime)! - Int64(Date().timeIntervalSince1970) <= 1800{
                let navController = KBOpenOrderMapNavigationController.controller(orderNumber: order?.orderNumber ?? "")
                self.present(navController, animated: true, completion: nil)
            }
        
    }
    func updateOpenOrderBadge(){
        if(openOrdersVM.openOrders.count == 0){
            self.viewControllers?.first?.tabBarItem.badgeValue = nil
            return
        }
        self.viewControllers?.first?.tabBarItem.badgeValue = String(openOrdersVM.openOrders.count)
    }
}
//bills
extension KBMainTabBarController{
    func checkUnpaidBills(){
        KBBillManager.shared.shouldShowPayBill(start: {
                self.payBillController?.showLoading("")
            }, success:{(shouldShowPayBill:Bool) in
                if(shouldShowPayBill){
                self.performSegue(withIdentifier: "PaymentSegue", sender: self)
                }
            }, failure:{(error:NSError) in
                self.showError(NSLocalizedString("An error had occurred, please try again", comment: ""))
            })
    }
    func showNewOrderView(newOrders:[KBOrder]){
        let controller = UIStoryboard(name: "OpenOrderNotification", bundle: nil).instantiateViewController(withIdentifier: "KBOpenOrderNotificationVC") as! KBOpenOrderNotifcationVC
        controller.modalPresentationStyle = .overCurrentContext
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalTransitionStyle = .coverVertical
        controller.currentOrders = newOrders
        self.present(controller, animated: true, completion: {})
    }
    
}

//notification & locatino settings
extension KBMainTabBarController{
    func checkPushNotification(){
        if(KBAppHelper.isPushNotificationEnabled() == false){
            self.showEnablePushNotificationViewController()
        }
    }
    func showEnablePushNotificationViewController(){
        if(self.enablePushNotificationController == nil){
            self.enablePushNotificationController = GDEnablePushNotificationViewController.controller()
            self.enablePushNotificationController?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        }
        if(self.presentedViewController !== self.enablePushNotificationController){
            self.present(self.enablePushNotificationController!,
                         animated: true) {
            }
        }
    }
    func checkLocationService(){
        if(KBAppHelper.isLocationServiceEnabled() == false){
            self.showEnabledLocationServiceViewController()
        }
    }
    func showEnabledLocationServiceViewController() {
        if(self.enableLocationServiceController == nil){
            self.enableLocationServiceController = GDEnableLocationServiceViewController.controller()
            self.enableLocationServiceController?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        }
        if(self.presentedViewController !== self.enableLocationServiceController){
            self.present(self.enableLocationServiceController!,
                         animated: true) {
            }
        }
    }
    
}
// MARK : UITabbarController Delegate
extension KBMainTabBarController:UITabBarControllerDelegate{
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if(viewController.restorationIdentifier == "KBOnlineStatusViewController"){
            if(self.mainTabbarVM.isAccountOnline == true){
                let alertController = UIAlertController(title: NSLocalizedString("Confirm Going Offline", comment: ""),
                                                        message: nil,
                                                        preferredStyle: .actionSheet)
                let defaultAction =  UIAlertAction.init(title: NSLocalizedString("Offline", comment: ""),
                                                        style: .default,
                                                        handler: { (action:UIAlertAction) in
                                                            self.takeAccountOffline()
                })
                alertController.addAction(defaultAction)
                let cancelAction =  UIAlertAction.init(title: NSLocalizedString("Cancel", comment: ""),
                                                       style: .cancel,
                                                       handler:nil)
                alertController.addAction(cancelAction)
                self.present(alertController,
                             animated: true,
                             completion: nil)
            }else{
                let alertController = UIAlertController(title: NSLocalizedString("Confirm Going Online", comment: ""),
                                                        message: nil,
                                                        preferredStyle: .actionSheet)
                let defaultAction =  UIAlertAction.init(title: NSLocalizedString("Online", comment: ""),
                                                        style: .default,
                                                        handler: { (action:UIAlertAction) in
                                                            self.takeAccountOnline()
                })
                alertController.addAction(defaultAction)
                let cancelAction =  UIAlertAction.init(title: NSLocalizedString("Cancel", comment: ""),
                                                       style: .cancel,
                                                       handler:nil)
                alertController.addAction(cancelAction)
                self.present(alertController,
                             animated: true,
                             completion: nil)
            }
            return false
        }
        return true
    }
}
// MARK : account
extension KBMainTabBarController{
    /*func startAccountOnlineStatusTimer() {
        if(self.updateOnlineStatusViewTimer != nil){
            return
        }
        self.updateOnlineStatusViewTimer = Timer.scheduledTimer(withTimeInterval: 1.0,
                                                                repeats: true,
                                                                block: { (timer:Timer) in
                                                                self.updateOnlineStatusTabbarColor()
        })
        self.updateOnlineStatusViewTimer?.fire()
    }*/
    func updateOnlineStatusTabbarColor(){
        DispatchQueue.main.async {
            if let onlineTabbar = self.tabBar.items?[2]{
                onlineTabbar.imageInsets = UIEdgeInsetsMake(-2, 0, 2, 0);
                if(self.mainTabbarVM.isAccountOnline == true){
                    onlineTabbar.image = UIImage(named: "ic-online-1.png")!.withRenderingMode(.alwaysOriginal)
                }else{
                    onlineTabbar.image = UIImage(named: "ic-online-2.png")!.withRenderingMode(.alwaysOriginal)
                }
                onlineTabbar.title = ""//"离线"
            }
        }
    }
    func takeAccountOnline(){
        if let account = self.accountVM.account{
            if(account.is_online == true){
                return
            }
            self.accountVM.takeAccountOnline(start: {
                self.showLoading("")
            }, success: {
                self.dismissLoading()
                self.updateOnlineStatusTabbarColor()
            }, failure: { (error:NSError) in
                self.showError(error.message)
            })
        }
    }
    func takeAccountOffline(){
        if let account = self.accountVM.account{
            if(account.is_online == false){
                return
            }
        }
        self.accountVM.takeAccountOffline(start: {
            self.showLoading("")
        }, success: {
            self.dismissLoading()
            self.updateOnlineStatusTabbarColor()
        }, failure: { (error:NSError) in
            self.showError(error.message)
        })
    }
}
// MARK: waiver
extension KBMainTabBarController{
    func showWaiver(){
        if(self.accountVM.account?.isAccountAgreedWaiver == true){
            return
        }
        let controller = KBWaiverViewController.controller
            //let navController = UINavigationController(rootViewController: controller)
        self.present(controller,
                     animated: true,
                     completion: nil)
    }
}
