//
//  GDAPS.swift
//  Driver
//
//  Created by ZY on 2017-04-25.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class GDAPS {
    
    var title = ""
    var message = "";
    var action = ""
    
    init(_ aps:Dictionary<String, AnyObject?>) {
        
        // perform some initialization here
        
        if let alert = aps["alert"] as? [String:AnyObject?]
        {
            if let title = alert["title"] as? String
            {
                self.title = title
            }
            
            if let message = alert["body"] as? String
            {
                self.message = message
            }
            
        }
        
        
        
        if let action = aps["action"] as? String
        {
            self.action = action
        }
        
        
    }
    
}
