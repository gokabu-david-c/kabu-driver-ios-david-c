//
//  GDResetPasswordNavigationController.swift
//  Driver
//
//  Created by ZY on 2017-04-09.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDResetPasswordNavigationController: UINavigationController {

    
    class func controller() -> GDResetPasswordNavigationController
    {
        let controller = UIStoryboard(name: "ResetPassword", bundle: nil)
            .instantiateViewController(withIdentifier: "GDResetPasswordNavigationController") as! GDResetPasswordNavigationController
        
        return controller
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
