//
//  GDViewController.swift
//  Driver
//
//  Created by ZY on 2017-04-06.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

enum UIViewControllerLoadingType:Int {
    
    case KVNProgress = 0,
    UIAlert = 1
    
}


// Put this piece of code anywhere you like
extension UIViewController {
    
    /// Dismiss the keyboard
    func registerDismissKeyboardWhenTappedOnView(_ view:UIView)
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    /// Dismiss  the keyboard
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    
    
    /// Animate moves the view up or down when text input is pressed
    ///
    /// - parameter up:      Bool true is we should move the view up else false.
    /// - parameter upValue: CGFloat the value.
    func ViewUpanimateMoving (up:Bool, upValue :CGFloat)
    {
        let durationMovement:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -upValue : upValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(durationMovement)
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
    
    func showLoading(_ status:String) {
        
        if(status.isEmpty == false)
        {
            KVNProgress.show(withStatus: status)
            
            return
        }
        KVNProgress.show()
    }
    
    
    func dismissLoading() {
        KVNProgress.dismiss()
    }
    
    
    func showError(_ error:String, _ loadingType:UIViewControllerLoadingType = UIViewControllerLoadingType.KVNProgress) {
        
        if(loadingType == UIViewControllerLoadingType.KVNProgress)
        {
            KVNProgress.showError(withStatus: error)
        }
        else
        {
            KVNProgress.dismiss()
            
            let alertController = UIAlertController(
                title: error,
                message: nil,
                preferredStyle: UIAlertControllerStyle.alert
            )
            
            let confirmAction = UIAlertAction(
            title: NSLocalizedString("Confirm", comment: ""), style: UIAlertActionStyle.default) { (action) in
                
            }
            
            alertController.addAction(confirmAction)
            
            self.present(alertController,
                         animated: true,
                         completion: {
                            
            })

        }
        
        
    }
    
    func showSuccess(_ message:String?) {
        
        KVNProgress.showSuccess(withStatus: message)
        
    }
    
    
    func dismissError() {
        
        KVNProgress.dismiss()
        
    }
    
    
    
    
    /*
    func setTabBarVisible(visible:Bool, animated:Bool) {
        
        //This cannot be called before viewDidLayoutSubviews(), because the frame is not set before this time
        
        // bail if the current state matches the desired state
        if (tabBarIsVisible() == visible) { return }
        
        // get a frame calculation ready
        let frame = self.tabBarController?.tabBar.frame
        let height = frame?.size.height
        let offsetY = (visible ? -height! : height)
        
        // zero duration means no animation
        let duration:NSTimeInterval = (animated ? 0.3 : 0.0)
        
        //  animate the tabBar
        if frame != nil {
            UIView.animateWithDuration(duration) {
                self.tabBarController?.tabBar.frame = CGRectOffset(frame!, 0, offsetY!)
                return
            }
        }
    }
    
    func tabBarIsVisible() ->Bool {
        return self.tabBarController?.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame)
    }
    */
}
