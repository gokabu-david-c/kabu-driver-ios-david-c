//
//  KBBackgroundTaskManager.swift
//  Driver
//
//  Created by Kabu on 2018-01-08.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class KBBackgroundTaskManager: NSObject {
    static let shared = KBBackgroundTaskManager()
    private var downloadMeTimer:Timer?
    private var updateLocationTimer:Timer?
    private var downloadOpenOrdersTimer:Timer?
    private var downloadReservationPoolTimer:Timer?
    override init() {
        super.init()
        NotificationCenter.addObserver(self,
                                       action: #selector(applicationDidBecomeActive),
                                       name: NSNotification.Name.UIApplicationDidBecomeActive)
        NotificationCenter.addObserver(self,
                                       action: #selector(applicationDidEnterBackground),
                                       name: NSNotification.Name.UIApplicationDidEnterBackground)
    }
    @objc func applicationDidBecomeActive(){
        if(KBAccountManager.shared.isLoggedIn() == true){
            KBOpenOrdersManager.shared.downloadOpenOrders(start: nil,
                                                          success: {
                                                            
            }, failure: { (error) in
                
            })
            KBReservationPoolManager.shared.downloadReservationPool(start: nil,
                                                                    success: {
                                                                        
            }, failure: { (error) in
                
            })
            KBAccountManager.shared.updateLocation()
        }
    }
    @objc func applicationDidEnterBackground(){
        if(KBAccountManager.shared.isLoggedIn() == true){
            KBAccountManager.shared.updateLocation()
        }
    }
    func startBackgroundTasks(){
        GDLocationManager.shared.start()
        self.startDownloadMeTimer()
        self.startUpdateMyLoctionTimer()
        self.startDownloadOpenOrders()
        self.startDownloadingReservationPool()
    }
    func stopBackgroundTasks(){
        self.stopDownloadMeTimer()
        self.stopUpdateMyLoctionTimer()
        self.stopDownloadOpenOrders()
        self.stopDownloadingReservationPool()
    }
    func startDownloadMeTimer(){
        if(self.downloadMeTimer != nil){
            return;
        }
        self.downloadMeTimer = Timer.scheduledTimer(withTimeInterval: KBConfig.downloadOrderTimeInterval,
                                                    repeats: true,
                                                    block: { (timer:Timer) in
                                                        // Move to a background thread to do some long running work
                                                        DispatchQueue.global(qos: .userInitiated).async {
                                                            if(KBAccountManager.shared.isLoggedIn() == false){
                                                                return
                                                            }
                                                            NSLog("Download me task fired")
                                                            KBAccountManager.shared.downloadMe(start: nil,
                                                                            success: {
                                                                                NSLog("Download me task success")
                                                            }, failed: { (error) in
                                                                NSLog("Download me task failure")
                                                            })
                                                        }
        })
        self.downloadMeTimer?.fire()
    }
    func stopDownloadMeTimer(){
        if(self.downloadMeTimer == nil){
            return
        }
        self.downloadMeTimer!.invalidate()
        self.downloadMeTimer = nil
    }
    func startUpdateMyLoctionTimer(){
        if(self.updateLocationTimer != nil){
            return;
        }
        self.updateLocationTimer = Timer.scheduledTimer(withTimeInterval: KBConfig.updatelocationTimeInterval,
                                                        repeats: true,
                                                        block: { (timer:Timer) in
                                                            DispatchQueue.global(qos: .userInitiated).async {
                                                                if(KBAccountManager.shared.isLoggedIn() == false){
                                                                    return
                                                                }
                                                                KBAccountManager.shared.updateLocation()
                                                            }
        })
        self.updateLocationTimer?.fire()
    }
    func stopUpdateMyLoctionTimer(){
        if(self.updateLocationTimer == nil){
            return
        }
        self.updateLocationTimer?.invalidate()
        self.updateLocationTimer = nil
    }
    
    func startDownloadOpenOrders(){
        if(self.downloadOpenOrdersTimer != nil){
            return;
        }
        self.downloadOpenOrdersTimer = Timer.scheduledTimer(withTimeInterval: KBConfig.downloadOrderTimeInterval,
                                                            repeats: true,
                                                            block: { (timer:Timer) in
                                                                // Move to a background thread to do some long running work
                                                                DispatchQueue.global(qos: .userInitiated).async {
                                                                    if(KBAccountManager.shared.isLoggedIn() == false){
                                                                        KBOpenOrdersManager.shared.openOrders.removeAll()
                                                                        return
                                                                    }
                                                                    NSLog("Download open orders task fired")
                                                                    KBOpenOrdersManager.shared.downloadOpenOrders(start: nil,
                                                                                                                  success: {
                                                                                                
                                                                    }, failure: { (error) in
                                                                        
                                                                    })
                                                                }
        })
        self.downloadOpenOrdersTimer?.fire()
    }
    func stopDownloadOpenOrders(){
        if(self.downloadOpenOrdersTimer == nil){
            return
        }
        self.downloadOpenOrdersTimer?.invalidate()
        self.downloadOpenOrdersTimer = nil
    }
    func startDownloadingReservationPool(){
        if(self.downloadReservationPoolTimer != nil){
            return;
        }
        self.downloadReservationPoolTimer = Timer.scheduledTimer(withTimeInterval: KBConfig.downloadOrderTimeInterval,
                                                                 repeats: true,
                                                                 block: { (timer:Timer) in
                                                                    // Move to a background thread to do some long running work
                                                                    KBReservationPoolManager.shared.downloadReservationPool(start: nil,
                                                                                                                            success: {
                                                                                                    
                                                                    }, failure: { (error) in
                                                                        
                                                                    })
        }
        )
        self.downloadReservationPoolTimer!.fire()
    }
    func stopDownloadingReservationPool(){
        if(self.downloadReservationPoolTimer == nil){
            return
        }
        self.downloadReservationPoolTimer!.invalidate()
        self.downloadReservationPoolTimer = nil
    }
}
