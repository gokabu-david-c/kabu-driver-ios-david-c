//
//  GDMeViewController.swift
//  Driver
//
//  Created by ZY on 2017-05-20.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBMyAccountCenterViewController: KBViewController, UITableViewDelegate, UITableViewDataSource, GDSwitchTableViewCellDelegate {
    let accountVM = KBAccountVM()
    let systemVM = KBSystemSettingsVM()
    @IBOutlet weak var tableView: UITableView!
    var onlineStatusCell:GDSwitchTableViewCell?
    class func controller() -> KBMyAccountCenterViewController {
        let controller = UIStoryboard(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "KBMyAccountCenterViewController") as! KBMyAccountCenterViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        /*if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
        }*/
        self.automaticallyAdjustsScrollViewInsets = false

        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        self.registerNotifications()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func registerNotifications(){
        NotificationCenter.addObserver(self,
                                       action: #selector(performMeOnlineStatusChangedAction),
                                       name: NSNotification.Name.KB_Me_Online_Status_Changed);
    }
    @objc func performMeOnlineStatusChangedAction(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITableViewDelegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            let cellIdentifier = "cell"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
            if(cell == nil){
                cell = UITableViewCell(style: UITableViewCellStyle.value1,
                                       reuseIdentifier: cellIdentifier)
            }
            cell?.textLabel?.text = NSLocalizedString("Service Area", comment: "")
            if let account = KBAccountManager.shared.getAccount(){
                cell?.detailTextLabel?.text = account.rangeDisplayText()
            }
            else{
                cell?.detailTextLabel?.text = ""
            }
            cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            return cell!
        }
        else if(indexPath.row == 1){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDSwitchTableViewCell.cellIdentifier) as? GDSwitchTableViewCell
            if(cell == nil){
                cell = GDSwitchTableViewCell.cell()
            }
            cell?.textLabel?.text = NSLocalizedString("Reservation Notifications", comment: "")
            if let account = KBAccountManager.shared.getAccount(){
                cell?.switchView.isOn = account.isReceivingNewReservationNotification == true ? true : false
                /*if(account.is_busy == true){
                    cell?.switchView.onTintColor = UIColor.Busy
                }else if(account.is_online == true){
                    cell?.switchView.onTintColor = UIColor.Online
                }
                else{
                    cell?.switchView.onTintColor = UIColor.Offline
                }*/
            }
            cell?.delegate = self
            cell?.tag = 1
            self.onlineStatusCell = cell
            return cell!
        }
        else if(indexPath.row == 2){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDSwitchTableViewCell.cellIdentifier) as? GDSwitchTableViewCell
            if(cell == nil){
                cell = GDSwitchTableViewCell.cell()
            }
            cell?.textLabel?.text = NSLocalizedString("New Reservation Popup Window", comment: "Popup Window for displaying New Reservation that comes in")
            cell?.switchView.isOn = systemVM.isDisplayingNewReservationPopup
            cell?.tag = 2
            cell?.delegate = self
            return cell!
        }
        else if(indexPath.row == 3){
            let cellIdentifier = "cell"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
            if(cell == nil){
                cell = UITableViewCell(style: UITableViewCellStyle.value1,
                                       reuseIdentifier: cellIdentifier)
            }
            cell?.textLabel?.text = NSLocalizedString("Pay Bills", comment: "")
            cell?.detailTextLabel?.text = ""
            cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            return cell!
        }
        if(indexPath.row == 4){
            let cellIdentifier = "cell"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
            if(cell == nil){
                cell = UITableViewCell(style: UITableViewCellStyle.value1,
                                       reuseIdentifier: cellIdentifier)
            }
            cell?.textLabel?.text = NSLocalizedString("Kabu Benefits", comment: "")
            cell?.detailTextLabel?.text = ""
            cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            return cell!
        }
        if(indexPath.row == 5){
            let cellIdentifier = "cell"
            var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
            if(cell == nil){
                cell = UITableViewCell(style: UITableViewCellStyle.value1,
                                       reuseIdentifier: cellIdentifier)
            }
            cell?.textLabel?.text = NSLocalizedString("Settings", comment: "")
            cell?.detailTextLabel?.text = ""
            cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            return cell!
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.row == 0){
            let controller = KBOrderRangePickerViewController.controller()
            self.navigationController?.pushViewController(controller, animated: true)
            return
        }
        if(indexPath.row == 3){
            self.performSegue(withIdentifier:"PaymentSegue", sender: self)
            return
        }
        if(indexPath.row == 4){
            let controller = GDPureWebViewController.controller(urlString: KBConfig.URLString_DriverMemberShipCard, title: NSLocalizedString("Kabu Benefits", comment: ""))
            self.navigationController?.pushViewController(controller, animated: true)
            return
        }
        if(indexPath.row == 5){
            let controller = GDSettingsTableViewController.controller()
            self.navigationController?.pushViewController(controller, animated: true)
            return
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0000001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0000001
    }
    // MARK: - GDSwitchTableViewCellDelegate
    func GDSwitchTableViewCellValueChanged(_ sender: GDSwitchTableViewCell) {
        if(sender.tag == 1){
            var driverData = [String:String]()
            if(sender.switchView.isOn){
                driverData[KBDriver.KEY_IS_RECEIVING_NEW_RESERVATION_NOTIFICATION] = String(GDDriverIsReceivingNewReservationNotification.True.rawValue)
            }
            else{
                driverData[KBDriver.KEY_IS_RECEIVING_NEW_RESERVATION_NOTIFICATION] = String(GDDriverIsReceivingNewReservationNotification.False.rawValue)
            }
            if let currentLocation = GDLocationManager.shared.currentLocation{
                let currentLocationString = currentLocation.stringValue()
                driverData[KBDriver.Key_LastLoginCoordinate] = currentLocationString
            }
            self.accountVM.updateAccount(data: driverData,
                                         start: {
                                            self.showLoading("")
            }, success: { (driver:KBDriver) in
                self.dismissLoading()
                self.tableView.reloadData()
            }, failure: { (error:NSError) in
                self.showError(error.message)
                self.tableView.reloadData()
            })
            /*var driverData = [String:String]()
            if(sender.switchView.isOn){
                driverData[KBDriver.KEY_IS_ONLINE] = String(GDDriverISOnline.True.rawValue)
            }
            else{
                driverData[KBDriver.KEY_IS_ONLINE] = String(GDDriverISOnline.False.rawValue)
            }
            if let currentLocation = GDLocationManager.shared.currentLocation{
                let currentLocationString = currentLocation.stringValue()
                driverData[KBDriver.Key_LastLoginCoordinate] = currentLocationString
            }
            KBAccountManager.shared.update(driverData,
                                           start: {
                                            self.showLoading("")
            }, success: {
                self.dismissLoading()
                self.tableView.reloadData()
            }, failure: { (error:NSError) in
                self.showError(error.message)
                self.tableView.reloadData()
            })
            return*/
        }
        else{
            systemVM.isDisplayingNewReservationPopup = sender.switchView.isOn
        }
    }
}
