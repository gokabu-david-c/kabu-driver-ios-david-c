//
//  GDChangePasswordTableViewController.swift
//  Driver
//
//  Created by ZY on 2017-04-18.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDChangePasswordTableViewController: GDTableViewController, GDTextFieldTableViewCellDelegate {
    let accountVM = KBAccountVM()
    var cell_CurrentPassword:GDTextFieldTableViewCell?
    var cell_NewPassword:GDTextFieldTableViewCell?
    var cell_ConfirmNewPassword:GDTextFieldTableViewCell?
    var currentPassword:String = ""
    var newPassword:String = ""
    var confirmNewPassword:String = ""
    weak var delegate:GDChangePasswordTableViewControllerDelegate?
    class func controller(delegate aDelegate:GDChangePasswordTableViewControllerDelegate) -> GDChangePasswordTableViewController{
        let controller = UIStoryboard(name: "ChangePassword", bundle: nil)
            .instantiateViewController(withIdentifier: "GDChangePasswordTableViewController") as! GDChangePasswordTableViewController
        controller.delegate = aDelegate
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 52
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.dismissKeyboard()
    }
    func save(){
        if(self.currentPassword.count == 0){
            showError(NSLocalizedString("Please enter Current Password", comment: ""))
            if(self.cell_CurrentPassword != nil){
                self.cell_CurrentPassword?.tf_Content.becomeFirstResponder()
            }
            return
        }
        
        if(self.newPassword.count == 0){
            showError(NSLocalizedString("Please enter New Password", comment: ""))
            if(self.cell_NewPassword != nil){
                self.cell_NewPassword?.tf_Content.becomeFirstResponder()
            }
            return
        }
        if(self.confirmNewPassword.count == 0){
            showError(NSLocalizedString("Please confirm New Password", comment: ""))
            if(self.cell_ConfirmNewPassword != nil){
                self.cell_ConfirmNewPassword?.tf_Content.becomeFirstResponder()
            }
            return
        }
        if(self.newPassword != self.confirmNewPassword){
            showError(NSLocalizedString("New Passwords DO NOT Match", comment: ""))
            return
        }
        dismissKeyboard()
        let driverData = ["current_password": self.currentPassword,
                          "new_password" : self.newPassword]
        self.accountVM.updateAccount(data: driverData,
                                     start: {
                                        self.showLoading("")
        }, success: { (driver:KBDriver) in
            self.dismissLoading()
            self.delegate?.GDChangePasswordTableViewControllerDidSaveSuccess?(self)
        }) { (error:NSError) in
            self.showError(error.message)
        }
    }
    @IBAction func saveButtonTapped(_ sender: Any) {
        self.save()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 3
        }
        return 0
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            if(indexPath.row == 0){
                var cell = tableView.dequeueReusableCell(withIdentifier: GDTextFieldTableViewCell.cellIdentifier) as? GDTextFieldTableViewCell
                if(cell == nil){
                    cell = GDTextFieldTableViewCell.cell()
                }
                cell?.setupCell(NSLocalizedString("Current Password", comment: ""), self.currentPassword, self)
                cell?.tf_Content.isSecureTextEntry = true;
                self.cell_CurrentPassword = cell
                return cell!
            }
            if(indexPath.row == 1){
                var cell = tableView.dequeueReusableCell(withIdentifier: GDTextFieldTableViewCell.cellIdentifier) as? GDTextFieldTableViewCell
                if(cell == nil){
                    cell = GDTextFieldTableViewCell.cell()
                }
                cell?.setupCell(NSLocalizedString("New Password", comment: ""), self.currentPassword, self)
                cell?.tf_Content.isSecureTextEntry = true;
                self.cell_NewPassword = cell
                return cell!
            }
            if(indexPath.row == 2){
                var cell = tableView.dequeueReusableCell(withIdentifier: GDTextFieldTableViewCell.cellIdentifier) as? GDTextFieldTableViewCell
                if(cell == nil){
                    cell = GDTextFieldTableViewCell.cell()
                }
                cell?.setupCell(NSLocalizedString("Confirm New Password", comment: ""), self.currentPassword, self)
                cell?.tf_Content.isSecureTextEntry = true;
                self.cell_ConfirmNewPassword = cell
                return cell!
            }
        }
        return UITableViewCell()
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath,
                              animated: true)
    }
    //MARK: - GDTextFieldTableViewCellDelegate
    func GDTextFieldTableViewCellValueChanged(_ sender: GDTextFieldTableViewCell) {
        if(sender == self.cell_CurrentPassword){
            self.currentPassword = sender.tf_Content.text!
        }else if(sender == self.cell_NewPassword){
            self.newPassword = sender.tf_Content.text!
        }else if(sender == self.cell_ConfirmNewPassword){
            self.confirmNewPassword = sender.tf_Content.text!
        }
    }
}
@objc protocol GDChangePasswordTableViewControllerDelegate : class{
    @objc optional func GDChangePasswordTableViewControllerDidSaveSuccess(_ sender:GDChangePasswordTableViewController)
}
