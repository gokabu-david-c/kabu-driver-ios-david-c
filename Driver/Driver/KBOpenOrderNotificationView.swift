//
//  KBNewOrderNotificationView.swift
//  Driver
//
//  Created by Jack Du on 2018-04-13.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

enum CountdownButtonState {
    case normal, zero, ready
}

protocol  KBOpenOrderNotificationViewDelegate{
    func onFinishedNotificationCountdown()
}

class KBOpenOrderNotificationView : UIView {
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var dismissView1: UIView!
    @IBOutlet var dismissView2: UIView!
    
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var addressLabel: UILabel!
    
    @IBOutlet var moneyLabel: UILabel!
    
    @IBOutlet var peopleLabel: UILabel!
    
    @IBOutlet var commentView: UIView!
    @IBOutlet var commentLabel: UILabel!
    @IBOutlet var commentIcon: UIImageView!

    @IBOutlet var countdownView: UIView!
    @IBOutlet var countdownLabel: UILabel!
    @IBOutlet var countdownTitleLabel: UILabel!
    @IBOutlet var countdownButton: UIButton!
    @IBOutlet var countdownCheckView: UIImageView!
    
    var parentVC : KBOpenOrderNotificationViewDelegate?
    var openOrder : KBOrder?
    var secondTimer : Timer?
    var secondsLeft : Int64 = 10
    
    var dateFormatter : DateFormatter = DateFormatter.init()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setOpenOrder(openOrder : KBOrder?, parentVC:KBOpenOrderNotificationViewDelegate){
        
        guard let newOrder = openOrder else{
            return
        }
        self.openOrder = newOrder
        self.parentVC = parentVC
        self.peopleLabel.text = String(newOrder.numberOfPassengers)
        self.moneyLabel.text = "$ \(String(Int(newOrder.total)))"
        self.addressLabel.text = newOrder.startAddress
        self.setupDate()
        self.setupComment()
        self.setupCountdown()

    }
    
    func setupDate(){
        let orderDate = Date.init(timeIntervalSince1970:Double(self.openOrder!.reservationPickupTime))
        dateFormatter.dateFormat = "hh:mma"
        let time = dateFormatter.string(from: orderDate)
        dateLabel.text = DateHelper().dayDifference(orderDate)
        timeLabel.text = time
    }
    
    func setupComment(){
        if(self.openOrder!.customerNote.replacingOccurrences(of: " ", with: "").count <= 0){
            self.commentView.isHidden = true
        }
        else{
            self.commentView.isHidden = false
            self.commentLabel.text = openOrder?.customerNote
        }
    }

    func raceSuccessful(){
        countdownLabel.isHidden = true
        self.changeButtonState(state: .ready)
    }

}

//Countdown Timer
extension KBOpenOrderNotificationView{
    
    func setupCountdown(){
        
        secondsLeft = 10
        self.countdownSecond()
    }
    
    @objc func countdownSecond(){
        
        if(self.secondsLeft > 0){
            self.countdownLabel.text = String(self.secondsLeft)
            self.secondsLeft -= 1
            if(self.secondTimer != nil){
                secondTimer?.invalidate()
            }
            secondTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: .countdownSecond, userInfo: nil, repeats: false)
        }
        else{
            parentVC?.onFinishedNotificationCountdown()
        }
    }
    
    func stopTimer(){
        secondTimer?.invalidate()
    }
    
}

//UI Functions
extension KBOpenOrderNotificationView{
    
    func changeButtonState(state:CountdownButtonState)
    {
        switch state {
        case .ready:
            UIView.animate(withDuration: 0.5, animations: {
                self.countdownCheckView.isHidden = false
                self.countdownTitleLabel.isHidden = true
                self.countdownButton.backgroundColor = UIColor(red:0.61, green:0.87, blue:0.35, alpha:1)
            })

        case .zero:
            self.countdownButton.backgroundColor = UIColor.orange
            self.countdownButton.isEnabled = true
        default:
            ()
        }
    }
}


fileprivate extension Selector {
    static let countdownSecond = #selector(KBOpenOrderNotificationView.countdownSecond)
}
