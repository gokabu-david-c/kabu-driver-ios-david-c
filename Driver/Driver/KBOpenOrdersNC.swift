//
//  KBOpenOrdersNC.swift
//  Driver
//
//  Created by ZY on 2017-05-20.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBOpenOrdersNC: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let controller = KBOpenOrdersVC.controller()
        self.setViewControllers([controller], animated: false)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
