//
//  KBPaymentAlipayVC.swift
//  Driver
//
//  Created by Jack Du on 2018-05-01.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//


class KBPaymentAlipayVC : UIViewController{
    
    @IBOutlet var backgroundView: GDView!
    override func viewDidLoad() {
        super.viewDidLoad()
        KBBillManager.shared.getAlipayOrderInfo(
            start: ({}),
            success: { orderInfo in
                AlipaySDK.defaultService().payOrder(orderInfo, fromScheme: "kabudriver", callback: ({_ in
                }))
                
        },
            failure: {error in
            self.showError(error.message)
            Timer.scheduledTimer(timeInterval: 2, target: self, selector: .dismissView, userInfo: nil, repeats: false)
        })
        NotificationCenter.addObserver(self,
                                       action: #selector(returnFromAlipay),
                                       name: NSNotification.Name.UIApplicationDidBecomeActive);
        
    }
    
    @objc func returnFromAlipay(){
        print("Return from alipay")
        self.showLoading("")

        DispatchQueue.main.async {
        }
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: .dismissView, userInfo: nil, repeats: false)
    }
    
    @objc func dismissSelf(){
        self.dismissLoading()
        self.navigationController?.popToRootViewController(animated: true)
    }
}

fileprivate extension Selector{
    static let dismissView = #selector(KBPaymentAlipayVC.dismissSelf)

    
}
