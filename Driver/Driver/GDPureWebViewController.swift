//
//  GDDriverPolicyViewController.swift
//  Driver
//
//  Created by ZY on 2017-04-18.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import WebKit

class GDPureWebViewController: KBViewController, WKNavigationDelegate, UIScrollViewDelegate {

    var webView: WKWebView!
    var progressView: UIProgressView!
    var urlString:String?
    var needsReload = true
    
    class func controller(urlString aURLString:String?, title aTitle:String?) -> GDPureWebViewController{
        let controller = UIStoryboard(name: "GDPureWebViewController", bundle: nil)
            .instantiateViewController(withIdentifier: "GDPureWebViewController") as! GDPureWebViewController
        controller.urlString = aURLString
        if aTitle != nil{
            controller.title = aTitle
        }
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = UINavigationItem.LargeTitleDisplayMode.never
        }
        self.automaticallyAdjustsScrollViewInsets = false
        let webViewConfig = WKWebViewConfiguration()
        if let account = KBAccountManager.shared.getAccount(){
            let userContentController = WKUserContentController();
            let cookieScript = WKUserScript(source: "document.cookie = 'uid=" + String(account.id) + "';document.cookie = 'user_access_token=" + account.userAccessToken + "';",
                                            injectionTime: WKUserScriptInjectionTime.atDocumentStart,
                                            forMainFrameOnly: false)
            userContentController.addUserScript(cookieScript)
            webViewConfig.userContentController = userContentController;
        }
        self.webView = WKWebView(frame: CGRect.zero, configuration: webViewConfig)
        self.webView.navigationDelegate = self
        self.webView.scrollView.delegate = self
        self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.webView)
        self.webView.superview?.addConstraint(NSLayoutConstraint(item: self.webView,
                                                                 attribute: NSLayoutAttribute.left,
                                                                 relatedBy: NSLayoutRelation.equal,
                                                                 toItem: self.webView.superview,
                                                                 attribute: NSLayoutAttribute.left,
                                                                 multiplier: 1.0,
                                                                 constant: 0))
        self.webView.superview?.addConstraint(NSLayoutConstraint(item: self.webView,
                                                                 attribute: NSLayoutAttribute.right,
                                                                 relatedBy: NSLayoutRelation.equal,
                                                                 toItem: self.webView.superview,
                                                                 attribute: NSLayoutAttribute.right,
                                                                 multiplier: 1.0,
                                                                 constant: 0))
        self.webView.superview?.addConstraint(NSLayoutConstraint(item: self.webView,
                                                                 attribute: NSLayoutAttribute.top,
                                                                 relatedBy: NSLayoutRelation.equal,
                                                                 toItem: self.webView.superview,
                                                                 attribute: NSLayoutAttribute.top,
                                                                 multiplier: 1.0,
                                                                 constant: 0))
        self.webView.superview?.addConstraint(NSLayoutConstraint(item: self.webView,
                                                                 attribute: NSLayoutAttribute.bottom,
                                                                 relatedBy: NSLayoutRelation.equal,
                                                                 toItem: self.webView.superview,
                                                                 attribute: NSLayoutAttribute.bottom,
                                                                 multiplier: 1.0,
                                                                 constant: 0))
        self.progressView = UIProgressView(progressViewStyle: .default)
        self.progressView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.progressView)
        self.progressView.superview?.addConstraint(NSLayoutConstraint(item: self.progressView,
                                                                      attribute: NSLayoutAttribute.left,
                                                                      relatedBy: NSLayoutRelation.equal,
                                                                      toItem: self.progressView.superview,
                                                                      attribute: NSLayoutAttribute.left,
                                                                      multiplier: 1.0,
                                                                      constant: 0))
        self.progressView.superview?.addConstraint(NSLayoutConstraint(item: self.progressView,
                                                                      attribute: NSLayoutAttribute.right,
                                                                      relatedBy: NSLayoutRelation.equal,
                                                                      toItem: self.progressView.superview,
                                                                      attribute: NSLayoutAttribute.right,
                                                                      multiplier: 1.0,
                                                                      constant: 0))
        self.progressView.superview?.addConstraint(NSLayoutConstraint(item: self.progressView,
                                                                      attribute: NSLayoutAttribute.top,
                                                                      relatedBy: NSLayoutRelation.equal,
                                                                      toItem: self.progressView.superview,
                                                                      attribute: NSLayoutAttribute.top,
                                                                      multiplier: 1.0,
                                                                      constant: 0))
        self.progressView.superview?.addConstraint(NSLayoutConstraint(item: self.progressView,
                                                                      attribute: NSLayoutAttribute.height,
                                                                      relatedBy: NSLayoutRelation.equal,
                                                                      toItem: nil,
                                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                                      multiplier: 1.0,
                                                                      constant: 2))
        self.webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    }
    deinit {
        webView!.removeObserver(self, forKeyPath:#keyPath(WKWebView.estimatedProgress))
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navigationController = self.navigationController{
            navigationController.isNavigationBarHidden = false
        }
        if(self.isViewWillAppeared == false){
            if self.urlString != nil{
                let url = URL.init(string: self.urlString!)
                var urlRequest = URLRequest.init(url: url!)
                //HTTPCookieStorage.shared.cookieAcceptPolicy = HTTPCookie.AcceptPolicy.always
                if let account = KBAccountManager.shared.getAccount(){
                    urlRequest.addValue("uid=" + String(account.id) + ";user_access_token=" + account.userAccessToken + ";",
                                        forHTTPHeaderField: "Cookie")
                }
                self.webView.load(urlRequest)
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        // Display Progress Bar While Loading Pages
        if keyPath == "estimatedProgress" {
            self.progressView.progress = Float(self.webView.estimatedProgress)
            if(self.progressView.progress >= 1){
                self.progressView.isHidden = true
            }
            else{
                self.progressView.isHidden = false
            }
        }
    }

    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true) {
        }
    }
    
    // MARK: UIScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.webView.setNeedsDisplay()

    }
    
    // MARK: WKNavigationDelegate
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.setNeedsDisplay()
        /*if(self.needsReload){
            webView.reload()
            self.needsReload = false
        }*/
    }
    
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        
    }
}
