//
//  GoogleDirectionsAPIHelper.swift
//  Driver
//
//  Created by ZY on 2017-04-29.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class GoogleDirectionsAPIHelper {
    /*static var key:String{
        if(GDAppManager.isDevMode()){
            return "AIzaSyBqz7KY7Ruz-laHd8seU8eg5OlnB2BKwss"
        }
        return "AIzaSyA8n2XWOM9B66N2WVw20tnt1FLSbQaFLIk"
    }*/
    class func downloadBestDistance(startLocation aStartLocation:CLLocation,
                                    endLocation aEndLocation:CLLocation,
                                    useShortestRoute aUseShortestRoute:Bool,
                                    start sStart:() -> (),
                                    success aSuccess:@escaping(_ points:[CLLocationCoordinate2D]) -> (),
                                    failure aFailure:@escaping(_ error:NSError) -> ()){
        let group = DispatchGroup()
        //not avoiding highway
        var distance_WithHighway = 0
        var route_WithHighway = [CLLocationCoordinate2D]()
        group.enter()
        GoogleDirectionsAPIHelper.downloadDirections(startLocation: aStartLocation,
                                                     endLocation: aEndLocation,
                                                     avoids: ["ferries", "indoor"],
                                                     start: {
        }, success: { (_ distance:Int, _ duraiton:Int, _ route:[CLLocationCoordinate2D]) in
            distance_WithHighway = distance
            route_WithHighway = route
            group.leave()
        }, faliure: { (_ error:NSError) in
            group.leave()
        })
        //avoiding highway
        var distance_NoHighway = 0
        var route_NoHighway = [CLLocationCoordinate2D]()
        if(aUseShortestRoute == true){
            group.enter()
            GoogleDirectionsAPIHelper.downloadDirections(startLocation: aStartLocation,
                                                         endLocation: aEndLocation,
                                                         avoids: ["highways", "ferries", "indoor"],
                                                         start: {
            }, success: { (_ distance:Int, _ duraiton:Int, _ route:[CLLocationCoordinate2D]) in
                distance_NoHighway = distance
                route_NoHighway = route
                group.leave()
            }, faliure: { (_ error:NSError) in
                group.leave()
            })
        }
        group.notify(queue: DispatchQueue.main) {
            if(route_WithHighway.count == 0 && route_NoHighway.count == 0){
                aSuccess([CLLocationCoordinate2D]())
                return
            }
            if(route_WithHighway.count > 0 && route_NoHighway.count == 0){
                aSuccess(route_WithHighway)
                return
            }
            if(route_NoHighway.count > 0 && route_WithHighway.count == 0){
                aSuccess(route_NoHighway)
                return
            }
            if(distance_WithHighway <= distance_NoHighway){
                aSuccess(route_WithHighway)
                return
            }
            if(distance_NoHighway < distance_WithHighway){
                aSuccess(route_NoHighway)
                return
            }
            aSuccess([CLLocationCoordinate2D]())
        }
    }
    private class func downloadDirections(startLocation aStartLocation:CLLocation,
                                  endLocation aEndLocation:CLLocation,
                                  avoids aVoids:[String],
                                  start aStart:() -> (),
                                  success aSuccess:@escaping (_ distance:Int, _ duration:Int, _ points:[CLLocationCoordinate2D]) -> (),
                                  faliure aFailure:@escaping (_ error:NSError) -> ()){
        aStart()
        var url = "https://maps.googleapis.com/maps/api/directions/json?"
        url += "origin=" + aStartLocation.stringValue().addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        url += "&destination=" + aEndLocation.stringValue().addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        //url += "&mode=driving&key=" + GoogleDirectionsAPIHelper.key
        url += "&mode=driving&key=" + KBConfig.Key_GoogleAPI
        url += "&avoid=" + aVoids.joined(separator: ",").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let manager = AFHTTPSessionManager.newInstance()
        manager.get(url,
                    parameters: nil,
                    progress: nil,
                    success: { (task:URLSessionDataTask, responseObject:Any?) in
                        if (responseObject as? [String:Any]) != nil{
                            let (distance, duration, route) = GoogleDirectionsAPIHelper.getPoints((responseObject as? [String : Any])!)
                            aSuccess(distance, duration, route)
                            return
                        }
                        aFailure(NSError.Error_UnknownError())
        }) { (task:URLSessionDataTask?, error:Error) in
            aFailure(NSError.Error_UnknownError())
        }
    }
    //return distance, duration, routes
    class func getPoints(_ googleDirectionsAPIData:[String:Any]) -> (Int, Int, [CLLocationCoordinate2D]){
        let routes = googleDirectionsAPIData["routes"] as? NSArray
        if(routes == nil){
            return (0,0,[])
        }
        if(routes?.count == 0){
            return (0,0,[])
        }
        let firstRoute = routes?[0] as! [String:Any]
        var distance = 0
        var duration = 0
        if let legs = firstRoute["legs"] as? [[String:Any]]{
            if(legs.count > 0){
                let firstLeg = legs[0]
                if let distanceDict = firstLeg["distance"] as? [String:Any]{
                    if let distanceValue = distanceDict["value"] as? Int{
                        distance = distanceValue
                    }
                }
                if let durationDict = firstLeg["duration"] as? [String:Any]{
                    if let durationValue = durationDict["value"] as? Int{
                        duration = durationValue
                    }
                }
            }
        }
        var result:[CLLocationCoordinate2D] = []
        if let overViewPolyLine = firstRoute["overview_polyline"] as? [String:Any]{
            if let pointsString = overViewPolyLine["points"] as? String{
                let coordinates: [CLLocationCoordinate2D]? = decodePolyline(pointsString)
                if(coordinates != nil){
                    result.append(contentsOf: coordinates!)
                }
            }
        }
        return (distance, duration, result)
    }
}
