//
//  KBAccountVM.swift
//  Driver
//
//  Created by Kabu on 2017-11-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBAccountVM: NSObject {
    var account:KBDriver?{
        return KBAccountManager.shared.getAccount()
    }
    func login(username aUsername:String,
               password aPassword:String,
               start aStart:(() -> ())?,
               success aSuccess:@escaping () -> (),
               failed aFailed:@escaping (_ error:String) -> ()){
        if(aUsername.count == 0){
            aFailed(NSLocalizedString("Please enter your Account Name", comment: ""))
            return
        }
        if(aPassword.count == 0){
            aFailed(NSLocalizedString("Please Enter Password" , comment: ""))
            return
        }
        KBAccountManager.shared.login(aUsername,
                                      aPassword,
                                      start: {
                                        aStart?()
        }, success: {
            aSuccess()
        }) { (error:String) in
            aFailed(error)
        }
    }
    func takeAccountOnline(start aStart:(() -> ())?,
                           success aSuccess:@escaping(() -> ()),
                           failure aFailure:@escaping((_ error:NSError) -> ())){
        var driverData = [String : String]()
        if let currentLocation = GDLocationManager.shared.currentLocation{
            let currentLocationString = currentLocation.stringValue()
            driverData[KBDriver.Key_LastLoginCoordinate] = currentLocationString
        }
        driverData[KBDriver.KEY_IS_ONLINE] = String(GDDriverISOnline.True.rawValue)
        self.updateAccount(data: driverData,
                           start: {
                            aStart?()
        }, success: { (driver:KBDriver) in
            aSuccess()
        }) { (error:NSError) in
            aFailure(error)
        }
    }
    func takeAccountOffline(start aStart:(() -> ())?,
                           success aSuccess:@escaping(() -> ()),
                           failure aFailure:@escaping((_ error:NSError) -> ())){
        var driverData = [String : String]()
        if let currentLocation = GDLocationManager.shared.currentLocation{
            let currentLocationString = currentLocation.stringValue()
            driverData[KBDriver.Key_LastLoginCoordinate] = currentLocationString
        }
        driverData[KBDriver.KEY_IS_ONLINE] = String(GDDriverISOnline.False.rawValue)
        self.updateAccount(data: driverData,
                           start: {
                            aStart?()
        }, success: { (driver:KBDriver) in
            aSuccess()
        }) { (error:NSError) in
            aFailure(error)
        }
    }
    func downloadAccount(start aStart:(() -> ())?,
                       success aSuccess:@escaping(() -> ()),
                       failure aFailure:@escaping((_ error:NSError) -> ())){
        if(KBAccountManager.shared.isLoggedIn() == false){
            aFailure(NSError.Error(message: NSLocalizedString("Please Login", comment:"")))
            return
        }
        KBAccountManager.shared.downloadMe(start: {
            aStart?()
        }, success: {
            aSuccess()
        }) { (error:NSError) in
            aFailure(error)
        }
    }
    func updateAccount(data aData:[String:String],
                       start aStart:() -> (),
                       success aSuccess:@escaping (_ driver:KBDriver)->(),
                       failure aFailure:@escaping (_ error:NSError)->()){
        if(self.account == nil){
            return
        }
        if(aData.count == 0){
            return
        }
        KBDriversAPI.updateDriver(driverID: self.account!.id,
                                  driverData: aData,
                                  start: {
                                    aStart()
        }, success: { (driver:KBDriver) in
            KBAccountManager.shared.account = driver
            aSuccess(driver)
        }) { (error:NSError) in
            aFailure(error)
        }
    }
}
