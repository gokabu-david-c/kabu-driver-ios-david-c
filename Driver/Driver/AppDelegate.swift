//
//  AppDelegate.swift
//  Driver
//
//  Created by Developer on 03/04/2017.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import UserNotifications
import GoogleMaps
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let accountVM = KBAccountVM()
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        if(UserDefaults.isFirstRun()){
            GDAppManager.clearData()
            UserDefaults.setIsFirstRun(false)
        }else if(GDAppManager.getUserDefaultsDataVersion() != GDAppManager.getBuild()){
            GDAppManager.clearData()
        }
        GDAppManager.setUserDefaultsDataVersion()
        KBBackgroundTaskManager.shared.startBackgroundTasks()
        // Init GAI.
        self.iniGAI()
        NotificationCenter.addObserver(self,
                                       action: #selector(userLoggedIn),
                                       name: NSNotification.Name.GD_Me_LoggedIn)
        //AIzaSyA8n2XWOM9B66N2WVw20tnt1FLSbQaFLIk
        GMSServices.provideAPIKey(KBConfig.Key_GoogleAPI)
        _ = GDLocationManager.shared
        self.registerForPushNotification()
        
        WXApi.registerApp(KBConfig.Wechat_App_Id)

        KBScreenHelper.shared.updateScreen()
        self.loadAppearance()
        return true
    }
    func iniGAI(){
        if let Tracker = GAI.sharedInstance(){
            //Add Publisher Track ID
            Tracker.tracker(withTrackingId: "UA-98598775-1")
        }
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        GDAppManager.shared.deviceToken = token
        if(KBAccountManager.shared.isLoggedIn()){
            let driverData = [KBDriver.Key_DeviceType : "iOS",
                              KBDriver.Key_DeviceToken : token]
            self.accountVM.updateAccount(data: driverData,
                                    start: {
                                        
            }, success: { (driver:KBDriver) in
                
            }, failure: { (error:NSError) in
                
            })
        }
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let aps = GDAPS(userInfo["aps"] as! Dictionary<String, AnyObject>)
        if(aps.action == NSNotification.Action_Orders_NewOrder){
            let openOrderVM = KBOpenOrdersVM()
            openOrderVM.downloadOpenOrders(start:nil, success: {
                
            }, failure: { (error) in
                
            })
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        application.applicationIconBadgeNumber = 0; // Clear badge when app is or resumed
    }
    @objc func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.isIdleTimerDisabled = true
        UIApplication.shared.applicationIconBadgeNumber = 0
        print("Did become active")
        if(KBAccountManager.shared.isLoggedIn()){
            self.registerForPushNotification()
            let accountVM = KBAccountVM()
            accountVM.downloadAccount(start: nil,
                                      success: {
                                        
            }, failure: { (error:NSError) in
                
            })
            let driverData = [KBDriver.Key_DeviceType: "iOS"]
            self.accountVM.updateAccount(data: driverData,
                                         start: {
                                            
            }, success: { (driver:KBDriver) in
                
            }, failure: { (error:NSError) in
                
            })
            GDLocationManager.shared.start()
        }
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    @objc func userLoggedIn(){
        let driverData = [KBDriver.Key_DeviceType: "iOS"]
        self.accountVM.updateAccount(data: driverData,
                                     start: {
                                        
        }, success: { (driver:KBDriver) in
            
        }) { (error:NSError) in
            
        }
        self.registerForPushNotification()
    }
    func registerForPushNotification(){
        UIApplication.shared.registerForRemoteNotifications()
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                // Enable or disable features based on authorization.
                if granted {
                    // update application settings
                }
            }
        } else {
            // Fallback on earlier versions
            let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert,
                                                             UIUserNotificationType.badge,
                                                             UIUserNotificationType.sound]
            let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes,
                                                                      categories: nil)
            UIApplication.shared.registerUserNotificationSettings(pushNotificationSettings)
        }
    }
    func loadAppearance(){
        UITableView.appearance().backgroundColor = UIColor.init(red: 240.0/255.0,
                                                                green: 240.0/255.0,
                                                                blue: 240.0/255.0,
                                                                alpha: 1.0)
    }
}
extension AppDelegate : UNUserNotificationCenterDelegate{
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
        let userInfo = notification.request.content.userInfo
        let aps = GDAPS(userInfo["aps"] as? Dictionary<String, AnyObject> ?? Dictionary<String, AnyObject>())
        if(aps.action == NSNotification.Action_Orders_NewOrder
            || aps.action == NSNotification.Action_Orders_OrderCanceled){
            let openOrderVM = KBOpenOrdersVM()
            openOrderVM.downloadOpenOrders(start: nil,
                                           success: {
                                            
            }, failure: { (error) in
            })
        }
        completionHandler([.badge, .sound])
        /*
        if(aps.action != NSNotification.Action_Orders_NewOrder){
            //completionHandler([.alert, .badge, .sound])
            completionHandler([.badge, .sound])
        }*/
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void){
        completionHandler()
    }
}

//MARK: WeChat API delegate
extension AppDelegate : WXApiDelegate{
    
    func onReq(_ req: BaseReq!) {
        

    }
    
    func onResp(_ resp: BaseResp!) {
        // do optional stuff
    }
    
}

//MARK: External URL handlers
extension AppDelegate{
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let sendingAppID = options[.sourceApplication]
        print("app ID: \(url.host)")
        
        //如果极简开发包不可用，会跳转支付宝钱包进行支付，需要将支付宝钱包的支付结果回传给开发包
//        if(url.host == "safepay"){
//                AlipaySDK.defaultService().processOrder(withPaymentResult: url, standbyCallback: {(resultDic: Dictionary<AnyHashable, Any>) in
//                } as! CompletionBlock)
//
//        }
//
//        //如果极简开发包不可用，会跳转支付宝钱包进行支付，需要将支付宝钱包的支付结果回传给开发包
//        if ([url.host isEqualToString:@"safepay"]) {
//            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
//                //【由于在跳转支付宝客户端支付的过程中，商户app在后台很可能被系统kill了，所以pay接口的callback就会失效，请商户对standbyCallback返回的回调结果进行处理,就是在这个方法里面处理跟callback一样的逻辑】
//                NSLog(@"result = %@",resultDic);
//                }];
//        }
//        if ([url.host isEqualToString:@"platformapi"]){//支付宝钱包快登授权返回authCode
//
//            [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
//                //【由于在跳转支付宝客户端支付的过程中，商户app在后台很可能被系统kill了，所以pay接口的callback就会失效，请商户对standbyCallback返回的回调结果进行处理,就是在这个方法里面处理跟callback一样的逻辑】
//                NSLog(@"result = %@",resultDic);
//                }];
//        }
//        return true;
        return WXApi.handleOpen(url, delegate: self)
        
    }
    

}
