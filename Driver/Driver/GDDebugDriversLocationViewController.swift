//
//  GDDriversLocationViewController.swift
//  Driver
//
//  Created by ZY on 2017-05-02.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import GoogleMaps

class GDDebugDriversLocationViewController: KBViewController, GDMapViewDelegate {
    var mapView = GDMapView.view()
    var selectedDriver:KBDriver?
    var driverMarkers:[GMSMarker] = [GMSMarker]()
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var lbl_DriverDisplayName: UILabel!
    @IBOutlet weak var lbl_CarModel: UILabel!
    @IBOutlet weak var lbl_Phone: UILabel!
    @IBOutlet weak var lbl_Capacity: UILabel!
    @IBOutlet weak var constraint_Bottom_DriverInfoContainer: NSLayoutConstraint!
    @IBOutlet weak var view_DriverInfoContainer: UIView!
    @IBOutlet var citySegmentedControlContainer: UIView!
    var citySegmentedControl:HMSegmentedControl?
    let areasDataVM = KBAreasDataVM()
    var cityPolygons = [GMSPolygon]()
    var selectedAreaID = 0
    var parentAreas = [KBArea]()
    var driversVM = KBDriversDataVM()
    class func controller() -> GDDebugDriversLocationViewController {
        let controller = UIStoryboard(name: "Debug", bundle: nil).instantiateViewController(withIdentifier: "GDDebugDriversLocationViewController") as! GDDebugDriversLocationViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "司机位置"
        self.mapView.delegate = self
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.mapContainerView.addSubview(self.mapView)
        if let superview = self.mapView.superview{
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1.0,
                                                       constant: 0))
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.trailing,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.trailing,
                                                       multiplier: 1.0,
                                                       constant: 0))
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1.0,
                                                       constant: 0))
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.bottom,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.bottom,
                                                       multiplier: 1.0,
                                                       constant: 0))
        }
        self.mapView.mapView.isMyLocationEnabled = true
        self.mapView.mapView.settings.rotateGestures = false
        self.driversVM.driversUpdated = {
            self.dismissLoading()
            self.reloadDrivers(self.driversVM.drivers)
        }
        self.driversVM.driversUpdateFailed = { error in
            self.showError(error)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.areasDataVM.downloadCities(start: {
            
        }, success: { (cities:[KBArea]) in
            self.updateAreas(cities)
        }) { (error:NSError) in
            
        }
        self.driversVM.downloadDrivers()
    }
    @IBAction func rightButtonTapped(_ sender: Any) {
        if(KBAccountManager.shared.isLoggedIn()){
            self.showLoading("")
            self.driversVM.downloadDrivers()
        }
    }
    @IBAction func phoneButtonTapped(_ sender: Any) {
        if self.selectedDriver != nil{
            if let url = NSURL(string: "tel://" + (self.selectedDriver?.phone)!){
                if(UIApplication.shared.canOpenURL(url as URL)){
                    UIApplication.shared.open(url as URL,
                                              options: [:],
                                              completionHandler: { (_ complete:Bool) in
                    })
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func segmentedControlChangedValue(sender aSender:HMSegmentedControl) {
        if(aSender == self.citySegmentedControl){
            if(aSender.selectedSegmentIndex == 0){
                self.selectedAreaID = 0
            }
            else{
                self.selectedAreaID = self.parentAreas[aSender.selectedSegmentIndex - 1].id
            }
            self.updateAreas(self.areasDataVM.areas)
            self.reloadDrivers(self.driversVM.drivers)
        }
    }
    func updateAreas(_ cities:[KBArea]){
        if(cities.count == 0){
            return
        }
        self.parentAreas.removeAll()
        for currentPolygon in self.cityPolygons {
            currentPolygon.map = nil
        }
        for currentCity in cities {
            if(currentCity.pid != 0){
                continue
            }
            self.parentAreas.append(currentCity)
            if(currentCity.bounds.count == 0){
                continue
            }
            let path = GMSMutablePath()
            for currentCoordinate in currentCity.bounds{
                path.add(currentCoordinate.coordinate)
            }
            let currentCityPolygon = GMSPolygon()
            currentCityPolygon.path = path
            currentCityPolygon.fillColor = UIColor(red: 251.0/255.0, green: 188.0/255.0, blue: 5.0/255.0, alpha: 0.2)
            currentCityPolygon.map = self.mapView.mapView
            self.cityPolygons.append(currentCityPolygon)
        }
        if(self.citySegmentedControl == nil){
            self.citySegmentedControl = HMSegmentedControl.init()
            self.citySegmentedControl!.selectionIndicatorHeight = 2.0
            self.citySegmentedControl!.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
            self.citySegmentedControl!.selectionStyle = HMSegmentedControlSelectionStyle.box
            self.citySegmentedControl!.selectionIndicatorBoxColor = UIColor.clear
            self.citySegmentedControl!.titleTextAttributes = [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 14)]
            self.citySegmentedControl!.addTarget(self, action: #selector(segmentedControlChangedValue(sender:)), for: UIControlEvents.valueChanged)
        }
        if(self.citySegmentedControlContainer.subviews.contains(self.citySegmentedControl!) == false){
            self.citySegmentedControlContainer.addSubview(self.citySegmentedControl!)
            self.citySegmentedControl?.translatesAutoresizingMaskIntoConstraints = false
            self.citySegmentedControl?.superview?.addConstraint(NSLayoutConstraint.init(item: self.citySegmentedControl!,
                                                                                   attribute: NSLayoutAttribute.left,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.citySegmentedControl?.superview,
                                                                                   attribute: NSLayoutAttribute.left,
                                                                                   multiplier: 1,
                                                                                   constant: 0))
            self.citySegmentedControl?.superview?.addConstraint(NSLayoutConstraint.init(item: self.citySegmentedControl!,
                                                                                   attribute: NSLayoutAttribute.right,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.citySegmentedControl?.superview,
                                                                                   attribute: NSLayoutAttribute.right,
                                                                                   multiplier: 1,
                                                                                   constant: 0))
            self.citySegmentedControl?.superview?.addConstraint(NSLayoutConstraint.init(item: self.citySegmentedControl!,
                                                                                   attribute: NSLayoutAttribute.top,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.citySegmentedControl?.superview,
                                                                                   attribute: NSLayoutAttribute.top,
                                                                                   multiplier: 1,
                                                                                   constant: 4))
            self.citySegmentedControl?.superview?.addConstraint(NSLayoutConstraint.init(item: self.citySegmentedControl!,
                                                                                   attribute: NSLayoutAttribute.bottom,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.citySegmentedControl?.superview,
                                                                                   attribute: NSLayoutAttribute.bottom,
                                                                                   multiplier: 1,
                                                                                   constant: -0))
        }
        var titles = self.parentAreas.map{
            $0.name
        }
        titles.insert("所有城市", at: 0)
        self.citySegmentedControl?.sectionTitles = titles
        var bounds = [CLLocationCoordinate2D]()
        for currentArea in self.parentAreas{
            if(currentArea.bounds.count == 0){
                continue
            }
            let currentBounds = currentArea.bounds.map{
                $0.coordinate
            }
            if(self.selectedAreaID == 0 || self.selectedAreaID == currentArea.id){
                bounds += currentBounds
            }
        }
        self.mapView.animateToCamera(locations: bounds, padding: 8)
    }
    func reloadDrivers(_ drivers:[KBDriver]) {
        for currentMarker in self.driverMarkers{
            currentMarker.map = nil
        }
        self.driverMarkers.removeAll()
        var idleCount = 0
        var totalCount = 0
        for currentDriver in drivers{
            if(self.selectedAreaID == 0 || self.selectedAreaID == currentDriver.parentAreaID){
                totalCount += 1
                if(currentDriver.is_busy == false){
                    idleCount += 1
                }
            }
        }
        self.title = "司机位置(" + String(idleCount) + "/" + String(totalCount) + ")"
        for currentDriver in drivers{
            if let coordinate = currentDriver.lastLocation(){
                let marker = GMSMarker(position: coordinate.coordinate)
                self.driverMarkers.append(marker)
                marker.userData = currentDriver
                if let account = KBAccountManager.shared.getAccount(){
                    if(account.username == currentDriver.username){
                        marker.icon = UIImage.init(named: "ic-location-7")
                        marker.map = self.mapView.mapView
                        continue
                    }
                }
                if(currentDriver.is_busy == true){
                    marker.icon = UIImage.init(named: "ic-location-5")
                }else if(currentDriver.is_online == true){
                    marker.icon = UIImage.init(named: "ic-location-4")
                }else{
                    marker.icon = UIImage.init(named: "ic-location-6")
                }
                marker.map = self.mapView.mapView
            }
        }
        
    }

    
    
    
    // MARK: - GDMapViewDelegate
    func mapView(_ mapView: GDMapView, didTap marker: GMSMarker) -> Bool {
        
        if let currentDriver = marker.userData as? KBDriver
        {
            self.selectedDriver = currentDriver
            
            self.usernameLabel.text = ""
            self.lbl_DriverDisplayName.text = ""
            self.lbl_CarModel.text = ""
            self.lbl_Phone.text = ""
            self.lbl_Capacity.text = ""
            
            self.usernameLabel.text = currentDriver.username
            
            self.lbl_DriverDisplayName.text = currentDriver.nick_name
            
            self.lbl_CarModel.text = currentDriver.car_desc
            
            self.lbl_Phone.text = currentDriver.phone.toPhoneFormat()
            
            self.lbl_Capacity.text = String(currentDriver.car_capacity)
            
            self.view.layoutIfNeeded()
            
            if(self.constraint_Bottom_DriverInfoContainer.constant <= self.view_DriverInfoContainer.frame.size.height)
            {
                self.constraint_Bottom_DriverInfoContainer.constant = self.view_DriverInfoContainer.frame.size.height
                
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
            
            
        }
        
        
        
        return true
        
    }
    
    
    func mapView(_ mapView: GDMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        if(self.constraint_Bottom_DriverInfoContainer.constant > 0)
        {
            self.constraint_Bottom_DriverInfoContainer.constant = 0
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    func mapView(_ mapView: GDMapView, didChange position: GMSCameraPosition) {
        
        if(self.constraint_Bottom_DriverInfoContainer.constant > 0)
        {
            self.constraint_Bottom_DriverInfoContainer.constant = 0
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    func mapView(_ mapView: GDMapView, idleAt position: GMSCameraPosition) {
        
    }
    

    func mapView(_ mapView: GDMapView, willMove gesture: Bool) {
        
    }
}
