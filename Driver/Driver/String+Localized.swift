//
//  String+Localized.swift
//  Driver
//
//  Created by Jack Du on 2018-04-20.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func localized(withComment:String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: withComment)
    }
}
