//
//  GDResponse.swift
//  Driver
//
//  Created by ZY on 2017-04-21.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class GDResponse {
    
    
    let Key_Code = "code"
    let Key_Message = "message"
    let Key_Data = "data"
    let Key_Meta = "meta"
    
    let Value_Code_OK = "OK"
    let Value_Code_Error_InvalidLogin = "Error_InvalidLogin"
    
    var code:String?
    var message:String?
    var data:Any?
    var meta:Any?
    
    
    
    init(json:[String:Any]) {
        
        if let code = json[Key_Code] as? String
        {
            self.code = code
        }
        
        if let message = json[Key_Message] as? String
        {
            self.message = message
        }
        
        self.data = json[Key_Data]
        
        self.meta = json[Key_Meta]
    }
    
    
    
    func isOK() -> Bool {
        
        if(self.code == nil)
        {
            return false
        }
        
        return self.code?.lowercased() == Value_Code_OK.lowercased()
        
    }
    
    
    func isInvalidLogin() -> Bool {
        
        if(self.code == nil)
        {
            return false
        }
        
        return self.code?.lowercased() == Value_Code_Error_InvalidLogin.lowercased()
        
    }
    /*
    func getExchangeRage() -> Float? {
        
        if let metaDict = self.meta as? [String:Any?]
        {
            if let exchangeRage = metaDict["exchange_rate"] as? Float
            {
                return exchangeRage
            }
        }
        
        return nil
        
    }
    
    func getDefaultCurrencySymbol() -> String {
        
        if let metaDict = self.meta as? [String:Any?]
        {
            if let defaultSymbol = metaDict["default_currency_symbol"] as? String
            {
                return defaultSymbol
            }
        }
        
        return "$"
        
    }
    
    func getSecondCurrencySymbol() -> String {
        
        if let metaDict = self.meta as? [String:Any?]
        {
            if let defaultSymbol = metaDict["second_currency_symbol"] as? String
            {
                return defaultSymbol
            }
        }
        
        return "￥"
        
    }
    */
}
