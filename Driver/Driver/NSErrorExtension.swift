//
//  GDError.swift
//  Driver
//
//  Created by ZY on 2017-04-21.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

extension NSError {
    static let Domain_GoKabu_Driver = "com.gokabu.driver";
    static let Value_Message_Unknownerroroccurred = NSLocalizedString("An Unknown Error has occurred", comment: "")
    public static func Error_UnknownError() -> NSError{
        let error = NSError(domain: NSError.Domain_GoKabu_Driver,
                            code: 0,
                            userInfo: [NSLocalizedDescriptionKey:NSError.Value_Message_Unknownerroroccurred])
        return error
    }
    public static func Error_InvalidLogin(message aMessage:String) -> NSError{
        let error = NSError(domain: NSError.Domain_GoKabu_Driver,
                            code: HTTPStatusCode.unauthorized.rawValue,
                            userInfo: [NSLocalizedDescriptionKey:aMessage])
        return error
    }
    public static func Error(message aMessage:String) -> NSError{
        let error = NSError(domain: NSError.Domain_GoKabu_Driver,
                            code: 0,
                            userInfo: [NSLocalizedDescriptionKey:aMessage])
        return error
    }
    public static func Error(HTTPCode aHTTPCode:Int) -> NSError
    {
        if(aHTTPCode >= 400
        && aHTTPCode <= 499)
        {
            let error = NSError(domain: NSError.Domain_GoKabu_Driver,
                                code: 0,
                                userInfo: [NSLocalizedDescriptionKey:NSLocalizedString("Experiencing temporary service problem, please try again later" , comment: "")])
            
            return error
        }
        
        
        let error = NSError(domain: NSError.Domain_GoKabu_Driver,
                            code: 0,
                            userInfo: [NSLocalizedDescriptionKey:NSError.Value_Message_Unknownerroroccurred])
        
        return error
    }
    
    
    
    var message:String {
        
        return self.localizedDescription
        
    }
    
    
    
    
}
