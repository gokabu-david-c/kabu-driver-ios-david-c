//
//  GDButtonTableViewCell.swift
//  Driver
//
//  Created by ZY on 2017-04-05.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDButtonTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var button: UIButton!
    
    weak var delegate: AnyObject?
    
    
    var buttonBackgroundColor:UIColor?
    
    
    class func cell(_ title:String?,_ delegate:AnyObject?, titleColor aTitleColor:UIColor? = .white, buttonBackgroundColor aButtonBackgroundColor:UIColor? = .black) -> GDButtonTableViewCell{
    
        let cell = Bundle.main.loadNibNamed("GDButtonTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! GDButtonTableViewCell
        
        if(title != nil)
        {
            cell.button.setTitle(title, for: UIControlState.normal)
        }
        
        
        cell.delegate = delegate
        
        cell.button.setTitleColor(aTitleColor, for: UIControlState.normal)
        cell.button.backgroundColor = aButtonBackgroundColor
        
        
        return cell
    }
    
    class var cellIdentifier:String{
    
        return "GDButtonTableViewCell"
        
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buttonTapped(_ sender: Any) {
        
        if(self.delegate != nil
            && (self.delegate?.responds(to: Selector(("GDButtonTableViewCellTapped:"))))!)
        {
            (self.delegate as! GDButtonTableViewCellDelegate).GDButtonTableViewCellTapped(self)
        }
    }
}


protocol GDButtonTableViewCellDelegate {
    
    func GDButtonTableViewCellTapped(_ sender:GDButtonTableViewCell)
    
    
}
