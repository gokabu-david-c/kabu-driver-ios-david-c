//
//  KBDebugMyOpenOrdersViewController.swift
//  Driver
//
//  Created by George on 2018-01-17.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class KBDebugMyOpenOrdersViewController: KBViewController {
    let openOrdersVM = KBOpenOrdersVM()
    @IBOutlet weak var tableView: UITableView!
    class var controller : KBDebugMyOpenOrdersViewController {
        let controller = UIStoryboard(name: "Debug", bundle: nil).instantiateViewController(withIdentifier: "KBDebugMyOpenOrdersViewController") as! KBDebugMyOpenOrdersViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "当前订单"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension KBDebugMyOpenOrdersViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.openOrdersVM.openOrders.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        }
        cell?.textLabel?.text = self.openOrdersVM.openOrders[indexPath.row].orderNumber ?? ""
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = self.openOrdersVM.openOrders[indexPath.row].orderNumber ?? ""
        showSuccess("复制成功")
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(self.openOrdersVM.openOrders.count == 0){
            return "没有任何订单"
        }
        return String(self.openOrdersVM.openOrders.count) + " 个订单"
    }
}
