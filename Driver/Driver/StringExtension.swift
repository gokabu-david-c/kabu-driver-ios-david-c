//
//  StringExtension.swift
//  Driver
//
//  Created by ZY on 2017-04-08.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

extension String{
    func toCLLocation() -> CLLocation?{
        if(self.count == 0){
            return nil
        }
        if(self.contains(",") == false){
            return nil
        }
        let array = self.replacingOccurrences(of: " ", with: "").components(separatedBy: ",")
        if array.count <= 1{
            return nil
        }
        return CLLocation(latitude: Double(array[0])!,
                          longitude: Double(array[1])!)
    }
    
    /// Validates an email.
    ///
    ///
    /// - returns: true valid email else false.
    func isValidEmail() -> Bool{
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self)
        return result
    }
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    func toPhoneFormat() -> String {
        var result = self
        if(self.count == 10){
            result.insert("-", at: result.index(result.startIndex, offsetBy: 3))
            result.insert("-", at: result.index(result.startIndex, offsetBy: 7))
            
        }
        else if(self.count == 11){
            result.insert("-", at: result.index(result.startIndex, offsetBy: 1))
            result.insert("-", at: result.index(result.startIndex, offsetBy: 5))
            result.insert("-", at: result.index(result.startIndex, offsetBy: 9))
        }
        else if(self.count == 13){
            result.insert("-", at: result.index(result.startIndex, offsetBy: 2))
            result.insert("-", at: result.index(result.startIndex, offsetBy: 6))
            result.insert("-", at: result.index(result.startIndex, offsetBy: 11))
        }
        return result
    }
    func toDate(format aFormat:String) -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = aFormat
        let date = dateFormatter.date(from:self)
        return date
    }
}
