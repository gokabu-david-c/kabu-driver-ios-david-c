//
//  GDLoadingView.swift
//  Driver
//
//  Created by ZY on 2017-05-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDLoadingView: UIView {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    class func view() -> GDLoadingView{
    
        let view = Bundle.main.loadNibNamed("GDLoadingView",
                                            owner: self,
                                            options: nil)?.first as! GDLoadingView
        
        return view
        
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func startAnimating()
    {
        if(self.activityIndicator.isHidden)
        {
            self.activityIndicator.isHidden = false
        }
        
        
        if(self.activityIndicator.isAnimating == false)
        {
            self.activityIndicator.startAnimating()
        }
    }
    
    
    func stopAnimating() {
        
        if(self.activityIndicator.isHidden == false)
        {
            self.activityIndicator.isHidden = true
        }
        
    }

}
