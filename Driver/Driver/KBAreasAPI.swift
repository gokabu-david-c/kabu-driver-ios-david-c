//
//  KBCitiesAPIHelper.swift
//  Driver
//
//  Created by Kabu on 2017-11-14.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class KBAreasAPI: KBAPI {
    static func downloadAllAreas(start aStart:(() -> ())?,
                                 success aSuccess:@escaping(_ areas:[KBArea]) -> (),
                                 failure aFailure:@escaping(_ error:NSError) -> ()){
        aStart?()
        let url = URL_Areas()
        let manager = AFHTTPSessionManager.newInstance()
        manager.get(url,
                    parameters: nil,
                    progress: nil,
                    success: { (task, responseObject) in
                        let r = GDResponse.init(json: responseObject as! [String : Any])
                        if(r.isOK() == false){
                            let message = r.message ?? "发生未知错误"
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        var cities = [KBArea]()
                        if let data = r.data as? [Any?]{
                            for currentData in data{
                                if let json = currentData as? [String:Any]{
                                    let city = KBArea.init(json: json)
                                    cities.append(city)
                                }
                            }
                        }
                        aSuccess(cities)
        }) { (task, error) in
            aFailure(NSError.Error_UnknownError())
        }
    }
}
