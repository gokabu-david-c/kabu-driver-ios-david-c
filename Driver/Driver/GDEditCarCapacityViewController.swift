//
//  GDEditCarCapacityViewController.swift
//  Driver
//
//  Created by ZY on 2017-06-27.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDEditCarCapacityViewController: KBViewController, UITableViewDelegate, UITableViewDataSource {
    let accountVM = KBAccountVM()
    @IBOutlet weak var tableView: UITableView!
    weak var delegate:GDEditCarCapacityViewControllerDelegate?
    class func controller(delegate aDelegate:GDEditCarCapacityViewControllerDelegate?) -> GDEditCarCapacityViewController {
        let controller = UIStoryboard(name: "EditCarCapacity", bundle: nil).instantiateViewController(withIdentifier: "GDEditCarCapacityViewController") as! GDEditCarCapacityViewController
        controller.delegate = aDelegate
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func save(capacity aCapacity:Int) {
        let driverData:[String:String] = [KBDriver.Key_CarCapacity : String(aCapacity)]
        self.accountVM.updateAccount(data: driverData,
                                     start: {
                                        self.showLoading("")
        }, success: { (driver:KBDriver) in
            self.dismissLoading()
            self.delegate?.GDEditCarCapacityViewControllerDidSaveSuccess?(sender: self)
        }) { (error:NSError) in
            self.showError(error.message)
        }
    }
    // MARK: - UITableViewDelegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellIdentifier)
        }
        cell?.textLabel?.text = String(indexPath.row + 1)
        var carCapacity = 0
        if(KBAccountManager.shared.isLoggedIn()){
            let account = KBAccountManager.shared.getAccount()
            if(account != nil){
                if let accountCarCapacity = account?.car_capacity{
                    carCapacity = accountCarCapacity
                }
            }
        }
        if(carCapacity == indexPath.row + 1){
            cell?.accessoryType = UITableViewCellAccessoryType.checkmark
        }else{
            cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("Please enter Passenger Capacity", comment: "")
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.save(capacity: indexPath.row + 1)
    }
}
@objc protocol GDEditCarCapacityViewControllerDelegate : class{
    @objc optional func GDEditCarCapacityViewControllerDidSaveSuccess(sender aSender:GDEditCarCapacityViewController)
}
