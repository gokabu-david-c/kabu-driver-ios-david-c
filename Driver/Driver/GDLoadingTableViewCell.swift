//
//  GDLoadingTableViewCell.swift
//  Driver
//
//  Created by ZY on 2017-05-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDLoadingTableViewCell: UITableViewCell {

    
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    class func cell() -> GDLoadingTableViewCell{
        
        let cell = Bundle.main.loadNibNamed("GDLoadingTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! GDLoadingTableViewCell
        
        
        return cell
    }
    
    class var cellIdentifier:String
    {
        return "GDLoadingTableViewCell"
    }
    
    func startAnimating(){
    
        if(self.activityIndicator.isHidden)
        {
            self.activityIndicator.isHidden = false
        }
        
        
        if(self.activityIndicator.isAnimating == false)
        {
            self.activityIndicator.startAnimating()
        }
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
