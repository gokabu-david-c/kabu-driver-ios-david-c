//
//  KBMyAccountMessageViewController.swift
//  Driver
//
//  Created by Kabu on 2017-11-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBMyAccountMessageViewController: KBViewController {
    @IBOutlet var tableView: UITableView!
    class func controller() -> KBMyAccountMessageViewController {
        let controller = UIStoryboard(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "KBMyAccountMessageViewController") as! KBMyAccountMessageViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadTableView()
    }
    func loadTableView(){
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 56
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension KBMyAccountMessageViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: KBImageTableViewCell.reuseIdentifier) as? KBImageTableViewCell
        if cell == nil{
            cell = KBImageTableViewCell.cell()
        }
        if(indexPath.row == 0){
            cell?.iconImageView.image = UIImage.init(named: "ic-speaker-1")
            cell?.titleLabel.text = NSLocalizedString("Official News", comment: "")
        }else{
            cell?.iconImageView.image = UIImage.init(named: "ic-message-1")
            cell?.titleLabel.text = NSLocalizedString("Contact Customer Service", comment: "")
        }
        cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0000001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0000001
    }
}
extension KBMyAccountMessageViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.row == 0){
            let controller = KBMessageCenterViewController.controller()
            self.navigationController?.pushViewController(controller, animated: true)
            return
        }
        if(indexPath.row == 1){
            let controller = KBCustomerSupportViewController.controller()
            self.navigationController?.pushViewController(controller, animated: true)
            return
        }
    }
}
