//
//  GDView.swift
//  Driver
//
//  Created by ZY on 2017-05-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class GDView: UIView {
    var loadingView:NVActivityIndicatorView? //:GDLoadingView?
    var messageView:GDMessageView?
    func showContent(_ contentView:UIView) {
        if(self.loadingView != nil){
            self.loadingView?.removeFromSuperview()
        }
        if(self.messageView != nil){
            self.messageView?.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.2,
                       animations: {
                        for currentSubview in self.subviews{
                            if(currentSubview === self.loadingView){
                                continue
                            }
                            currentSubview.isHidden = false
                            currentSubview.alpha = 1
                        }
        }) { (complete:Bool) in
            
        }
        if(self.subviews.contains(contentView) == false){
            return
        }
        if(contentView.isHidden){
            contentView.isHidden = false
        }
        self.bringSubview(toFront: contentView)
        if(self.loadingView != nil && self.loadingView?.isHidden == true){
            self.loadingView?.isHidden = true
        }
    }
    func showLoading(_ alpha:Float = 1, hideSubviews aHideSubViews:Bool = true) {
        if(self.messageView != nil){
            self.messageView?.removeFromSuperview()
            self.messageView = nil
        }
        if(self.loadingView == nil){
            let loadingView = NVActivityIndicatorView(frame: CGRect.zero,
                                                      type: NVActivityIndicatorType.ballPulseSync,
                                                      color: UIColor.black,
                                                      padding: 0)// GDLoadingView.view()
            self.loadingView = loadingView
        }
        if(self.subviews.contains(self.loadingView!) == false){
            if let loadingView = self.loadingView{
                loadingView.translatesAutoresizingMaskIntoConstraints = false
                self.addSubview(loadingView)
                loadingView.addConstraint(NSLayoutConstraint(item: loadingView,
                                                             attribute: NSLayoutAttribute.width,
                                                             relatedBy: NSLayoutRelation.equal,
                                                             toItem: nil,
                                                             attribute: NSLayoutAttribute.notAnAttribute,
                                                             multiplier: 1,
                                                             constant: 40))
                loadingView.addConstraint(NSLayoutConstraint(item: loadingView,
                                                             attribute: NSLayoutAttribute.height,
                                                             relatedBy: NSLayoutRelation.equal,
                                                             toItem: nil,
                                                             attribute: NSLayoutAttribute.notAnAttribute,
                                                             multiplier: 1,
                                                             constant: 40))
                loadingView.superview?.addConstraint(NSLayoutConstraint(item: loadingView,
                                                                        attribute: NSLayoutAttribute.centerX,
                                                                        relatedBy: NSLayoutRelation.equal,
                                                                        toItem: loadingView.superview,
                                                                        attribute: NSLayoutAttribute.centerX,
                                                                        multiplier: 1,
                                                                        constant: 0))
                loadingView.superview?.addConstraint(NSLayoutConstraint(item: loadingView,
                                                                        attribute: NSLayoutAttribute.centerY,
                                                                        relatedBy: NSLayoutRelation.equal,
                                                                        toItem: loadingView.superview,
                                                                        attribute: NSLayoutAttribute.centerY,
                                                                        multiplier: 1,
                                                                        constant: 0))
                self.setNeedsLayout()
                self.layoutIfNeeded()
            }
        }
        self.loadingView?.alpha = CGFloat(alpha)
        if let loadingView = self.loadingView{
            self.bringSubview(toFront: loadingView)
            if(loadingView.isHidden){
                loadingView.isHidden = false
            }
            for currentSubview in self.subviews{
                if(currentSubview === self.loadingView){
                    continue
                }
                if(aHideSubViews == true){
                    currentSubview.alpha = 0
                }
                currentSubview.isHidden = aHideSubViews
            }
            loadingView.startAnimating()
        }
    }
    func showMessage(_ message:String)
    {
        if(self.loadingView != nil)
        {
            self.loadingView?.removeFromSuperview()
            self.loadingView = nil
        }
        
        if(self.messageView == nil)
        {
            let messageView = GDMessageView.view(message)
            
            
            self.messageView = messageView
        }
        
        if(self.subviews.contains(self.messageView!) == false)
        {
            if let messageView = self.messageView
            {
                messageView.translatesAutoresizingMaskIntoConstraints = false
                
                self.addSubview(messageView)
                
                messageView.superview?.addConstraint(NSLayoutConstraint(item: messageView,
                                                                        attribute: NSLayoutAttribute.leading,
                                                                        relatedBy: NSLayoutRelation.equal,
                                                                        toItem: messageView.superview,
                                                                        attribute: NSLayoutAttribute.leading,
                                                                        multiplier: 1,
                                                                        constant: 0))
                
                messageView.superview?.addConstraint(NSLayoutConstraint(item: messageView,
                                                                        attribute: NSLayoutAttribute.trailing,
                                                                        relatedBy: NSLayoutRelation.equal,
                                                                        toItem: messageView.superview,
                                                                        attribute: NSLayoutAttribute.trailing,
                                                                        multiplier: 1,
                                                                        constant: 0))
                
                messageView.superview?.addConstraint(NSLayoutConstraint(item: messageView,
                                                                        attribute: NSLayoutAttribute.top,
                                                                        relatedBy: NSLayoutRelation.equal,
                                                                        toItem: messageView.superview,
                                                                        attribute: NSLayoutAttribute.top,
                                                                        multiplier: 1,
                                                                        constant: 0))
                
                messageView.superview?.addConstraint(NSLayoutConstraint(item: messageView,
                                                                        attribute: NSLayoutAttribute.bottom,
                                                                        relatedBy: NSLayoutRelation.equal,
                                                                        toItem: messageView.superview,
                                                                        attribute: NSLayoutAttribute.bottom,
                                                                        multiplier: 1,
                                                                        constant: 0))
            }
        }
        if let messageView = self.messageView
        {
            self.bringSubview(toFront: messageView)
            
            if(messageView.isHidden)
            {
                messageView.isHidden = false
            }
            
            self.messageView?.showMessage(message)
        }
    }

}
