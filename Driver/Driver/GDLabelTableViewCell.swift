//
//  GDLabelTableViewCell.swift
//  Driver
//
//  Created by ZY on 2017-04-18.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDLabelTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var lbl_Content: UILabel!
    
    
    @IBOutlet weak var imageView_Separator: UIImageView!
    
    @IBOutlet weak var contentLabelTrailingConstraint: NSLayoutConstraint!
    
    
    
    class func cell() -> GDLabelTableViewCell{
        
        let cell = Bundle.main.loadNibNamed("GDLabelTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! GDLabelTableViewCell
        
        
        
        
        return cell
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if(self.accessoryType == UITableViewCellAccessoryType.none)
        {
            self.contentLabelTrailingConstraint.constant = 18
        }
        else
        {
            self.contentLabelTrailingConstraint.constant = 0
        }
    }
    
    
    class var cellIdentifier:String
    {
        return "GDLabelTableViewCell"
    }

    
    
    
    func setupCell(_ title:String?,_ content:String?) {
        
        
        if(title != nil)
        {
            self.lbl_Title.text = title;
        }
        
        if(content != nil)
        {
            self.lbl_Content.text = content
        }
        
        
    }
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let image_Separator = UIImage.image(.GDLightGray, size: CGSize(width: 1, height: 1))
        self.imageView_Separator.image = image_Separator
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.lbl_Title.text = ""
        self.lbl_Content.text = ""
    }
    func setSeparatorHidden(_ hidden:Bool){
        self.imageView_Separator.isHidden = hidden
    }

}
