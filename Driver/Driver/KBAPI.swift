//
//  KBAPIHelper.swift
//  Driver
//
//  Created by ZY on 2017-08-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class KBAPI{
    private static let Key_AppAccessToken = "appaccesstoken";
    static func URL_Login(username aUsername:String, password aPassword:String) -> String{
        var url = KBConfig.APIBaseURL
        url += "me/login?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        url += "&username=" + aUsername.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        url += "&password=" + aPassword.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        return url
    }
    static func URL_Logout(uid aUID:Int, userAccessToken aUserAccessToken:String) -> String {
        var url = KBConfig.APIBaseURL //getAPIBaseURL()
        url += "me/logout?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        url += "&uid=" + String(aUID)
        url += "&useraccesstoken=" + aUserAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        return url
    }
    static func URL_DownloadServiceRange() -> String {
        var url = KBConfig.APIBaseURL
        url += "settings/servicerange?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        return url
    }
    
    static func URL_Drivers() -> String{
        var url = KBConfig.APIBaseURL
        url += "drivers?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let account = KBAccountManager.shared.getAccount(){
            url += "&uid=" + String(account.id)
            url += "&useraccesstoken=" + account.userAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        return url
    }
    static func URL_DownloadDriver(driverID aDriverID:Int) -> String{
        var url = KBConfig.APIBaseURL
        url += "drivers/" + String(aDriverID) + "?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let account = KBAccountManager.shared.getAccount(){
            url += "&uid=" + String(account.id)
            url += "&useraccesstoken=" + account.userAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        return url
    }
    static func URL_UnpaidBills(driverID aDriverID:Int) -> String{
        var url = KBConfig.APIBaseURL
        url += "drivers/" + String(aDriverID) + "/bills/unpaid?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let account = KBAccountManager.shared.getAccount(){
            url += "&uid=" + String(account.id)
            url += "&useraccesstoken=" + account.userAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        url += "&timestamp=" + String(Int64(Date().millisecondsSince1970)).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        return url
    }
    static var URL_GetReservationPool:String {
        var url = KBConfig.APIBaseURL
        url += "orders/reservation_pool?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let account = KBAccountManager.shared.getAccount(){
            url += "&uid=" + String(account.id)
            url += "&useraccesstoken=" + account.userAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        url += "&timestamp=" + String(Int64(Date().millisecondsSince1970)).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        return url
    }
    static func URL_Areas() -> String{
        var url = KBConfig.APIBaseURL
        url += "cities?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let account = KBAccountManager.shared.getAccount(){
            url += "&uid=" + String(account.id)
            url += "&useraccesstoken=" + account.userAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        return url
    }
    static func URL_GetOpenOrdersForDriver(driverID aDriverID:Int) -> String {
        var url = KBConfig.APIBaseURL
        url += "drivers/" + String(aDriverID) + "/orders/open?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let account = KBAccountManager.shared.getAccount(){
            url += "&uid=" + String(account.id)
            url += "&useraccesstoken=" + account.userAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        return url
    }
    static func URL_GetClosedOrdersForDriver(startDate aStartDate:String,
                                                endDate aEndDate:String,
                                                pageIndex aPageIndex:Int,
                                                itemsPerPage aItemsPerPage:Int) -> String {
        let startDate = aStartDate.toDate(format: "yyyy-MM-dd")
        let endDate = aEndDate.toDate(format: "yyyy-MM-dd")
        var url = KBConfig.APIBaseURL
        url += "drivers/"
        if let account = KBAccountManager.shared.getAccount(){
            url += String(account.id)
        }
        url += "/orders/closed?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let account = KBAccountManager.shared.getAccount(){
            url += "&uid=" + String(account.id)
            url += "&useraccesstoken=" + account.userAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        url += "&"
        if let startTimeInterval = startDate?.timeIntervalSince1970{
            url += "start_time=" + String.init(Int64(startTimeInterval))
        }
        url += "&"
        if let endTimeInterval = endDate?.timeIntervalSince1970{
            url += "end_time=" + String.init(Int64(endTimeInterval) + 86400 - 1)
        }
        url += "&"
        url += "pageindex=" + String(aPageIndex)
        url += "&"
        url += "itemsperpage=" + String(aItemsPerPage)
        return url
    }
    static func URL_UpdateOrder(orderNumber aOrderNumber:String) -> String {
        var url = KBConfig.APIBaseURL
        url += "orders/" + aOrderNumber + "?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let account = KBAccountManager.shared.getAccount(){
            url += "&uid=" + String(account.id)
            url += "&useraccesstoken=" + account.userAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        print(url)

        return url
    }
    static func URL_ResetPassword(username aUsername:String, phone aPhone:String) -> String{
        var url = KBConfig.APIBaseURL
        url += "me/reset_password?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        url += "&username=" + aUsername.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        url += "&phone=" + aPhone.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        return url
    }
    static func URL_UpdateDriver(driverID aDriverID:Int) -> String {
        var url = KBConfig.APIBaseURL
        url += "drivers/" + String(aDriverID) + "?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let account = KBAccountManager.shared.getAccount(){
            url += "&uid=" + String(account.id)
            url += "&useraccesstoken=" + account.userAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        return url
    }
    static func URL_RequestAlipayOrderInfo(driverID aDriverID:Int) -> String {
        var url = KBConfig.APIBaseURL
        url += "pay/alipay/payment_data" + "?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let account = KBAccountManager.shared.getAccount(){
            url += "&uid=" + String(account.id)
            url += "&useraccesstoken=" + account.userAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        return url
    }
    
    static func URL_RequestWechatpayOrderInfo(driverID aDriverID:Int) -> String {
        var url = KBConfig.APIBaseURL
        url += "pay/ott/wechatpay_init" + "?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        if let account = KBAccountManager.shared.getAccount(){
            url += "&uid=" + String(account.id)
            url += "&useraccesstoken=" + account.userAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        return url
    }
}
