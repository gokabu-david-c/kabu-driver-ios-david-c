//
//  KBPaymentVC.swift
//  Driver
//
//  Created by Jack Du on 2018-04-11.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

import Foundation



class KBPaymentVC: KBViewController {
    
    @IBOutlet var creditcardCheckerView: UIImageView!
    @IBOutlet var wechatCheckerView: UIImageView!
    @IBOutlet var alipayCheckerView: UIImageView!
    @IBOutlet var alipayButton: UIButton!
    @IBOutlet var payButton: UIButton!
    @IBOutlet var avatarImageView: GDAvatarImageView!
    @IBOutlet var paymentBackgroundView: UIView!
    @IBOutlet var creditcardButton: UIButton!
    @IBOutlet var wechatButton: UIButton!
    @IBOutlet var owedLabel: UILabel!
    @IBOutlet var closeButton: UIBarButtonItem!
    @IBOutlet weak var nobillView: UIView!
    
    @IBOutlet weak var paymentStackView: UIStackView!
    
    var paymentMethod : PaymentMethod = .alipay
    var payBillController:GDPureWebViewNavigationController?
    var shouldDismiss = false
    let systemVM = KBSystemSettingsVM()
    let accountVM = KBAccountVM()
    
    class func controller() -> KBPaymentVC{
        let controller = UIStoryboard(name: "Payment", bundle: nil).instantiateViewController(withIdentifier: "KBPaymentVC") as! KBPaymentVC
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkBillsStatus()
        paymentMethod = systemVM.defaultPaymentMethod
        setupPaymentMethod()
        self.setupViews()

        if let account = self.accountVM.account{
            self.avatarImageView.imageURL = account.photoURL
        }
        self.setupButtons()
        self.loadAvatarImageView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.checkBillsStatus()
    }
    
    override func performApplicationDidBecomeActive() {
        super.performApplicationDidBecomeActive()
        self.checkBillsStatus()
    }
    

    func loadAvatarImageView(){
        if let account = self.accountVM.account{
            self.avatarImageView.imageURL = account.photoURL
        }
    }    
    
}

// UI Setup
extension KBPaymentVC{
    
    func setupViews(){
        paymentBackgroundView.layer.cornerRadius = 7
        paymentBackgroundView.layer.shadowRadius = 7
        paymentBackgroundView.layer.shadowOpacity = 0.2
        paymentBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 3)
        
        payButton.layer.cornerRadius = 7
        view.layer.masksToBounds = true;
    }
    
    func setupButtons(){
        alipayButton.tag = PaymentMethodTag.alipay.rawValue
        wechatButton.tag = PaymentMethodTag.wechat.rawValue
        creditcardButton.tag = PaymentMethodTag.creditcard.rawValue
        
        alipayButton.addTarget(self, action: .onClickPaymentTypeButton, for: .touchDown)
        creditcardButton.addTarget(self, action: .onClickPaymentTypeButton, for: .touchDown)
        wechatButton.addTarget(self, action: .onClickPaymentTypeButton, for: .touchDown)

        payButton.addTarget(self, action: .onClickPayButton, for: .touchUpInside)

        closeButton.target = self
        closeButton.action = .onClickCloseButton_paid
    }
    
    func enableCloseButton(isEnabled:Bool){
        isEnabled ? (closeButton.action = .onClickCloseButton_paid) : (closeButton.action = .onClickCloseButton_unpaid)
        shouldDismiss = isEnabled
    }
    
    func setOwedAmount(amount:Float){
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        self.owedLabel.text = String("$\(formatter.string(from: NSNumber.init(value: amount)) ?? "0")")
    }
    
    func setPaymentMethodChecker(method:PaymentMethod){
        switch method {
        case .alipay:
            alipayCheckerView.isHidden = false
            wechatCheckerView.isHidden = true
            creditcardCheckerView.isHidden = true
        case .creditcard:
            alipayCheckerView.isHidden = true
            wechatCheckerView.isHidden = true
            creditcardCheckerView.isHidden = false
        case .wechat:
            alipayCheckerView.isHidden = true
            wechatCheckerView.isHidden = false
            creditcardCheckerView.isHidden = true
        }
    }
    
    func setPayButton(){
        switch paymentMethod{
            case .alipay:
                payButton.backgroundColor = UIColor.payment.alipay
            case .wechat:
                payButton.backgroundColor = UIColor.payment.wechat
            case .creditcard:
                payButton.backgroundColor = UIColor.payment.creditcard
        }
    }
    
    func toggleNoBillOn(_ toggle:Bool){
        nobillView.isHidden = !toggle
        payButton.isHidden = toggle
        paymentStackView.isHidden = toggle
    }

}


//Bills Functions
extension KBPaymentVC {
    func checkBillsStatus(){
//        self.showLoading(NSLocalizedString("Loading", comment: ""))
        KBBillManager.shared.getBillinStatus(start: {
        }, success: {(total:Float, billingStatus:BillingStatus) in
            switch billingStatus {
            case .owedNothing:
                self.toggleNoBillOn(true)
                self.enableCloseButton(isEnabled: true)
            case .owedMoney:
                self.toggleNoBillOn(false)
                self.enableCloseButton(isEnabled: true)
            case .owedOverdue:
                self.toggleNoBillOn(false)
                self.enableCloseButton(isEnabled: false)
                self.showError(NSLocalizedString("Please pay for the bills first" , comment: ""))
            default:
                ()
            }
            self.setOwedAmount(amount:total)
//            self.dismissLoading()
        }, failure:{(error:NSError) in
            //            self.dismissLoading()
            self.showError(NSLocalizedString("An error had occurred, please try again", comment: ""))
        })
        
    }
    
    func setupPaymentMethod(){
        setPaymentMethodChecker(method: paymentMethod)
        setPayButton()
    }
    
    func changeDefaultPaymentMethod(paymentMethod:PaymentMethod){
        systemVM.defaultPaymentMethod = paymentMethod
        self.paymentMethod = paymentMethod
        setupPaymentMethod()
    }
    
}


//Gesture Functions
extension KBPaymentVC {
    @objc func onClickCloseButton_paidBill(){
        self.dismiss(animated: true)
    }
    
    @objc func onClickCloseButton_UnpaidBill(){
        if(self.shouldDismiss){
            self.dismiss(animated: true)
        }
        self.checkBillsStatus()
    }
    
    @objc func onClickPaymentTypeButton(sender:UIButton){
        let paymentType = PaymentMethodTag(rawValue:sender.tag)!
        
        switch paymentType {
        case .alipay:
            changeDefaultPaymentMethod(paymentMethod: .alipay)
        case .wechat:
            changeDefaultPaymentMethod(paymentMethod: .wechat)
        case .creditcard:
            changeDefaultPaymentMethod(paymentMethod: .creditcard)
        }
    }
    
    @objc func onClickPaybutton(){
        switch paymentMethod {
            case .alipay:
                performSegueAlipay()
            case .wechat:
                performSegueWeChatWebpage()
            case .creditcard:
                performSegueCreditCard()
        }
    }
    
    @objc func performSegueCreditCard() {
        let controller = GDPureWebViewController.controller(urlString: KBConfig.ULRString_PayCreditCard, title: NSLocalizedString("Credit Card Payment", comment: ""))
        self.navigationController?.pushViewController(controller, animated: true)
        return
    }
    
    @objc func performSegueWeChatWebpage() {
        let controller = GDPureWebViewController.controller(urlString: KBConfig.ULRString_PayWeChat, title: NSLocalizedString("WeChat Payment", comment: ""))
        self.navigationController?.pushViewController(controller, animated: true)
        return
    }
    
    @objc func performSegueAlipay() {
        self.performSegue(withIdentifier: "AlipaySegue", sender: self)
        return
    }
    
    @objc func performSegueWechat() {
        self.performSegue(withIdentifier: "WechatSegue", sender: self)
        return
    }
}


fileprivate extension Selector {
    static let performSegueCreditCard = #selector(KBPaymentVC.performSegueCreditCard)
    static let performSegueWeChat = #selector(KBPaymentVC.performSegueWeChatWebpage)
    static let onClickCloseButton_paid = #selector(KBPaymentVC.onClickCloseButton_paidBill)
    static let onClickCloseButton_unpaid = #selector(KBPaymentVC.onClickCloseButton_UnpaidBill)
    static let onClickPayButton = #selector(KBPaymentVC.onClickPaybutton)
    static let onClickPaymentTypeButton = #selector(KBPaymentVC.onClickPaymentTypeButton(sender:))


}
