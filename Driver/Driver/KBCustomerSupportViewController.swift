//
//  KBCustomerSupportViewController.swift
//  Driver
//
//  Created by Kabu on 2017-11-24.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBCustomerSupportViewController: KBViewController {
    class func controller() -> KBCustomerSupportViewController{
        let controller = UIStoryboard.init(name: "CustomerSupport", bundle: nil)
            .instantiateViewController(withIdentifier: "KBCustomerSupportViewController") as! KBCustomerSupportViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = UINavigationItem.LargeTitleDisplayMode.never
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
