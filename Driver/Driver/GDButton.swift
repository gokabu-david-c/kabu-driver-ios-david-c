//
//  GDButton.swift
//  Driver
//
//  Created by ZY on 2017-04-03.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDButton: UIButton {
    

    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        self.layer.cornerRadius = 4
        
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.black.cgColor
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
