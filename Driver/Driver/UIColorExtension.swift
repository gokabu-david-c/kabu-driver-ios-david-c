//
//  UIColorExtension.swift
//  Driver
//
//  Created by ZY on 2017-04-05.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation


extension UIColor{
    open class var GDLightGray:UIColor{
        return UIColor(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1)
    }
    open class var Success:UIColor{
        return UIColor(red: 60.0/255.0, green: 118.0/255.0, blue: 61.0/255.0, alpha: 1)
    }
    open class var Info:UIColor{
        return UIColor(red: 49.0/255.0, green: 112.0/255.0, blue: 143.0/255.0, alpha: 1)
    }
    open class var Warning:UIColor{
        return UIColor(red: 138.0/255.0, green: 109.0/255.0, blue: 59.0/255.0, alpha: 1)
    }
    open class var Danger:UIColor{
        return UIColor(red: 169.0/255.0, green: 68.0/255.0, blue: 66.0/255.0, alpha: 1)
    }
    open class var InstantOrder:UIColor{
        return UIColor(red: 82.0/255.0, green: 164.0/255.0, blue: 58.0/255.0, alpha: 1)
    }
    open class var ReservationOrder:UIColor{
        return UIColor(red: 28.0/255.0, green: 132.0/255.0, blue: 198.0/255.0, alpha: 1)
    }
}
