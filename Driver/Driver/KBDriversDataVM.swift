//
//  KBDriversDataVM.swift
//  Driver
//
//  Created by Kabu on 2017-11-15.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBDriversDataVM: NSObject {
    var driversUpdated:(() -> ())?
    var driversUpdateFailed:((_ error:String) -> ())?
    var drivers = [KBDriver]()
    var range = 0.0 //KM radius
    var location:CLLocationCoordinate2D?
    func downloadDrivers(){
        if(KBAccountManager.shared.isLoggedIn() == false){
            self.driversUpdateFailed?(NSLocalizedString("Please Login first", comment: ""))
        }
        KBDriversAPI.downloadDrivers(start: {
            
        }, success: { (drivers:[KBDriver], range:Double, currentLocation:CLLocationCoordinate2D?) in
            self.range = range
            self.location = currentLocation
            self.drivers = drivers
            self.driversUpdated?()
        }, failure: { (error:NSError) in
            self.driversUpdateFailed?(NSLocalizedString("Internet connection problem, please try again", comment: ""))
        })
    }
}
