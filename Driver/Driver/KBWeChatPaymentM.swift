//
//  KBWeChatPaymentModel.swift
//  Driver
//
//  Created by Jack Du on 2018-04-19.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

import Foundation


class KBWeChatPaymentM: NSObject {
    var id:Int = 0
    var prepaidId:Float = 0
    var signiture:String = ""
    var amountCad:Float = 0
    var amountYuan:Float = 0

    init(json:[String:Any]) {
        if let id = json["id"] as? Int{
            self.id = id
        }
        if let prepaidId = json["prepaidId"] as? Float{
            self.prepaidId = prepaidId
        }
        if let signiture = json["signiture"] as? String{
            self.signiture = signiture
        }
        if let amountCad = json["amountCad"] as? Float{
            self.amountCad = amountCad
        }
    }
}
