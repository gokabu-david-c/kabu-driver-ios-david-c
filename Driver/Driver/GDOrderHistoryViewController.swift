//
//  GDOrderHistoryViewController.swift
//  Driver
//
//  Created by ZY on 2017-05-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDOrderHistoryViewController: KBViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    let startDatePickerView:UIDatePicker = UIDatePicker()
    let endDatePickerView:UIDatePicker = UIDatePicker()
    let startDateToolbar:UIToolbar = UIToolbar()
    let endDateToolbar:UIToolbar = UIToolbar()
    @IBOutlet weak var ordersCountLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var commissionLabel: UILabel!
    @IBOutlet weak var resultContainerView: GDView!
    @IBOutlet weak var resultContentView: UIView!
    @IBOutlet weak var resultTable: UITableView!
    let refreshControl = UIRefreshControl()
    let orderHistoryVM = KBOrderHistoryVM()
    class func controller() -> GDOrderHistoryViewController {
        let controller = UIStoryboard(name: "OrderHistory", bundle: nil).instantiateViewController(withIdentifier: "GDOrderHistoryViewController") as! GDOrderHistoryViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        preferLargeNavigationTitle(isPrefered: true)
        self.navigationItem.title = NSLocalizedString("Order History", comment: "")
        self.startDatePickerView.datePickerMode = UIDatePickerMode.date
        self.endDatePickerView.datePickerMode = UIDatePickerMode.date
        let startDateLabel = UILabel()
        startDateLabel.textColor = UIColor.darkGray
        startDateLabel.text = NSLocalizedString("Start Date", comment: "")
        startDateLabel.sizeToFit()
        let startDateLabelItem = UIBarButtonItem(customView: startDateLabel)
        let startDateCancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel,
                                                    target: self,
                                                    action: #selector(startDateCancelButtonTapped))
        let startDateToolbarFlex = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace,
                                                   target: self,
                                                   action: nil)
        let startDateDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done,
                                                  target: self,
                                                  action: #selector(startDateDoneButtonTapped))
        self.startDateToolbar.setItems([startDateCancelButton, startDateToolbarFlex, startDateLabelItem, startDateToolbarFlex, startDateDoneButton], animated: true)
        self.startDateToolbar.tintColor = UIColor.white
        self.startDateToolbar.barTintColor = UIColor.black
        self.startDateToolbar.isTranslucent = true
        self.startDateToolbar.sizeToFit()
        let endDateLabel = UILabel()
        endDateLabel.textColor = UIColor.darkGray
        endDateLabel.text = NSLocalizedString("End Date", comment: "")
        endDateLabel.sizeToFit()
        let endDateLabelItem = UIBarButtonItem(customView: endDateLabel)
        let endDateCancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel,
                                                    target: self,
                                                    action: #selector(endDateCancelButtonTapped))
        let endDateToolbarFlex = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace,
                                                   target: self,
                                                   action: nil)
        let endDateDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done,
                                                target: self,
                                                action: #selector(endDateDoneButtonTapped))
        self.endDateToolbar.setItems([endDateCancelButton, endDateToolbarFlex, endDateLabelItem, endDateToolbarFlex, endDateDoneButton], animated: true)
        self.endDateToolbar.tintColor = UIColor.white
        self.endDateToolbar.barTintColor = UIColor.black
        self.endDateToolbar.isTranslucent = true
        self.endDateToolbar.sizeToFit()
        self.startDateTextField.inputView = self.startDatePickerView
        self.endDateTextField.inputView = self.endDatePickerView
        self.startDateTextField.inputAccessoryView = self.startDateToolbar
        self.endDateTextField.inputAccessoryView = self.endDateToolbar
        self.startDatePickerView.addTarget(self,
                                           action: #selector(datePickerValueChanged(_:)),
                                           for: UIControlEvents.valueChanged)
        self.endDatePickerView.addTarget(self,
                                         action: #selector(datePickerValueChanged(_:)),
                                         for: UIControlEvents.valueChanged)
        let startOfMonth = Date().startOfWeek(1)
        self.startDateTextField.text = startOfMonth.toString("yyyy-MM-dd")
        self.endDateTextField.text = Date().toString("yyyy-MM-dd")
        self.resultTable.rowHeight = UITableViewAutomaticDimension
        self.resultTable.estimatedRowHeight = 44
        self.refreshControl.addTarget(self,
                                      action: #selector(reloadOrders),
                                      for: UIControlEvents.valueChanged)
        self.resultTable.addSubview(self.refreshControl)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(self.isViewWillAppeared == false){
            self.downloadOrders(true, showLoadingView: true)
        }
        self.isViewWillAppeared = true
    }
    @objc func startDateCancelButtonTapped() {
        self.startDateTextField.resignFirstResponder()
    }
    @objc func startDateDoneButtonTapped() {
        self.endDateTextField.becomeFirstResponder()
    }
    @objc func endDateCancelButtonTapped() {
        self.endDateTextField.resignFirstResponder()
    }
    @objc func endDateDoneButtonTapped() {
        self.endDateTextField.resignFirstResponder()
        self.downloadOrders(true, showLoadingView: true)
    }
    @objc func reloadOrders() {
        self.downloadOrders(false, showLoadingView: false)
//        refreshControl.endRefreshing()
    }
    func downloadOrders(_ shouldReset:Bool, showLoadingView aShowLoadingView:Bool = false){
        if(self.orderHistoryVM.isDownloadingOrders == true){
            self.refreshControl.endRefreshing()
            
            return
        }
        let startDate = self.startDateTextField.text
        if startDate == nil || startDate?.count == 0{
                self.showError(NSLocalizedString("Please choose Start Date", comment: ""))
                self.refreshControl.endRefreshing()
            return
        }
        let endDate = self.endDateTextField.text
        if endDate == nil || endDate?.count == 0{
            self.showError(NSLocalizedString("Please choose End Date", comment: ""))
            self.refreshControl.endRefreshing()
            return
        }
        self.orderHistoryVM.downloadOrders(startDate!,
                                           endDate!,
                                           shouldReset,
                                           {
                                                //self.resultTable.reloadData()
                                                if(aShowLoadingView == true){
                                                    self.resultContainerView.showLoading(1,
                                                                                         hideSubviews: true)
                                                    self.resultTable.alpha = 0
                                                }
        }, {
            if(shouldReset == true){
                self.resultTable.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
            }
            self.ordersCountLabel.text = String(self.orderHistoryVM.orderCount) + " " + NSLocalizedString("Orders", comment: "")
            self.totalLabel.text = "$" + String(Int(self.orderHistoryVM.total))
            self.commissionLabel.text = String(format:"$%.2f", self.orderHistoryVM.commission)
            if(self.orderHistoryVM.orders.count > 0){
                self.resultContainerView.showContent(self.resultContentView)
                self.resultTable.alpha = 1
                self.resultTable.reloadData()
            }
            else{
                self.resultContainerView.showMessage(NSLocalizedString("No Orders", comment: ""))
            }

        }) { (error:NSError) in
            if(shouldReset == true){
                self.resultTable.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: false)
            }
            self.refreshControl.endRefreshing()
            if(self.orderHistoryVM.orders.count == 0){
                self.resultContainerView.showMessage(error.message)
            }
            self.resultTable.reloadData()
        }
        self.refreshControl.endRefreshing()

    }
    @objc func datePickerValueChanged(_ sender:UIDatePicker){
        if(sender === self.startDatePickerView){
            self.startDateTextField.text = self.startDatePickerView.date.toString("yyyy-MM-dd")
        }
        else if(sender === self.endDatePickerView){
            self.endDateTextField.text = self.endDatePickerView.date.toString("yyyy-MM-dd")
        }
    }
    // MARK: - UITableViewDelegate & UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        var sections = self.orderHistoryVM.dateList.count //self.orders.count
        if(self.orderHistoryVM.dateList.count > 0){
            sections += 1
        }
        return sections
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == self.orderHistoryVM.dateList.count){
            return 1
        }
        let date = self.orderHistoryVM.dateList[section]
        if let ordersForDate = self.orderHistoryVM.sortedOrders[date]{
            return ordersForDate.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == self.orderHistoryVM.dateList.count){
            if(self.orderHistoryVM.orders.count == self.orderHistoryVM.orderCount){
                var cell = tableView.dequeueReusableCell(withIdentifier: GDCenterTextTableViewCell.cellIdentifier) as? GDCenterTextTableViewCell
                if(cell == nil){
                    cell = GDCenterTextTableViewCell.cell()
                }
                cell?.backgroundColor = UIColor.clear
                cell?.label.text = NSLocalizedString("No more orders", comment: "")
                cell?.label.textColor = UIColor.lightGray
                NSLog("%@", "no more orders cell")
                return cell!
            }
            else if(self.orderHistoryVM.isErrorOccured == true){
                var cell = tableView.dequeueReusableCell(withIdentifier: GDCenterTextTableViewCell.cellIdentifier) as? GDCenterTextTableViewCell
                if(cell == nil){
                    cell = GDCenterTextTableViewCell.cell()
                }
                cell?.backgroundColor = UIColor.white
                cell?.label.text = NSLocalizedString("Download had failed, please try again", comment: "")
                cell?.label.textColor = UIColor.black
                NSLog("%@", "failed cell")
                return cell!
            }
            else{
                var cell = tableView.dequeueReusableCell(withIdentifier: GDLoadingTableViewCell.cellIdentifier) as? GDLoadingTableViewCell
                if(cell == nil){
                    cell = GDLoadingTableViewCell.cell()
                }
                cell?.backgroundColor = UIColor.clear
                cell?.startAnimating()
                NSLog("%@", "loading cell")
                return cell!
            }
        }
        var cell = tableView.dequeueReusableCell(withIdentifier: GDOrderSummaryTableViewCell.cellIdentifier) as? GDOrderSummaryTableViewCell
        if(cell == nil){
            cell = GDOrderSummaryTableViewCell.cell()
        }
        cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        let currentDate = self.orderHistoryVM.dateList[indexPath.section]
        if let ordersForDate = self.orderHistoryVM.sortedOrders[currentDate]{
            let currentOrder = ordersForDate[indexPath.row]
            if let phone = currentOrder.customerID{
                cell?.phoneLabel.text = phone.toPhoneFormat()
            }
            if(currentOrder.startAddress != nil){
                cell?.startAddressLabel.text = currentOrder.startAddress
            }
            if(currentOrder.destinationAddress != nil){
                cell?.destinationLabel.text = currentOrder.destinationAddress
            }
            cell?.commissionLabel.text = NSLocalizedString("App Commision", comment: "The amount driver pays to Kabu") + String(format:":$%.2f", currentOrder.commission)
            cell?.priceLabel.text = "$" + String(Int(currentOrder.total))
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.section == self.orderHistoryVM.dateList.count){
            if(self.orderHistoryVM.isErrorOccured == false){
                if(self.orderHistoryVM.orders.count < self.orderHistoryVM.orderCount){
                    self.downloadOrders(false, showLoadingView: false)
                }
            }
            return
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.section == self.orderHistoryVM.dateList.count){
            if(self.orderHistoryVM.isErrorOccured == true){
                self.downloadOrders(false, showLoadingView: false)
            }
            return
        }
        let date = self.orderHistoryVM.dateList[indexPath.section]
        if let orders = self.orderHistoryVM.sortedOrders[date]{
            let order = orders[indexPath.row]
            let controller = GDOrderHistoryOrderDetailViewController.controller(order: order)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == self.orderHistoryVM.dateList.count){
            return ""
        }
        let date = self.orderHistoryVM.dateList[section]
        return date
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UITableView.appearance().backgroundColor
        if(section != self.orderHistoryVM.dateList.count){
            let date = self.orderHistoryVM.dateList[section]
            let label = UILabel()
            label.text = date
            label.textColor = UIColor.gray
            view.addSubview(label)
            label.translatesAutoresizingMaskIntoConstraints = false
            label.superview?.addConstraint(NSLayoutConstraint(item: label,
                                                              attribute: NSLayoutAttribute.leading,
                                                              relatedBy: NSLayoutRelation.equal,
                                                              toItem: label.superview,
                                                              attribute: NSLayoutAttribute.leading,
                                                              multiplier: 1.0,
                                                              constant: 16.0))
            label.superview?.addConstraint(NSLayoutConstraint(item: label,
                                                              attribute: NSLayoutAttribute.trailing,
                                                              relatedBy: NSLayoutRelation.greaterThanOrEqual,
                                                              toItem: label.superview,
                                                              attribute: NSLayoutAttribute.trailing,
                                                              multiplier: 1.0,
                                                              constant: 12.0))
            label.superview?.addConstraint(NSLayoutConstraint(item: label,
                                                              attribute: NSLayoutAttribute.height,
                                                              relatedBy: NSLayoutRelation.greaterThanOrEqual,
                                                              toItem: nil,
                                                              attribute: NSLayoutAttribute.notAnAttribute,
                                                              multiplier: 1.0,
                                                              constant: 0.0))
            label.superview?.addConstraint(NSLayoutConstraint(item: label,
                                                              attribute: NSLayoutAttribute.centerY,
                                                              relatedBy: NSLayoutRelation.equal,
                                                              toItem: label.superview,
                                                              attribute: NSLayoutAttribute.centerY,
                                                              multiplier: 1.0,
                                                              constant: 0.0))
        }
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == self.orderHistoryVM.dateList.count){
            return 12
        }
        return 36
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.000000001
    }
}
