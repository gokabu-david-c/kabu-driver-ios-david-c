//
//  DateExtension.swift
//  Driver
//
//  Created by ZY on 2017-05-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

extension Date{
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var isToday : Bool {
        let todayString = Date().toString("yyyy-MM-dd")
        let dateString = self.toString("yyyy-MM-dd")
        return todayString == dateString
    }
    var isTomorrow : Bool {
        let tmrString = Date().tomorrow.toString("yyyy-MM-dd")
        let dateString = self.toString("yyyy-MM-dd")
        return tmrString == dateString
    }
    static func ToDate(_ timeInterval:TimeInterval) -> Date{
        let date = NSDate(timeIntervalSince1970: timeInterval)
        return date as Date
    }
    static func ToDate(_ timeInterval:Int64) -> Date{
        let date = NSDate(timeIntervalSince1970: Double(timeInterval))
        return date as Date
    }
    func startOfMonth() -> Date {
        
        let comp: DateComponents = Calendar.current.dateComponents([.year, .month], from: self)
        let startOfMonth = Calendar.current.date(from: comp)!
        
        return startOfMonth
    }
   
    
    func startOfWeek(_ startDayOffset:Int = 0) -> Date
    {
        let calendar = Calendar(identifier: .gregorian)
        
        var refDate = self
        if(startDayOffset > 0)
        {
            refDate = refDate.addingTimeInterval(TimeInterval(Double(startDayOffset) * 86400.0 * -1))
        }
        
        var date = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: refDate))
        
        if(startDayOffset >= 0)
        {
            date = date?.addingTimeInterval(TimeInterval(Double(startDayOffset) * 86400.0))
        }
        
        return date!
    }
    func toString(_ format:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self)
    }
    
    
    func year() -> Int
    {
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: self)
        
        return year
    }
    
    func month() -> Int
    {
        let calendar = Calendar.current
        
        let month = calendar.component(.month, from: self)
        
        return month
    }
    
    func day() -> Int
    {
        let calendar = Calendar.current
        
        let day = calendar.component(.day, from: self)
        
        return day
 
    }
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}
