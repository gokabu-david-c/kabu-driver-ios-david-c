//
//  UIViewExtension.swift
//  Driver
//
//  Created by ZY on 2017-07-21.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

extension UIView {
    func screenShot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImageFromMyView!
    }
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true, path:UIBezierPath? = nil) {
        let systemSettingsVM = KBSystemSettingsVM()
        if(systemSettingsVM.isShadowEnabled == false){
            self.layer.shadowOpacity = 0
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowRadius = 0
            self.layer.shadowPath = nil
            return
        }
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        var shadowPath = path
        if(shadowPath == nil){
            shadowPath = UIBezierPath(rect: self.bounds)
        }
        self.layer.shadowPath = shadowPath!.cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    func startShakeAnimation(){
        UIView.animate(withDuration: 0.1,
                       animations: {
                        self.transform = CGAffineTransform.init(translationX: -10, y: 0)
        }) { (completed) in
            if(completed == true){
                UIView.animate(withDuration: 0.1,
                               animations: {
                                self.transform = CGAffineTransform.init(translationX: 10, y: 0)
                }) { (completed) in
                    if(completed == true){
                        UIView.animate(withDuration: 0.1,
                                       animations: {
                                        self.transform = CGAffineTransform.init(translationX: -7, y: 0)
                        }) { (completed) in
                            if(completed == true){
                                UIView.animate(withDuration: 0.1,
                                               animations: {
                                                self.transform = CGAffineTransform.init(translationX: 7, y: 0)
                                }) { (completed) in
                                    if(completed == true){
                                        UIView.animate(withDuration: 0.1,
                                                       animations: {
                                                        self.transform = CGAffineTransform.init(translationX: -4, y: 0)
                                        }) { (completed) in
                                            if(completed == true){
                                                UIView.animate(withDuration: 0.1,
                                                               animations: {
                                                                self.transform = CGAffineTransform.init(translationX: 4, y: 0)
                                                }) { (completed) in
                                                    if(completed == true){
                                                        UIView.animate(withDuration: 0.1,
                                                                       animations: {
                                                                        self.transform = CGAffineTransform.init(translationX: 0, y: 0)
                                                        }) { (completed) in
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
