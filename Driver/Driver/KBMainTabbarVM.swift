//
//  KBMainTabbarVM.swift
//  Driver
//
//  Created by George on 2018-02-21.
//  Copyright © 2018 Developer. All rights reserved.
//

import Foundation

class KBMainTabbarVM : NSObject{
    var accountOnlineStatusChanged:(() -> ())?
    override init() {
        super.init()
        NotificationCenter.addObserver(self,
                                       action: #selector(performAccountOnlineStatusChanged),
                                       name: NSNotification.Name.KB_Me_Online_Status_Changed)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func performAccountOnlineStatusChanged(){
        self.accountOnlineStatusChanged?()
    }
    var isAccountOnline:Bool{
        if let account = KBAccountManager.shared.getAccount(){
            return account.is_online
        }
        return false
    }
}
