//
//  KBReservationPoolTableViewCell.swift
//  Driver
//
//  Created by Kabu on 2017-11-22.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBReservationPoolTableViewCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var newLabel: UILabel!
    @IBOutlet var reservationPickupTimeLabel: UILabel!
    @IBOutlet var ridersCountLabel: UILabel!
    @IBOutlet var fromAddressLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    private var updateViewTimer:Timer?
    var orderNumber:String?
    let reservationPoolVM = KBReservationPoolVM()
    class func cell() -> KBReservationPoolTableViewCell{
        let cell = Bundle.main.loadNibNamed("KBReservationPoolTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! KBReservationPoolTableViewCell
        return cell
    }
    class var reuseIdentifier:String{
        return "KBReservationPoolTableViewCell"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.containerView.clipsToBounds = true
        self.containerView.layer.cornerRadius = 8
        //let shadowPath = UIBezierPath(roundedRect: CGRect.init(x: 0, y: 0, width: self.containerView.bounds.size.width, height: self.containerView.bounds.size.height), cornerRadius: 16.0)
        //self.containerView.dropShadow(color: .black, opacity: 0.3, offSet: CGSize.zero, radius: 6, scale: true, path:shadowPath)
        //self.startTimer()
        self.containerView.addObserver(self,
                                       forKeyPath: "bounds",
                                       options: .old,
                                       context: nil)
    }
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        if(highlighted == true){
            UIView.animate(withDuration: 0.2, animations: {
                self.containerView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.containerView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.updateShadow()
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.resetCell()
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(object as? UIView === self.containerView && keyPath == "bounds"){
            self.updateShadow()
        }
    }
    func updateShadow(){
        let shadowPath = UIBezierPath(roundedRect: CGRect.init(x: 0,
                                                               y: 0,
                                                               width: self.containerView.bounds.size.width,
                                                               height: self.containerView.bounds.size.height), cornerRadius: 8.0)
        self.containerView.dropShadow(color: .black,
                                      opacity: 0.3,
                                      offSet: CGSize.zero,
                                      radius: 6,
                                      scale: true,
                                      path:shadowPath)
    }
    func startTimer(){
        if(self.updateViewTimer != nil){
            return
        }
        self.updateViewTimer = Timer.scheduledTimer(withTimeInterval: 0.1,
                                                    repeats: true,
                                                    block: { (timer:Timer) in
                                                        self.updateCell()
                                                        //self.setNeedsLayout()
                                                        //self.layoutIfNeeded()
        })
    }
    func setOrder(orderNumber aOrderNumber:String){
        self.orderNumber = aOrderNumber
        self.updateCell()
        self.startTimer()
    }
    func updateCell(){
        if(self.orderNumber == nil || self.orderNumber?.count == 0){
            self.resetCell()
            return
        }
        let order = self.reservationPoolVM.getOrder(orderNumber:self.orderNumber!)
        if(order == nil){
            self.resetCell()
            return
        }
        if(Int64(Date().timeIntervalSince1970) - order!.timeCreated <= 300){
            self.newLabel.isHidden = false
        }
        let timeString = Date.ToDate(order!.reservationPickupTime).toString("HH:mm")
        let dateString = Date.ToDate(order!.reservationPickupTime).toString("yyyy-MM-dd")
        self.reservationPickupTimeLabel.text = dateString + " " + timeString
        let riderCountIconAttachment:NSTextAttachment = NSTextAttachment()
        riderCountIconAttachment.image = UIImage(named: "ic-people-4.png")
        let riderCountIconString:NSAttributedString = NSAttributedString(attachment: riderCountIconAttachment)
        let riderCountTextString = NSAttributedString(string:" " + String(order!.numberOfPassengers))
        let ridersCountString = NSMutableAttributedString()
        ridersCountString.append(riderCountIconString)
        ridersCountString.append(riderCountTextString)
        self.ridersCountLabel.attributedText = ridersCountString
        self.fromAddressLabel.text = order!.startAddress ?? ""
        let dollarSignAttributes = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15.0, weight: .bold),
                                    NSAttributedStringKey.foregroundColor : UIColor.black]
        let dollarSignString = NSAttributedString(string: "$", attributes:  dollarSignAttributes)
        let totalValueString = NSAttributedString(string:" " + String(Int(order!.total)))
        let totalString = NSMutableAttributedString()
        totalString.append(dollarSignString)
        totalString.append(totalValueString)
        self.priceLabel.attributedText = totalString
        //self.priceLabel.text = "$" + String(Int(order!.total))
        if(order!.customerNote.count == 0){
            self.noteLabel.text = NSLocalizedString("none", comment: "")
        }else{
            self.noteLabel.text = order!.customerNote
        }
    }
    func resetCell(){
        self.newLabel.isHidden = true
        self.reservationPickupTimeLabel.text = ""
        self.fromAddressLabel.text = ""
        self.priceLabel.text = ""
        self.ridersCountLabel.text = ""
        self.noteLabel.text = NSLocalizedString("none", comment: "")
    }
}
