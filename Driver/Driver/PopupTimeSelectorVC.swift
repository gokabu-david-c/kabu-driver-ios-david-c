//
//  KBUIPickerView.swift
//  Driver
//
//  Created by Jack Du on 2018-04-23.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

class PopupTimeSelectorVC: UIViewController {
    

    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var confirmButton: UIButton!
    
    var parentVC:KBSystemSettingsViewController?
    
    let systemSettingsVM = KBSystemSettingsVM()
    var labelList : [String] = []
    var dataList : [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData();
        setDefaultPickerIndex()
    }

    @IBAction func confirm(_ sender: Any) {
        let row = pickerView.selectedRow(inComponent: 0)
        KBSystemSettingsVM().lengthOfNewReservationPopupMinutes = dataList[row]
        parentVC?.reloadTemporaryPopupTime()
        self.dismiss(animated: true, completion: {})
    }
    
    func setupData(){
        let mins = "mins"
        labelList = ["5 \(mins)", "10 \(mins)", "15 \(mins)", "20 \(mins)", "30 \(mins)", "60 \(mins)", "90 \(mins)", "120 \(mins)"]
        dataList = [5, 10, 15, 20, 30, 60, 90, 120]
        pickerView.reloadAllComponents()
    }

    func setDefaultPickerIndex(){
        var index = 0
        let min = KBSystemSettingsVM().lengthOfNewReservationPopupMinutes
        for i in 0..<dataList.count
        {
            if(min == dataList[i]){
                index = i
                break
            }
        }
        pickerView.selectRow(index, inComponent: 0, animated: false)
    }
    
    
}

extension PopupTimeSelectorVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return labelList.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return labelList[row]
    }
    
}
