//
//  GDViewController.swift
//  Driver
//
//  Created by ZY on 2017-04-11.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBViewController: UIViewController {

    var isViewWillAppeared = false
    var isViewDidAppeared = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        NotificationCenter.addObserver(self,
                                       action: #selector(performApplicationDidBecomeActive),
                                       name: .UIApplicationWillEnterForeground)
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        NotificationCenter.addObserver(self,
                                       action: #selector(performApplicationDidBecomeActive),
                                       name: .UIApplicationWillEnterForeground)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: NSStringFromClass(self.classForCoder))
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.isViewWillAppeared = true
        
        self.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.dismissKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func performApplicationDidBecomeActive() {
        
        let sel = NSSelectorFromString("applicationDidBecomeActive")
        
        if (self.responds(to: sel))
        {
            self.perform(sel)
        }
        
    }
    
    func preferLargeNavigationTitle(isPrefered:Bool){
        if #available(iOS 11.0, *) {
            if(isPrefered){
                self.navigationController?.navigationItem.largeTitleDisplayMode = .always
                self.navigationController?.navigationBar.prefersLargeTitles = true
            }
            else{
                self.navigationController?.navigationItem.largeTitleDisplayMode = .never
                self.navigationController?.navigationBar.prefersLargeTitles = false

            }
            guard let bar = self.navigationController?.navigationBar else{
                return
            }
//            bar.setNeedsLayout()
//            bar.layoutIfNeeded()
//            bar.setNeedsDisplay()
        }
    }
    
    func hideNavigationBarBorderLine(){
        let navigationBar = navigationController!.navigationBar
        navigationBar.shadowImage = UIImage()
    }
    
    // Refresh the v
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
