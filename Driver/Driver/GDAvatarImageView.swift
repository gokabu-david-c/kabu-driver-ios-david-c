//
//  GDAvatarImageView.swift
//  Driver
//
//  Created by ZY on 2017-07-11.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDAvatarImageView: AsyncImageView {
    var borderWidth = 4.0
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentMode = UIViewContentMode.scaleAspectFit
        self.layer.borderWidth = CGFloat(self.borderWidth)
        self.layer.borderColor = UIColor.white.cgColor
    }
    override func layoutSubviews(){
        super.layoutSubviews()
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.size.width * 0.5
    }
}
