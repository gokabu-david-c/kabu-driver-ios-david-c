//
//  GDSwitchTableViewCell.swift
//  Driver
//
//  Created by ZY on 2017-05-20.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDSwitchTableViewCell: UITableViewCell {
    weak var delegate:AnyObject?
    @IBOutlet weak var switchView: UISwitch!
    class func cell() -> GDSwitchTableViewCell{
        let cell = Bundle.main.loadNibNamed("GDSwitchTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! GDSwitchTableViewCell
        return cell
    }
    class var cellIdentifier:String{
        return "GDSwitchTableViewCell"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.tag = 0
        self.textLabel?.text = ""
        self.switchView.isOn = false
    }
    @IBAction func switchViewValueChanged(_ sender: Any) {
        if(self.delegate != nil
        && (self.delegate?.responds(to: #selector(GDSwitchTableViewCellDelegate.GDSwitchTableViewCellValueChanged(_:))))!){
            (self.delegate as! GDSwitchTableViewCellDelegate).GDSwitchTableViewCellValueChanged(self)
        }
    }
}
@objc protocol GDSwitchTableViewCellDelegate {
    @objc func GDSwitchTableViewCellValueChanged(_ sender:GDSwitchTableViewCell)
}
