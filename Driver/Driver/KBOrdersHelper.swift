//
//  KBOrdersHelper.swift
//  Driver
//
//  Created by Jack Du on 2018-04-18.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

class KBOrdersHelper : NSObject {
    
    func addNewReserveOrders(oldOrders:[KBOrder]?, newOrders:[KBOrder]?) -> [KBOrder]{
        if(newOrders == nil || newOrders!.count<1){
            return oldOrders ?? Array.init()
        }
        if(oldOrders == nil || oldOrders!.count<1){
            return newOrders ?? Array.init()
        }
        var combinedOrders : [KBOrder] = oldOrders!
        
        for newOrder in newOrders!
        {
            var isOld = false
            for oldOrder in combinedOrders
            {
                if(oldOrder.orderNumber == newOrder.orderNumber){
                    isOld = true
                    break
                }
            }
            if(!isOld){
                combinedOrders.append(newOrder)
            }
        }
        return combinedOrders
        
    }
    
    func processReserveOrders(reservedOrders:[KBOrder]?) -> [KBOrder]{
        guard let reservedOrderList = reservedOrders else{
            return [KBOrder]()
        }
        var newReservationList = [KBOrder]()
        let openPickupTimeList = getOpenOrdersPickupTimes()
        for reservedOrder in reservedOrderList {
            if(validateReserveOrder(reservedOrder: reservedOrder, openPickupTimeList: openPickupTimeList)){
                newReservationList.append(reservedOrder)
            }
        }
        return reservedOrderList
    }
    
    func validateReserveOrder(reservedOrder:KBOrder, openPickupTimeList:[Int64]) -> Bool{
        for openPickupTime in openPickupTimeList
        {
            if(reservedOrder.reservationPickupTime > openPickupTime && reservedOrder.reservationPickupTime - openPickupTime < KBConfig.reservationProhibitionTimeInterval){
                return false
            }
        }
        return true
    }
    
    
    func getOpenOrdersPickupTimes() -> [Int64]{
        let orders = KBOpenOrdersManager.shared.openOrders
        var timestamps = [Int64]()
        for order in orders{
            timestamps.append(order.reservationPickupTime)
        }
        return timestamps
    }
}
