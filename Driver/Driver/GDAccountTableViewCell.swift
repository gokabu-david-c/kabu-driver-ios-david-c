//
//  GDAccountTableViewCell.swift
//  Driver
//
//  Created by ZY on 2017-07-11.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Darwin

class GDAccountTableViewCell: UITableViewCell {

    
    @IBOutlet weak var avatarImageView: GDAvatarImageView!
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var starImageView1: UIImageView!
    
    @IBOutlet weak var starImageView2: UIImageView!
    
    @IBOutlet weak var starImageView3: UIImageView!
    
    @IBOutlet weak var starImageView4: UIImageView!
    
    @IBOutlet weak var starImageView5: UIImageView!
    
    class func cell() -> GDAccountTableViewCell{
        
        let cell = Bundle.main.loadNibNamed("GDAccountTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! GDAccountTableViewCell
        
        
        return cell
    }
    
    class var cellIdentifier:String{
    
        return "GDAccountTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.avatarImageView.image = nil
        self.usernameLabel.text = ""
        self.starImageView1.image = nil
        self.starImageView2.image = nil
        self.starImageView3.image = nil
        self.starImageView4.image = nil
        self.starImageView5.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var starImageViews : [UIImageView]{
    
        return [self.starImageView1,
        self.starImageView2,
        self.starImageView3,
        self.starImageView4,
        self.starImageView5]
    
    }
    
    func setRating(_ rating:Double) {
    
        
        if(rating <= 0.0)
        {
            for currentImageView in self.starImageViews {
                
                currentImageView.image = UIImage(named: "ic-star-3")
            }
            
            return
        }
        
        if(rating >= 4.75)
        {
            for currentImageView in self.starImageViews {
                
                currentImageView.image = UIImage(named: "ic-star-1")
            }
            
            return
        }
        
        var goldCount = Int(floor(rating))
        
        var halfStar = 0
        
        if(rating - Double(goldCount) >= 0.25 && rating - Double(goldCount) < 0.75)
        {
            halfStar = 1
        }
        else if(rating - Double(goldCount) >= 0.75)
        {
            goldCount += 1
        }
        
        //let grayStar = 5 - goldCount - halfStar
        
        if(goldCount > 0)
        {
            for i in 0 ... goldCount - 1
            {
                (self.starImageViews[i] as UIImageView).image = UIImage(named: "ic-star-1")
            }
        }
        
        if(halfStar > 0)
        {
            (self.starImageViews[goldCount] as UIImageView).image = UIImage(named: "ic-star-2")
        }
        
        
        if(goldCount + halfStar < 5)
        {
            for i in goldCount + halfStar ... 4
            {
                (self.starImageViews[i] as UIImageView).image = UIImage(named: "ic-star-3")
            }
        }
        
        
        
    }

}
