//
//  GDOrderHistoryOrderDetailViewController.swift
//  Driver
//
//  Created by ZY on 2017-05-27.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDOrderHistoryOrderDetailViewController: KBViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var order:KBOrder?
    class func controller(order aOrder:KBOrder) -> GDOrderHistoryOrderDetailViewController {
        let controller = UIStoryboard(name: "OrderHistory", bundle: nil).instantiateViewController(withIdentifier: "GDOrderHistoryOrderDetailViewController") as! GDOrderHistoryOrderDetailViewController
        controller.order = aOrder
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Order Detail", comment: "")
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
    }
    // MARK: - UITableViewDelegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        if(self.order == nil){
            return 0
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.order == nil){
            return 0
        }
        return 12
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text = NSLocalizedString("Order Detail", comment: "")
            if let orderNumber = self.order?.orderNumber{
                cell?.lbl_Content.text = orderNumber
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        if(indexPath.row == 1){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text = NSLocalizedString("Total Amount", comment: "")
            if let total = self.order?.total{
                cell?.lbl_Content.text = String(format:"$%.2f", total)
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        if(indexPath.row == 2){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text = NSLocalizedString("App Commision", comment: "")
            if let commission = self.order?.commission{
                cell?.lbl_Content.text = String(format:"$%.2f", commission)
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        if(indexPath.row == 3){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text = NSLocalizedString("Starting Location", comment: "")
            if let startAddress = self.order?.startAddress{
                cell?.lbl_Content.text = startAddress
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        if(indexPath.row == 4){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text = NSLocalizedString("Destination", comment: "")
            if let destination = self.order?.destinationAddress{
                cell?.lbl_Content.text = destination
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        if(indexPath.row == 5){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text = NSLocalizedString("Customer Phone Number", comment: "")
            if let phone = self.order?.customerID{
                cell?.lbl_Content.text = phone.toPhoneFormat()
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        if(indexPath.row == 6){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text = NSLocalizedString("Number of Customers", comment: "")
            if let numberOfPassgener = self.order?.numberOfPassengers{
                cell?.lbl_Content.text = String(numberOfPassgener)
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        if(indexPath.row == 7){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text =  NSLocalizedString("Original Distance", comment: "")
            if let distance = self.order?.distance{
                cell?.lbl_Content.text = String(format:"%.2fkm", distance)
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        if(indexPath.row == 8){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text = NSLocalizedString("Original Cost", comment: "")
            if let price = self.order?.price{
                cell?.lbl_Content.text = String(format:"$%.2f", price)
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        if(indexPath.row == 9){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text = NSLocalizedString("Additional Distance", comment: "")
            if let additionalDistance = self.order?.additional_distance{
                cell?.lbl_Content.text = String(format:"%.2fkm", additionalDistance)
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        if(indexPath.row == 10){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text = NSLocalizedString("Additional Cost", comment: "")
            if let additionalFee = self.order?.additional_fee{
                cell?.lbl_Content.text = String(format:"$%.2f", additionalFee)
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        if(indexPath.row == 11){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.lbl_Title.text = NSLocalizedString("Order Time", comment: "When the customer put down the order")
            if let timeCreated = self.order?.timeCreated{
                let timeInterval = TimeInterval(timeCreated)
                cell?.lbl_Content.text = Date.ToDate(timeInterval).toString("yyyy-MM-dd HH:mm")
            }
            cell?.lbl_Content.numberOfLines = 0
            return cell!
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
