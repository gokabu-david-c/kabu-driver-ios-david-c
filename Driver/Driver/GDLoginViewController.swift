
import UIKit
import CoreData
import AVFoundation

class GDLoginViewController: KBViewController, UITextFieldDelegate {
    let accountVM = KBAccountVM()
    @IBOutlet var topAdminView:     UIView!
    @IBOutlet weak var tf_User:        UITextField!
    @IBOutlet weak var tf_Password:    UITextField!
    @IBOutlet weak var videoContentView: UIView!
    @IBOutlet weak var videoViewContainer: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var constraint_Bottom_ContentView_SuperView: NSLayoutConstraint!
    var lastContentOffset:CGFloat = 0
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    var paused: Bool = false
    var rightButtonTapCount = 0
    class func controller() -> GDLoginViewController{
        let controller = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "GDLoginViewController") as! GDLoginViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contentView.alpha = 0
        self.videoViewContainer.alpha = 0
        self.tf_User.delegate      = self
        self.tf_Password.delegate  = self
        self.tf_User.tintColor = .white
        self.tf_Password.tintColor = .white
        self.tf_User.attributedPlaceholder = NSAttributedString(string: self.tf_User.placeholder!,
                                                               attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        self.tf_Password.attributedPlaceholder = NSAttributedString(string: self.tf_Password.placeholder!,
                                                                attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        self.tf_User.text = KBAccountManager.shared.getLastLoginUserName()
        registerDismissKeyboardWhenTappedOnView(self.view)
        let videoFileName = "login-animation-" + String(arc4random() % 3 + 1)
        let theURL = Bundle.main.url(forResource:videoFileName, withExtension: "mp4")
        avPlayer = AVPlayer(url: theURL!)
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
        } catch let error as NSError {
            print(error)
        }
        
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let error as NSError {
            print(error)
        }
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        avPlayer.volume = 0
        avPlayer.isMuted = true
        avPlayer.actionAtItemEnd = .none
        avPlayerLayer.frame = self.videoViewContainer.layer.bounds
        self.videoContentView.layer.insertSublayer(avPlayerLayer, at: 0)
        avPlayer.addObserver(self,
                             forKeyPath: "status",
                             options: .initial,
                             context: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: avPlayer.currentItem)
        NotificationCenter.addObserver(self,
                                       action: #selector(applicationWillResignActive),
                                       name: NSNotification.Name.UIApplicationWillResignActive)
        NotificationCenter.addObserver(self,
                                       action: #selector(applicationDidBecomeActive),
                                       name: NSNotification.Name.UIApplicationDidBecomeActive)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow),
                                               name: .UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide),
                                               name: .UIKeyboardWillHide,
                                               object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        avPlayer.play()
        paused = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self,
                                                  name: .UIKeyboardWillShow,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: .UIKeyboardWillHide,
                                                  object: nil)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        avPlayer.pause()
        paused = true
    }
    deinit {
        avPlayer.removeObserver(self,
                                forKeyPath: "status",
                                context: nil)
    }
    override var prefersStatusBarHidden: Bool{
        return true
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        avPlayerLayer.frame = self.videoContentView.layer.bounds
    }
    @objc func applicationWillResignActive() {
    
        if self.avPlayer != nil
        {
            self.avPlayer.pause()
        }
    }
    
    @objc func applicationDidBecomeActive() {
        
        if self.avPlayer != nil
        {
            self.avPlayer.seek(to: self.avPlayer.currentTime())
            self.avPlayer.play()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if let o = object as? AVPlayer
        {
            if o === self.avPlayer
            {
                if(keyPath == "status")
                {
                    DispatchQueue.main.async {
                     
                        if (self.avPlayer.status == AVPlayerStatus.readyToPlay)
                        {
                            if(self.videoViewContainer.alpha == 0)
                            {
                                UIView.animate(withDuration: 0.75,
                                               animations: {
                                    self.videoViewContainer.alpha = 1
                                })
                            }
                            
                            if(self.contentView.alpha == 0)
                            {
                                UIView.animate(withDuration: 1,
                                               animations: { 
                                    self.contentView.alpha = 1
                                })
                                
                            }
                        }
                        
                    }
                }
            }
        }
        
        
    }
    
    @objc func playerItemDidReachEnd(notification: Notification) {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: kCMTimeZero)
    }
    
    func showLoginFailed(_ notification:NSNotification) {
        
        
        var message = ""
        
        if let messageData = notification.userInfo?[NSNotification.Key_UserInfo_Message] as? String {
            
            message = messageData
            
        }
        
        
        
        self.showError(message, UIViewControllerLoadingType.UIAlert)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField == self.tf_User)
        {
            self.tf_Password.becomeFirstResponder()
        }
        else if(textField == self.tf_Password)
        {
            self.login()
        }
        
        return true
        
    }
    
    
    @IBAction func logoButtonTapped(_ sender: Any) {
        
        self.rightButtonTapCount += 1
        
        if(self.rightButtonTapCount >= 5)
        {
            self.rightButtonTapCount = 0;
            
            //if(self.isLocked_DebugMode == false)
            if(GDDebugManager.shared.isLocked() == false)
            {
                let controller = GDDebugNavigationController.controller()
                self.present(controller,
                             animated: true,
                             completion: {
                                
                })
                
                return
            }
            
            
            
            
            //1. Create the alert controller.
            let alert = UIAlertController(title: NSLocalizedString("Kabu Rider Service", comment: ""), message: NSLocalizedString("Please Enter Password", comment: ""), preferredStyle: .alert)
            
            //2. Add the text field. You can configure it however you need.
            alert.addTextField { (textField) in
                
            }
            
            alert.textFields?[0].isSecureTextEntry = true
            
            // 3. Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .default, handler: { [weak alert] (_) in
                
                let password = alert?.textFields![0].text // Force unwrapping because we know it exists.
                
                if(password != "kabu2017"
                    && password != GDDebugManager.shared.getDebugPageLoginPassword())
                {
                    
                    self.showError(NSLocalizedString("The Password is Incorrect", comment: ""))
                    
                    return
                    
                }
                
                GDDebugManager.shared.enteredPassword = password!
                let controller = GDDebugNavigationController.controller()
                self.present(controller,
                             animated: true,
                             completion: {
                                
                })
                
                
            }))
            
            
            
            // 3. Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "") , style: .cancel))
            
            
            // 4. Present the alert.
            self.present(alert, animated: true, completion: nil)
            
            
            
        }
        
    }
    
    
    @IBAction func loginButtonTapped(_ sender: Any) {
            
            
        self.login()
        
        
        
        
        
    }
    
    
    
    // Autenticate
    func login() {
        self.dismissKeyboard()
        let username:String   = self.tf_User.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let pass:String = self.tf_Password.text!
        if (username.isEmpty || username.count == 0
            || pass.isEmpty || pass.count == 0){
            showError(NSLocalizedString("Please enter account Name and Password", comment: ""))
            return
        }
        self.accountVM.login(username: username,
                             password: pass,
                             start: {
                                self.showLoading("")
        }, success: {
            self.dismissLoading()
            KBScreenHelper.shared.showMainController()
        }) { (error:String) in
            self.showError(error)
        }
    }
    @IBAction func forgetPasswordButtonTapped(_ sender: Any) {
        let controller = GDResetPasswordNavigationController.controller()
        self.present(controller, animated: true) {
        }
    }
    @IBAction func registerButtonTapped(_ sender: Any) {
        let controller = GDPureWebViewController.controller(urlString: KBConfig.ULRString_DriverApply, title: NSLocalizedString("Join KABU", comment: ""))
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        
        
        self.constraint_Bottom_ContentView_SuperView.constant = keyboardFrame.size.height * 0.3
        
        
        let keyboardAnimationCurve = (info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).uintValue
        
        
        UIView.animateKeyframes(withDuration: duration,
                                delay: 0,
                                options: [UIViewKeyframeAnimationOptions.init(rawValue: keyboardAnimationCurve)],
                                animations: { 
                                    
                                    self.view.layoutIfNeeded()
                                    
        }, completion: nil)

        
        
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        let info = notification.userInfo!
        
        
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        
        
        self.constraint_Bottom_ContentView_SuperView.constant = 0
        
        
        let keyboardAnimationCurve = (info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).uintValue
        
        
        UIView.animateKeyframes(withDuration: duration,
                                delay: 0,
                                options: [UIViewKeyframeAnimationOptions.init(rawValue: keyboardAnimationCurve)],
                                animations: {
                                    
                                    self.view.layoutIfNeeded()
                                    
        }, completion: nil)
    }
    
}


