//
//  KBMyProfileViewController.swift
//  Driver
//
//  Created by ZY on 2017-04-08.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBMyProfileViewController: KBViewController, GDEditNicknameViewControllerDelegate, GDChangePasswordTableViewControllerDelegate, GDEditCarModelViewControllerDelegate, GDEditCarCapacityViewControllerDelegate {
    @IBOutlet weak var tableView: UITableView!
    let accountVM = KBAccountVM()
    class func controller() -> KBMyProfileViewController {
        let controller = UIStoryboard(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "KBMyProfileViewController") as! KBMyProfileViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = UINavigationItem.LargeTitleDisplayMode.never
        }
        self.title = NSLocalizedString("Personal Information", comment: "")
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 52
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        NotificationCenter.addObserver(self,
                                       action: #selector(meChanged),
                                       name: NSNotification.Name.GD_Me_Changed);
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @objc func meChanged() {
        self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - GDEditNicknameViewControllerDelegate
    func GDEditNicknameViewControllerDidSave(sender aSender: GDEditNicknameViewController) {
        aSender.navigationController?.popViewController(animated: true)
    }
    // MARK: - GDChangePasswordTableViewControllerDelegate
    func GDChangePasswordTableViewControllerDidSaveSuccess(_ sender: GDChangePasswordTableViewController) {
        sender.navigationController?.popViewController(animated: true)
    }
    // MARK: - GDEditCarModelViewControllerDelegate
    func GDEditCarModelViewControllerDelegateDidSaveSuccess(sender aSender:GDEditCarModelViewController) {
        aSender.navigationController?.popViewController(animated: true)
    }
    // MARK: - GDEditCarCapacityViewControllerDelegate
    func GDEditCarCapacityViewControllerDidSaveSuccess(sender aSender: GDEditCarCapacityViewController) {
        aSender.navigationController?.popViewController(animated: true)
    }
}
extension KBMyProfileViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == 0){
            var cell = tableView.dequeueReusableCell(withIdentifier: KBMyProfileAvatarTableViewCell.reuseIdentifier) as? KBMyProfileAvatarTableViewCell
            if(cell == nil){
                cell = KBMyProfileAvatarTableViewCell.cell()
            }
            cell?.titleLabel.text = NSLocalizedString("Profile Picture", comment: "")
            if let account = self.accountVM.account{
                cell?.avatarImageView.imageURL = account.photoURL
            }
            return cell!
        }
        if(indexPath.row == 1){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            var nickname = "";
            if(KBAccountManager.shared.isLoggedIn()){
                let account = KBAccountManager.shared.getAccount()
                if(account != nil){
                    if let accountNickname = account?.nick_name{
                        nickname = accountNickname
                    }
                }
            }
            cell?.setupCell(NSLocalizedString("Nickname", comment: ""), nickname)
            cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell?.setSeparatorHidden(true)
            return cell!
        }
        if(indexPath.row == 2){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            var phone = ""
            if(KBAccountManager.shared.isLoggedIn()){
                let account = KBAccountManager.shared.getAccount()
                if(account != nil){
                    if let accountPhone = account?.phone{
                        phone = accountPhone
                    }
                }
            }
            cell?.setupCell(NSLocalizedString("Phone Number", comment: ""), phone.toPhoneFormat())
            cell?.accessoryType = UITableViewCellAccessoryType.none
            cell?.setSeparatorHidden(true)
            return cell!
        }
        if(indexPath.row == 3){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            var carModel = ""
            if(KBAccountManager.shared.isLoggedIn()){
                let account = KBAccountManager.shared.getAccount()
                if(account != nil){
                    if let accountCarModel = account?.car_desc{
                        carModel = accountCarModel
                    }
                }
            }
            cell?.setupCell(NSLocalizedString("Car Model", comment: ""), carModel)
            cell?.accessoryType = UITableViewCellAccessoryType.none
            cell?.setSeparatorHidden(true)
            return cell!
        }
        if(indexPath.row == 4){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            var city = ""
            if let account = self.accountVM.account{
                city = account.parentCityName
            }
            cell?.setupCell(NSLocalizedString("Residing City", comment: ""), city)
            cell?.accessoryType = UITableViewCellAccessoryType.none
            cell?.setSeparatorHidden(true)
            return cell!
        }
        if(indexPath.row == 5){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            var carCapacity = 0
            if(KBAccountManager.shared.isLoggedIn()){
                let account = KBAccountManager.shared.getAccount()
                if(account != nil){
                    if let accountCarCapacity = account?.car_capacity{
                        carCapacity = accountCarCapacity
                    }
                }
            }
            cell?.setupCell(NSLocalizedString("Passenger Capacity", comment: ""), String(carCapacity) + NSLocalizedString("People", comment: ""))
            cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell?.setSeparatorHidden(true)
            return cell!
        }
        if(indexPath.row == 6){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
            if(cell == nil){
                cell = GDLabelTableViewCell.cell()
            }
            cell?.setupCell(NSLocalizedString("Change Password", comment: ""), "")
            cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell?.setSeparatorHidden(true)
            return cell!
        }
        return UITableViewCell()
    }
}
extension KBMyProfileViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.row == 1){
            let controller = GDEditNicknameViewController.controller(delegate: self)
            self.navigationController?.pushViewController(controller, animated: true)
            return
        }
        if(indexPath.row == 5){
            let controller = GDEditCarCapacityViewController.controller(delegate: self)
            self.navigationController?.pushViewController(controller, animated: true)
            return
        }
        if(indexPath.row == 6){
            let controller = GDChangePasswordTableViewController.controller(delegate: self)
            self.navigationController?.pushViewController(controller, animated: true)
            return
        }
    }
}
