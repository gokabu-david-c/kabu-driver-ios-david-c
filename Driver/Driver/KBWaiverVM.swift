//
//  KBWaiverVM.swift
//  Driver
//
//  Created by George on 2018-02-14.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class KBWaiverVM: NSObject {
    var waiverURLString:String{
        return KBConfig.URLString_Waiver
    }
    var isUserAgreed:Bool{
        return KBAccountManager.shared.account?.isAccountAgreedWaiver ?? false
    }
    func agreeToWaiver(startCallback aStartCallback:(() -> ())?,
                       successCallback aSuccessCallback:@escaping(() -> ()),
                       failureCallback aFailureCallback:@escaping((_ error:NSError) -> ())){
        if let account = KBAccountManager.shared.account{
            KBDriversAPI.updateDriver(driverID: account.id,
                                      driverData: [KBDriver.Key_Waiver_Agree_Time:"anything but empty"],
                                      start: {
                                        aStartCallback?()
            }, success: { (accountData:KBDriver) in
                KBAccountManager.shared.account = accountData
                aSuccessCallback()
            }, failure: { (error:NSError) in
                aFailureCallback(error)
            })
        }
    }
}
