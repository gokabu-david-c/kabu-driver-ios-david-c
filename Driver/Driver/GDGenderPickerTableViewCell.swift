//
//  GDGenderPickerTableViewCell.swift
//  Driver
//
//  Created by ZY on 2017-04-05.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDGenderPickerTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var containerView: UIView!
    
    var selectedIndex:Int = 0
    
    var segmentedControl: BetterSegmentedControl?
    
    weak var delegate:AnyObject?
    
    
    class func cell() -> GDGenderPickerTableViewCell{
        
        let cell = Bundle.main.loadNibNamed("GDGenderPickerTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! GDGenderPickerTableViewCell
        
        return cell
    }
    
    class var cellIdentifier:String{
        
        return "GDGenderPickerTableViewCell"
        
    }
    
    func setupCell(_ selectedIndex:Int, _ delegate:AnyObject?) {
        
        self.selectedIndex = selectedIndex
        
        self.delegate = delegate
        
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let control = BetterSegmentedControl(
            frame: CGRect(x: 0.0, y: 0, width: self.containerView.frame.width, height: self.containerView.frame.size.height),
            titles: ["男", "女"],
            index: UInt(self.selectedIndex),
            backgroundColor: .GDLightGray,
            titleColor: .black,
            indicatorViewBackgroundColor: .black,
            selectedTitleColor: .white)
        control.cornerRadius = 20;
        
        control.addTarget(self, action: #selector(GDGenderPickerTableViewCell.segmentedControlValueChanged(_:)), for: .valueChanged)
        
        self.segmentedControl = control
        
        self.containerView.addSubview(control)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.segmentedControl!.frame = CGRect(x: 0.0, y: 0, width: self.containerView.frame.width, height: self.containerView.frame.size.height)
    }
    
    @objc func segmentedControlValueChanged(_ sender: BetterSegmentedControl) {
        
        if(self.delegate != nil
            && (self.delegate?.responds(to: Selector(("GDGenderPickerTableViewCell:"))))!)
        {
            (self.delegate as! GDGenderPickerTableViewCellDelegate).GDGenderPickerTableViewCellValueChanged(self)
        }
        
    }
    
    func GetGender() -> String {
        
        return (self.segmentedControl?.titles[Int((self.segmentedControl?.index)!)])!
        
    }

}


protocol GDGenderPickerTableViewCellDelegate {
    
    func GDGenderPickerTableViewCellValueChanged(_ sender:GDGenderPickerTableViewCell)
    
}
