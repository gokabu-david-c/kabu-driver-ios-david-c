//
//  GDNotificationCenter.swift
//  Driver
//
//  Created by ZY on 2017-04-08.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation


extension NotificationCenter {
    
    //static let Name_UserLoggedOut:String = "GDNotification.UserLoggedOut"
    
    
    class func addObserver(_ observer:AnyObject, action selector:Selector, name aname:NSNotification.Name?) {
        
        NotificationCenter.default.removeObserver(observer, name: aname, object: nil)
        
        NotificationCenter.default.addObserver(observer,
                                               selector:selector,
                                               name: aname,
                                               object: nil)
    }
    
    
}
