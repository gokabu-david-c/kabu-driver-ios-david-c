//
//  GDEnableLocationServiceViewController.swift
//  Driver
//
//  Created by ZY on 2017-04-19.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDEnableLocationServiceViewController: KBViewController {
    class func controller() -> GDEnableLocationServiceViewController {
        let controller = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: "GDEnableLocationServiceViewController") as! GDEnableLocationServiceViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func openSettingButtonTapped(_ sender: Any) {
        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
        if UIApplication.shared.canOpenURL(settingsUrl!) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsUrl!, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                    if(success == false){
                        self.showError(NSLocalizedString("Trouble opening, Please manually turn on the Location Service in the Settings", comment: ""))
                    }
                })
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(settingsUrl!)
            }
        }
    }
}
