//
//  GDTextFieldTableViewCell.swift
//  Driver
//
//  Created by ZY on 2017-04-05.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDTextFieldTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var tf_Content: UITextField!
    
    
    @IBOutlet weak var imageView_Separator: UIImageView!
    
    
    weak var delegate:AnyObject?
    
    class func cell() -> GDTextFieldTableViewCell{
        
        let cell = Bundle.main.loadNibNamed("GDTextFieldTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! GDTextFieldTableViewCell
        
        
        
        
        return cell
    }
    
    
    class var cellIdentifier:String
    {
        return "GDTextFieldTableViewCell"
    }
    
    
    
    func setupCell(_ title:String?,_ content:String?, _ delegate:AnyObject?) {
        
        
        if(title != nil)
        {
            self.lbl_Title.text = title;
        }
        
        if(content != nil)
        {
            self.tf_Content.text = content
        }
        
        
        
        self.delegate = delegate
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.tf_Content.delegate = self
        
        self.tf_Content.addTarget(self, action: #selector(GDTextFieldTableViewCell.textFieldTextChanged(_:)), for: UIControlEvents.editingChanged)
        
        
        let image_Separator = UIImage.image(.GDLightGray, size: CGSize(width: 1, height: 1))
        self.imageView_Separator.image = image_Separator
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
    
    @objc func textFieldTextChanged(_ sender:UITextField!) {
        
        if(self.delegate != nil
            && (self.delegate?.responds(to: #selector(GDTextFieldTableViewCellDelegate.GDTextFieldTableViewCellValueChanged(_:))))!)
        {
            (self.delegate as! GDTextFieldTableViewCellDelegate).GDTextFieldTableViewCellValueChanged(self)
        }
        
    }
    
    
    
    // MARK:
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(textField == self.tf_Content)
        {
            self.tf_Content.resignFirstResponder()
        }
        
        return true
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(self.delegate != nil
            && (self.delegate?.responds(to: #selector(GDTextFieldTableViewCellDelegate.GDTextFieldTableViewCellBeginEditing(_:))))!)
        {
            (self.delegate as! GDTextFieldTableViewCellDelegate).GDTextFieldTableViewCellBeginEditing!(self)
        }
        
    }

}


@objc protocol GDTextFieldTableViewCellDelegate {
    
    @objc func GDTextFieldTableViewCellValueChanged(_ sender:GDTextFieldTableViewCell)
    
    
    @objc optional func GDTextFieldTableViewCellBeginEditing(_ sender:GDTextFieldTableViewCell)
    
}
