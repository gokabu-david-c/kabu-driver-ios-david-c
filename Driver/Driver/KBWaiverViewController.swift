//
//  KBWaiverViewController.swift
//  Driver
//
//  Created by George on 2018-02-14.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import WebKit

class KBWaiverViewController: KBViewController {
    let waiverVM = KBWaiverVM()
    var isPageFinishLoading = false{
        didSet{
            if(self.isPageFinishLoading != oldValue){
                if(self.isPageFinishLoading == true){
                    self.agreeButton.isEnabled = true
                }
            }
        }
    }
    var isReadingFinished = false{
        didSet{
            if(self.isPageFinishLoading && self.isReadingFinished == true){
                if(self.agreeButton.title(for: UIControlState.normal) != NSLocalizedString("I had read and agreed", comment: "") ){
                    self.agreeButton.setTitle(NSLocalizedString("I had read and agreed", comment: ""), for: UIControlState.normal)
                }
            }
        }
    }
    
    @IBOutlet weak var webViewContainer: UIView!
    var webView: WKWebView!
    var progressView: UIProgressView!
    @IBOutlet weak var agreeButton: UIButton!
    class var controller : KBWaiverViewController {
        let controller = UIStoryboard(name: "Waiver", bundle: nil).instantiateViewController(withIdentifier: "KBWaiverViewController") as! KBWaiverViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.loadWebView()
        self.loadProgressBar()
    }
    func loadWebView(){
        let webViewConfig = WKWebViewConfiguration()
        if let account = KBAccountManager.shared.getAccount(){
            let userContentController = WKUserContentController();
            let cookieScript = WKUserScript(source: "document.cookie = 'uid=" + String(account.id) + "';document.cookie = 'user_access_token=" + account.userAccessToken + "';",
                                            injectionTime: WKUserScriptInjectionTime.atDocumentStart,
                                            forMainFrameOnly: false)
            userContentController.addUserScript(cookieScript)
            webViewConfig.userContentController = userContentController;
        }
        self.webView = WKWebView(frame: CGRect.zero, configuration: webViewConfig)
        self.webView.navigationDelegate = self
        self.webView.scrollView.delegate = self
        self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.webViewContainer.addSubview(self.webView)
        self.webView.superview?.addConstraint(NSLayoutConstraint(item: self.webView,
                                                                 attribute: NSLayoutAttribute.left,
                                                                 relatedBy: NSLayoutRelation.equal,
                                                                 toItem: self.webView.superview,
                                                                 attribute: NSLayoutAttribute.left,
                                                                 multiplier: 1.0,
                                                                 constant: 0))
        self.webView.superview?.addConstraint(NSLayoutConstraint(item: self.webView,
                                                                 attribute: NSLayoutAttribute.right,
                                                                 relatedBy: NSLayoutRelation.equal,
                                                                 toItem: self.webView.superview,
                                                                 attribute: NSLayoutAttribute.right,
                                                                 multiplier: 1.0,
                                                                 constant: 0))
        self.webView.superview?.addConstraint(NSLayoutConstraint(item: self.webView,
                                                                 attribute: NSLayoutAttribute.top,
                                                                 relatedBy: NSLayoutRelation.equal,
                                                                 toItem: self.webView.superview,
                                                                 attribute: NSLayoutAttribute.top,
                                                                 multiplier: 1.0,
                                                                 constant: 0))
        self.webView.superview?.addConstraint(NSLayoutConstraint(item: self.webView,
                                                                 attribute: NSLayoutAttribute.bottom,
                                                                 relatedBy: NSLayoutRelation.equal,
                                                                 toItem: self.webView.superview,
                                                                 attribute: NSLayoutAttribute.bottom,
                                                                 multiplier: 1.0,
                                                                 constant: 0))
        self.webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        
    }
    func loadProgressBar(){
        self.progressView = UIProgressView(progressViewStyle: .default)
        self.progressView.translatesAutoresizingMaskIntoConstraints = false
        self.webViewContainer.addSubview(self.progressView)
        self.progressView.superview?.addConstraint(NSLayoutConstraint(item: self.progressView,
                                                                      attribute: NSLayoutAttribute.left,
                                                                      relatedBy: NSLayoutRelation.equal,
                                                                      toItem: self.progressView.superview,
                                                                      attribute: NSLayoutAttribute.left,
                                                                      multiplier: 1.0,
                                                                      constant: 0))
        self.progressView.superview?.addConstraint(NSLayoutConstraint(item: self.progressView,
                                                                      attribute: NSLayoutAttribute.right,
                                                                      relatedBy: NSLayoutRelation.equal,
                                                                      toItem: self.progressView.superview,
                                                                      attribute: NSLayoutAttribute.right,
                                                                      multiplier: 1.0,
                                                                      constant: 0))
        self.progressView.superview?.addConstraint(NSLayoutConstraint(item: self.progressView,
                                                                      attribute: NSLayoutAttribute.top,
                                                                      relatedBy: NSLayoutRelation.equal,
                                                                      toItem: self.progressView.superview,
                                                                      attribute: NSLayoutAttribute.top,
                                                                      multiplier: 1.0,
                                                                      constant: 0))
        self.progressView.superview?.addConstraint(NSLayoutConstraint(item: self.progressView,
                                                                      attribute: NSLayoutAttribute.height,
                                                                      relatedBy: NSLayoutRelation.equal,
                                                                      toItem: nil,
                                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                                      multiplier: 1.0,
                                                                      constant: 2))
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadWaiverPage()
    }
    func loadWaiverPage(){
        let url = URL.init(string: self.waiverVM.waiverURLString)
        let urlRequest = URLRequest.init(url: url!)
        self.webView.load(urlRequest)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        self.webView?.removeObserver(self, forKeyPath:#keyPath(WKWebView.estimatedProgress))
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        // Display Progress Bar While Loading Pages
        if keyPath == "estimatedProgress" {
            self.progressView.progress = Float(self.webView.estimatedProgress)
            if(self.progressView.progress >= 1){
                self.progressView.isHidden = true
            }
            else{
                self.progressView.isHidden = false
            }
        }
    }
    @IBAction func agreeButtonTapped(_ sender: Any) {
        if(self.waiverVM.isUserAgreed == false && self.isReadingFinished == false){
            self.showError(NSLocalizedString("Please read and agree to the Waiver", comment: ""), UIViewControllerLoadingType.UIAlert);
            return
        }else if(self.waiverVM.isUserAgreed == true){
            self.dismiss(animated: true, completion: nil)
            return
        }
        self.waiverVM.agreeToWaiver(startCallback: {
            self.showLoading("")
        }, successCallback: {
            self.dismissLoading()
            self.dismiss(animated: true, completion: nil)
        }) { (error:NSError) in
            self.showError(error.message, UIViewControllerLoadingType.UIAlert)
        }
    }
    
}
extension KBWaiverViewController : WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if(webView === self.webView){
            self.isPageFinishLoading = true
        }
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
    }
}
extension KBWaiverViewController : UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView === self.webView.scrollView){
            print("y:" + String(format:"%.3f",scrollView.contentOffset.y) + "; bounds:" + String(format:"%.3f",scrollView.bounds.size.height) + "; content size:" + String(format:"%.3f",scrollView.contentSize.height));
            if(scrollView.contentOffset.y + scrollView.bounds.size.height >= scrollView.contentSize.height - 10){
                if(self.isPageFinishLoading == true){
                    self.isReadingFinished = true
                }
            }
        }
    }
}
