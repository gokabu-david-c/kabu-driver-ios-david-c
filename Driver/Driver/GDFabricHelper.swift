//
//  GDFabricHelper.swift
//  Driver
//
//  Created by ZY on 2017-08-09.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation
import Fabric
import Crashlytics

class GDFabricHelper{

    static func logUser(uid aUID:Int, username aUsername:String) {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        
        Crashlytics.sharedInstance().setUserIdentifier(String(aUID))
        Crashlytics.sharedInstance().setUserName(aUsername)
    }
    
}
