//
//  GDOrder.swift
//  Driver
//
//  Created by ZY on 2017-04-11.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

enum GDOrderStatus : Int {
    case OrderPlaced = 0,
    OrderStarted = 1,
    OrderComplete = 2,
    OrderCanceled_ByCustomer = 3,
    OrderCanceled_ByCustomerService = 4,
    DriverConfirmed = 5,
    OrderArrived = 6,
    OrderDeleted = 7
}
enum KBOrderIsReservation : Int{
    case False = 0,
    True = 1
}

class KBOrder {
    
    //public static let Key_OrderID = "id"
    public static let Key_OrderNumber = "order_no"
    public static let Key_CustomerID = "rider_username";
    public static let Key_DriverID = "driver_id";
    public static let Key_OrderStatus = "order_status";
    public static let Key_AddressFrom = "address_from";
    public static let Key_LatlngFrom = "latlng_from";
    public static let Key_AddressTo = "address_to";
    public static let Key_LatlngTo = "latlng_to";
    public static let Key_PassengerNumber = "riders_count";
    public static let Key_Distance = "distance";
    public static let Key_Price = "price";
    //public static let Key_Duration = "duration";
    public static let Key_FinalLatlng = "final_latlng";
    //public static let Key_StartTime = "start_time";
    //public static let Key_FinishTime = "finish_time";
    //public static let Key_UpdateBy = "update_by";
    public static let Key_CreateTime = "create_time";
    //public static let Key_UpdateTime = "update_time";
    public static let Key_AdditionalDistance = "additional_distance";
    public static let Key_AdditionalFee = "additional_fee";
    public static let Key_Total = "total";
    public static let Key_Commission = "commission"
    public static let Key_ExchangeRate = "exchange_rate"
    public static let Key_DefaultCurrencySymbol = "default_currency_symbol"
    public static let Key_SecondCurrencySymbol = "second_currency_symbol"
    public static let Key_ContactPhone = "contact_phone"
    public static let Key_IS_RESERVATION = "is_reservation"
    public static let KEY_RESERVATION_PICKUP_TIME = "reservation_pickup_time"
    public static let Value_AdditionalPrice_PerKM:Float = 1.8
    private static let KEY_SHOULD_COUNT_ADDITIONAL_DISTANCE = "ShouldCountAdditionalDistance"
    private static let KEY_CURRENT_ADDITIONAL_DISTANCE = "AdditionalDistance"
    private static let KEY_CURRENT_ADDITIONAL_FEE = "AdditionalFee"
    private static let KEY_CURRENT_ADDITIONAL_PREV_LOCATION = "AdditionalTravel_PrevLocation"
    private static let KEY_CUSTOMER_NOTE = "customer_note"
    
    
    //var orderID:Int = 0
    
    var orderNumber:String?
    
    var customerID:String?
    
    var driverID:Int = 0
    
    var orderStatus:GDOrderStatus?
    
    var startAddress:String?
    
    var startCoordinate:String?
    
    var destinationAddress:String?
    
    var destinationCoordinate:String?
    
    var numberOfPassengers:Int = 0
    
    var distance:Float = 0.0
    
    var price:Float = 0.0
    
    var contactPhone:String?
    
    //var duration:Int64 = 0
    
    //var finalCoordinate:String?
    
    //var startTime:Int64 = 0
    
    //var finishTime:Int64 = 0
    
    //var updatedBy:String?
    
    var timeCreated:Int64 = 0
    
    //var timeUpdated:Int64 = 0
    
    var additional_distance:Float = 0
    
    var additional_fee:Float = 0
    
    var total:Float = 0
    
    var commission:Float = 0
    
    var exchangeRate:Float?
    
    var defaultCurrencySymbol:String = ""
    
    var secondCurrencySymbol:String = ""
    var isReservation = false
    var reservationPickupTime:Int64 = 0
    var customerNote:String = ""
    
    
    
    init(_ data:Dictionary<String, Any>) {
        
        //self.orderID = data[GDOrder.Key_OrderID] as! Int
        
        self.orderNumber = data[KBOrder.Key_OrderNumber] as? String
        
        self.customerID = data[KBOrder.Key_CustomerID] as? String
        
        if let driverID = data[KBOrder.Key_DriverID] as? Int
        {
            self.driverID = driverID
        }
        
        if let orderStatus = data[KBOrder.Key_OrderStatus] as? Int
        {
            switch orderStatus
            {
                case 0:
                    self.orderStatus = GDOrderStatus.OrderPlaced
                case 1:
                    self.orderStatus = GDOrderStatus.OrderStarted
                case 2:
                    self.orderStatus = GDOrderStatus.OrderComplete
                case 3:
                    self.orderStatus = GDOrderStatus.OrderCanceled_ByCustomer
                case 4:
                    self.orderStatus = GDOrderStatus.OrderCanceled_ByCustomerService
                case 5:
                    self.orderStatus = GDOrderStatus.DriverConfirmed
                case 6:
                    self.orderStatus = GDOrderStatus.OrderArrived
                default: break
                
            }
        }
        

        self.startAddress = data[KBOrder.Key_AddressFrom] as? String
        
        self.startCoordinate = data[KBOrder.Key_LatlngFrom] as? String
        
        self.destinationAddress = data[KBOrder.Key_AddressTo] as? String
        
        self.destinationCoordinate = data[KBOrder.Key_LatlngTo] as? String
        
        self.contactPhone = data[KBOrder.Key_ContactPhone] as? String
        
        if let numberOfPassengers = data[KBOrder.Key_PassengerNumber] as? Int
        {
            self.numberOfPassengers = numberOfPassengers
        }
        
        //self.distance = data[GDOrder.Key_Distance] as! Float
        if let distance = data[KBOrder.Key_Distance] as? Double
        {
            self.distance = Float(distance)
        }
        
        if let price = data[KBOrder.Key_Price] as? Double
        {
            self.price = Float(price)
        }
                
        if let createTime = data[KBOrder.Key_CreateTime] as? Int64
        {
            self.timeCreated = createTime
        }
        
    
        
        if let additional_distance = data[KBOrder.Key_AdditionalDistance] as? Double
        {
            self.additional_distance = Float(additional_distance)
        }
        
        if let additional_fee = data[KBOrder.Key_AdditionalFee] as? Double
        {
            self.additional_fee = Float(additional_fee)
        }
        
        
        if let total = data[KBOrder.Key_Total] as? Double
        {
            self.total = Float(total)
        }
        
        if let commission = data[KBOrder.Key_Commission] as? Double
        {
            self.commission = Float(commission)
        }
        
        
        if let exchangeRate = data[KBOrder.Key_ExchangeRate] as? String{
            self.exchangeRate = Float(exchangeRate)
        }
        
        if let defaultCurrencySymbol = data[KBOrder.Key_DefaultCurrencySymbol] as? String
        {
            self.defaultCurrencySymbol = defaultCurrencySymbol
        }
        
        if let secondCurrencySymbol = data[KBOrder.Key_SecondCurrencySymbol] as? String
        {
            self.secondCurrencySymbol = secondCurrencySymbol
        }
        if let isReservation = data[KBOrder.Key_IS_RESERVATION] as? Int{
            if(isReservation == 0){
                self.isReservation = false
            }else if(isReservation == 1) {
                self.isReservation = true
            }
        }
        if let reservationPickupTime = data[KBOrder.KEY_RESERVATION_PICKUP_TIME] as? Int64{
            self.reservationPickupTime = reservationPickupTime
        }
        if let customerNote = data[KBOrder.KEY_CUSTOMER_NOTE] as? String{
            self.customerNote = customerNote
        }
        
    }
    
    
    
    func getStartCoordinate() -> CLLocation? {
        
        
        if(self.startCoordinate?.count == 0)
        {
            return nil
        }
        
        
        if(self.startCoordinate?.contains(",") == false)
        {
            return nil
        }
        
        
        let coordinateData = self.startCoordinate?.split(separator: ",").map(String.init)
        
        if(coordinateData == nil)
        {
            return nil
        }
        
        if((coordinateData?.count)! <= 1)
        {
            return nil
        }
        
        let latitude = coordinateData?[0].floatValue
        let longitude = coordinateData?[1].floatValue
        
        return CLLocation(latitude: CLLocationDegrees(latitude!),
                          longitude: CLLocationDegrees(longitude!))
        
        
        
    }
    
    
    
    func getDestinationCoordinate() -> CLLocation? {
        
        if(self.destinationCoordinate?.count == 0)
        {
            return nil
        }
        
        
        if(self.destinationCoordinate?.contains(",") == false)
        {
            return nil
        }
        
        
        let coordinateData = self.destinationCoordinate?.split(separator: ",").map(String.init)
        
        if(coordinateData == nil)
        {
            return nil
        }
        
        if((coordinateData?.count)! <= 1)
        {
            return nil
        }
        
        let latitude = coordinateData?[0].floatValue
        let longitude = coordinateData?[1].floatValue
        
        return CLLocation(latitude: CLLocationDegrees(latitude!),
                          longitude: CLLocationDegrees(longitude!))
        
        
        
    }
    func getAdditionalDistance() -> (Bool, Float, Float, CLLocation?) {
        let shouldCount = UserDefaults.standard.object(forKey: self.orderNumber! + "_" + KBOrder.KEY_SHOULD_COUNT_ADDITIONAL_DISTANCE) as? Bool ?? false
        let distance = UserDefaults.standard.object(forKey: self.orderNumber! + "_" + KBOrder.KEY_CURRENT_ADDITIONAL_DISTANCE) as? Float ?? 0
        let fee = UserDefaults.standard.object(forKey: self.orderNumber! + "_" + KBOrder.KEY_CURRENT_ADDITIONAL_FEE) as? Float ?? 0
        var prevLocation:CLLocation? = nil
        if let prevLocationString = UserDefaults.standard.object(forKey: self.orderNumber! + "_" + KBOrder.KEY_CURRENT_ADDITIONAL_PREV_LOCATION) as? String{
            prevLocation = prevLocationString.toCLLocation()
        }
        return (shouldCount, distance, fee, prevLocation)
    }
    func setAdditionalDistance(shouldCount aShouldCount:Bool,
                               distance aDistance:Float,
                               fee aFee:Float,
                               prevLocation aPrevLocation:String?){
        UserDefaults.standard.set(aShouldCount, forKey: self.orderNumber! + "_" + KBOrder.KEY_SHOULD_COUNT_ADDITIONAL_DISTANCE)
        UserDefaults.standard.set(aDistance, forKey: self.orderNumber! + "_" + KBOrder.KEY_CURRENT_ADDITIONAL_DISTANCE)
        UserDefaults.standard.set(aFee, forKey: self.orderNumber! + "_" + KBOrder.KEY_CURRENT_ADDITIONAL_FEE)
        UserDefaults.standard.set(aPrevLocation, forKey: self.orderNumber! + "_" + KBOrder.KEY_CURRENT_ADDITIONAL_PREV_LOCATION)
        UserDefaults.standard.synchronize()
    }
    func removeAdditionalDistance(){
        UserDefaults.standard.removeObject(forKey: self.orderNumber! + "_" + KBOrder.KEY_SHOULD_COUNT_ADDITIONAL_DISTANCE)
        UserDefaults.standard.removeObject(forKey: self.orderNumber! + "_" + KBOrder.KEY_CURRENT_ADDITIONAL_DISTANCE)
        UserDefaults.standard.removeObject(forKey: self.orderNumber! + "_" + KBOrder.KEY_CURRENT_ADDITIONAL_FEE)
        UserDefaults.standard.removeObject(forKey: self.orderNumber! + "_" + KBOrder.KEY_CURRENT_ADDITIONAL_PREV_LOCATION)
        UserDefaults.standard.synchronize()
    }
}



extension KBOrder: Equatable {
    static func ==(left: KBOrder, right: KBOrder) -> Bool {
        if(left.orderNumber != right.orderNumber){
            return false
        }
        if(left.customerID != right.customerID){
            return false
        }
        if(left.driverID != right.driverID){
            return false
        }
        if(left.orderStatus != right.orderStatus){
            return false
        }
        if(left.startAddress != right.startAddress){
            return false
        }
        if(left.startCoordinate != right.startCoordinate){
            return false
        }
        if(left.destinationAddress != right.destinationAddress){
            return false
        }
        if(left.destinationCoordinate != right.destinationCoordinate){
            return false
        }
        if(left.numberOfPassengers != right.numberOfPassengers){
            return false
        }
        if(left.isReservation != right.isReservation){
            return false
        }
        if(left.reservationPickupTime != right.reservationPickupTime){
            return false
        }
        if(left.customerNote != right.customerNote){
            return false
        }
        return true
    }
}
