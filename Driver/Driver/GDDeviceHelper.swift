//
//  GDDeviceHelper.swift
//  Driver
//
//  Created by ZY on 2017-04-09.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class KBDeviceHelper{
    static let Width_Screen_3_5:CGFloat = 320
    static let Height_Screen_3_5:CGFloat = 480    
    static let Width_Screen_4_0:CGFloat = 320
    static let Height_Screen_4_0:CGFloat = 568
    static func getSystemVersion() -> String {
        return UIDevice.current.systemVersion
    }
    static var screenSize:CGSize{
        return UIScreen.main.bounds.size
    }
}
