//
//  KBReservationPoolManager.swift
//  Driver
//
//  Created by Kabu on 2017-11-22.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBReservationPoolManager: NSObject {
    static let shared = KBReservationPoolManager()
    var reservationOrders = [KBOrder](){
        didSet{
            var oldOrderIDs = ""
            //let sortedOldValue = oldValue.sorted { $0.orderNumber!.localizedCaseInsensitiveCompare($1.orderNumber!) == ComparisonResult.orderedAscending }
            for currentValue in oldValue{
                oldOrderIDs += currentValue.orderNumber!
                if(currentValue !== oldValue.last){
                    oldOrderIDs += ","
                }
            }
            var newOrderIDs = ""
            //let sortedOpenOrders = openOrders.sorted { $0.orderNumber!.localizedCaseInsensitiveCompare($1.orderNumber!) == ComparisonResult.orderedAscending }
            for currentOrder in reservationOrders{
                newOrderIDs += currentOrder.orderNumber!
                if(currentOrder !== reservationOrders.last){
                    newOrderIDs += ","
                }
            }
            
            if(oldOrderIDs != newOrderIDs){
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name.KB_ReservationPool_OrderChanged,
                                                    object: nil)
                }
            }else{
                for currentOrder in self.reservationOrders{
                    for currentOldOrder in oldValue{
                        if(currentOrder.orderNumber != currentOldOrder.orderNumber){
                            continue
                        }
                        if(currentOrder != currentOldOrder){
                            print("Order Data Changed")
                            DispatchQueue.main.async {
                                NotificationCenter.default.post(name: NSNotification.Name.KB_ReservationPool_OrderDataChanged,
                                                                object: nil)
                            }

                        }
                    }
                }
            }
        }
    }
    var isDownloadingReservationPool = false{
        didSet{
            if(self.isDownloadingReservationPool != oldValue){
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name.KB_ReservationPool_DownloadingStatusChanged, object: nil)
                }
            }
        }
    }
    var reservationPoolError:String? {
        didSet{
            if(self.reservationPoolError != oldValue){
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name.KB_ReservationPool_ErrorChanged, object: nil)
                }
            }
        }
    }
    var reservationPoolLastUpdateTime:Date?{
        didSet{
            if(self.reservationPoolLastUpdateTime != oldValue){
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name.KB_ReservationPool_LastUpdateTimeChanged, object: nil)
                }
            }
        }
    }
    override init() {
        super.init()
        NotificationCenter.addObserver(self,
                                       action: #selector(userLoggedOut),
                                       name: NSNotification.Name.GD_Me_LoggedOut)
    }
    func downloadReservationPool(start aStart:(() -> ())?,
                                 success aSuccess:@escaping(() -> ()),
                                 failure aFailure:@escaping((_ aError:NSError) -> ())){
        if(KBAccountManager.shared.isLoggedIn() == false){
            self.reservationOrders.removeAll()
            self.reservationPoolLastUpdateTime = nil
            aFailure(NSError.Error(message: NSLocalizedString("Please Login first", comment: "")))
            return
        }
        /*if(self.isUpdatingReservationPool == true){
            aFailure(NSError.Error(message: "正在更新中"))
            return
        }*/
        let prevAccount = KBAccountManager.shared.getAccount()!
        let prevAccountUID = prevAccount.id
        self.isDownloadingReservationPool = true
        KBOrdersAPI.downloadReservationPool(start: {
            aStart?()
            NSLog("Download reservation pool task fired")
        }, success: { (orders:[KBOrder]) in
            if let currentAccount = KBAccountManager.shared.getAccount(){
                if(currentAccount.id == prevAccountUID){
                    self.reservationOrders = KBOrdersHelper().processReserveOrders(reservedOrders: orders)
                    self.reservationPoolLastUpdateTime = Date()
                }
            }
            self.reservationPoolError = nil
            self.isDownloadingReservationPool = false
            aSuccess()
            NSLog("Download reservation pool success")
        }, failure: { (error:NSError) in
            self.isDownloadingReservationPool = false
            self.reservationOrders = [KBOrder]()
            self.reservationPoolError = error.message
            aFailure(error)
            NSLog("Download reservation pool failure")
        })
    }
    func raceForOrder(orderNumber aOrderNumber:String,
                      start aStart:(() -> ())?,
                      success aSuccess:@escaping(() -> ()),
                      failure aFailure:@escaping((_ aError:NSError) -> ())){
        if(KBAccountManager.shared.isLoggedIn() == false){
            aFailure(NSError.Error(message: NSLocalizedString("Please Login first", comment: "")))
            return
        }
        var orderData = [String:String]()
        if let account = KBAccountManager.shared.getAccount(){
            orderData[KBOrder.Key_DriverID] = String(account.id)
        }
        KBOrdersAPI.updateOrder(orderNumber: aOrderNumber,
                                orderData: orderData,
                                start: {
                                    aStart?()
        }, success: { (orde:KBOrder) in
            aSuccess()
        }) { (error:NSError) in
            aFailure(error)
        }
    }
    @objc func userLoggedOut(){
        self.reservationOrders.removeAll()
    }
    
    func getOpenOrdersPickupTimes() -> [Int64]?{
        let orders = KBOpenOrdersManager.shared.openOrders
        var timestamps = [Int64]()
        for order in orders{
            timestamps.append(order.reservationPickupTime)
        }
        return timestamps
    }
}
