//
//  GDCurrentOrderViewController.swift
//  Driver
//
//  Created by ZY on 2017-04-09.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import GoogleMaps
import Darwin
import MapKit

class KBOpenOrderMapViewController: KBViewController, GDMapViewDelegate {
    let accountVM = KBAccountVM()
    @IBOutlet var orderContainerView: UIView!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var secondCurrentyLabel: UILabel!
    @IBOutlet var startAddressLabel: UILabel!
    @IBOutlet var toAddressLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var ridersCountLabel: UILabel!
    @IBOutlet var orderContainerViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var confirmActionContainerView: UIView!
    @IBOutlet var actionContainerViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var mapContainerView: UIView!
    @IBOutlet var ridersCountRightConstraint: NSLayoutConstraint!
    @IBOutlet var actionLabel: UILabel!
    @IBOutlet var actionButton: GDButton!
    @IBOutlet var keepDrivingLabel: UILabel!
    @IBOutlet var keepDrivingSwitch: UISwitch!
    @IBOutlet var keepDrivingSeparatorHeightConstraint: NSLayoutConstraint!
    @IBOutlet var keepDrivingHeightConstraint: NSLayoutConstraint!
    @IBOutlet var startAddressButton: UIButton!
    @IBOutlet var destinationButton: UIButton!
    private var updateOrderInfoTimer:Timer?
    var mapView: GDMapView = GDMapView.view()
    var isMapViewMoving = false
    var shouldAnimate = false
    var orderNumber:String = ""
    var rightButtonTapCount = 0
    var openOrderVM = KBOpenOrdersVM()
    var currentOrder:KBOrder?{
        if(self.orderNumber.count == 0){
            return nil
        }
        for currentOrder in self.openOrderVM.openOrders {
            if(currentOrder.orderNumber == self.orderNumber){
                return currentOrder
            }
        }
        return nil
    }
    class func controller() -> KBOpenOrderMapViewController {
        let controller = UIStoryboard(name: "OpenOrderMap", bundle: nil).instantiateViewController(withIdentifier: "KBOpenOrderMapViewController") as! KBOpenOrderMapViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.view.frame.size.width <= KBDeviceHelper.Width_Screen_4_0){
            self.priceLabel?.font = UIFont(name: self.priceLabel.font.fontName, size: 18)
            self.secondCurrentyLabel?.font = UIFont(name: self.secondCurrentyLabel.font.fontName, size: 13)
            
            self.ridersCountRightConstraint?.constant = 8
        }
        
        self.confirmActionContainerView?.layer.cornerRadius = self.confirmActionContainerView.frame.size.height * 0.5
        
        self.mapView.delegate = self
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.mapContainerView?.addSubview(self.mapView)
        if let superview = self.mapView.superview{
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1.0,
                                                       constant: 0))
            
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.trailing,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.trailing,
                                                       multiplier: 1.0,
                                                       constant: 0))
            
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1.0,
                                                       constant: 0))
            
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.bottom,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.bottom,
                                                       multiplier: 1.0,
                                                       constant: 0))
        }

        self.mapView.mapView.isMyLocationEnabled = true
        self.mapView.mapView.settings.rotateGestures = false
        self.updateOrderInfoTimer = Timer.scheduledTimer(withTimeInterval: 1.0,
                                                         repeats: true,
                                                         block: { (timer:Timer) in
                                                            self.updateOrderInfoView()
        })
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mapView.viewWillAppear()
        self.updateOrderInfoView()
        self.updateMapView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.shouldAnimate = true
        self.updateMapView()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.confirmActionContainerView?.layer.cornerRadius = self.confirmActionContainerView.frame.size.height * 0.5
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func rightButtonTapped(_ sender: Any) {
        self.rightButtonTapCount += 1
        if(self.rightButtonTapCount >= 5){
            self.rightButtonTapCount = 0;
            if(GDDebugManager.shared.isLocked() == false){
                let controller = GDDebugNavigationController.controller()
                self.present(controller,
                             animated: true,
                             completion: {
                })
                
                return
            }
            
            //1. Create the alert controller.
            let alert = UIAlertController(title: NSLocalizedString("Kabu Ride Service", comment: ""), message: NSLocalizedString("Please Enter Password", comment: ""), preferredStyle: .alert)
            
            //2. Add the text field. You can configure it however you need.
            alert.addTextField { (textField) in
                
            }
            
            alert.textFields?[0].isSecureTextEntry = true
            
            // 3. Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { [weak alert] (_) in
                
                let password = alert?.textFields![0].text // Force unwrapping because we know it exists.
                
                if(password != GDDebugManager.shared.getDebugPageLoginPassword())
                {
                    
                    self.showError(NSLocalizedString("The Password is Incorrect", comment: ""))
                    
                    return
                    
                }
                
                GDDebugManager.shared.enteredPassword = password!
                let controller = GDDebugNavigationController.controller()
                self.present(controller,
                             animated: true,
                             completion: {
                                
                })
                
                
            }))
            
            
            
            // 3. Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel))
            
            
            // 4. Present the alert.
            self.present(alert, animated: true, completion: nil)
            
            
            
        }
        
    }
    func updateOrderInfoView(){
        let order = self.currentOrder
        DispatchQueue.main.async {
            if(order == nil){
                self.dismiss(animated: true,
                             completion: nil)
                return
            }
            if(self.openOrderVM.containsOrder(orderNumber: (order?.orderNumber)!) == false){
                self.dismiss(animated: true,
                             completion: nil)
                return
            }
            self.priceLabel?.text = String(format:"%@%.0f", order!.defaultCurrencySymbol, order!.total)
            if(order!.exchangeRate == nil){
                self.secondCurrentyLabel?.text = ""
            }else{
                let secondPrice = order!.total * order!.exchangeRate!
                self.secondCurrentyLabel?.text = "(" + order!.secondCurrencySymbol + String(format:"%.0f", secondPrice) + ")"
            }
            self.startAddressLabel?.text = order!.startAddress
            self.toAddressLabel?.text = order!.destinationAddress
            self.phoneLabel?.text = order!.contactPhone?.toPhoneFormat()
            self.ridersCountLabel?.text = String(order!.numberOfPassengers)
            self.updateAdditionalTravelUI()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            self.actionLabel?.text = ""
            if let orderStatus = order!.orderStatus{
                if(order!.orderStatus == GDOrderStatus.OrderArrived){
                    if(self.keepDrivingSeparatorHeightConstraint != nil && self.keepDrivingSeparatorHeightConstraint.constant != 44){
                        self.keepDrivingSeparatorHeightConstraint.constant = 1
                        self.keepDrivingHeightConstraint?.constant = 44
                        UIView.animate(withDuration: 0.3) {
                            self.view.layoutIfNeeded()
                        }
                    }
                }else{
                    if(self.keepDrivingSeparatorHeightConstraint != nil && self.keepDrivingSeparatorHeightConstraint.constant != 0){
                        self.keepDrivingSeparatorHeightConstraint.constant = 0
                        self.keepDrivingHeightConstraint?.constant = 0
                        UIView.animate(withDuration: 0.3) {
                            self.view.layoutIfNeeded()
                        }
                    }
                }
                if(orderStatus == GDOrderStatus.OrderPlaced){
                    self.actionLabel?.text = NSLocalizedString("Go Now", comment: "")
                }
                else if(orderStatus == GDOrderStatus.DriverConfirmed){
                    self.actionLabel?.text = NSLocalizedString("Travel Route", comment: "Route for the ride after picking up the passenger")
                }
                else if(orderStatus == GDOrderStatus.OrderStarted){
                    self.actionLabel?.text = NSLocalizedString("Safely Arrived", comment: "")
                }
                else if(orderStatus == GDOrderStatus.OrderArrived){
                    self.actionLabel?.text = NSLocalizedString("Finish Order", comment: "")
                }
            }
            //self.setFullScreenMapViewEnabled(false)
        }
    }
    func updateMapView(){
        let order = self.currentOrder
        if(order == nil){
            return
        }
        self.mapView.mapView.clear()
        if let coordinate = order!.getStartCoordinate(){
            let coordinate_Start = CLLocationCoordinate2D(latitude: coordinate.coordinate.latitude,
                                                          longitude: coordinate.coordinate.longitude)
            let marker_StartAddress = GMSMarker(position: coordinate_Start)
            marker_StartAddress.icon = UIImage.init(named: "ic-location-2")
            marker_StartAddress.map = self.mapView.mapView
        }
        if let coordinate = order!.getDestinationCoordinate(){
            let position_Destination = CLLocationCoordinate2D(latitude: coordinate.coordinate.latitude,
                                                              longitude: coordinate.coordinate.longitude)
            let marker_Destination = GMSMarker(position: position_Destination)
            marker_Destination.icon = UIImage.init(named: "ic-location-3")
            marker_Destination.map = self.mapView.mapView
        }
        var locations = [CLLocationCoordinate2D]()
        if let coordinate = GDLocationManager.shared.currentLocation{
            locations.append(coordinate.coordinate)
        }
        if let coordinate = order!.getStartCoordinate(){
            locations.append(coordinate.coordinate)
        }
        if let coordinate = order!.getDestinationCoordinate(){
            locations.append(coordinate.coordinate)
        }
        self.mapView.animateToCamera(locations: locations, padding: 100.0)
        if(order!.orderStatus == GDOrderStatus.OrderPlaced
            || order!.orderStatus == GDOrderStatus.DriverConfirmed){
            if let fromLocation = order!.getStartCoordinate(){
                if let driverLocation = KBAccountManager.shared.getAccount()?.lastLocation(){
                    GoogleDirectionsAPIHelper.downloadBestDistance(startLocation: driverLocation,
                                                                   endLocation: fromLocation,
                                                                   useShortestRoute: false,
                                                                   start: {
                    }, success: { (route:[CLLocationCoordinate2D]) in
                        if(route.count > 0){
                            self.mapView.setRoute(points: route)
                        }
                    }, failure: { (error:NSError) in
                    })
                }
            }
        }else if(order!.orderStatus == GDOrderStatus.OrderStarted){
            if let toLocation = order!.getDestinationCoordinate(){
                if let driverLocation = KBAccountManager.shared.getAccount()?.lastLocation(){
                    GoogleDirectionsAPIHelper.downloadBestDistance(startLocation: driverLocation,
                                                                   endLocation: toLocation,
                                                                   useShortestRoute: true,
                                                                   start: {
                    }, success: { (route:[CLLocationCoordinate2D]) in
                        if(route.count > 0){
                            self.mapView.setRoute(points: route)
                        }
                    }, failure: { (error:NSError) in
                    })
                }
            }
        }
    }
    func updateAdditionalTravelUI() {
        let order = self.currentOrder
        if(order == nil){
            return
        }
        let (shouldCount, distance, fee, _) = order!.getAdditionalDistance()
        if(shouldCount == false){
            self.keepDrivingLabel?.text = NSLocalizedString("Continue Driving", comment: "")
            if(self.keepDrivingSwitch != nil && self.keepDrivingSwitch.isOn){
                self.keepDrivingSwitch?.setOn(false, animated: true)
            }
            self.priceLabel?.text = String(format:"$%.0f", order!.total)
            self.secondCurrentyLabel?.text = ""
            if(order!.exchangeRate != nil){
                let secondPrice = order!.total * order!.exchangeRate!
                self.secondCurrentyLabel?.text = "(" + order!.secondCurrencySymbol + String(format:"%.0f", secondPrice) + ")"
            }
            return
        }
        if(self.keepDrivingSwitch?.isOn == false){
            self.keepDrivingSwitch?.setOn(true, animated: true)
        }
        self.keepDrivingLabel?.text = String(format:"%.1f km", distance) + "      " + String(format:"$ %0.f", fee)
        if(shouldCount == true){
            self.priceLabel?.text = String(format:"$%.0f+$%.0f", order!.total, fee)
            self.secondCurrentyLabel?.text = ""
            if(order!.exchangeRate != nil){
                let secondPrice = (order!.total + fee) * order!.exchangeRate!
                self.secondCurrentyLabel?.text = "(" + order!.secondCurrencySymbol + String(format:"%.0f", secondPrice) + ")"
            }
            
        }
    }
    
    
    @IBAction func switchValueChanged(_ sender: Any) {
        let order = self.currentOrder
        if(order == nil){
            return
        }
        let (_, distance, fee, prevLocation) = order!.getAdditionalDistance()
        if(self.keepDrivingSwitch.isOn){
            order!.setAdditionalDistance(shouldCount: self.keepDrivingSwitch.isOn,
                                         distance: distance,
                                         fee: fee,
                                         prevLocation: prevLocation?.stringValue())
        }else{
            order!.setAdditionalDistance(shouldCount: self.keepDrivingSwitch.isOn,
                                         distance: distance,
                                         fee: fee,
                                         prevLocation: nil)
        }
        self.updateAdditionalTravelUI()
    }
    @IBAction func makePhoneCallButtonTapped(_ sender: Any) {
        self.phoneLabel?.becomeFirstResponder()
        let copy = UIMenuItem.init(title: NSLocalizedString("Copy", comment: ""),
                                   action: #selector(copyCurrentOrderPhone))
        let call = UIMenuItem.init(title: NSLocalizedString("Call", comment: ""),
                                   action: #selector(callCurrentOrderPhone))
        let cancel = UIMenuItem.init(title: NSLocalizedString("Cancel", comment: ""),
                                     action: #selector(cancelMenu))
        UIMenuController.shared.menuItems = [copy, call, cancel]
        
        let rect = CGRect(x: 0,
                          y: 0,
                          width: 50,
                          height: 50)
        UIMenuController.shared.setTargetRect(rect, in: self.phoneLabel)
        UIMenuController.shared.setMenuVisible(true, animated: true)
    }
    @IBAction func buttonTapped(_ sender: Any) {
        if(sender as! UIButton === self.startAddressButton){
            if let currentOrder = self.currentOrder{
                if let startCoordinate = currentOrder.getStartCoordinate(){
                    if let driverLocation = KBAccountManager.shared.getAccount()?.lastLocation(){
                        GoogleDirectionsAPIHelper.downloadBestDistance(startLocation: driverLocation,
                                                                       endLocation: startCoordinate,
                                                                       useShortestRoute: false,
                                                                       start: {
                        }, success: { (route:[CLLocationCoordinate2D]) in
                            if(route.count > 0){
                                self.mapView.setRoute(points: route)
                                let locations = [startCoordinate.coordinate,
                                                 driverLocation.coordinate]
                                self.mapView.animateToCamera(locations: locations, padding: 100.0)
                            }
                        }, failure: { (error:NSError) in
                        })
                    }
                }
                self.startAddressLabel?.becomeFirstResponder()
                let copy = UIMenuItem.init(title: NSLocalizedString("Copy", comment: ""),
                                             action: #selector(copyFromAddress))
                let inAppNavigateOption = UIMenuItem.init(title: NSLocalizedString("Navigation", comment: ""),
                                           action: #selector(inAppNavigateFromAddress))
                let navigateWithGoogleMaps = UIMenuItem.init(title: "Google " + NSLocalizedString("Navigation", comment: ""), action: #selector(navigateFromAddressWithGoogleMaps))
                let navigateWithAppleMaps = UIMenuItem.init(title: "Apple " + NSLocalizedString("Navigation", comment: ""), action: #selector(navigateFromAddressWithAppleMaps))
                let cancel = UIMenuItem.init(title: NSLocalizedString("Close", comment: ""),
                                             action: #selector(cancelMenu))
                UIMenuController.shared.menuItems = [copy,
                                                     inAppNavigateOption,
                                                     navigateWithGoogleMaps,
                                                     navigateWithAppleMaps,
                                                     cancel]
                let rect = CGRect(x: 0,
                                  y: 0,
                                  width: 50,
                                  height: 50)
                UIMenuController.shared.setTargetRect(rect, in: self.startAddressLabel)
                UIMenuController.shared.setMenuVisible(true, animated: true)
            }
            return
        }
        if(sender as! UIButton === self.destinationButton){
            if let currentOrder = self.currentOrder{
                if let destinationCoordinate = currentOrder.getDestinationCoordinate(){
                    if let driverLocation = KBAccountManager.shared.getAccount()?.lastLocation(){
                        GoogleDirectionsAPIHelper.downloadBestDistance(startLocation: driverLocation,
                                                                       endLocation: destinationCoordinate,
                                                                       useShortestRoute: true,
                                                                       start: {
                        }, success: { (route:[CLLocationCoordinate2D]) in
                            if(route.count > 0){
                                self.mapView.setRoute(points: route)
                                let locations = [destinationCoordinate.coordinate,
                                                 driverLocation.coordinate]
                                self.mapView.animateToCamera(locations: locations, padding: 100.0)
                            }
                        }, failure: { (error:NSError) in
                        })
                    }
                }
                self.toAddressLabel?.becomeFirstResponder()
                let copy = UIMenuItem.init(title: NSLocalizedString("Copy", comment: ""),
                                           action: #selector(copyToAddress))
                let inAppNavigateOption = UIMenuItem.init(title: NSLocalizedString("Navigation", comment: ""),
                                                          action: #selector(inAppNavigateToAddress))
                let navigateWithGoogleMaps = UIMenuItem.init(title: "Google " + NSLocalizedString("Navigation", comment: ""),
                                                             action: #selector(navigateToAddressWithGoogleMaps))
                let navigateWithAppleMaps = UIMenuItem.init(title: "Apple" + NSLocalizedString("Navigation", comment: ""),
                                                            action: #selector(navigateToAddressWithAppleMaps))
                let cancel = UIMenuItem.init(title: NSLocalizedString("Close", comment: ""),
                                             action: #selector(cancelMenu))
                UIMenuController.shared.menuItems = [copy,
                                                     inAppNavigateOption,
                                                     navigateWithGoogleMaps,
                                                     navigateWithAppleMaps,
                                                     cancel]
                let rect = CGRect(x: 0,
                                  y: 0,
                                  width: 50,
                                  height: 50)
                UIMenuController.shared.setTargetRect(rect, in: self.toAddressLabel)
                UIMenuController.shared.setMenuVisible(true, animated: true)
            }
            return
        }
        if(sender as! UIButton === self.actionButton){
            self.updateOrderStatus()
        }
    }
    override var canBecomeFirstResponder: Bool{
        return true
    }
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if(action == #selector(copyCurrentOrderPhone)){
            return true
        }
        if(action == #selector(callCurrentOrderPhone)){
            return true
        }
        if(action == #selector(copyFromAddress)){
            return true
        }
        if(action == #selector(copyToAddress)){
            return true
        }
        if(action == #selector(navigateFromAddressWithGoogleMaps)){
            return true
        }
        if(action == #selector(navigateFromAddressWithAppleMaps)){
            return true
        }
        if(action == #selector(navigateToAddressWithGoogleMaps)){
            return true
        }
        if(action == #selector(navigateToAddressWithAppleMaps)){
            return true
        }
        if(action == #selector(cancelMenu)){
            return true
        }
        return super.canPerformAction(action, withSender: sender)
    }
    @objc func cancelMenu(){
    }
    @objc func copyCurrentOrderPhone(){
        if let phone = self.currentOrder?.contactPhone{
            let pasteBoard = UIPasteboard.general
            pasteBoard.string = phone
            showSuccess(NSLocalizedString("Copy Successful", comment: ""))
        }
    }
    @objc func callCurrentOrderPhone(){
        if let phone = self.currentOrder?.contactPhone{
            if let url = NSURL(string: "tel://" + phone){
                if(UIApplication.shared.canOpenURL(url as URL)){
                    UIApplication.shared.open(url as URL,
                                              options: [:],
                                              completionHandler: { (_ complete:Bool) in
                    })
                }
            }
        }
    }
    @objc func copyFromAddress(){
        if let address = self.currentOrder?.startAddress{
            let pasteBoard = UIPasteboard.general
            pasteBoard.string = address
            showSuccess(NSLocalizedString("Copy Successful", comment: ""))
        }
    }
    @objc func copyToAddress(){
        if let address = self.currentOrder?.destinationAddress{
            let pasteBoard = UIPasteboard.general
            pasteBoard.string = address
            showSuccess(NSLocalizedString("Copy Successful", comment: ""))
        }
    }
    @objc func inAppNavigateFromAddress(){
        let order = self.currentOrder
        if order == nil{
            return
        }
        if let fromLocation = order!.getStartCoordinate(){
            if let driverLocation = KBAccountManager.shared.getAccount()?.lastLocation(){
                GoogleDirectionsAPIHelper.downloadBestDistance(startLocation: driverLocation,
                                                               endLocation: fromLocation,
                                                               useShortestRoute: false,
                                                               start: {
                }, success: { (route:[CLLocationCoordinate2D]) in
                    if(route.count > 0){
                        self.mapView.setRoute(points: route)
                        self.mapView.setNavigationEnabled(enabled: true)
                    }
                }, failure: { (error:NSError) in
                })
            }
        }
    }
    @objc func inAppNavigateToAddress(){
        let order = self.currentOrder
        if order == nil{
            return
        }
        if let toLocation = order!.getDestinationCoordinate(){
            if let driverLocation = KBAccountManager.shared.getAccount()?.lastLocation(){
                GoogleDirectionsAPIHelper.downloadBestDistance(startLocation: driverLocation,
                                                               endLocation: toLocation,
                                                               useShortestRoute: true,
                                                               start: {
                }, success: { (route:[CLLocationCoordinate2D]) in
                    if(route.count > 0){
                        self.mapView.setRoute(points: route)
                        self.mapView.setNavigationEnabled(enabled: true)
                    }
                }, failure: { (error:NSError) in
                })
            }
        }
    }
    @objc func navigateFromAddressWithGoogleMaps(){
        let order = self.currentOrder
        if order == nil{
            return
        }
        if let url = URL(string:"comgooglemaps://"){
            if(UIApplication.shared.canOpenURL(url) == true){
                if let coordinate = order!.getStartCoordinate(){
                    if let addressURL = URL(string: "comgooglemaps://?saddr=&daddr=" + coordinate.stringValue() + "&directionsmode=driving"){
                        UIApplication.shared.open(addressURL,
                                                  options: [:],
                                                  completionHandler: { (_ complete:Bool) in
                        })
                    }
                }
            }else{
                self.showError(NSLocalizedString("Cannot Open Google Maps, Please make sure that it's installed", comment: ""))
            }
        }
    }
    @objc func navigateFromAddressWithAppleMaps(){
        let order = self.currentOrder
        if order == nil{
            return
        }
        if let coordinate = order!.getStartCoordinate(){
            let regionDistance:CLLocationDistance = 10000
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinate.coordinate, regionDistance, regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinate.coordinate, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            if let address = order!.startAddress{
                mapItem.name = address
            }
            mapItem.openInMaps(launchOptions: options)
        }
    }
    @objc func navigateToAddressWithGoogleMaps(){
        let order = self.currentOrder
        if order == nil{
            return
        }
        if let url = URL(string:"comgooglemaps://"){
            if(UIApplication.shared.canOpenURL(url) == true){
                if let coordinate = order!.getDestinationCoordinate(){
                    if let addressURL = URL(string: "comgooglemaps://?saddr=&daddr=" + coordinate.stringValue() + "&directionsmode=driving"){
                        UIApplication.shared.open(addressURL,
                                                  options: [:],
                                                  completionHandler: { (_ complete:Bool) in
                        })
                    }
                }
            }else{
                self.showError(NSLocalizedString("Cannot Open Google Maps, Please make sure that it's installed", comment: ""))
            }
        }
    }
    @objc func navigateToAddressWithAppleMaps(){
        let order = self.currentOrder
        if order == nil{
            return
        }
        if let coordinate = order!.getDestinationCoordinate(){
            let regionDistance:CLLocationDistance = 10000
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinate.coordinate, regionDistance, regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinate.coordinate, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            if let address = order!.destinationAddress{
                mapItem.name = address
            }
            mapItem.openInMaps(launchOptions: options)
        }
    }
    //MARK: - GMSMapViewDelegate
    func mapView(_ mapView: GDMapView, didTap marker: GMSMarker) -> Bool {
        return true
    }
    func mapView(_ mapView: GDMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if(mapView === self.mapView){
            if(self.isMapViewMoving == false){
                self.setFullScreenMapViewEnabled(!self.isFullscreenMapEnabled())
            }
        }
    }
    
    func mapView(_ mapView: GDMapView, willMove gesture: Bool) {
        if(mapView === self.mapView)
        {
            self.isMapViewMoving = true
        }
    }
    
    
    func mapView(_ mapView: GDMapView, idleAt position: GMSCameraPosition) {
        
        if(mapView === self.mapView)
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            
                self.isMapViewMoving = false
                
            })
            
            
        }
        
    }
    
    func mapView(_ mapView: GDMapView, didChange position: GMSCameraPosition) {
        
    }
    
    func mapViewDidStartNavigation(_ mapView: GDMapView) {
        
        if(mapView === self.mapView)
        {
            self.setFullScreenMapViewEnabled(true)
        }
        
    }
    
    func mapViewDidEndNavigation(_ mapView: GDMapView) {
        
        if(mapView === self.mapView)
        {
            self.setFullScreenMapViewEnabled(false)
        }
        
    }
    
    // GMSMapViewDelegate ends
    
    func setFullScreenMapViewEnabled(_ enabled:Bool){
    
        if(enabled == true){
            if let _ = self.orderContainerView{
                if(self.orderContainerViewTopConstraint?.constant != -self.orderContainerView.frame.size.height)
                {
                    self.orderContainerViewTopConstraint?.constant = -self.orderContainerView.frame.size.height
                    self.actionContainerViewBottomConstraint?.constant = self.confirmActionContainerView.frame.size.height * -1 - 16
                    
                    UIView.animate(withDuration: 0.3) {
                        self.view.layoutIfNeeded()
                    }
                }
            }
            
        }
        else
        {
            
            if(self.orderContainerViewTopConstraint?.constant != 0)
            {
                self.orderContainerViewTopConstraint?.constant = 0
                self.actionContainerViewBottomConstraint?.constant = 16
                
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    func isFullscreenMapEnabled() -> Bool{
    
        return !(self.orderContainerViewTopConstraint?.constant == 0)
        
    }
    
    
    func updateOrderStatus(){
        let order = self.currentOrder
        if(order == nil){
            return
        }
        if(order!.orderStatus == GDOrderStatus.OrderPlaced){
            let orderData = [KBOrder.Key_OrderStatus : String(GDOrderStatus.DriverConfirmed.rawValue)]
            self.openOrderVM.updateOrder(orderNumber: order?.orderNumber ?? "",
                                         orderData: orderData,
                                         startCallback: {
                                            self.showLoading("")
            }, successCallback: { (order:KBOrder) in
                KBOpenOrdersManager.shared.downloadOpenOrders(start: nil,
                                                              success: {
                                                                self.dismissLoading()
                }, failure: { (error) in
                    self.showError(error.message)
                })
            }, failureCallback: { (error:NSError) in
                self.showError(error.message)
            })
            return
        }
        if(order!.orderStatus == GDOrderStatus.DriverConfirmed){
            let orderData = [KBOrder.Key_OrderStatus : String(GDOrderStatus.OrderStarted.rawValue)]
            self.openOrderVM.updateOrder(orderNumber: order?.orderNumber ?? "",
                                         orderData: orderData,
                                         startCallback: {
                                            self.showLoading("")
            }, successCallback: { (order:KBOrder) in
                KBOpenOrdersManager.shared.downloadOpenOrders(start: nil,
                                                              success: {
                                                                self.dismissLoading()
                }, failure: { (error) in
                    self.showError(error.message)
                })
            }, failureCallback: { (error:NSError) in
                self.showError(error.message)
            })
            return
        }
        if(order!.orderStatus == GDOrderStatus.OrderStarted){
            let orderData = [KBOrder.Key_OrderStatus : String(GDOrderStatus.OrderArrived.rawValue)]
            self.openOrderVM.updateOrder(orderNumber: order?.orderNumber ?? "",
                                         orderData: orderData,
                                         startCallback: {
                                            self.showLoading("")
            }, successCallback: { (order:KBOrder) in
                KBOpenOrdersManager.shared.downloadOpenOrders(start: nil,
                                                              success: {
                                                                self.dismissLoading()
                }, failure: { (error) in
                    self.showError(error.message)
                })
            }, failureCallback: { (error:NSError) in
                self.showError(error.message)
            })
            return
        }
        if(order!.orderStatus == GDOrderStatus.OrderArrived){
            let alert = UIAlertController(title: NSLocalizedString("Confirm Finishing Order?", comment: ""),
                                          message: nil,
                                          preferredStyle: .actionSheet)
            let OKAction = UIAlertAction(title: NSLocalizedString("Confirm", comment: ""),
                                         style: UIAlertActionStyle.default,
                                         handler: { (_ action:UIAlertAction) in
                                            self.keepDrivingSwitch?.isOn = false
                                            var orderData:[String:String] = [KBOrder.Key_OrderStatus : String(GDOrderStatus.OrderComplete.rawValue)]
                                            if(GDLocationManager.shared.currentLocation != nil){
                                                orderData[KBOrder.Key_FinalLatlng] = GDLocationManager.shared.currentLocation?.stringValue()
                                            }
                                            let (shouldCount, distance, fee, _) = order!.getAdditionalDistance()
                                            if(shouldCount == true){
                                                orderData[KBOrder.Key_AdditionalDistance] = String(distance)
                                                orderData[KBOrder.Key_AdditionalFee] = String(fee)
                                            }else{
                                                orderData[KBOrder.Key_AdditionalDistance] = String(0)
                                                orderData[KBOrder.Key_AdditionalFee] = String(0)
                                            }
                                            self.openOrderVM.updateOrder(orderNumber: order?.orderNumber ?? "",
                                                                         orderData: orderData,
                                                                         startCallback: {
                                                                            self.showLoading("")
                                            }, successCallback: { (order:KBOrder) in
                                                order.removeAdditionalDistance()
                                                KBOpenOrdersManager.shared.downloadOpenOrders(start: nil,
                                                                                              success: {
                                                }, failure: { (error) in
                                                })
                                                if let currentLocation = GDLocationManager.shared.currentLocation{
                                                    let currentLocationString = currentLocation.stringValue()
                                                    let driverData = [KBDriver.Key_LastLoginCoordinate:currentLocationString]
                                                    self.accountVM.updateAccount(data: driverData,
                                                                                 start: {
                                                    }, success: { (driver:KBDriver) in
                                                        self.dismissLoading()
                                                        self.dismiss(animated: true, completion: nil)
                                                    }, failure: { (error:NSError) in
                                                        print("Driver Update failed")
                                                        self.dismissLoading()
                                                        self.dismiss(animated: true, completion: nil)
                                                    })
                                                }else{
                                                    self.dismissLoading()
                                                    self.dismiss(animated: true, completion: nil)
                                                }
                                            }, failureCallback: { (error:NSError) in
                                                print("Order Update failed")
                                                self.showError(error.message)
                                            })
            })
            alert.addAction(OKAction)
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""),
                                             style: UIAlertActionStyle.cancel,
                                             handler: { (_ action:UIAlertAction) in
            })
            
            alert.addAction(cancelAction)
            self.present(alert,
                         animated: true,
                         completion: {
            })
        }
    }
}
