//
//  GDAccountManager.swift
//  Driver
//
//  Created by ZY on 2017-04-08.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class KBAccountManager : NSObject {
    static let shared = KBAccountManager()
    var isLoggingIn:Bool = false
    var isLoggingOut:Bool = false
    var isValidatingLogin:Bool = false
    var isDownloadingStats = false

    var account:KBDriver?{
        didSet{
            var accountChanged = false
            if(self.account == nil && oldValue != nil){
                accountChanged = true
            }else if(self.account != nil && oldValue == nil){
                accountChanged = true
            }else if(self.getAccount() != oldValue){
                accountChanged = true
            }
            if(self.account == nil){
                let data = NSKeyedArchiver.archivedData(withRootObject: "")
                self.keychain?.setObject(data, forKey: kSecAttrAccount)
            }else{
                let userData = NSKeyedArchiver.archivedData(withRootObject: self.account!)
                self.keychain?.setObject(userData, forKey: kSecAttrAccount)
                if let username = self.account?.username{
                    UserDefaults.setLastLoginUsername(username: username)
                }
            }
            if(accountChanged == true){
                if(self.account == nil){
                    GDFabricHelper.logUser(uid: 0, username: "")
                    NotificationCenter.default.post(name: NSNotification.Name.GD_Me_LoggedOut,
                                                    object: nil)
                    /*NotificationCenter.default.post(name: NSNotification.Name.GD_Me_LoggedOut,
                                                    object: nil,
                                                    userInfo: [NSNotification.Key_UserInfo_Message:aMessage])*/
                }else{
                    GDFabricHelper.logUser(uid: self.account!.id, username: self.account!.username)
                }
                NotificationCenter.default.post(name: NSNotification.Name.GD_Me_Changed,
                                                object: nil)
            }
            if(self.account != nil && oldValue != nil){
                if(self.account?.is_online != oldValue?.is_online){
                    NotificationCenter.default.post(name: NSNotification.Name.KB_Me_Online_Status_Changed,
                                                    object: nil)
                }
            }
        }
    }
    var keychain:KeychainItemWrapper?
    static let Key_Token:String = "token"
    override init() {
        super.init()
        if(self.keychain == nil){
            self.keychain = KeychainItemWrapper(identifier: "AccountData", accessGroup: nil)
            self.keychain?.setObject("MY_APP_CREDENTIALS", forKey: kSecAttrService)
        }
    }
    func downloadMe(start aStart:(() -> ())?,
                    success aSuccess:(() -> ())?,
                    failed aFailed: ((_ error:NSError) -> ())?) {
        if self.isLoggedIn() == false{
            return
        }
        KBDriversAPI.downloadDriver(driverID: self.account!.id,
                                    start: {
                                        aStart?()
        }, success: { (driver:KBDriver) in
            KBAccountManager.shared.account = driver
            aSuccess?()
        }) { (error:NSError) in
            if((error as NSError).code == HTTPStatusCode.unauthorized.rawValue){
                let message = NSLocalizedString("Please Login again", comment: "")
                KBMeAPI.logout(uid: self.account?.id ?? 0,
                               userAccessToken: self.account?.userAccessToken ?? "",
                               message: message,
                               startCallback: {
                                
                }, successCallback: {
                    
                }, failureCallback: { (error:NSError) in
                    
                })
            }
            else{
                aFailed?(NSError.Error_UnknownError())
            }
        }
    }
    func getLastLoginUserName() -> String {
        if let username = UserDefaults.getLastLoginUsername(){
            return username
        }
        return ""
    }
    func getAccount() -> KBDriver? {
        if(self.account == nil){
            let data = self.keychain?.object(forKey: kSecAttrAccount)
            if(data == nil || data is NSData == false){
                return nil
            }
            let receivedObject = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            if(receivedObject == nil || receivedObject is KBDriver == false){
                return nil
            }
            let account = receivedObject as! KBDriver
            self.account = account
            return account
        }
        return self.account
    }
    func login(_ username:String,
               _ password:String,
               start aStart:(() -> ())?,
               success aSuccess:@escaping () -> (),
               failed aFailed:@escaping (_ error:String) -> ()){
        if(self.isLoggingIn){
            return
        }
        self.isLoggingIn = true
        KBMeAPI.login(username: username,
                      password: password,
                      parameters: [KBDriver.Key_DeviceType:"iOS",
                                   KBDriver.Key_DeviceToken:GDAppManager.shared.deviceToken],
                      start: {
                        aStart?()
        }, success: { (account) in
            self.isLoggingIn = false
            KBAccountManager.shared.account = account
            UserDefaults.setLastLoginUsername(username: account.username)
            aSuccess()
            NotificationCenter.default.post(name: NSNotification.Name.GD_Me_LoggedIn,
                                            object: nil)
        }) { (error) in
            self.isLoggingIn = false
            aFailed(error)
        }
    }
    /*func logout(_ message:String,
                start aStart:() -> (),
                success aSuccess:@escaping() -> (),
                failure aFailure:@escaping (_ error:NSError)->()){
        if(self.isLoggedIn() == false){
            NotificationCenter.default.post(name: NSNotification.Name.GD_Me_LoggedOut,
                                            object: nil,
                                            userInfo: nil)
            aSuccess()
            return
        }
        if(self.isLoggingOut){
            return
        }
        self.isLoggingOut = true
        aStart()
        let url = GDAppManager.shared.getURL_Logout()
        let manager = AFHTTPSessionManager.newInstance()
        manager.post(url,
                     parameters: nil,
                     progress: nil,
                     success: { (task:URLSessionDataTask, responseObject:Any?) in
                        self.isLoggingOut = false
                        self.account = nil
                        NotificationCenter.default.post(name: NSNotification.Name.GD_Me_LoggedOut,
                                                        object: nil,
                                                        userInfo: [NSNotification.Key_UserInfo_Message:message])
                        aSuccess()
        }) { (task, error) in
            self.isLoggingOut = false
            aFailure(NSError.Error_UnknownError())
        }
    }*/
    func resetPassword(_ username:String,
                       _ phone:String,
                       start aStart:@escaping(() -> ()),
                       success aSuccess:@escaping(_ message:String) -> (),
                       failure aFailure:@escaping(_ error:NSError) -> ()) {
        if(username.count == 0){
            aFailure(NSError.Error(message: NSLocalizedString("Please enter your Account Name" , comment: "")))
            return
        }
        if(phone.count == 0){
            aFailure(NSError.Error(message: NSLocalizedString("Please enter your Phone Number" , comment: "")))
            return
        }
        KBMeAPI.resetPassword(username: username,
                              phone: "1" + phone,
                              start: {
                                aStart()
        }, success: { (message) in
            aSuccess(message)
        }) { (error:NSError) in
            aFailure(error)
        }
    }
    func isLoggedIn() -> Bool{
        if(self.getAccount() == nil){
            return false;
        }
        if(self.getAccount()?.username == nil
            || self.getAccount()?.username.count == 0
            || self.getAccount()?.userAccessToken.count == 0){
            return false;
        }
        return true;
    }
    func updateLocation() {
        let currentLocation = GDLocationManager.shared.currentLocation
        if (currentLocation == nil){
            return
        }
        let currentLocationString = currentLocation?.stringValue()
        let driverData = [KBDriver.Key_LastLoginCoordinate:currentLocationString]
        if self.account != nil{
            KBDriversAPI.updateDriver(driverID: (self.account?.id)!,
                                      driverData: driverData as! [String:String],
                                      start: {
                                        
            }, success: { (driver:KBDriver) in
                NSLog("Update my location success")
            }, failure: { (error:NSError) in
                NSLog("Update my location failure")
                NSLog("%@", error.message)
            })
        }
    }
}
