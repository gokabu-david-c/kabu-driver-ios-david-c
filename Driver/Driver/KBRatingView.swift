//
//  KBRatingView.swift
//  Driver
//
//  Created by Kabu on 2017-11-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
//width:height - 100:18
class KBRatingView: UIView {
    @IBOutlet weak var starImageView1: UIImageView!
    @IBOutlet weak var starImageView2: UIImageView!
    @IBOutlet weak var starImageView3: UIImageView!
    @IBOutlet weak var starImageView4: UIImageView!
    @IBOutlet weak var starImageView5: UIImageView!
    var starImageViews:[UIImageView]{
        return [self.starImageView1,
                self.starImageView2,
                self.starImageView3,
                self.starImageView4,
                self.starImageView5]
    }
    var rating = 0.0{
        didSet{
            if(self.rating <= 0.0){
                for currentImageView in self.starImageViews {
                    currentImageView.image = UIImage(named: "ic-star-3")
                }
                return
            }
            if(rating >= 4.75){
                for currentImageView in self.starImageViews {
                    currentImageView.image = UIImage(named: "ic-star-1")
                }
                return
            }
            var goldCount = Int(floor(rating))
            var halfStar = 0
            if(rating - Double(goldCount) >= 0.25 && rating - Double(goldCount) < 0.75){
                halfStar = 1
            }else if(rating - Double(goldCount) >= 0.75){
                goldCount += 1
            }
            if(goldCount > 0){
                for i in 0 ... goldCount - 1{
                    (self.starImageViews[i] as UIImageView).image = UIImage(named: "ic-star-1")
                }
            }
            if(halfStar > 0){
                (self.starImageViews[goldCount] as UIImageView).image = UIImage(named: "ic-star-2")
            }
            if(goldCount + halfStar < 5){
                for i in goldCount + halfStar ... 4{
                    (self.starImageViews[i] as UIImageView).image = UIImage(named: "ic-star-3")
                }
            }
        }
    }
    class func view() -> KBRatingView{
        let cell = Bundle.main.loadNibNamed("KBRatingView",
                                            owner: self,
                                            options: nil)?.first as! KBRatingView
        return cell
    }
}
