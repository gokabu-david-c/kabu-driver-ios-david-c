//
//  KBMeAPIHelper.swift
//  Driver
//
//  Created by ZY on 2017-08-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class KBMeAPI: KBAPI {
    static func login(username aUsername:String,
                      password aPassword:String,
                      parameters aParameters:[String:Any],
                      start aStart:(() -> ())?,
                      success aSuccess:@escaping(_ account:KBDriver) -> (),
                      failure aFailure:@escaping(_ error:String) -> ()){
        aStart?()
        let url = URL_Login(username: aUsername, password: aPassword)
        let manager = AFHTTPSessionManager.newInstance()
        manager.post(url,
                     parameters: aParameters,
                     progress: nil,
                     success: { (task, responseObject) in
                        let r = GDResponse.init(json: responseObject as! [String : Any])
                        if(r.isOK() == false){
                            let message = r.message ?? NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(message)
                            return
                        }
                        let driver = KBDriver.init(json: r.data as! [String : Any])
                        aSuccess(driver)
        }) { (task, error) in
            aFailure(NSLocalizedString("An Unknown Error has occurred", comment: ""))
        }
    }
    static func logout(uid aUID:Int,
                       userAccessToken aUserAccessToken:String,
                       message aMessage:String,
                       startCallback aStartCallback:(() -> ())?,
                       successCallback aSuccessCallback:@escaping() -> (),
                       failureCallback aFailureCallback:@escaping (_ error:NSError)->()){
        aStartCallback?()
        let url = URL_Logout(uid: aUID, userAccessToken: aUserAccessToken)
        let manager = AFHTTPSessionManager.newInstance()
        manager.post(url,
                     parameters: nil,
                     progress: nil,
                     success: { (task:URLSessionDataTask, responseObject:Any?) in
                        KBAccountManager.shared.account = nil
                        aSuccessCallback()
        }) { (task, error) in
            aFailureCallback(NSError.Error_UnknownError())
        }
    }
    static func resetPassword(username aUsername:String,
                              phone aPhone:String,
                              start aStart:(() -> ())?,
                              success aSuccess:@escaping(_ message:String) -> (),
                              failure aFailure:@escaping(_ error:NSError) -> ()){
        aStart?()
        let url = URL_ResetPassword(username: aUsername, phone: aPhone)
        let manager = AFHTTPSessionManager.newInstance()
        manager.post(url,
                     parameters: nil,
                     progress: { (progress) in
                        
        }, success: { (task, responseObject) in
            let r = GDResponse.init(json: responseObject as! [String : Any])
            if(r.isOK() == false){
                let message = r.message ?? NSLocalizedString("An Unknown Error has occurred", comment: "")
                aFailure(NSError.Error(message: message))
                return
            }
            let message = r.message ?? NSLocalizedString("Your new password will be sent to you via text, please check your text messages", comment: "")
            aSuccess(message)
        }) { (task, error) in
            aFailure(NSError.Error(message: NSLocalizedString("An Unknown Error has occurred", comment: "")))
        }
    }
}
