//
//  CLLocationExtension.swift
//  Driver
//
//  Created by ZY on 2017-04-29.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

extension CLLocation
{
    func stringValue() -> String {
        
        return String(self.coordinate.latitude) + "," + String(self.coordinate.longitude)
        
    }
}
