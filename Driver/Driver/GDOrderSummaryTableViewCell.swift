//
//  GDOrderSummaryTableViewCell.swift
//  Driver
//
//  Created by ZY on 2017-05-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDOrderSummaryTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var startAddressLabel: UILabel!
    
    @IBOutlet weak var destinationLabel: UILabel!
    
    @IBOutlet weak var commissionLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    
    @IBOutlet weak var separatorImageView: UIImageView!
    
    
    
    class func cell() -> GDOrderSummaryTableViewCell{
        
        let cell = Bundle.main.loadNibNamed("GDOrderSummaryTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! GDOrderSummaryTableViewCell
        
        
        
        
        return cell
    }
    
    
    class var cellIdentifier:String
    {
        return "GDOrderSummaryTableViewCell"
    }

    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.separatorImageView.image = UIImage.image(UIColor.init(red: 217.0/255.0,
                                                                   green: 217.0/255.0,
                                                                   blue: 217.0/255.0,
                                                                   alpha: 1.0),
                                                      size: CGSize(width: 1.0, height: 1.0))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
