//
//  KBMyProfileAvatarTableViewCell.swift
//  Driver
//
//  Created by Kabu on 2017-11-24.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBMyProfileAvatarTableViewCell: UITableViewCell {
    @IBOutlet var avatarImageView: GDAvatarImageView!
    @IBOutlet var titleLabel: UILabel!
    class func cell() -> KBMyProfileAvatarTableViewCell{
        let cell = Bundle.main.loadNibNamed("KBMyProfileAvatarTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! KBMyProfileAvatarTableViewCell
        return cell
    }
    class var reuseIdentifier:String{
        return "KBMyProfileAvatarTableViewCell"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.avatarImageView.borderWidth = 0
    }
}
