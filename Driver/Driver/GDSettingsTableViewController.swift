//
//  GDSettingsTableViewController.swift
//  Driver
//
//  Created by ZY on 2017-07-11.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDSettingsTableViewController: KBViewController {
    let systemVM = KBSystemSettingsVM()
    
    @IBOutlet var tableView: UITableView!
    class func controller() -> GDSettingsTableViewController {
        let controller = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "GDSettingsTableViewController") as! GDSettingsTableViewController
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func reloadCountry(){
        self.tableView.reloadRows(at: [IndexPath(row: 1, section: 1)], with: UITableViewRowAnimation.automatic)
    }
}

extension GDSettingsTableViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.section == 0){
            if(indexPath.row == 0){
                let controller = GDPureWebViewController.controller(urlString: KBConfig.URLString_CustomerSupport, title: NSLocalizedString("Contact Customer Service", comment: ""))
                self.navigationController?.pushViewController(controller, animated: true)
                return
            }
            if(indexPath.row == 1){
                let controller = GDPureWebViewController.controller(urlString: KBConfig.URLString_DriverPolicy, title: NSLocalizedString("Driver Agreement", comment: ""))
                self.navigationController?.pushViewController(controller, animated: true)
                return
            }
            if(indexPath.row == 2){
                let controller = GDPureWebViewController.controller(urlString: KBConfig.URLString_Waiver, title: "")
                self.navigationController?.pushViewController(controller, animated: true)
                return
            }
        }
        if(indexPath.section == 1){
            if(indexPath.row == 0){
                let controller = KBSystemSettingsViewController.controller
                self.navigationController?.pushViewController(controller, animated: true)
                return
            }
            else{
                presenLanguageSelectorVC()
            }
            
        }
        if(indexPath.section == 2){
            if(indexPath.row == 0){
                let alert = UIAlertController(title: NSLocalizedString("Are you sure that you want to log out?", comment: ""),
                                              message: nil,
                                              preferredStyle: UIAlertControllerStyle.actionSheet);
                let action_Logout = UIAlertAction(title: NSLocalizedString("Logout", comment: ""), style: UIAlertActionStyle.destructive, handler: { action in
                    if(KBOpenOrdersManager.shared.openOrders.count > 0){
                        self.showError("Please finish the current order(s) before logging out")
                        return
                    }
                    if let account = KBAccountManager.shared.account{
                        KBMeAPI.logout(uid: account.id,
                                       userAccessToken: account.userAccessToken,
                                       message: "",
                                       startCallback: {
                                        self.showLoading("")
                        }, successCallback: {
                            self.dismissLoading()
                        }, failureCallback: { (error:NSError) in
                            self.showError(error.message)
                        })
                    }
                })
                alert.addAction(action_Logout)
                let defaultAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
                alert.addAction(defaultAction)
                self.present(alert,
                             animated: true,
                             completion: nil)
                return
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 3
        }else if(section == 1){
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            if(indexPath.row == 0){
                let cellIdentifier = "cell"
                var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
                if(cell == nil){
                    cell = UITableViewCell(style: UITableViewCellStyle.value1,
                                           reuseIdentifier: cellIdentifier)
                }
                cell?.textLabel?.text = NSLocalizedString("Contact Customer Service", comment: "")
                cell?.detailTextLabel?.text = ""
                cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                return cell!
            }
            if(indexPath.row == 1){
                let cellIdentifier = "cell"
                var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
                if(cell == nil){
                    cell = UITableViewCell(style: UITableViewCellStyle.value1,
                                           reuseIdentifier: cellIdentifier)
                }
                cell?.textLabel?.text = NSLocalizedString("Driver Agreement", comment: "")
                cell?.detailTextLabel?.text = ""
                cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                return cell!
            }
            if(indexPath.row == 2){
                let cellIdentifier = "cell"
                var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
                if(cell == nil){
                    cell = UITableViewCell(style: UITableViewCellStyle.value1,
                                           reuseIdentifier: cellIdentifier)
                }
                cell?.textLabel?.text = NSLocalizedString("Waiver", comment: "")
                cell?.detailTextLabel?.text = ""
                cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                return cell!
            }
        }
        if(indexPath.section == 1){
            if(indexPath.row == 0){
                let cellIdentifier = "cell"
                var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
                if(cell == nil){
                    cell = UITableViewCell(style: UITableViewCellStyle.value1,
                                           reuseIdentifier: cellIdentifier)
                }
                cell?.textLabel?.text = NSLocalizedString("System Settings", comment: "")
                cell?.detailTextLabel?.text = ""
                cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                return cell!
            }
            else{
                var cell = tableView.dequeueReusableCell(withIdentifier: GDLabelTableViewCell.cellIdentifier) as? GDLabelTableViewCell
                if(cell == nil){
                    cell = GDLabelTableViewCell.cell()
                }
                cell?.lbl_Title.text = "Language 语言"
                cell?.lbl_Content.text = "\(systemVM.defaultLanguage.rawValue)"
                return cell!
            }
        }
        if(indexPath.section == 2){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDCenterTextTableViewCell.cellIdentifier) as? GDCenterTextTableViewCell
            if(cell == nil){
                cell = GDCenterTextTableViewCell.cell()
            }
            cell?.label.textColor = UIColor.red
            cell?.label.text = NSLocalizedString("Logout", comment: "")
            return cell!
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        return cell
    }
    func presenLanguageSelectorVC(){
        
        let controller = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "CountryPickerVC") as! CountryPickerVC
        controller.parentVC = self
        controller.modalPresentationStyle = .overCurrentContext
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalTransitionStyle = .coverVertical
        self.present(controller, animated: true, completion: {})
    }
    

}
