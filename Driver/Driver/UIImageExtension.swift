//
//  UIImageExtension.swift
//  Driver
//
//  Created by ZY on 2017-04-06.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

extension UIImage {

    
    open class func image(_ color:UIColor, size:CGSize) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(size, true, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
        
    }
    
}
