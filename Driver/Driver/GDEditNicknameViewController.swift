//
//  GDEditNicknameViewController.swift
//  Driver
//
//  Created by ZY on 2017-06-26.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDEditNicknameViewController: KBViewController, UITextFieldDelegate {
    let accountVM = KBAccountVM()
    weak var delegate:GDEditNicknameViewControllerDelegate?
    
    @IBOutlet weak var textField: UITextField!
    
    class func controller(delegate aDelegate:GDEditNicknameViewControllerDelegate?) -> GDEditNicknameViewController {
        
        let controller = UIStoryboard(name: "EditNickname", bundle: nil).instantiateViewController(withIdentifier: "GDEditNicknameViewController") as! GDEditNicknameViewController
        
        controller.delegate = aDelegate
        
        return controller
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let account = KBAccountManager.shared.getAccount()
        {
            self.textField.text = account.nick_name
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.textField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.textField.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func saveButtonTapped(_ sender: Any) {
        
        self.save()
    }
    
    func save() {
        
        if(self.textField.text == nil || self.textField.text?.count == 0)
        {
            showError(NSLocalizedString("Please enter your Nickname", comment: ""))
            
            return
        }
        
        self.dismissKeyboard()
        
        let driverData:[String:String] = [KBDriver.Key_NickName : self.textField.text ?? ""]
        self.accountVM.updateAccount(data: driverData,
                                     start: {
                                        self.showLoading("")
        }, success: { (driver:KBDriver) in
            self.delegate?.GDEditNicknameViewControllerDidSave!(sender: self)
            self.dismissLoading()
        }) { (error:NSError) in
            self.showError(error.message)
        }
    }
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.save()
        return false;
    }

}


@objc protocol GDEditNicknameViewControllerDelegate : class {
    
    @objc optional func GDEditNicknameViewControllerDidSave(sender aSender:GDEditNicknameViewController)
    
}
