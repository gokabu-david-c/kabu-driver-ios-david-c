//
//  KBDebugViewController.swift
//  Driver
//
//  Created by George on 2018-01-17.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class KBDebugViewController: KBViewController {
    let debugVM = KBDebugVM()
    @IBOutlet weak var tableView: UITableView!
    class var controller : KBDebugViewController {
        let controller = UIStoryboard(name: "Debug", bundle: nil).instantiateViewController(withIdentifier: "KBDebugViewController") as! KBDebugViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension KBDebugViewController : UITableViewDataSource, UITableViewDelegate{
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 3
        }
        if(section == 1){
            return 1
        }
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            if(indexPath.row == 0){
                var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                if(cell == nil){
                    cell = UITableViewCell.init(style: UITableViewCellStyle.value1,
                                                reuseIdentifier: "cell")
                }
                cell?.accessoryType = UITableViewCellAccessoryType.none
                cell?.textLabel?.text = "Driver ID"
                cell?.detailTextLabel?.text = ""
                if let account = KBAccountManager.shared.getAccount(){
                    cell?.detailTextLabel?.text = String.init(account.id)
                }
                return cell!
            }
            if(indexPath.row == 1){
                var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                if(cell == nil){
                    cell = UITableViewCell.init(style: UITableViewCellStyle.value1,
                                                reuseIdentifier: "cell")
                }
                cell?.accessoryType = UITableViewCellAccessoryType.none
                cell?.textLabel?.text = NSLocalizedString("Status", comment: "")
                cell?.detailTextLabel?.text = ""
                if let account = KBAccountManager.shared.getAccount(){
                    if(account.is_busy == true){
                        cell?.detailTextLabel?.text = NSLocalizedString("", comment: "")
                    }else if(account.is_online == true){
                        cell?.detailTextLabel?.text = "在线"
                    }else{
                        cell?.detailTextLabel?.text = "不在线"
                    }
                }
                return cell!
            }
            if(indexPath.row == 2){
                var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                if(cell == nil){
                    cell = UITableViewCell.init(style: UITableViewCellStyle.value1,
                                                reuseIdentifier: "cell")
                }
                cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell?.textLabel?.text = "当前订单"
                cell?.detailTextLabel?.text = ""
                return cell!
            }
        }
        if(indexPath.section == 1){
            if(indexPath.row == 0){
                var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                if(cell == nil){
                    cell = UITableViewCell.init(style: UITableViewCellStyle.default,
                                                reuseIdentifier: "cell")
                }
                cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell?.textLabel?.text = "司机位置"
                return cell!
            }
        }
        if(indexPath.section == 2){
            if(indexPath.row == 0){
                var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                if(cell == nil){
                    cell = UITableViewCell.init(style: UITableViewCellStyle.value1,
                                                reuseIdentifier: "cell")
                }
                cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell?.textLabel?.text = "Debug密码"
                cell?.detailTextLabel?.text = GDDebugManager.shared.getDebugPageLoginPassword()
                return cell!
            }
            if(indexPath.row == 1){
                var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                if(cell == nil){
                    cell = UITableViewCell.init(style: UITableViewCellStyle.value1,
                                                reuseIdentifier: "cell")
                }
                cell?.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
                cell?.textLabel?.text = "模式"
                cell?.detailTextLabel?.text = self.debugVM.isDevMode == true ? "开发模式" : "生产模式"
                return cell!
            }
            if(indexPath.row == 2){
                var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                if(cell == nil){
                    cell = UITableViewCell.init(style: UITableViewCellStyle.value1,
                                                reuseIdentifier: "cell")
                }
                cell?.accessoryType = UITableViewCellAccessoryType.none
                cell?.textLabel?.text = "版本"
                cell?.detailTextLabel?.text = GDAppManager.getBuild()
                return cell!
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath,
                              animated: true)
        if(indexPath.section == 0){
            if(indexPath.row == 2){
                let controller = KBDebugMyOpenOrdersViewController.controller
                self.navigationController?.pushViewController(controller, animated: true)
                return
            }
        }
        if(indexPath.section == 1){
            if(indexPath.row == 0){
                let controller = GDDebugDriversLocationViewController.controller()
                self.navigationController?.pushViewController(controller,
                                                              animated: true)
            }
        }else if(indexPath.section == 2){
            if(indexPath.row == 0){
                let pasteBoard = UIPasteboard.general
                pasteBoard.string = GDDebugManager.shared.getDebugPageLoginPassword()
                showSuccess("复制成功")
            }else if(indexPath.row == 1){
                let controller = GDDebugModePickerTableViewController.controller()
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
}
