//
//  KBReservationPoolVC.swift
//  Driver
//
//  Created by Kabu on 2018-01-08.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class KBReservationPoolVC: KBViewController {
    @IBOutlet weak var tableView: UITableView!
    weak var parentVC: KBViewController?

    let reservationPoolVM = KBReservationPoolVM()
    var isReservationPoolTableViewScrolling = false
    var shouldShowLoading = true
    let refreshControl = UIRefreshControl()
    class func controller() -> KBReservationPoolVC {
        let controller = UIStoryboard(name: "ReservationPool", bundle: nil).instantiateViewController(withIdentifier: "KBReservationPoolVC") as! KBReservationPoolVC
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.preferLargeNavigationTitle(isPrefered: true)
        self.reservationPoolVM.ordersChanged = {
            self.updateView(animated: true)
        }
        /*self.reservationPoolVM.ordersDataChanged = {
            self.reloadTableView(animated: true)
        }*/
        self.reservationPoolVM.reservationPoolErrorChanged = {
            self.updateView(animated: true)
        }
        self.reservationPoolVM.isDownloadingReservationPoolChanged = {
            self.updateView(animated: true)
        }
        self.reservationPoolVM.lastUpdateTimeChanged = {
            self.updateLastUpdateTime()
        }
        self.loadTableView()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(self.shouldShowLoading == true){
            if let view = self.view as? GDView{
                view.showLoading()
            }
        }
        self.downloadReservationPool()

        //self.updateView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadTableView(){
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 80
        self.refreshControl.addTarget(self,
                                      action: #selector(downloadReservationPool),
                                      for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl)
    }
    @objc func downloadReservationPool(){
        self.reservationPoolVM.downloadReservationPool(start: {
            
        }, success: {
            //self.reloadTableView(animated: true)
            self.updateView(animated: true)
        }) { (error:NSError) in
            self.updateView(animated: true)
            //self.showError(error.message)
        }
        self.refreshControl.endRefreshing()

    }
    func reloadTableView(animated aAnimated:Bool){
        if(self.isReservationPoolTableViewScrolling == true){
            return
        }
        if(aAnimated == false){
            self.tableView.reloadData()
            return
        }
        if(self.tableView.numberOfSections == 0 || self.tableView.numberOfRows(inSection: 0) == self.reservationPoolVM.orders.count){
            self.tableView.reloadData()
            return
        }
        let range = NSMakeRange(0, 1)
        let sections = NSIndexSet(indexesIn: range)
        self.tableView.beginUpdates()
        self.tableView.reloadSections(sections as IndexSet, with: .automatic)
        self.tableView.endUpdates()
    }
    func updateView(animated aAnimated:Bool){
        if(self.reservationPoolVM.orders.count == 0){
            if let view = self.view as? GDView{
                view.showMessage(self.reservationPoolVM.reservationPoolError ?? NSLocalizedString("No Orders", comment: ""))
            }
        }else{
            if let view = self.view as? GDView{
                view.showContent(self.tableView)
            }
            self.reloadTableView(animated: aAnimated)
        }
    }
    func updateLastUpdateTime(){
        if(self.reservationPoolVM.lastUpdateTime == nil){
            self.tableView.headerView(forSection: 0)?.textLabel?.text = ""
            return
        }
        self.tableView.headerView(forSection: 0)?.textLabel?.text = NSLocalizedString("Updated on", comment: "") + " " + (self.reservationPoolVM.lastUpdateTime?.toString("yyyy-MM-dd HH:mm    "))!
    }
}
extension KBReservationPoolVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.reservationPoolVM.orders.count == 0){
            return 1
        }
        return self.reservationPoolVM.orders.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(self.reservationPoolVM.orders.count == 0){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDCenterTextTableViewCell.cellIdentifier) as? GDCenterTextTableViewCell
            if(cell == nil){
                cell = GDCenterTextTableViewCell.cell()
            }
            cell?.label.text = self.reservationPoolVM.reservationPoolError ?? NSLocalizedString("No Orders", comment: "")
            return cell!
        }
        var cell = tableView.dequeueReusableCell(withIdentifier: KBReservationPoolTableViewCell.reuseIdentifier) as? KBReservationPoolTableViewCell
        if(cell == nil){
            cell = KBReservationPoolTableViewCell.cell()
        }
        if(self.reservationPoolVM.orders.count >= indexPath.row + 1){
            let order = self.reservationPoolVM.orders[indexPath.row]
            cell?.setOrder(orderNumber: order.orderNumber!)
        }
        cell?.setNeedsLayout()
        cell?.layoutIfNeeded()
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(self.reservationPoolVM.orders.count == 0){
            return
        }
        if(tableView === self.tableView){
            let order = self.reservationPoolVM.orders[indexPath.row]
            let alert = UIAlertController(title: NSLocalizedString("You are about to Accept the following Reservation Order", comment: ""),
                                          message: "",
                                          preferredStyle: UIAlertControllerStyle.alert)
            let paragraphStyle = NSMutableParagraphStyle()//alert.value(forKey: "attributedMessage") as! NSMutableParagraphStyle
            paragraphStyle.alignment = NSTextAlignment.left
            paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
            paragraphStyle.lineSpacing = 1.5
            let attributes = [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 14)]//NSParagraphStyleAttributeName:paragraphStyle
            let pickupTimeIconAttachment:NSTextAttachment = NSTextAttachment()
            pickupTimeIconAttachment.image = UIImage(named: "ic-time-1.png")
            let picupTimeIconString:NSAttributedString = NSAttributedString(attachment: pickupTimeIconAttachment)
            let newLine = NSAttributedString(string:"\n", attributes: attributes)
            let weekdayName = NSAttributedString(string:Date.ToDate(order.reservationPickupTime).toString(" | EEEE | "), attributes:attributes)
            let timeString = NSAttributedString(string:Date.ToDate(order.reservationPickupTime).toString(" HH:mm"), attributes: attributes)
            let dateString = NSAttributedString(string:Date.ToDate(order.reservationPickupTime).toString("yyyy-MM-dd"), attributes: attributes)
            let startAddressIconAttachment:NSTextAttachment = NSTextAttachment()
            startAddressIconAttachment.image = UIImage(named: "ic-location-10.png")
            let startAddressIconString:NSAttributedString = NSAttributedString(attachment: startAddressIconAttachment)
            let startAddressTextString = NSAttributedString(string:" " + order.startAddress!, attributes: attributes)
            let alertMesageString = NSMutableAttributedString()
            alertMesageString.append(newLine)
            alertMesageString.append(picupTimeIconString)
            alertMesageString.append(timeString)
            alertMesageString.append(weekdayName)
            alertMesageString.append(dateString)
            alertMesageString.append(newLine)
            alertMesageString.append(startAddressIconString)
            alertMesageString.append(startAddressTextString)
            let riderCountIconAttachment:NSTextAttachment = NSTextAttachment()
            riderCountIconAttachment.image = UIImage(named: "ic-people-4.png")
            let riderCountIconString:NSAttributedString = NSAttributedString(attachment: riderCountIconAttachment)
            let riderCountTextString = NSAttributedString(string:"  " + String(order.numberOfPassengers))
            alertMesageString.append(newLine)
            alertMesageString.append(riderCountIconString)
            alertMesageString.append(riderCountTextString)
            if(order.customerNote.replacingOccurrences(of: " ", with: "").count != 0){
                alertMesageString.append(newLine)
                let customerNoteString = NSAttributedString(string:NSLocalizedString("Comment", comment: "") + ": " + order.customerNote, attributes: attributes)
                alertMesageString.append(customerNoteString)
            }
            alert.setValue(alertMesageString, forKey: "attributedMessage")
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""),
                                          style: UIAlertActionStyle.cancel,
                                          handler: { (action) in
                                            
            }))
            let OKAction = UIAlertAction(title: NSLocalizedString("Confirm", comment: ""),
                                         style: UIAlertActionStyle.default,
                                         handler: { (action) in
                                            self.reservationPoolVM.raceForOrder(orderNumber: order.orderNumber!,
                                                                                start: {
                                                                                    self.showLoading("")
                                            }, success: {
                                                self.downloadReservationPool()
                                                self.showSuccess("")
                                            }, failure: { (error:NSError) in
                                                self.downloadReservationPool()
                                                self.showError(error.message)
                                            })
            })
            alert.addAction(OKAction)
            alert.preferredAction = OKAction
            self.present(alert, animated: true, completion: nil)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 12
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(self.reservationPoolVM.lastUpdateTime == nil){
            return ""
        }
        return NSLocalizedString("Updated on", comment: "") + " " + self.reservationPoolVM.lastUpdateTime!.toString("yyyy-MM-dd HH:mm    ")
    }
}
extension KBReservationPoolVC : UIScrollViewDelegate{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if(scrollView === self.tableView){
            self.isReservationPoolTableViewScrolling = true
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if(scrollView === self.tableView){
            if(decelerate == false){
                self.isReservationPoolTableViewScrolling = false
            }
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView === self.tableView){
            self.isReservationPoolTableViewScrolling = false
        }
    }
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if(scrollView.contentOffset.y > 20){
//            parentVC?.preferLargeNavigationTitle(isPrefered: false)
//        }
//        else{
//            parentVC?.preferLargeNavigationTitle(isPrefered: true)
//        }
//    }
    
}
