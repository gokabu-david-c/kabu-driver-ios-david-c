//
//  GDCenterTextTableViewCell.swift
//  Driver
//
//  Created by ZY on 2017-05-20.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDCenterTextTableViewCell: UITableViewCell {

    
    
    
    @IBOutlet weak var label: UILabel!
    
    
    class func cell() -> GDCenterTextTableViewCell{
        
        let cell = Bundle.main.loadNibNamed("GDCenterTextTableViewCell",
                                            owner: self,
                                            options: nil)?.first as! GDCenterTextTableViewCell
        
        
        
        
        return cell
    }
    
    
    class var cellIdentifier:String
    {
        return "GDCenterTextTableViewCell"
    }

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
