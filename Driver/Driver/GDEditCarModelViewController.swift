//
//  GDEditCarModelViewController.swift
//  Driver
//
//  Created by ZY on 2017-06-27.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDEditCarModelViewController: KBViewController, UITextFieldDelegate {
    let accountVM = KBAccountVM()
    weak var delegate:GDEditCarModelViewControllerDelegate?
    @IBOutlet weak var textField: UITextField!
    class func controller(delegate aDelegate:GDEditCarModelViewControllerDelegate?) -> GDEditCarModelViewController {
        let controller = UIStoryboard(name: "EditCarModel", bundle: nil).instantiateViewController(withIdentifier: "GDEditCarModelViewController") as! GDEditCarModelViewController
        controller.delegate = aDelegate
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let account = KBAccountManager.shared.getAccount(){
            self.textField.text = account.car_desc
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.textField.becomeFirstResponder()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func save(){
        if(self.textField.text == nil || self.textField.text?.count == 0){
            showError(NSLocalizedString("Please enter the Car Description", comment: ""))
            return
        }
        self.dismissKeyboard()
        let driverData:[String:String] = [KBDriver.Key_CarDescription : self.textField.text ?? ""]
        self.accountVM.updateAccount(data: driverData,
                                     start: {
                                        self.showLoading("")
        }, success: { (driver:KBDriver) in
            self.delegate?.GDEditCarModelViewControllerDelegateDidSaveSuccess?(sender: self)
        }) { (error:NSError) in
            self.showError(error.message)
        }
    }
    @IBAction func saveButtonTapped(_ sender: Any) {
        self.save()
    }
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.save()
        return false;
    }
}
@objc protocol GDEditCarModelViewControllerDelegate : class{
    @objc optional func GDEditCarModelViewControllerDelegateDidSaveSuccess(sender aSender:GDEditCarModelViewController)
}
