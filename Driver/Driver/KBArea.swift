//
//  KBCity.swift
//  Driver
//
//  Created by Kabu on 2017-11-14.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBArea: NSObject {
    var id:Int = 0
    var name = ""
    var bounds:[CLLocation] = [CLLocation]()
    var pid = 0
    init(json:[String:Any]) {
        if let id = json["id"] as? Int{
            self.id = id
        }
        if let name = json["name"] as? String{
            self.name = name
        }
        if let boundsData = json["polygon"] as? [String]{
            for currentVertexData in boundsData{
                if let currentVertex = currentVertexData.toCLLocation(){
                    self.bounds.append(currentVertex)
                }
            }
        }
        if let pid = json["pid"] as? Int{
            self.pid = pid
        }
    }
}
