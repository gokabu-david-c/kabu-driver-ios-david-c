//
//  GDDriversAroundViewController.swift
//  Driver
//
//  Created by ZY on 2017-05-20.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import GoogleMaps
import QuartzCore
//import MapboxDirections
//import MapboxCoreNavigation
//import MapboxNavigation

class GDDriversAroundViewController: KBViewController, GDMapViewDelegate {
    @IBOutlet var nearbyLabelBackground: UIVisualEffectView!
    @IBOutlet var nearbyLabel: UILabel!
    //var drivers:[GDDriver]?
    var selectedDriver:KBDriver?
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var mapContainerView: UIView!
    let mapView:GDMapView = GDMapView.view()
    @IBOutlet weak var driverInfoContainerView: UIView!
    @IBOutlet weak var driverUsernameLabel: UILabel!
    @IBOutlet weak var driverDisplayNameLabel: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var capacityLabel: UILabel!
    @IBOutlet weak var constraint_Bottom_DriverInfoContainer: NSLayoutConstraint!
    @IBOutlet weak var updateTimeLabel: UILabel!
    let longitudePerKM = 0.0137
    var driversRangeCricle:GMSCircle?
    var orderRangeCricle:GMSCircle?
    let driverDataVM = KBDriversDataVM()
    let accountVM = KBAccountVM()
    @IBOutlet var legendViewContainer: UIView!
    class func controller() -> GDDriversAroundViewController {
        let controller = UIStoryboard(name: "DriversAround", bundle: nil).instantiateViewController(withIdentifier: "GDDriversAroundViewController") as! GDDriversAroundViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nearbyLabel.text = NSLocalizedString("Nearby Drivers", comment: "")
        self.mapView.delegate = self
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.mapContainerView.addSubview(self.mapView)
        self.nearbyLabelBackground.layer.cornerRadius = 14
        if let superview = self.mapView.superview{
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1.0,
                                                       constant: 0))
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.trailing,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.trailing,
                                                       multiplier: 1.0,
                                                       constant: 0))
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1.0,
                                                       constant: 0))
            superview.addConstraint(NSLayoutConstraint(item: superview,
                                                       attribute: NSLayoutAttribute.bottom,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.mapView,
                                                       attribute: NSLayoutAttribute.bottom,
                                                       multiplier: 1.0,
                                                       constant: 0))
        }
        self.mapView.mapView.isMyLocationEnabled = true
        self.mapView.mapView.settings.rotateGestures = false
        self.mapView.mapView.settings.tiltGestures = false
        self.legendViewContainer.layer.cornerRadius = 4
        self.driverDataVM.driversUpdated = {
            self.dismissLoading()
            self.reloadDrivers(self.driverDataVM.drivers)
            if let view = self.view as? GDView{
                view.showContent(self.contentView)
            }
            self.updateOrderRangeMask()
            self.updateMask()
            self.updateTimeLabel.text = NSLocalizedString("Updated on", comment: "") + " " + Date().toString("yyyy-MM-dd HH:mm:ss")
        }
        self.driverDataVM.driversUpdateFailed = { error in
            if let view = self.view as? GDView{
                view.showContent(self.contentView)
            }
            self.showError(error)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mapView.viewWillAppear()
        if(self.isViewWillAppeared == false){
            /*let coordinate_TopLeft = CLLocationCoordinate2D(latitude: 49.284950,
                                                            longitude: -123.330307)
            let coordinate_BotRight = CLLocationCoordinate2D(latitude: 49.012251,
                                                             longitude: -122.772023)
            let bounds = GMSCoordinateBounds(coordinate: coordinate_TopLeft,
                                             coordinate: coordinate_BotRight)
            self.mapView.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 0.0))
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()*/
            self.updateMapCamera()
        }
 
        self.downloadDrivers()
        self.updateOrderRangeMask()
        self.isViewWillAppeared = true
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(self.isViewDidAppeared == false){
            self.updateMapCamera()
        }
        self.isViewDidAppeared = true
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    @objc func applicationDidBecomeActive(){
        self.downloadDrivers()
    }
    @IBAction func rightButtonTapped(_ sender: Any) -> Void {
        self.downloadDrivers()
    }
    @IBAction func phoneButtonTapped(_ sender: Any) {
        if self.selectedDriver != nil{
            if let url = NSURL(string: "tel://" + (self.selectedDriver?.phone)!){
                if(UIApplication.shared.canOpenURL(url as URL)){
                    UIApplication.shared.open(url as URL,
                                              options: [String:Any](),
                                              completionHandler: nil)
                }
            }
        }
    }
    @IBAction func legendQuestionButtonTapped(_ sender: Any) {
        
//        let origin = Waypoint(coordinate: CLLocationCoordinate2D(latitude: 38.9131752, longitude: -77.0324047), name: "Mapbox")
//        let destination = Waypoint(coordinate: CLLocationCoordinate2D(latitude: 38.8977, longitude: -77.0365), name: "White House")
//
//        let options = NavigationRouteOptions(waypoints: [origin, destination])
//
//        Directions.shared.calculate(options) { (waypoints, routes, error) in
//            guard let route = routes?.first else { return }
//
//            let viewController = NavigationViewController(for: route)
//            self.present(viewController, animated: true, completion: nil)
//        }
//        let controller = KBDriversAroundLegendQuestionViewController.controller()
//        let navController = UINavigationController.init(rootViewController: controller)
//        self.present(navController, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func downloadDrivers(){
        if let view = self.view as? GDView{
            view.showLoading(1,
                             hideSubviews: true)
        }
        self.driverDataVM.downloadDrivers()
    }
    func updateMapCamera(){
        var range = 7.0
        if let account = self.accountVM.account{
            if let userCurrentLocation = account.lastLocation(){
                if self.driverDataVM.range != 0{
                    range = self.driverDataVM.range
                }
                if(Double(account.range) > range){
                    range = Double(account.range)
                }
                let coordinate_TopLeft = CLLocationCoordinate2D(latitude: userCurrentLocation.coordinate.latitude,
                                                                longitude: userCurrentLocation.coordinate.longitude - range * self.longitudePerKM * 1.5)
                let coordinate_BotRight = CLLocationCoordinate2D(latitude: userCurrentLocation.coordinate.latitude,
                                                                 longitude: userCurrentLocation.coordinate.longitude + range * self.longitudePerKM * 1.5)
                let bounds = GMSCoordinateBounds(coordinate: coordinate_TopLeft,
                                                 coordinate: coordinate_BotRight)
                self.mapView.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 0.0))
            }
        }
    }
    func updateMask(){
        if(self.driverDataVM.range == 0 || self.driverDataVM.location == nil){
            if(self.driversRangeCricle != nil){
                self.driversRangeCricle?.map = nil
            }
            return
        }
        if(self.driversRangeCricle == nil){
            self.driversRangeCricle = GMSCircle(position: self.driverDataVM.location!, radius: self.driverDataVM.range * 1000)
        }
        self.driversRangeCricle?.position = self.driverDataVM.location!
        self.driversRangeCricle?.fillColor = UIColor(red: 66.0/255.0, green: 133.0/255.0, blue: 244.0/255.0, alpha: 0.2)
        self.driversRangeCricle?.strokeWidth = 0
        self.driversRangeCricle?.map = self.mapView.mapView
    }
    func updateOrderRangeMask(){
        if let account = KBAccountManager.shared.getAccount(){
            if let location = account.lastLocation(){
                if(account.range > 0){
                    if(self.orderRangeCricle == nil){
                        self.orderRangeCricle = GMSCircle(position: location.coordinate, radius: (Double)(account.range * 1000))
                    }
                    self.orderRangeCricle?.radius = (Double)(account.range * 1000)
                    self.orderRangeCricle?.position = location.coordinate
                    self.orderRangeCricle?.fillColor = UIColor(red: 58.0/255.0, green: 167.0/255.0, blue: 87.0/255.0, alpha: 0.2)
                    self.orderRangeCricle?.strokeWidth = 0
                    self.orderRangeCricle?.map = self.mapView.mapView
                    return;
                }
            }
        }
        if(self.orderRangeCricle != nil){
            self.orderRangeCricle?.map = nil
        }
    }
    func reloadDrivers(_ drivers:[KBDriver]) {
        self.mapView.mapView.clear()
        var count_Idle = 0
        for currentDriver in drivers{
            if(currentDriver.is_busy == false){
                count_Idle += 1
            }
        }
        self.nearbyLabel.text = NSLocalizedString("Nearby Drivers", comment: "") + "(" + String(count_Idle) + ")"
        for currentDriver in drivers{
            if let coordinate = currentDriver.lastLocation(){
                let marker = GMSMarker(position: coordinate.coordinate)
                marker.userData = currentDriver
                if let account = KBAccountManager.shared.getAccount(){
                    if(account.username == currentDriver.username){
                        marker.icon = UIImage.init(named: "ic-location-7")
                        marker.map = self.mapView.mapView
                        continue
                    }
                }
                if(currentDriver.is_busy == true){
                    marker.icon = UIImage.init(named: "ic-location-5")
                }else if(currentDriver.is_online == true){
                    marker.icon = UIImage.init(named: "ic-location-4")
                }else{
                    marker.icon = UIImage.init(named: "ic-location-6")
                }
                marker.map = self.mapView.mapView
            }
        }
    }
    // MARK: - GDMapViewDelegate
    func mapView(_ mapView: GDMapView, didTap marker: GMSMarker) -> Bool {
        if(self.driverDataVM.range - 0.0 > 0.001){
            return false
        }
        if let currentDriver = marker.userData as? KBDriver{
            self.selectedDriver = currentDriver
            self.driverUsernameLabel.text = ""
            self.driverDisplayNameLabel.text = ""
            self.carModelLabel.text = ""
            self.phoneLabel.text = ""
            self.capacityLabel.text = ""
            self.driverUsernameLabel.text = currentDriver.username
            self.driverDisplayNameLabel.text = currentDriver.nick_name
            self.carModelLabel.text = currentDriver.car_desc
            self.phoneLabel.text = currentDriver.phone.toPhoneFormat()
            self.capacityLabel.text = String(currentDriver.car_capacity)
            self.view.layoutIfNeeded()
            if(self.constraint_Bottom_DriverInfoContainer.constant <= self.driverInfoContainerView.frame.size.height){
                self.constraint_Bottom_DriverInfoContainer.constant = self.driverInfoContainerView.frame.size.height
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        return true
    }
    func mapView(_ mapView: GDMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if(self.constraint_Bottom_DriverInfoContainer.constant > 0){
            self.constraint_Bottom_DriverInfoContainer.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    func mapView(_ mapView: GDMapView, didChange position: GMSCameraPosition) {
        if(self.constraint_Bottom_DriverInfoContainer.constant > 0){
            self.constraint_Bottom_DriverInfoContainer.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        self.updateOrderRangeMask()
        self.updateMask()
    }
    func mapView(_ mapView: GDMapView, idleAt position: GMSCameraPosition) {
    }
    
    func mapView(_ mapView: GDMapView, willMove gesture: Bool) {
    }
}
