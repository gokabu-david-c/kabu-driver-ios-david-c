//
//  KBOpenOrdersPageVC.swift
//  Driver
//
//  Created by Jack Du on 2018-04-25.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

import Foundation

class KBOpenOrdersPageVC: UIPageViewController {
    
    weak var mainSegmentedControl:HMSegmentedControl?
    var pages = [UIViewController]()
    var pageViewControllerPendingViewController:UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.dataSource = self
        
        let page1: UIViewController! = UIStoryboard(name: "MyOpenOrders", bundle: nil).instantiateInitialViewController()!
        let page2: UIViewController! = UIStoryboard(name: "ReservationPool", bundle: nil).instantiateInitialViewController()!
        
        pages.append(page1)
        pages.append(page2)
        
        setViewControllers([page1], direction: .forward, animated: false, completion: nil)
    }
    
}


extension KBOpenOrdersPageVC : UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController === pages[0]{
            return nil
        }
        return pages[0]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController === pages[1]{
            return nil
        }
        return pages[1]
    }
    
}
extension KBOpenOrdersPageVC : UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.pageViewControllerPendingViewController = pendingViewControllers.first
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if(completed == true){
            if(self.pageViewControllerPendingViewController === pages[0]){
                self.mainSegmentedControl?.selectedSegmentIndex = 0
            }else if(self.pageViewControllerPendingViewController === pages[1]){
                self.mainSegmentedControl?.selectedSegmentIndex = 1
            }
        }
    }
}
