//
//  KBConstants.swift
//  Driver
//
//  Created by George on 2018-01-17.
//  Copyright © 2018 Developer. All rights reserved.
//

import Foundation

struct KBConfig {
    static let Key_GoogleAPI = "AIzaSyA8n2XWOM9B66N2WVw20tnt1FLSbQaFLIk"
    static var apiDomain : String {
        if(GDAppManager.isDevMode){
            return "https://api-dev.kabu.ca/"
        }
        return "https://api.kabu.ca/"
    }
    static var APIBaseURL : String{
        return KBConfig.apiDomain + "driver/v1/"
    }
    static var appAccessToken : String{
        if(GDAppManager.isDevMode == true){
            return "gokabu2017"
        }
        return "1sxLJwG49k0xtcUJ6eNW85o1AF7gHVvz"
    }
    static var driverWebDomain:String{
        if(GDAppManager.isDevMode){
            return "https://drivers-dev.kabu.ca/"
        }
        return "https://drivers.kabu.ca/"
    }
    static var URLString_CustomerSupport:String {
        return KBConfig.driverWebDomain + "customer_support_contact"
    }
    
    static var URLString_DriverPolicy:String {
        return KBConfig.driverWebDomain + "terms"
    }
    static var URLString_Waiver:String {
        return KBConfig.driverWebDomain + "waiver"
    }
    static var URLString_DriverMemberShipCard:String {
        return KBConfig.driverWebDomain + "membership_card?timestamp=" + Date().toString("yyyyMMddHHmmss")
    }
    
    static var ULRString_DriverApply:String {
        let url = KBConfig.driverWebDomain + "apply?timestamp=" + Date().toString("yyyyMMddHHmmss")
        return url
    }
    
    static var ULRString_PayCommission:String {
        //let url = KBConstants.driverWebDomain + "api/public/driver/pay_commission"
        let url = KBConfig.driverWebDomain + "bills/pay?timestamp=" + Date().toString("yyyyMMddHHmmss")
        return url
    }
    
    static var ULRString_PayCreditCard:String {
        let url = KBConfig.driverWebDomain + "bills/pay/realex" 
        return url
    }
    static var ULRString_PayWeChat:String {
        let url = KBConfig.driverWebDomain + "bills/pay/ottpay"
        return url
    }
    
    static var processingFee : Float {
        return 1.5
    }
    
    //Time(seconds) differences required between reservation orders
    static var reservationProhibitionTimeInterval : Int {
        return 3600
    }
    
    static var downloadOrderTimeInterval : TimeInterval {
        let isSavingData = KBSystemSettingsVM().isSavingData
        if(isSavingData){
            return 40.0
        }
        else{
            return 20.0
        }
    }
    
    static var updatelocationTimeInterval : TimeInterval {
        return 10
    }
    
    static var Alipay_App_Id : String{
        return "2018050202618012"
    }
    
    static var Alipay_Public_Key : String{
        return "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBANkSeyYSXjDchgEBDjSEl/62R+Q3oYv2A5P4JVtW8AjpyWlYnPJPBkJ1gv1eto+IFlWQNRvoktPhJB/87k2CQeUCAwEAAQ=="
    }
    
    static var Wechat_App_Id : String{
        return "wx4280bdcc078ecd98"
    }
    
}
