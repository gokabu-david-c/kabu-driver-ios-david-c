//
//  GDDownloadCitiesVM.swift
//  Driver
//
//  Created by Kabu on 2017-11-15.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBAreasDataVM: NSObject {
    var areas:[KBArea] = [KBArea]()
    func downloadCities(start aStart:(()->())?,
                        success aSuccess:@escaping(_ cities:[KBArea]) -> (),
                        failure aFailure:@escaping(_ error:NSError) -> ()) {
        KBAreasAPI.downloadAllAreas(start: {
            aStart?()
        }, success: { (areas:[KBArea]) in
            self.areas = areas
            aSuccess(areas)
        }) { (error:NSError) in
            aFailure(error)
        }
    }
}
