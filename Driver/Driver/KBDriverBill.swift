//
//  KBDriverBill.swift
//  Driver
//
//  Created by Kabu on 2017-11-03.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class KBDriverBill: NSObject {
    var id:Int = 0
    var amount:Float = 0
    var deadline:Int64 = 0
    init(json:[String:Any]) {
        if let id = json["id"] as? Int{
            self.id = id
        }
        if let amount = json["amount"] as? Double{
            self.amount = Float(amount)
        }
        if let deadline = json["deadline"] as? Int64{
            self.deadline = deadline
        }
    }
}
