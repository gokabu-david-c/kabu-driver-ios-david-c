//
//  KBMyAccountViewController.swift
//  Driver
//
//  Created by Kabu on 2017-11-23.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBMyAccountViewController: KBViewController {
    @IBOutlet var editProfileButton: UIButton!
    @IBOutlet var avatarContainerView: UIView!
    @IBOutlet var avatarImageView: GDAvatarImageView!
    @IBOutlet var ratingContainerView: UIView!
    @IBOutlet var nicknameLabel: UILabel!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var mainSegmentedControlContainerView: UIView!
    @IBOutlet var pageControllerContainerView: UIView!
    var pageViewController:UIPageViewController!
    var pageViewControllerPendingViewController:UIViewController?
    var myAccountMessageViewController = KBMyAccountMessageViewController.controller()
    var myAccountCenterViewController = KBMyAccountCenterViewController.controller()
    let mainSegmentedControl = HMSegmentedControl.init()
    var ratingView:KBRatingView?
    let accountVM = KBAccountVM()
    class func controller() -> KBMyAccountViewController {
        let controller = UIStoryboard(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "KBMyAccountViewController") as! KBMyAccountViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        preferLargeNavigationTitle(isPrefered: true)
        if let account = self.accountVM.account{
            self.navigationItem.title = account.nick_name
        }else{
            self.navigationItem.title = NSLocalizedString("My Account", comment: "")
        }
        self.loadNavigationBar()
        self.loadPageViewController()
        self.loadRatingView()
        self.loadAvatarImageView()
        self.loadSegmentedControl()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateAccountView()
    }
    func loadNavigationBar(){
        if let navController = self.navigationController{
            navController.view.backgroundColor = UIColor.white
            let image = UIImage.image(.white, size: CGSize.init(width: 1, height: 1))
            navController.navigationBar.barTintColor = UIColor.white
            navController.navigationBar.shadowImage = image
        }
    }
    func loadPageViewController(){
        self.pageViewController = UIPageViewController(transitionStyle: .scroll,
                                                       navigationOrientation: .horizontal,
                                                       options: nil)
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        self.addChildViewController(self.pageViewController)
        self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.pageControllerContainerView.addSubview(self.pageViewController.view)
        self.pageViewController.view.superview?.addConstraint(NSLayoutConstraint.init(item: self.pageViewController.view,
                                                                                      attribute: NSLayoutAttribute.left,
                                                                                      relatedBy: NSLayoutRelation.equal,
                                                                                      toItem: self.pageViewController.view.superview,
                                                                                      attribute: NSLayoutAttribute.left,
                                                                                      multiplier: 1.0,
                                                                                      constant: 0))
        self.pageViewController.view.superview?.addConstraint(NSLayoutConstraint.init(item: self.pageViewController.view,
                                                                                      attribute: NSLayoutAttribute.right,
                                                                                      relatedBy: NSLayoutRelation.equal,
                                                                                      toItem: self.pageViewController.view.superview,
                                                                                      attribute: NSLayoutAttribute.right,
                                                                                      multiplier: 1.0,
                                                                                      constant: 0))
        self.pageViewController.view.superview?.addConstraint(NSLayoutConstraint.init(item: self.pageViewController.view,
                                                                                      attribute: NSLayoutAttribute.top,
                                                                                      relatedBy: NSLayoutRelation.equal,
                                                                                      toItem: self.pageViewController.view.superview,
                                                                                      attribute: NSLayoutAttribute.top,
                                                                                      multiplier: 1.0,
                                                                                      constant: 0))
        self.pageViewController.view.superview?.addConstraint(NSLayoutConstraint.init(item: self.pageViewController.view,
                                                                                      attribute: NSLayoutAttribute.bottom,
                                                                                      relatedBy: NSLayoutRelation.equal,
                                                                                      toItem: self.pageViewController.view.superview,
                                                                                      attribute: NSLayoutAttribute.bottom,
                                                                                      multiplier: 1.0,
                                                                                      constant: 0))
        self.pageViewController.didMove(toParentViewController: self)
        self.pageViewController.setViewControllers([self.myAccountCenterViewController],
                                                   direction: UIPageViewControllerNavigationDirection.forward,
                                                   animated: false,
                                                   completion: nil)
    }
    func loadRatingView(){
        self.ratingView = KBRatingView.view()
        self.ratingContainerView.translatesAutoresizingMaskIntoConstraints = false
        self.ratingContainerView.addSubview(self.ratingView!)
        self.ratingView!.superview?.addConstraint(NSLayoutConstraint.init(item: self.ratingView!,
                                                                          attribute: NSLayoutAttribute.left,
                                                                          relatedBy: NSLayoutRelation.equal,
                                                                          toItem: self.ratingView!.superview,
                                                                          attribute: NSLayoutAttribute.left,
                                                                          multiplier: 1.0,
                                                                          constant: 0))
        self.ratingView!.superview?.addConstraint(NSLayoutConstraint.init(item: self.ratingView!,
                                                                          attribute: NSLayoutAttribute.right,
                                                                          relatedBy: NSLayoutRelation.equal,
                                                                          toItem: self.ratingView!.superview,
                                                                          attribute: NSLayoutAttribute.right,
                                                                          multiplier: 1.0,
                                                                          constant: 0))
        self.ratingView!.superview?.addConstraint(NSLayoutConstraint.init(item: self.ratingView!,
                                                                          attribute: NSLayoutAttribute.top,
                                                                          relatedBy: NSLayoutRelation.equal,
                                                                          toItem: self.ratingView!.superview,
                                                                          attribute: NSLayoutAttribute.top,
                                                                          multiplier: 1.0,
                                                                          constant: 0))
        self.ratingView!.superview?.addConstraint(NSLayoutConstraint.init(item: self.ratingView!,
                                                                          attribute: NSLayoutAttribute.bottom,
                                                                          relatedBy: NSLayoutRelation.equal,
                                                                          toItem: self.ratingView!.superview,
                                                                          attribute: NSLayoutAttribute.bottom,
                                                                          multiplier: 1.0,
                                                                          constant: 0))
    }
    func loadAvatarImageView(){
        self.avatarImageView.backgroundColor = UIColor.white
        let shadowPath = UIBezierPath(roundedRect: CGRect.init(x: 0, y: 0, width: self.avatarContainerView.frame.size.width, height: self.avatarContainerView.frame.size.height), cornerRadius: 40.0)
        self.avatarContainerView.dropShadow(color: .black, opacity: 0.3, offSet: CGSize.zero, radius: 6, scale: true, path:shadowPath)
        if let account = self.accountVM.account{
            self.avatarImageView.imageURL = account.photoURL
        }
    }
    func loadSegmentedControl(){
        self.mainSegmentedControlContainerView.addSubview(self.mainSegmentedControl)
        self.mainSegmentedControl.selectionIndicatorHeight = 2.0
        self.mainSegmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        self.mainSegmentedControl.selectionStyle = HMSegmentedControlSelectionStyle.box
        self.mainSegmentedControl.selectionIndicatorBoxColor = UIColor.clear
        self.mainSegmentedControl.titleTextAttributes = [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 14),
                                                         NSAttributedStringKey.foregroundColor:UIColor.lightGray]
        self.mainSegmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 14),
                                                                NSAttributedStringKey.foregroundColor:UIColor.black]
        self.mainSegmentedControl.selectionIndicatorColor = UIColor.black
        self.mainSegmentedControl.addTarget(self, action: #selector(segmentedControlChangedValue(sender:)), for: UIControlEvents.valueChanged)
        self.mainSegmentedControl.translatesAutoresizingMaskIntoConstraints = false
        self.mainSegmentedControl.sectionTitles = [NSLocalizedString("Information", comment: "Information from the company"),NSLocalizedString("Personal Space", comment: "")]
        self.mainSegmentedControl.selectedSegmentIndex = 1
        self.mainSegmentedControl.superview?.addConstraint(NSLayoutConstraint.init(item: self.mainSegmentedControl,
                                                                                   attribute: NSLayoutAttribute.left,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.mainSegmentedControl.superview,
                                                                                   attribute: NSLayoutAttribute.left,
                                                                                   multiplier: 1,
                                                                                   constant: 12))
        self.mainSegmentedControl.superview?.addConstraint(NSLayoutConstraint.init(item: self.mainSegmentedControl,
                                                                                   attribute: NSLayoutAttribute.right,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.mainSegmentedControl.superview,
                                                                                   attribute: NSLayoutAttribute.right,
                                                                                   multiplier: 1,
                                                                                   constant: -12))
        self.mainSegmentedControl.superview?.addConstraint(NSLayoutConstraint.init(item: self.mainSegmentedControl,
                                                                                   attribute: NSLayoutAttribute.top,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.mainSegmentedControl.superview,
                                                                                   attribute: NSLayoutAttribute.top,
                                                                                   multiplier: 1,
                                                                                   constant: 0))
        self.mainSegmentedControl.superview?.addConstraint(NSLayoutConstraint.init(item: self.mainSegmentedControl,
                                                                                   attribute: NSLayoutAttribute.bottom,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.mainSegmentedControl.superview,
                                                                                   attribute: NSLayoutAttribute.bottom,
                                                                                   multiplier: 1,
                                                                                   constant: -1))
    }
    func updateAccountView(){
        if let account = self.accountVM.account{
            self.nicknameLabel.text = account.nick_name
            self.usernameLabel.text = account.username
            self.ratingView?.rating = account.score
            self.avatarImageView.imageURL = account.photoURL
        }
        self.loadAvatarImageView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func segmentedControlChangedValue(sender aSender:HMSegmentedControl) {
        if(aSender == self.mainSegmentedControl){
            if(aSender.selectedSegmentIndex == 0){
                self.pageViewController.setViewControllers([self.myAccountMessageViewController],
                                                           direction: UIPageViewControllerNavigationDirection.reverse,
                                                           animated: true,
                                                           completion: nil)
            }else if(aSender.selectedSegmentIndex == 1){
                self.pageViewController.setViewControllers([self.myAccountCenterViewController],
                                                           direction: UIPageViewControllerNavigationDirection.forward,
                                                           animated: true,
                                                           completion: nil)
            }
        }
    }
    @IBAction func editProfileButtonTapped(_ sender: Any) {
        let controller = KBMyProfileViewController.controller()
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension KBMyAccountViewController : UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController === self.myAccountMessageViewController{
            return nil
        }
        return self.myAccountMessageViewController
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController === self.myAccountCenterViewController{
            return nil
        }
        return self.myAccountCenterViewController
    }
}
extension KBMyAccountViewController : UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.pageViewControllerPendingViewController = pendingViewControllers.first
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if(completed == true){
            if(self.pageViewControllerPendingViewController === self.myAccountMessageViewController){
                self.mainSegmentedControl.selectedSegmentIndex = 0
            }else if(self.pageViewControllerPendingViewController === self.myAccountCenterViewController){
                self.mainSegmentedControl.selectedSegmentIndex = 1
            }
        }
    }
}
