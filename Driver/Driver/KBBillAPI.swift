//
//  KBBillAPIHelper.swift
//  Driver
//
//  Created by Kabu on 2017-11-03.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class KBBillAPI: KBAPI {
    static func getUnpaidBill(start aStart:(() -> ())?,
                              success aSuccess:@escaping(_ bills:[KBDriverBill]) -> (),
                              failure aFailure:@escaping(_ error:NSError) -> ()){
        let account = KBAccountManager.shared.account
        if(account == nil){
            return
        }
        aStart?()
        let url = URL_UnpaidBills(driverID: (account?.id)!)
        let manager = AFHTTPSessionManager.newInstance()
        manager.get(url,
                    parameters: nil,
                    progress: nil,
                    success: { (task, responseObject) in
                        let r = GDResponse.init(json: responseObject as! [String : Any])
                        if(r.isOK() == false){
                            let message = r.message ?? NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        var bills = [KBDriverBill]()
                        if let data = r.data as? [Any?]{
                            for currentData in data{
                                if let json = currentData as? [String:Any]{
                                    let bill = KBDriverBill.init(json: json)
                                    bills.append(bill)
                                }
                            }
                        }
                        aSuccess(bills)
        }) { (task, error) in
            aFailure(NSError.Error_UnknownError())
        }
    }
    
    static func getAlipayInfo (start aStart:(() -> ())?,
                               success aSuccess:@escaping(_ orderData:String) -> (),
                               failure aFailure:@escaping(_ error:NSError) -> ()){
        let account = KBAccountManager.shared.account
        if(account == nil){
            return
        }
        aStart?()
        let url = URL_RequestAlipayOrderInfo(driverID: (account?.id)!)
        let manager = AFHTTPSessionManager.newInstance()
        manager.get(url,
                    parameters: nil,
                    progress: nil,
                    success: { (task, responseObject) in
                        let r = GDResponse.init(json: responseObject as! [String : Any])
                        if(r.isOK() == false){
                            let message = r.message ?? NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        if let alipayOrderString = r.data as? String{
                            print(alipayOrderString)
                            aSuccess(alipayOrderString)
                        }
                        else{
                            let message = r.message ?? NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
        }) { (task, error) in
            aFailure(NSError.Error_UnknownError())
        }
    }
    
    static func getWechatpayInfo (start aStart:(() -> ())?,
                               success aSuccess:@escaping(_ orderData:PayReq) -> (),
                               failure aFailure:@escaping(_ error:NSError) -> ()){
        let account = KBAccountManager.shared.account
        if(account == nil){
            return
        }
        aStart?()
        let url = URL_RequestWechatpayOrderInfo(driverID: (account?.id)!)
        let manager = AFHTTPSessionManager.newInstance()
        manager.get(url,
                    parameters: nil,
                    progress: nil,
                    success: { (task, responseObject) in
                        let r = GDResponse.init(json: responseObject as! [String : Any])
                        if(r.isOK() == false){
                            let message = r.message ?? NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
                        if let wechatOrderInfo = r.data as? [String:Any]{
                            wechatOrderInfo.forEach{
                                print($0)
                            }

                            let wechatPayReqeust =  PayReq.init()
                            wechatPayReqeust.nonceStr = wechatOrderInfo["nonceStr"] as! String
                            wechatPayReqeust.package = wechatOrderInfo["package"] as! String
                            wechatPayReqeust.partnerId = wechatOrderInfo["partner_id"] as! String
                            wechatPayReqeust.prepayId = wechatOrderInfo["prepay_id"] as! String
                            wechatPayReqeust.sign = wechatOrderInfo["paySign"] as! String
                            let timestamp = wechatOrderInfo["timeStamp"] as! String
                            wechatPayReqeust.timeStamp = UInt32(timestamp)!

                            aSuccess(wechatPayReqeust)
                        }
                        else{
                            let message = r.message ?? NSLocalizedString("An Unknown Error has occurred", comment: "")
                            aFailure(NSError.Error(message: message))
                            return
                        }
        }) { (task, error) in
            aFailure(NSError.Error_UnknownError())
        }
    }
    
}
