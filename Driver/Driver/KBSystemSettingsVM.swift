//
//  KBSystemSettingsVM.swift
//  Driver
//
//  Created by George on 2018-01-15.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

enum LanguageNames : String{
    case chineseSimplified = "中文（简体）"
    case english = "English"
}
enum LanguageCodes : String{
    case chineseSimplified = "zh-Hans"
    case english = "en"
}

class KBSystemSettingsVM: NSObject {
    
    var isShadowEnabled:Bool{
        set{
            GDAppManager.isShadowEnabled = newValue
        }
        get{
            return GDAppManager.isShadowEnabled
        }
    }
    var isDisplayingTrafficOnMap:Bool{
        set{
            GDAppManager.isDisplayingTrafficOnMap = newValue
        }get{
            return GDAppManager.isDisplayingTrafficOnMap
        }
    }
    var isDisplayingNewReservationPopup:Bool{
        set{
            GDAppManager.shared.isDisplayingNewReservationPopup = newValue
        }get{
                return GDAppManager.shared.isDisplayingNewReservationPopup
            
        }
    }
    var isSavingData:Bool{
        set{
            GDAppManager.isDataSaverOn = newValue
            GDAppManager.isDisplayingTrafficOnMap = !newValue
            KBBackgroundTaskManager.shared.stopBackgroundTasks()
            KBBackgroundTaskManager.shared.startBackgroundTasks()
        }get{
            return GDAppManager.isDataSaverOn
        }
    }
    var lengthOfNewReservationPopupMinutes:Int{
        set{
            GDAppManager.lengthOfNewReservationPopup = newValue
        }get{
            return GDAppManager.lengthOfNewReservationPopup
        }
    }
    
    var defaultPaymentMethod:PaymentMethod{
        set{
            GDAppManager.paymentMethod = newValue
        }get{
            return GDAppManager.paymentMethod
        }
    }
    
    func pauseReservationPopup(){
        GDAppManager.shared.isTempDisableNewReservationPopup = true
    }

    var defaultLanguage:LanguageNames{
        set{
            var languageCode : LanguageCodes
            
            switch newValue {
            case .english:
                languageCode = .english
            case .chineseSimplified:
                languageCode = .chineseSimplified
            default:
                languageCode = .english
            }
            Bundle.setLanguage(languageCode.rawValue)
            GDAppManager.systemLanguage = languageCode
        }get{
            var languageName : LanguageNames
            switch GDAppManager.systemLanguage {
            case .english:
                languageName = .english
            case .chineseSimplified:
                languageName = .chineseSimplified
            default:
                languageName = .english
            }
            return languageName
        }
    }
    
}
