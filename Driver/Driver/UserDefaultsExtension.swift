//
//  UserDefaultsExtension.swift
//  Driver
//
//  Created by ZY on 2017-07-06.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

extension UserDefaults{
    static func isFirstRun() -> Bool {
        if let result = UserDefaults.standard.object(forKey: "IsFirstRun") as? Bool{
            return result
        }
        return true
    }
    static func setIsFirstRun(_ isFirstRun:Bool){
        UserDefaults.standard.setValue(isFirstRun, forKey: "IsFirstRun")
        UserDefaults.standard.synchronize()
    }
    static func getLastLoginUsername() -> String? {
        return UserDefaults.standard.value(forKey: "LastLoginUsername") as? String
    }
    static func setLastLoginUsername(username aUsername:String) {
        UserDefaults.standard.setValue(aUsername,
                                       forKey: "LastLoginUsername")
        UserDefaults.standard.synchronize()
    }
    static func getUserLocale() -> String {
        guard let localeId = UserDefaults.standard.value(forKey: "AppLocaleId") as? String else{
            return Locale.current.identifier
        }
        return localeId
    }
    static func setUserLocale(_ localeId:String) {
        UserDefaults.standard.setValue(localeId, forKey: "AppLocaleId")
        UserDefaults.standard.synchronize()
    }
    
    static func getsetReserveOrderPopupIsOn() -> Bool {
        guard let isOn = UserDefaults.standard.value(forKey: "ReserveOrderPopupIsOn") as? Bool else{
            return true
        }
        return isOn
    }
    
    static func setReserveOrderPopupIsOn(_ isOn:Bool) {
        UserDefaults.standard.setValue(isOn, forKey: "ReserveOrderPopupIsOn")
        UserDefaults.standard.synchronize()
    }
    
}
