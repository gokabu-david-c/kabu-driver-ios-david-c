//
//  GDAppManager.swift
//  Driver
//
//  Created by ZY on 2017-04-08.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation



class GDAppManager : NSObject {
    private let Key_AppAccessToken = "appaccesstoken";
    static let shared = GDAppManager()
    static let Mode_App_Production:String = "Prod"
    static let Mode_App_Development:String = "Dev"
    
    
    private var temporaryPopupDisableTimer:Timer?
    public var lastUpdatedTime : Int64?
    public var lastReservationPopupTime : Int64?
    public var lastNewReservationTime : Int64?

    
    private static var currentAppMode:String?
    var deviceToken = ""
    var locale = Locale.current.identifier
    class func clearData() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier ?? "")
        UserDefaults.standard.synchronize()
        let data = NSKeyedArchiver.archivedData(withRootObject: "")
        if let keychain:KeychainItemWrapper = KeychainItemWrapper(identifier: "AccountData", accessGroup: nil){
            keychain.setObject("MY_APP_CREDENTIALS", forKey: kSecAttrService)
            keychain.setObject(data, forKey: kSecAttrAccount)
        }
    }
    class func getUserDefaultsDataVersion() -> String {
        if let build = UserDefaults.standard.value(forKey: "build") as? String{
            return build
        }
        return ""
    }
    class func setUserDefaultsDataVersion() {
        UserDefaults.standard.set(GDAppManager.getBuild(),
                                  forKey: "build")
        UserDefaults.standard.synchronize()
    }
    class var isShadowEnabled:Bool{
        set{
            let userDefaults = UserDefaults.standard
            if(newValue == true){
                userDefaults.set("1", forKey: "Kabu.Driver.SystemSetting_IsShadowEnabled")
            }else{
                userDefaults.set("0", forKey: "Kabu.Driver.SystemSetting_IsShadowEnabled")
            }
            userDefaults.synchronize()
        }
        get{
            let isEnabled = UserDefaults.standard.object(forKey: "Kabu.Driver.SystemSetting_IsShadowEnabled") as? String
            if(isEnabled == nil){
                return true
            }
            if(isEnabled == "0"){
                return false
            }
            return true
        }
    }
    class var isDisplayingTrafficOnMap:Bool{
        set{
            let userDefaults = UserDefaults.standard
            userDefaults.set(newValue, forKey: "DisplayTrafficOnMap")
            userDefaults.synchronize()
        }
        get{
            let userDefaults = UserDefaults.standard
            if let shouldDisplay = userDefaults.object(forKey: "DisplayTrafficOnMap") as? Bool{
                return shouldDisplay
            }
            return false
        }
    }
    
     var isDisplayingNewReservationPopup:Bool{
        set{
            isTempDisableNewReservationPopup = false
            let userDefaults = UserDefaults.standard
            userDefaults.set(newValue, forKey: "DisplayNewReservationPopup")
            userDefaults.synchronize()
        }
        get{
            if(self.isTempDisableNewReservationPopup){
                return false
            }
            let userDefaults = UserDefaults.standard
            if let shouldDisplay = userDefaults.object(forKey: "DisplayNewReservationPopup") as? Bool{
                return shouldDisplay
            }
            return true
        }
    }
    
    var isTempDisableNewReservationPopupVar : Bool = false
     var isTempDisableNewReservationPopup:Bool{
        set{
            self.temporaryPopupDisableTimer?.invalidate()
            if(newValue == true){
                 self.temporaryPopupDisableTimer = Timer.scheduledTimer(withTimeInterval: Double(GDAppManager.lengthOfNewReservationPopup*60), repeats: false, block: {_ in
                    self.isTempDisableNewReservationPopupVar = false
                })
            }
            isTempDisableNewReservationPopupVar = newValue
        }
        get{
            
            return isTempDisableNewReservationPopupVar
        }
    }
    class var isDataSaverOn:Bool{
        set{
            let userDefaults = UserDefaults.standard
            userDefaults.set(newValue, forKey: "DataSaver")
            userDefaults.synchronize()
        }
        get{
            let userDefaults = UserDefaults.standard
            if let isOn = userDefaults.object(forKey: "DataSaver") as? Bool{
                return isOn
            }
            return false
        }
    }
    
    class var lengthOfNewReservationPopup:Int{
        set{
            let userDefaults = UserDefaults.standard
            userDefaults.set(newValue, forKey: "LengthNewReservationPopup")
            userDefaults.synchronize()
        }
        get{
            let userDefaults = UserDefaults.standard
            if let shouldDisplay = userDefaults.object(forKey: "LengthNewReservationPopup") as? Int{
                return shouldDisplay
            }
            return 5
        }
    }
    
    class var paymentMethod:PaymentMethod{
        set{
            let userDefaults = UserDefaults.standard
            userDefaults.set(newValue.rawValue, forKey: "PaymentMethod")
            userDefaults.synchronize()
        }
        get{
            let userDefaults = UserDefaults.standard
            if let payment = userDefaults.object(forKey: "PaymentMethod") as? String{
                return PaymentMethod(rawValue: payment)!
            }
            return .alipay
        }
    }
    
    class var isDevMode : Bool{
        #if targetEnvironment(simulator)
            return true
        #endif
        
        if let appMode = GDAppManager.currentAppMode{
            if(appMode == GDAppManager.Mode_App_Development){
                return true
            }
            return false
        }
        if let appMode = UserDefaults.standard.object(forKey: "AppMode") as? String{
            GDAppManager.currentAppMode = appMode
            if appMode == Mode_App_Development{
                return true
            }
        }
        return false
    }
    static var appMode:String{
        set{
            let oldValue = appMode
            if(newValue != Mode_App_Development
                && appMode != Mode_App_Production){
                GDAppManager.currentAppMode = Mode_App_Production
                UserDefaults.standard.set(Mode_App_Production, forKey: "AppMode")
                UserDefaults.standard.synchronize()
            }else{
                GDAppManager.currentAppMode = newValue
                UserDefaults.standard.set(newValue, forKey: "AppMode")
                UserDefaults.standard.synchronize()
            }
            if(newValue != oldValue){
                NotificationCenter.default.post(name: NSNotification.Name.KB_Debug_AppModeChanged,
                                                object: nil,
                                                userInfo: nil)
            }
        }
        get{
            if(self.currentAppMode == nil){
                if let appModeString = UserDefaults.standard.object(forKey: "AppMode") as? String{
                    GDAppManager.currentAppMode = appModeString
                }else{
                    GDAppManager.currentAppMode = Mode_App_Production
                }
            }
            return GDAppManager.currentAppMode!
        }
    }
    class func getVersion() -> String {
        if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return text
        }
        return ""
    }
    class func getBuild() -> String {
        if let text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            return text
        }
        return ""
    }
    func getURL_UpdateOrder(orderNumber aOrderNumber:String) -> String {
        var url = KBConfig.APIBaseURL //getAPIBaseURL()
        url += "orders/" + aOrderNumber + "?"
        url += Key_AppAccessToken + "=" + KBConfig.appAccessToken.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        return url
    }
    
    class var systemLanguage:LanguageCodes{
        set{
            let userDefaults = UserDefaults.standard
            userDefaults.set([newValue.rawValue], forKey: "AppleLanguages")
            userDefaults.synchronize()
        }
        get{
            let userDefaults = UserDefaults.standard
            if let languages = userDefaults.object(forKey: "AppleLanguages") as? [String]{
                switch languages[0] {
                case LanguageCodes.english.rawValue:
                    return .english
                case LanguageCodes.chineseSimplified.rawValue:
                    return .chineseSimplified
                default:
                    return .english
                }
            }
            return .english
        }
    }
    
    

}
