//
//  KBOpenOrdersManager.swift
//  Driver
//
//  Created by Kabu on 2017-11-17.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

struct NewOpenOrders {
    var orders : [KBOrder]?
    var lastCheckedTime : Date?
    var lastUpdatedTime :Date?
}


class KBOpenOrdersManager: NSObject {
    static let shared = KBOpenOrdersManager()
    var isDownloadingOpenOrders = false{
        didSet{
            if(isDownloadingOpenOrders != oldValue){
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name.KB_OpenOrders_Downloading_Status_Changed,
                                                    object: nil)
                }
            }
        }
    }
    var openOrdersLastUpdateTime:Date?{
        didSet{
            if(openOrdersLastUpdateTime != oldValue){
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name.KB_OpenOrders_Last_Update_Time_Changed,
                                                    object: nil)
                }
            }
        }
    }
    
//    var newOpenOrders:NewOpenOrders{
//        didSet{
//            if(newOpenOrders.lastCheckedTime != oldValue.lastCheckedTime){
//                DispatchQueue.main.async {
//                    NotificationCenter.default.post(name: NSNotification.Name.KB_OpenOrders_Last_Checked_Time_Changed,
//                                                object: self.newOpenOrders)
//                }
//            }
//
//        }
//    }
    
    var openOrders = [KBOrder](){
        didSet{
            var oldOrderIDs = ""
            //let sortedOldValue = oldValue.sorted { $0.orderNumber!.localizedCaseInsensitiveCompare($1.orderNumber!) == ComparisonResult.orderedAscending }
            for currentValue in oldValue{
                oldOrderIDs += currentValue.orderNumber!
                if(currentValue !== oldValue.last){
                    oldOrderIDs += ","
                }
            }
            var newOrderIDs = ""
            //let sortedOpenOrders = openOrders.sorted { $0.orderNumber!.localizedCaseInsensitiveCompare($1.orderNumber!) == ComparisonResult.orderedAscending }
//            self.newOpenOrders = NewOpenOrders(openOrders)
            for currentOrder in openOrders{
                newOrderIDs += currentOrder.orderNumber!
                if(currentOrder !== openOrders.last){
                    newOrderIDs += ","
                }
            }
            var hasNewOrder = false
            var hasNewInstantOrder = false
            var hasNewReservationOrder = false
            for currentOrder in openOrders {
                var containsCurrentOrder = false
                for currentOldOrder in oldValue{
                    if(currentOrder.orderNumber == currentOldOrder.orderNumber){
                        containsCurrentOrder = true
                        break
                    }
                }
                if(containsCurrentOrder == false){
                    hasNewOrder = true
                    if(currentOrder.isReservation){
                        hasNewReservationOrder = true
                    }else{
                        hasNewInstantOrder = true
                    }
                }
            }
            if(hasNewInstantOrder == true && hasNewReservationOrder == false){
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name.KB_OpenOrders_NewInstantOrder,
                                                    object: nil)
                }
            }else if(hasNewReservationOrder == true && hasNewInstantOrder == false){
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name.KB_OpenOrders_NewReservationOrder,
                                                    object: nil)
                }
            }else if(hasNewOrder == true){
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name.KB_OpenOrders_NewOrder,
                                                    object: nil)
                }
            }else if(oldOrderIDs != newOrderIDs){
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name.KB_OpenOrders_Changed,
                                                    object: nil)
                }
            }
            for currentOpenOrder in self.openOrders{
                for currentOldOpenOrder in oldValue{
                    if(currentOpenOrder.orderNumber != currentOldOpenOrder.orderNumber){
                        continue
                    }
                    if(currentOpenOrder != currentOldOpenOrder){
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name: NSNotification.Name.KB_OpenOrders_Data_Changed,
                                                            object: nil)
                        }
                    }
                }
            }
        }
    }
    override init() {
        super.init()
        NotificationCenter.addObserver(self,
                                       action: #selector(userLoggedOut),
                                       name: NSNotification.Name.GD_Me_LoggedOut)
        NotificationCenter.addObserver(self,
                                       action: #selector(locationUpdated),
                                       name: NSNotification.Name.GD_Location_LocationUpdated)
    }
    
    func downloadOpenOrders(start aStart:(() -> ())?,
                            success aSuccess:@escaping(() -> ()),
                            failure aFailure:@escaping((_ aError:NSError) -> ())){
        if KBAccountManager.shared.isLoggedIn() == false{
            self.openOrders.removeAll()
            aFailure(NSError.Error(message: NSLocalizedString("Please Login first", comment: "")))
            return
        }
        self.isDownloadingOpenOrders = true
        let prevAccount = KBAccountManager.shared.getAccount()
        let prevAccountUID = prevAccount!.id
        KBOrdersAPI.downloadOpenOrders(driverID: prevAccountUID,
                                       start: {
                                        aStart?()
        }, success: { (orders:[KBOrder]) in
            if let currentAccount = KBAccountManager.shared.getAccount(){
                if currentAccount.id == prevAccountUID{
                    var instantOrders = [KBOrder]()
                    var reservationOrders = [KBOrder]()
                    for currentOrder in orders {
                        if(currentOrder.isReservation == false){
                            instantOrders.append(currentOrder)
                        }else{
                            reservationOrders.append(currentOrder)
                        }
                    }
                    let sortedInstantOrders = instantOrders.sorted { $0.timeCreated < $1.timeCreated }
                    let sortedReservationOrders = reservationOrders.sorted { $0.reservationPickupTime < $1.reservationPickupTime }
                    let result = sortedInstantOrders + sortedReservationOrders
                    self.openOrders = result
                }
            }
            self.openOrdersLastUpdateTime = Date()
            self.isDownloadingOpenOrders = false
            aSuccess()
            NSLog("Download open orders success")
        }, failure: { (error:NSError) in
            self.isDownloadingOpenOrders = false
            aFailure(error)
            NSLog("Download open orders failure")
        })
    }
    @objc func userLoggedOut(){
        self.openOrders.removeAll()
    }
    @objc func locationUpdated(_ notification:NSNotification) {
        if(self.openOrders.count == 0){
            return
        }
        if GDLocationManager.shared.currentLocation == nil{
            return
        }
        for currentOrder in self.openOrders {
            let (shouldCount, distance, fee, prevLocation) = currentOrder.getAdditionalDistance()
            if(shouldCount == false){
                continue
            }
            if let currentLocation = GDLocationManager.shared.currentLocation{
                if(currentLocation.horizontalAccuracy > kCLLocationAccuracyKilometer){
                    return
                }
            }
            var newDistance = distance
            if prevLocation == nil{
                currentOrder.setAdditionalDistance(shouldCount: shouldCount,
                                                   distance: newDistance,
                                                   fee: fee,
                                                   prevLocation: GDLocationManager.shared.currentLocation?.stringValue())
                continue
            }
            let movedKM = Float(prevLocation!.distance(from: GDLocationManager.shared.currentLocation!)) / 1000.0
            newDistance += movedKM
            let newFee = round(newDistance * KBOrder.Value_AdditionalPrice_PerKM)
            currentOrder.setAdditionalDistance(shouldCount: shouldCount,
                                               distance: newDistance,
                                               fee: newFee,
                                               prevLocation: GDLocationManager.shared.currentLocation?.stringValue())
        }
    }
}
