//
//  GDDebugNavigationController.swift
//  Driver
//
//  Created by ZY on 2017-05-02.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDDebugNavigationController: UINavigationController {
    class func controller() -> GDDebugNavigationController {
        let controller = UIStoryboard(name: "Debug", bundle: nil).instantiateViewController(withIdentifier: "GDDebugNavigationController") as! GDDebugNavigationController
        return controller
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
