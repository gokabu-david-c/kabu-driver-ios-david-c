//
//  KBOrderHistoryVM.swift
//  Driver
//
//  Created by Kabu on 2017-11-16.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBOrderHistoryVM: NSObject {
    var pageIndex = -1
    var itemsPerPage = 20
    var orderCount = 0
    var total:Float = 0.0
    var commission:Float = 0.0
    var orders:[KBOrder] = []
    var dateList:[String] = []
    var sortedOrders = Dictionary<String, [KBOrder]>()
    var isDownloadingOrders = false
    var isErrorOccured = false
    func downloadOrders(_ startDate:String,
                        _ endDate:String,
                        _ shouldReset:Bool,
                        _ start:(() -> ())?,
                        _ success:@escaping(() -> ()),
                        _ failure:@escaping((_ error:NSError) -> ())){
        KBOrdersAPI.downloadClosedOrders(startDate: startDate,
                                         endDate: endDate,
                                         pageIndex: (shouldReset) ? 0 : self.pageIndex + 1,
                                         itemsPerPage: self.itemsPerPage,
                                         start: {
                                            self.isErrorOccured = false
                                            self.isDownloadingOrders = true
                                            start?()   
        }, success: { (orders:[KBOrder], count:Int, total:Float, commission:Float) in
            self.isErrorOccured = false
            self.isDownloadingOrders = false
            if(shouldReset == true){
                self.orders.removeAll()
                self.pageIndex = -1
            }
            self.orderCount = count
            self.total = total
            self.commission = commission
            self.orders += orders
            self.sortOrders()
            self.pageIndex += 1
            success()
        }) { (error:NSError) in
            self.isDownloadingOrders = false
            self.isErrorOccured = true
            failure(error)
        }
    }
    func sortOrders() {
        self.dateList.removeAll()
        self.sortedOrders.removeAll()
        if(self.orders.count == 0){
            return
        }
        for currentOrder in self.orders {
            let timeInterval = TimeInterval(currentOrder.timeCreated);
            let currentOrderDate = Date.ToDate(timeInterval).toString("yyyy-MM-dd")
            if(self.dateList.contains(currentOrderDate) == false){
                self.dateList.append(currentOrderDate)
            }
            var ordersForDate = self.sortedOrders[currentOrderDate]
            if(ordersForDate == nil){
                ordersForDate = [KBOrder]()
            }
            ordersForDate?.append(currentOrder)
            self.sortedOrders[currentOrderDate] = ordersForDate
        }
    }
}
