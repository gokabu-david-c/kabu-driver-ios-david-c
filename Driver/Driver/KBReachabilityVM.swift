//
//  KBReachabilityVM.swift
//  Driver
//
//  Created by Kabu on 2017-11-28.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBReachabilityVM: NSObject {
    //var reachabilityChangedHandler:((_ connection:Reachability.Connection?) -> ())?
    var connected:(() -> ())?
    var disconnected:(() -> ())?
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged),
                                               name: NSNotification.Name.KB_Reachability_Changed,
                                               object: nil)
        KBReachabilityManager.shared.start()
    }
   @objc func reachabilityChanged(){
        if(KBReachabilityManager.shared.isConnected){
            DispatchQueue.main.async{
                self.connected?()
            }
        }else{
            DispatchQueue.main.async{
                self.disconnected?()
            }
        }
    }
    /*func reachabilityChanged(note: Notification){
        if let reachability = note.object as? Reachability{
            switch reachability.connection {
            case .wifi:
                print("Reachable via WiFi")
                DispatchQueue.main.async {
                    self.connected?()
                }
            case .cellular:
                print("Reachable via Cellular")
                DispatchQueue.main.async {
                    self.connected?()
                }
            case .none:
                print("Network not reachable")
                DispatchQueue.main.async {
                    self.disconnected?()
                }
            }
        }
    }*/
}
