//
//  TurnByTurnVCDelegate.swift
//  Driver
//
//  Created by Jack Du on 2018-05-31.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

//import MapboxDirections
//import MapboxCoreNavigation
//import MapboxNavigation
//
//class MapBoxNavigationController: NavigationViewController {
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.delegate = self
//    }
//}
//
//extension MapBoxNavigationController : NavigationViewControllerDelegate{
//    @objc private func navigationViewController(_ navigationViewController : NavigationViewController, didArriveAt destination: MGLAnnotation)
//    {
//        print("MapBox Traveled: \(navigationViewController.routeController.routeProgress.distanceTraveled)")
//    }
//}
