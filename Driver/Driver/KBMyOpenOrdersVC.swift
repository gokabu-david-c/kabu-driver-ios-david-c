//
//  KBMyOpenOrdersVC.swift
//  Driver
//
//  Created by ZY on 2017-04-08.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Whisper

class KBMyOpenOrdersVC: KBViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: GDView!
    weak var parentVC: KBViewController?

    let openOrdersVM = KBOpenOrdersVM()
    var isReservationPoolTableViewScrolling = false
    var isTableViewCellSelected = false
    let refreshControl = UIRefreshControl()
    var shouldShowLoading = true
    class func controller() -> KBMyOpenOrdersVC {
        let controller = UIStoryboard(name: "MyOpenOrders", bundle: nil).instantiateViewController(withIdentifier: "KBMyOpenOrdersVC") as! KBMyOpenOrdersVC
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferLargeNavigationTitle(isPrefered: true)
        self.loadTableView()
        self.loadVM()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isReservationPoolTableViewScrolling = false
        self.isTableViewCellSelected = false
        self.reloadTableView(animated: false)
        if(self.shouldShowLoading == true){
            self.containerView.showLoading()
            self.shouldShowLoading = false
        }
        self.downloadOpenOrders()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    @objc func applicationDidBecomeActive(){
        self.downloadOpenOrders()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    func loadTableView(){
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 80
        self.refreshControl.addTarget(self,
                                      action: #selector(downloadOpenOrders),
                                      for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl)
    }
    func loadVM(){
        self.openOrdersVM.lastUpdateTimeChanged = self.updateLastUpdateTime
        self.openOrdersVM.newInstantOrderAdded = {
            self.reloadTableView(animated: true)
            self.updateView()
        }
        self.openOrdersVM.newReservationOrderAdded = {
            self.reloadTableView(animated: true)
            self.updateView()
        }
        self.openOrdersVM.openOrdersChanged = {
            self.reloadTableView(animated: true)
            self.updateView()
        }
        self.openOrdersVM.newOrderAdded = {
            self.reloadTableView(animated: true)
            self.updateView()
        }
        //self.openOrdersVM.openOrdersDataChanged = self.updateView
        self.openOrdersVM.downloadingStatusChanged = {
            self.updateView()
        }
    }
    @objc func downloadOpenOrders(){
        self.openOrdersVM.downloadOpenOrders(start: {
            
        }, success: {
            self.updateView()
            self.reloadTableView(animated: true)
        }) { (error:NSError) in
            self.refreshControl.endRefreshing()
            self.showError(error.message)
        }
        self.refreshControl.endRefreshing()

    }
    func updateView(){
        if(self.openOrdersVM.openOrders.count == 0){
            self.containerView.showMessage(self.openOrdersVM.openOrderStatusMessage ?? NSLocalizedString("You don't have any order right now", comment: ""))
        }else{
            if(self.tableView.isHidden){
                self.tableView.isHidden = false
            }
            self.containerView.showContent(self.tableView)
        }
    }
    func updateLastUpdateTime(){
        if(self.openOrdersVM.lastUpdateTime == nil){
            tableView.headerView(forSection: 0)?.textLabel?.text = ""
            return
        }
        tableView.headerView(forSection: 0)?.textLabel?.text = NSLocalizedString("Updated on", comment: "") + " " + (self.openOrdersVM.lastUpdateTime?.toString("yyyy-MM-dd HH:mm    "))!
    }
    // MARK: - UPDATE VIEW
    func reloadTableView(animated aAnimated:Bool){
        if(self.isReservationPoolTableViewScrolling == true){
            return
        }
        if(self.isTableViewCellSelected == true){
            return
        }
        if(aAnimated == false){
            self.tableView.reloadData()
            return
        }
        if(self.tableView.numberOfSections == 0 || self.tableView.numberOfRows(inSection: 0) == self.openOrdersVM.openOrders.count){
            self.tableView.reloadData()
        }else{
            let range = NSMakeRange(0, 1)
            let sections = NSIndexSet(indexesIn: range)
            self.tableView.beginUpdates()
            self.tableView.reloadSections(sections as IndexSet, with: .automatic)
            self.tableView.endUpdates()
        }
    }
}
extension KBMyOpenOrdersVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.openOrdersVM.openOrders.count == 0){
            return 1
        }
        return self.openOrdersVM.openOrders.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(self.openOrdersVM.openOrders.count == 0){
            var cell = tableView.dequeueReusableCell(withIdentifier: GDCenterTextTableViewCell.cellIdentifier) as? GDCenterTextTableViewCell
            if(cell == nil){
                cell = GDCenterTextTableViewCell.cell()
            }
            cell?.label.text = NSLocalizedString("You don't have any order right now", comment: "")
            return cell!
        }
        let order = self.openOrdersVM.openOrders[indexPath.row]
        var cell = tableView.dequeueReusableCell(withIdentifier: KBMyOpenOrdersTableViewCell.reuseIdentifier) as? KBMyOpenOrdersTableViewCell
        if(cell == nil){
            cell = KBMyOpenOrdersTableViewCell.cell()
        }
        cell?.setOrder(orderNumber: order.orderNumber!)
        cell?.delegate = self
        cell?.setNeedsLayout()
        cell?.layoutIfNeeded()
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(self.openOrdersVM.openOrders.count == 0){
            return
        }
        let order = self.openOrdersVM.openOrders[indexPath.row]
        if(order.isReservation == true && order.reservationPickupTime - Int64(Date().timeIntervalSince1970) > 1800){
            let murmur = Murmur.init(title: NSLocalizedString("Function disabled until within 30 minutes of the order", comment: "") ,
                                     backgroundColor: .Danger,
                                     titleColor: .white,
                                     font: UIFont.systemFont(ofSize: 13),
                                     action: {
            })
            // Show and hide a message after delay
            Whisper.show(whistle: murmur, action: .show(2.5))
            if let cell = tableView.cellForRow(at: indexPath) as? KBMyOpenOrdersTableViewCell{
                cell.startShakeAnimation()
            }
            return
        }
        let navController = KBOpenOrderMapNavigationController.controller(orderNumber: order.orderNumber ?? "")
        self.present(navController, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(self.openOrdersVM.lastUpdateTime == nil){
            return ""
        }
        return NSLocalizedString("Updated on", comment: "") + " " + (self.openOrdersVM.lastUpdateTime?.toString("yyyy-MM-dd HH:mm    "))!
    }
}
extension KBMyOpenOrdersVC : UIScrollViewDelegate{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if(scrollView === self.tableView){
            self.isReservationPoolTableViewScrolling = true
        }
        if(scrollView.contentOffset.y > 0){
            preferLargeNavigationTitle(isPrefered: false)
        }
        else{
            preferLargeNavigationTitle(isPrefered: true)
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if(scrollView === self.tableView){
            if(decelerate == false){
                self.isReservationPoolTableViewScrolling = false
            }
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView === self.tableView){
            self.isReservationPoolTableViewScrolling = false
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.contentOffset.y > 20){
            preferLargeNavigationTitle(isPrefered: false)
        }
        else{
            preferLargeNavigationTitle(isPrefered: true)
        }
    }
    
}
extension KBMyOpenOrdersVC : KBMyOpenOrdersTableViewCellDelegate{
    func KBMyOpenOrdersTableViewCellDidTapPhone(_ sender: KBMyOpenOrdersTableViewCell) {
        if let orderNumber = sender.orderNumber{
            if let order = self.openOrdersVM.getOrder(orderNumber: orderNumber){
                if let phone = order.contactPhone{
                    if let url = NSURL(string: "tel://" + phone){
                        if(UIApplication.shared.canOpenURL(url as URL)){
                            UIApplication.shared.open(url as URL,
                                                      options: [:],
                                                      completionHandler: { (_ complete:Bool) in
                            })
                        }
                    }
                }
            }
        }
    }
}
