//
//  GDResetPasswordTableViewController.swift
//  Driver
//
//  Created by ZY on 2017-04-09.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDResetPasswordTableViewController: GDTableViewController, GDTextFieldTableViewCellDelegate, GDButtonTableViewCellDelegate {
    
    var driverID:String = ""
    var phone:String = ""
    var cell_DriverID:GDTextFieldTableViewCell?
    var cell_Phone:GDTextFieldTableViewCell?
    var cell_OK:GDButtonTableViewCell?
    
    class func controller() -> GDResetPasswordTableViewController{
        let controller = UIStoryboard(name: "ResetPassword", bundle: nil)
            .instantiateViewController(withIdentifier: "GDResetPasswordTableViewController") as! GDResetPasswordTableViewController
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 60
    }

    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.dismissKeyboard()
        self.dismiss(animated: true) {
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return 2
        }
        else if(section == 1){
            return 1
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            if(indexPath.row == 0){
                var cell = tableView.dequeueReusableCell(withIdentifier: GDTextFieldTableViewCell.cellIdentifier) as? GDTextFieldTableViewCell
                if(cell == nil){
                    cell = GDTextFieldTableViewCell.cell()
                }
                cell?.setupCell(NSLocalizedString("Driver Account Name", comment: ""), self.driverID, self)
                cell?.tf_Content.placeholder = NSLocalizedString("Please enter Account Name", comment: "")
                self.cell_DriverID = cell
                return cell!
            }
            if(indexPath.row == 1){
                var cell = tableView.dequeueReusableCell(withIdentifier: GDTextFieldTableViewCell.cellIdentifier) as? GDTextFieldTableViewCell
                if(cell == nil){
                    cell = GDTextFieldTableViewCell.cell()
                }
                cell?.setupCell(NSLocalizedString("Phone Number", comment: "") + " +1", self.phone, self)
                cell?.tf_Content.placeholder = "123-456-7890"
                cell?.tf_Content.keyboardType = UIKeyboardType.phonePad
                self.cell_Phone = cell
                return cell!
            }
        }
        else if(indexPath.section == 1){
            if(indexPath.row == 0){
                var cell = tableView.dequeueReusableCell(withIdentifier: GDButtonTableViewCell.cellIdentifier) as? GDButtonTableViewCell
                if(cell == nil){
                    cell = GDButtonTableViewCell.cell(NSLocalizedString("Confirm", comment: ""), self)
                }
                self.cell_OK = cell
                return cell!
            }
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.section == 0 && indexPath.row == 0){
            if(self.cell_DriverID != nil){
                self.cell_DriverID?.tf_Content.becomeFirstResponder()
            }
        }
        else if(indexPath.section == 0 && indexPath.row == 1){
            if(self.cell_Phone != nil){
                self.cell_Phone?.tf_Content.becomeFirstResponder()
            }
        }
    }
    
    // MARK: - GDTextFieldTableViewCellDelegate
    func GDTextFieldTableViewCellValueChanged(_ sender: GDTextFieldTableViewCell) {
        if(sender == self.cell_DriverID){
            if let text = self.cell_DriverID?.tf_Content.text{
                self.driverID = text
            }
        }
        else if(sender == self.cell_Phone){
            if let text = self.cell_Phone?.tf_Content.text{
                self.phone = text
            }
        }
    }
    
    // MARK: - GDButtonTableViewCellDelegate
    func GDButtonTableViewCellTapped(_ sender: GDButtonTableViewCell) {
        if(self.driverID.count == 0){
            self.showError(NSLocalizedString("Please enter Driver Account Name", comment: ""))
            return
        }
        if(self.phone.count == 0){
            self.showError(NSLocalizedString("Please enter Phone Number", comment: ""))
            return
        }
        self.dismissKeyboard()
        KBAccountManager.shared.resetPassword(self.driverID,
                                              self.phone,
                                              start: {
                                                self.showLoading("")
        }, success: { (message:String) in
            self.dismiss(animated: true, completion: {
            })
            self.showSuccess(message)
        }) { (error:NSError) in
            self.showError(error.message)
        }
    }
    
}
