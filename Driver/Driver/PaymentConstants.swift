//
//  PaymentConstants.swift
//  Driver
//
//  Created by Jack Du on 2018-04-24.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//


enum PaymentMethod : String{
    case creditcard = "creditcard"
    case wechat = "wechat"
    case alipay = "alipay"
}

enum PaymentMethodTag : Int{
    case creditcard = 1
    case wechat = 2
    case alipay = 3
}


extension UIColor {
    struct payment {
        static var creditcard: UIColor  { return UIColor(red: 0.92, green: 0.71, blue: 0.26, alpha: 1) }
        static var wechat: UIColor { return UIColor(red: 0.24, green: 0.69, blue: 0.31, alpha: 1) }
        static var alipay: UIColor { return UIColor(red: 0.10, green: 0.63, blue: 0.93, alpha: 1) }
    }
}
