//
//  KBAppHelper.swift
//  Driver
//
//  Created by Kabu on 2017-11-27.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import UserNotifications

class KBAppHelper: NSObject {
    static var displayName:String{
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String
        /*
        if let displayName =  Bundle.main.localizedInfoDictionary?["CFBundleDisplayName"] as? String{
            return displayName
        }
        return Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String*/
    }
    static func isPushNotificationEnabled() -> Bool{
        let group = DispatchGroup()
        group.enter()
        let notificationCenter = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound, .badge];
        var enabled = false
        notificationCenter.requestAuthorization(options: options) { (granted, error) in
            enabled = granted
            group.leave()
        }
        group.wait()
        return enabled
    }
    static func isLocationServiceEnabled() -> Bool{
        if CLLocationManager.locationServicesEnabled(){
            switch(CLLocationManager.authorizationStatus()){
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            }
        }
        else{
            return false
        }
    }
}
