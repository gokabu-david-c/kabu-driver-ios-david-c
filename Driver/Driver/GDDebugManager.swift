//
//  GDDebugHelper.swift
//  Driver
//
//  Created by ZY on 2017-06-16.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

class GDDebugManager : NSObject {

    static let shared = GDDebugManager()
    
    var enteredPassword = ""
    
    func isLocked() -> Bool {
    
        if let account = KBAccountManager.shared.getAccount()
        {
            if(account.id == 1
                || account.id == 39
                || account.id == 42
                || account.id == 120
                || account.id == 141
                || account.id == 162
                || account.id == 171
                || account.id == 205
                || account.id == 245
                || account.id == 868)
            {
                
                return false
            }
        }

        return !(self.getDebugPageLoginPassword() == enteredPassword)
        
    }
    
    func getDebugPageLoginPassword() -> String {
//        let day = Date().day()
        let month = Date().month()
        let year = Date().year() as Int
        let v = String(Int64(month + 899) * Int64(year + 1987) * 8 * 9 * 6)
        
        
        var password = "";
        for i in 0...v.count - 1
        {
            if(i % 2 == 0)
            {
                continue
            }
            
            let start = v.index(v.startIndex, offsetBy: i)
            let end = v.index(v.startIndex, offsetBy: i + 1)
            let range = start..<end
            password += String(v.substring(with: range))
            
            if(password.count >= 6)
            {
                break
            }
        }
        
        // MARK: Dev password
        print(password)
        
        return password
        
    }
    
}

