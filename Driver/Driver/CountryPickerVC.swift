//
//  CountryPickerVC.swift
//  Driver
//
//  Created by Jack Du on 2018-05-03.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//


class CountryPickerVC : UIViewController {
    
    
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var confirmButton: UIButton!
    @IBOutlet var topView: GDView!
    
    var parentVC:GDSettingsTableViewController?
    
    let systemSettingsVM = KBSystemSettingsVM()
    var labelList : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData();
        setDefaultPickerIndex()
    }
    
    @IBAction func confirm(_ sender: Any) {
        let row = pickerView.selectedRow(inComponent: 0)
        KBSystemSettingsVM().defaultLanguage = LanguageNames(rawValue: labelList[row])!
        parentVC?.reloadCountry()
        self.topView.showLoading()
         KBScreenHelper.shared.showClearController()
        Timer.scheduledTimer(withTimeInterval: 1,
                             repeats: false,
                             block: { (timer:Timer) in
                                    KBScreenHelper.shared.shouldForceReload = true
                                    KBScreenHelper.shared.showMainController()
        })
        
    }
    
    func setupData(){
        labelList = [LanguageNames.chineseSimplified.rawValue, LanguageNames.english.rawValue]
        pickerView.reloadAllComponents()
    }
    
    func setDefaultPickerIndex(){
        var index = 0
        let language = KBSystemSettingsVM().defaultLanguage
        switch language {
        case .chineseSimplified:
            index = 0
        case .english:
            index = 1
        default:
            index = 0
        }
        
//        for i in 0..<labelList.count
//        {
//            if(language == labelList[i]){
//                index = i
//                break
//            }
//        }
        pickerView.selectRow(index, inComponent: 0, animated: false)
    }
    
    
}

extension CountryPickerVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return labelList[row]
    }
    
}
