//
//  KBBillManager.swift
//  Driver
//
//  Created by Kabu on 2017-11-03.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

enum BillingStatus{
    case owedNothing, owedMoney, owedOverdue, accountLock
}

class KBBillManager: NSObject {
    static let shared = KBBillManager()
    func getUnpaidBills(start aStart:(() -> ())?,
                        success aSuccess:@escaping(_ bills:[KBDriverBill]) -> (),
                        failure aFailure:@escaping(_ error:NSError) -> ()){
        KBBillAPI.getUnpaidBill(start: {
            aStart?()
        }, success: { (bills:[KBDriverBill]) in
            aSuccess(bills)
        }) { (error:NSError) in
            aFailure(error)
        }
    }
    
    func getBillinStatus(start aStart:(() -> ())?,
                         success aSuccess:@escaping(_ totalOwed:Float, _ billingStatus:BillingStatus) -> (),
                        failure aFailure:@escaping(_ error:NSError) -> ()){
        KBBillAPI.getUnpaidBill(start: {
            aStart?()
        }, success: {
            (bills:[KBDriverBill]) in
            
            if(bills.count < 1){
                aSuccess(0, .owedNothing)
            }
            let total = self.getAmountOwed(bills)
            print(total)
            if(total <= 0){
                aSuccess(total, .owedNothing)
            }
            else{
                let deadline = self.getEarliestDeadline(bills)
                let currentDate = Date()
                let currentTimestamp = Int64(currentDate.timeIntervalSince1970)
                if(deadline != Int64.max && deadline < currentTimestamp){
                    aSuccess(total, .owedOverdue)
                }
                else{
                    aSuccess(total, .owedMoney)
                }
            }
        }) { (error:NSError) in
            aFailure(error)
        }
    }
    
    func getAmountOwed(_ bills:[KBDriverBill]) -> Float{
        var total:Float = 0
        for bill in bills{
            total += bill.amount
        }
        if(total>0){
            total += KBConfig.processingFee
        }
        return total
    }
    
    func getEarliestDeadline(_ bills:[KBDriverBill]) -> Int64{
        var deadline = Int64.max
        for currentBill in bills{
            if(currentBill.amount > 0 && currentBill.deadline != 0 && currentBill.deadline < deadline){
                deadline = currentBill.deadline
            }
        }
        return deadline
    }
    
    func shouldShowPayBill (start aStart:(() -> ())?,
                success aSuccess:@escaping(_ shouldShowPayBill:Bool) -> (),
                failure aFailure:@escaping(_ error:NSError) -> ()){
        self.getBillinStatus(start:{},
                success:{(totalOwed:Float, billingStatus:BillingStatus) in
                    switch billingStatus {
                    case .owedOverdue:
                        aSuccess(true)
                    case .accountLock:
                        aSuccess(true)
                    default:
                        aSuccess(false)

                    }
                },
                failure:{(error:NSError) in
                    aFailure(error)})
        }
}

// Alipay and Wechat Pay functions
extension KBBillManager {
    
    func getAlipayOrderInfo(start aStart:(() -> ())?,
                            success aSuccess:@escaping(_ orderInfo:String) -> (),
                            failure aFailure:@escaping(_ error:NSError) -> ()){
    
        KBBillAPI.getAlipayInfo(start: {
            aStart?()
        }, success: { (orderInfo:String) in
            aSuccess(orderInfo)
        }) { (error:NSError) in
            aFailure(error)
        }
    }
    
    func getWechatpayOrderInfo(start aStart:(() -> ())?,
                               success aSuccess:@escaping(_ orderInfo:PayReq) -> (),
                               failure aFailure:@escaping(_ error:NSError) -> ()){
        
        KBBillAPI.getWechatpayInfo(start: {
            
            aStart?()
        }, success: { (orderInfo:PayReq) in
            aSuccess(orderInfo)
        }) { (error:NSError) in
            aFailure(error)
        }
    }
    
}

