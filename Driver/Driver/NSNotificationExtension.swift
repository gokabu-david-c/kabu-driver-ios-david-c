//
//  NSNotificationExtension.swift
//  Driver
//
//  Created by ZY on 2017-04-09.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation

extension NSNotification
{
    static let Action_Orders_NewOrder = "com.gokabu.orders.neworder"
    static let Action_Orders_OrderCanceled = "com.gokabu.orders.ordercanceled"
    
    static let Key_UserInfo_Message = "message"
    
    func message() -> String {
        
        var message = ""
        
        if let messageData = self.userInfo?[NSNotification.Key_UserInfo_Message] as? String {
            
            message = messageData
            
        }
        
        return message
    }
    
    
    func data() -> AnyObject? {
        
        if let data = self.userInfo?[NSNotification.Key_UserInfo_Message]
        {
            return data as AnyObject
        }
        
        return nil
    }
}

extension Notification.Name {
    static let KB_OpenOrders_Changed = Notification.Name("KB.OpenOrders.Changed")
    static let KB_OpenOrders_NewOrder = Notification.Name("KB.OpenOrders.NewOrder") //trigger when mutiple new orders mixed of instant & reservation
    static let KB_OpenOrders_NewInstantOrder = Notification.Name("KB.OpenOrders.NewInstatntOrder") //trigger when only new instant orders
    static let KB_OpenOrders_NewReservationOrder = Notification.Name("KB.OpenOrders.NewReservationOrder") //trigger when only new reservation orders
    static let KB_OpenOrders_Data_Changed = Notification.Name("KB.OpenOrders.DataChanged")
    static let KB_ReservationPool_OrderChanged = Notification.Name("KB.ReservationPool.OrderChanged")
    static let KB_ReservationPool_OrderDataChanged = Notification.Name("KB.ReservationPool.OrderDataChanged")
    static let KB_ReservationPool_DownloadingStatusChanged = Notification.Name("KB.ReservationPool.DownloadingStatusChanged")
    static let KB_ReservationPool_LastUpdateTimeChanged = Notification.Name("KB.ReservationPool.LastUpdateTimeChanged")
    static let KB_ReservationPool_ErrorChanged = Notification.Name("KB.ReservationPool.ErrorChanged")
    static let KB_OpenOrders_Last_Update_Time_Changed = Notification.Name("KB.OpenOrders.LastUpdateTimeChanged")
    static let KB_OpenOrders_Downloading_Status_Changed = Notification.Name("KB.OpenOrders.DownloadingStatusChanged")
    static let KB_OpenOrders_Last_Checked_Time_Changed = Notification.Name("KB.OpenOrders.LastUpdateTimeChanged")
    static let GD_Location_LocationUpdated = Notification.Name("GD.Location.LocationUpdated")
    static let GD_Location_HeadingUpdated = Notification.Name("GD.Location.HeadingUpdated")
    static let GD_Me_LoggedIn = Notification.Name("GD.Me.LoggedIn")
    static let GD_Me_LoggedOut = Notification.Name("GD.Me.LoggedOut")
    static let GD_Me_Changed = Notification.Name("GD.Me.Changed")
    static let KB_Me_Online_Status_Changed = Notification.Name("GD.Me.Online_Status_Changed")
    static let KB_Reachability_Changed = Notification.Name("GD.Reachability.Changed")
    static let KB_Debug_AppModeChanged = Notification.Name("KB.Debug.AppModeChanged")
    static let KB_Return_From_Alipay = Notification.Name("KB.Payment.ReturnFromAlipay")

}
