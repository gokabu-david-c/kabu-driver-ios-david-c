//
//  GDMainViewController.swift
//  Driver
//
//  Created by ZY on 2017-04-08.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBOpenOrdersVC: KBViewController {
    let debugVM = KBDebugVM()
    @IBOutlet weak var mainSegmentedControlContainerView: UIView!
    let mainSegmentedControl = HMSegmentedControl.init()
    var pageViewController:UIPageViewController!
    var pageViewControllerPendingViewController:UIViewController?
    var myOpenOrdersVC = UIStoryboard(name: "MyOpenOrders", bundle: nil).instantiateInitialViewController()!
//    var reservationPoolVC = KBReservationPoolVC.controller()
    var reservationPoolVC = UIStoryboard(name: "ReservationPool", bundle: nil).instantiateInitialViewController()!
    var rightButtonTapCount = 0
    let reservationPoolVM = KBReservationPoolVM()
    class func controller() -> KBOpenOrdersVC {
        let controller = UIStoryboard(name: "OpenOrders", bundle: nil).instantiateViewController(withIdentifier: "KBOpenOrdersVC") as! KBOpenOrdersVC
        return controller
    }
    override func viewDidLoad() {

        super.viewDidLoad()
        self.hideNavigationBarBorderLine()
        self.loadVM()
        self.loadSegmentedControl()
        self.loadPageViewController()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

//        //if(GDAppManager.isDevMode()){
//        if(self.debugVM.isDevMode == true){
//            self.title = "当前订单(开发)"
//        }else{
//            self.title = "当前订单"
//        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadVM(){
        self.reservationPoolVM.ordersChanged = {
            if(self.reservationPoolVM.orders.count == 0){
                self.mainSegmentedControl.sectionTitles = [NSLocalizedString("Current", comment: ""), NSLocalizedString("Reserved", comment: "")]
            }else{
                self.mainSegmentedControl.sectionTitles = [NSLocalizedString("Current", comment: ""),NSLocalizedString("Reserved", comment: "") + "(" + String(self.reservationPoolVM.orders.count) + ")"]
            }
        }
    }
    func loadSegmentedControl(){
        self.mainSegmentedControlContainerView.addSubview(self.mainSegmentedControl)
        self.mainSegmentedControl.selectionIndicatorHeight = 2.0
        self.mainSegmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        self.mainSegmentedControl.selectionStyle = HMSegmentedControlSelectionStyle.box
        self.mainSegmentedControl.selectionIndicatorBoxColor = UIColor.clear
        self.mainSegmentedControl.titleTextAttributes = [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 14),
                                                         NSAttributedStringKey.foregroundColor:UIColor.lightGray]
        self.mainSegmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 14),
                                                                 NSAttributedStringKey.foregroundColor:UIColor.black]
        self.mainSegmentedControl.selectionIndicatorColor = UIColor.black
        self.mainSegmentedControl.addTarget(self, action: #selector(segmentedControlChangedValue(sender:)), for: UIControlEvents.valueChanged)
        self.mainSegmentedControl.translatesAutoresizingMaskIntoConstraints = false
        self.mainSegmentedControl.sectionTitles = [NSLocalizedString("Current", comment: ""),NSLocalizedString("Reserved", comment: "")]
        self.mainSegmentedControl.selectedSegmentIndex = 0
        self.mainSegmentedControl.superview?.addConstraint(NSLayoutConstraint.init(item: self.mainSegmentedControl,
                                                                                   attribute: NSLayoutAttribute.left,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.mainSegmentedControl.superview,
                                                                                   attribute: NSLayoutAttribute.left,
                                                                                   multiplier: 1,
                                                                                   constant: 0))
        self.mainSegmentedControl.superview?.addConstraint(NSLayoutConstraint.init(item: self.mainSegmentedControl,
                                                                                   attribute: NSLayoutAttribute.right,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.mainSegmentedControl.superview,
                                                                                   attribute: NSLayoutAttribute.right,
                                                                                   multiplier: 1,
                                                                                   constant: 0))
        self.mainSegmentedControl.superview?.addConstraint(NSLayoutConstraint.init(item: self.mainSegmentedControl,
                                                                                   attribute: NSLayoutAttribute.top,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.mainSegmentedControl.superview,
                                                                                   attribute: NSLayoutAttribute.top,
                                                                                   multiplier: 1,
                                                                                   constant: 0))
        self.mainSegmentedControl.superview?.addConstraint(NSLayoutConstraint.init(item: self.mainSegmentedControl,
                                                                                   attribute: NSLayoutAttribute.bottom,
                                                                                   relatedBy: NSLayoutRelation.equal,
                                                                                   toItem: self.mainSegmentedControl.superview,
                                                                                   attribute: NSLayoutAttribute.bottom,
                                                                                   multiplier: 1,
                                                                                   constant: 0))
    }
    func loadPageViewController(){
        self.pageViewController = UIPageViewController(transitionStyle: .scroll,
                                                       navigationOrientation: .horizontal,
                                                       options: nil)
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        self.addChildViewController(self.pageViewController)
        self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.view.superview?.addConstraint(NSLayoutConstraint.init(item: self.pageViewController.view,
                                                                                      attribute: NSLayoutAttribute.left,
                                                                                      relatedBy: NSLayoutRelation.equal,
                                                                                      toItem: self.pageViewController.view.superview,
                                                                                      attribute: NSLayoutAttribute.left,
                                                                                      multiplier: 1.0,
                                                                                      constant: 0))
        self.pageViewController.view.superview?.addConstraint(NSLayoutConstraint.init(item: self.pageViewController.view,
                                                                                      attribute: NSLayoutAttribute.right,
                                                                                      relatedBy: NSLayoutRelation.equal,
                                                                                      toItem: self.pageViewController.view.superview,
                                                                                      attribute: NSLayoutAttribute.right,
                                                                                      multiplier: 1.0,
                                                                                      constant: 0))
        self.pageViewController.view.superview?.addConstraint(NSLayoutConstraint.init(item: self.pageViewController.view,
                                                                                      attribute: NSLayoutAttribute.top,
                                                                                      relatedBy: NSLayoutRelation.equal,
                                                                                      toItem: self.pageViewController.view.superview,
                                                                                      attribute: NSLayoutAttribute.top,
                                                                                      multiplier: 1.0,
                                                                                      constant: -40))

        self.pageViewController.view.superview?.addConstraint(NSLayoutConstraint.init(item: self.pageViewController.view,
                                                                                      attribute: NSLayoutAttribute.bottom,
                                                                                      relatedBy: NSLayoutRelation.equal,
                                                                                      toItem: self.pageViewController.view.superview,
                                                                                      attribute: NSLayoutAttribute.bottom,
                                                                                      multiplier: 1.0,
                                                                                      constant: 84))
        self.pageViewController.didMove(toParentViewController: self)
        self.pageViewController.setViewControllers([self.myOpenOrdersVC],
                                                   direction: UIPageViewControllerNavigationDirection.forward,
                                                   animated: false,
                                                   completion: nil)
    }
    @objc func segmentedControlChangedValue(sender aSender:HMSegmentedControl) {
        if(aSender == self.mainSegmentedControl){
            if(aSender.selectedSegmentIndex == 0){

                self.pageViewController.setViewControllers([self.myOpenOrdersVC],
                                                           direction: UIPageViewControllerNavigationDirection.reverse,
                                                           animated: true,
                                                           completion: nil)
            }else if(aSender.selectedSegmentIndex == 1){
                self.pageViewController.setViewControllers([self.reservationPoolVC],
                                                           direction: UIPageViewControllerNavigationDirection.forward,
                                                           animated: true,
                                                           completion: nil)
            }
        }
    }
    @IBAction func rightButtonTapped(_ sender: Any) {
        self.rightButtonTapCount += 1
        if(self.rightButtonTapCount >= 5){
            self.rightButtonTapCount = 0;
            if(GDDebugManager.shared.isLocked() == false){
                let controller = GDDebugNavigationController.controller()
                self.present(controller,
                             animated: true,
                             completion: {
                })
                return
            }
            //1. Create the alert controller.
            let alert = UIAlertController(title: NSLocalizedString("Kabu Ride Service", comment: ""), message: NSLocalizedString("Please Enter Password", comment: ""), preferredStyle: .alert)
            //2. Add the text field. You can configure it however you need.
            alert.addTextField { (textField) in
            }
            alert.textFields?[0].isSecureTextEntry = true
            // 3. Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .default, handler: { [weak alert] (_) in
                let password = alert?.textFields![0].text // Force unwrapping because we know it exists.
                if(password != GDDebugManager.shared.getDebugPageLoginPassword()){
                    self.showError(NSLocalizedString("The Password is Incorrect", comment: ""))
                    return
                }
                GDDebugManager.shared.enteredPassword = password!
                let controller = GDDebugNavigationController.controller()
                self.present(controller,
                             animated: true,
                             completion: {
                })
            }))
            // 3. Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel))
            // 4. Present the alert.
            self.present(alert, animated: true, completion: nil)
        }
    }
    override var prefersStatusBarHidden: Bool{
        return false
    }
    
    @objc func applicationDidBecomeActive(){
        
    }
}
extension KBOpenOrdersVC : UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController === self.myOpenOrdersVC{
            return nil
        }
        return self.myOpenOrdersVC
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController === self.reservationPoolVC{
            return nil
        }
        return self.reservationPoolVC
    }
    
}
extension KBOpenOrdersVC : UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.pageViewControllerPendingViewController = pendingViewControllers.first
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if(completed == true){
            if(self.pageViewControllerPendingViewController === self.myOpenOrdersVC){
                self.mainSegmentedControl.selectedSegmentIndex = 0
            }else if(self.pageViewControllerPendingViewController === self.reservationPoolVC){
                self.mainSegmentedControl.selectedSegmentIndex = 1
            }
        }
    }
}





