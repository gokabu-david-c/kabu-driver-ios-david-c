//
//  GDMapView.swift
//  Driver
//
//  Created by ZY on 2017-05-27.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import GoogleMaps

class GDMapView: UIView, GMSMapViewDelegate {
    let debugVM = KBDebugVM()
    @IBOutlet weak var resetLocationContainerView: UIView!
    @IBOutlet weak var resetLocationButton: UIButton!
    
    
    @IBOutlet weak var trafficContainerView: UIView!
    @IBOutlet weak var trafficImageView: UIImageView!
    @IBOutlet weak var trafficButton: UIButton!
    
    @IBOutlet weak var stopNavigationContainerView: UIView!
    @IBOutlet weak var stopNavigationButton: UIButton!
    
    
    @IBOutlet weak var mapView: GMSMapView!
    
    weak var delegate:AnyObject?
    
    
    private var currentMapViewPath:GMSPolyline?
    var isNavigationEnabled = false
    var isAutoHeading = true
    var isAutoLocation = true
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func view() -> GDMapView{
        
        let view = Bundle.main.loadNibNamed("GDMapView",
                                            owner: self,
                                            options: nil)?.first as! GDMapView
        return view
     
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.resetLocationContainerView.clipsToBounds = true
        
        self.trafficContainerView.clipsToBounds = true
        
        self.stopNavigationContainerView.clipsToBounds = true
    }
    
    func viewWillAppear()
    {   
        self.mapView.isTrafficEnabled = self.debugVM.isDisplayingTrafficOnMap
        self.trafficImageView.image = (self.debugVM.isDisplayingTrafficOnMap == true)
            ? UIImage.init(named: "ic-traffic-light-1")
            : UIImage.init(named: "ic-traffic-light-2")
        return
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.resetLocationContainerView.layer.cornerRadius = self.resetLocationContainerView.frame.size.height * 0.5
        self.trafficContainerView.layer.cornerRadius = self.trafficContainerView.frame.size.height * 0.5
        self.stopNavigationContainerView.layer.cornerRadius = self.stopNavigationContainerView.frame.size.height * 0.5
    }

    
    func setRoute(points aPoints:[CLLocationCoordinate2D]?) {
        
        if self.currentMapViewPath != nil
        {
            self.currentMapViewPath?.map = nil
            self.currentMapViewPath = nil
        }
        
        if let newPoints = aPoints
        {
            if(newPoints.count > 0)
            {
                let path = GMSMutablePath()
                for currentPoint in newPoints{
                    
                    path.add(currentPoint)
                }
                
                let rect = GMSPolyline(path: path)
                rect.strokeWidth = 4.0
                rect.strokeColor = UIColor.darkGray
                rect.map = self.mapView
                
                self.currentMapViewPath = rect
            }
        }
        
        if(self.currentMapViewPath != nil)
        {
            self.stopNavigationContainerView.isHidden = false
            self.stopNavigationButton.setTitle(NSLocalizedString("Start Navigation", comment: ""),
                                               for: UIControlState.normal)
            self.stopNavigationButton.isUserInteractionEnabled = true
        }
        else
        {
            self.stopNavigationContainerView.isHidden = true
            self.stopNavigationButton.isUserInteractionEnabled = false
        }
    }
    
    func setNavigationEnabled(enabled aEnabled:Bool) {
        
        self.isAutoLocation = aEnabled
        self.isAutoHeading = aEnabled
        self.mapView.settings.rotateGestures = aEnabled
        
        self.isNavigationEnabled = aEnabled
        
        self.stopNavigationContainerView.isHidden = !aEnabled
        self.stopNavigationButton.isUserInteractionEnabled = aEnabled
        
        if(aEnabled == true)
        {
            self.stopNavigationButton.setTitle(NSLocalizedString("Stop Navigation", comment: ""),
                                               for: UIControlState.normal)
            
            self.delegate?.mapViewDidStartNavigation?(self)
            
            if let currentLocation = GDLocationManager.shared.currentLocation
            {
                if let currentHeading = GDLocationManager.shared.currentHeading
                {
                    self.mapView.camera = GMSCameraPosition(target: currentLocation.coordinate,
                                                            zoom: 15,
                                                            bearing: currentHeading.trueHeading,
                                                            viewingAngle: 45)
                }
                
            }
            
            self.locationCoordinateUpdated(notification: nil)
            
            
            NotificationCenter.addObserver(self,
                                           action: #selector(locationCoordinateUpdated(notification:)),
                                           name: NSNotification.Name.GD_Location_LocationUpdated)
            
            NotificationCenter.addObserver(self
                , action: #selector(locationHeadingUpdated(notification:)),
                  name: NSNotification.Name.GD_Location_HeadingUpdated)
            
            GDLocationManager.shared.start()
        }
        else
        {
            self.delegate?.mapViewDidEndNavigation?(self)
            
            self.mapView.animate(toBearing: 0)
            self.mapView.animate(toViewingAngle: 0)
            
            NotificationCenter.default.removeObserver(self,
                                                      name: NSNotification.Name.GD_Location_LocationUpdated,
                                                      object: nil)
            
            NotificationCenter.default.removeObserver(self,
                                                      name: NSNotification.Name.GD_Location_HeadingUpdated,
                                                      object: nil)
            
            self.setRoute(points: nil)
        }
    }
    
    func animateToCamera(locations aLocations:[CLLocationCoordinate2D], padding aPadding:CGFloat) {
        
        if(aLocations.count <= 1)
        {
            return
        }
        
        var padding = aPadding
        
        if(padding < 0.0)
        {
            padding = 0.0
        }
        
        var top = -90.0
        var bottom = 90.0
        var left = 180.0
        var right = -180.0
        
        for currentLocation:CLLocationCoordinate2D in aLocations {
            
            let currentLatitude = currentLocation.latitude
            
            if currentLatitude > top
            {
                top = currentLatitude
            }
            
            if currentLatitude < bottom
            {
                bottom = currentLatitude
            }
            
            let currentLongitude = currentLocation.longitude
            
            if currentLongitude < left
            {
                left = currentLongitude
            }
            
            if currentLongitude > right
            {
                right = currentLongitude
            }
            
        }
        
        let coordinate_TopLeft = CLLocationCoordinate2D(latitude: top,
                                                        longitude: left)
        
        let coordinate_BotRight = CLLocationCoordinate2D(latitude: bottom,
                                                         longitude: right)
        
        let bounds = GMSCoordinateBounds(coordinate: coordinate_TopLeft,
                                         coordinate: coordinate_BotRight)
        
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: padding))

        
    }

    @IBAction func buttonTapped(_ sender: Any) {
        
        if(sender as! UIButton === self.resetLocationButton)
        {
            if(self.isNavigationEnabled == true)
            {
                self.isAutoHeading = true
                self.isAutoLocation = true
                
                self.locationCoordinateUpdated(notification: nil)
                self.locationHeadingUpdated(notification: nil)
                
            }
            else if(self.isNavigationEnabled == false)
            {
                if let userCurrentLocation = KBAccountManager.shared.getAccount()?.lastLocation()
                {
                    self.mapView.animate(toLocation: userCurrentLocation.coordinate)
                }
            }
            
            return
        }
        
        
        if(sender as! UIButton === self.trafficButton){
            self.debugVM.isDisplayingTrafficOnMap = !self.debugVM.isDisplayingTrafficOnMap
            self.mapView.isTrafficEnabled = self.debugVM.isDisplayingTrafficOnMap
            self.trafficImageView.image = (self.debugVM.isDisplayingTrafficOnMap == true)
                ? UIImage.init(named: "ic-traffic-light-1")
                : UIImage.init(named: "ic-traffic-light-2")
            return
        }
        
        if(sender as! UIButton === self.stopNavigationButton)
        {
            self.setNavigationEnabled(enabled: !self.isNavigationEnabled)
        }
    }
    
    
    
    // MARK: - GMSMapViewDelegate
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        
        if (self.delegate != nil)
        {
            if(self.delegate?.responds(to: #selector(GDMapViewDelegate.mapView(_:didTap:))) == true)
            {
                return (self.delegate as! GDMapViewDelegate).mapView(self, didTap: marker)
            }
        }
        
        return true
        
    }
    
    
    func mapView(_ aMapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        if (self.delegate != nil)
        {
            if(self.delegate?.responds(to: #selector(GDMapViewDelegate.mapView(_:didTapAt:))) == true)
            {
                (self.delegate as! GDMapViewDelegate).mapView(self, didTapAt: coordinate)
            }
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        if(self.isNavigationEnabled == true)
        {
            if(self.isAutoLocation == true && self.isAutoHeading == true)
            {
                self.resetLocationButton.setImage(UIImage.init(named: "ic-location-8"),
                                                  for: UIControlState.normal)
            }
            else
            {
                self.resetLocationButton.setImage(UIImage.init(named: "ic-location-9"),
                                                  for: UIControlState.normal)
            }
        }
        else if(self.isNavigationEnabled == false)
        {
            if let userCurrentCoordinate = KBAccountManager.shared.getAccount()?.lastLocation()
            {
                let mapCoordinate = CLLocation(latitude: mapView.camera.target.latitude,
                                               longitude: mapView.camera.target.longitude)
                
                let distance = userCurrentCoordinate.distance(from: mapCoordinate)
                
                if(distance <= 10)
                {
                    self.resetLocationButton.setImage(UIImage.init(named: "ic-location-8"),
                                                      for: UIControlState.normal)
                }
                else
                {
                    self.resetLocationButton.setImage(UIImage.init(named: "ic-location-9"),
                                                      for: UIControlState.normal)
                }
            }
        }
        
        if (self.delegate != nil)
        {
            if(self.delegate?.responds(to: #selector(GDMapViewDelegate.mapView(_:didChange:))) == true)
            {
                (self.delegate as! GDMapViewDelegate).mapView(self, didChange: position)
            }
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        if (self.delegate != nil)
        {
            if(self.delegate?.responds(to: #selector(GDMapViewDelegate.mapView(_:idleAt:))) == true)
            {
                (self.delegate as! GDMapViewDelegate).mapView(self, idleAt: position)
            }
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
        if(gesture == true)
        {
            self.isAutoHeading = false
            self.isAutoLocation = false
        }

        
        if (self.delegate != nil)
        {
            if(self.delegate?.responds(to: #selector(GDMapViewDelegate.mapView(_:willMove:))) == true)
            {
                (self.delegate as! GDMapViewDelegate).mapView(self, willMove: gesture)
            }
        }
    }
    
    //MARK: - Location Notifications
    
    @objc func locationCoordinateUpdated(notification aNotification:Notification?){
    
        if(self.isAutoLocation == false)
        {
            return
        }
        
        if let currentLocation = GDLocationManager.shared.currentLocation
        {
            self.mapView.animate(toLocation: currentLocation.coordinate)
            
        }
     }
    
    @objc func locationHeadingUpdated(notification aNotification:Notification?){
    
        if(self.isAutoHeading == false)
        {
            return
        }
        
        
        if let heading = GDLocationManager.shared.currentHeading
        {
            self.mapView.animate(toBearing: heading.trueHeading)
        }
    }
}


@objc protocol GDMapViewDelegate {

    @objc func mapView(_ mapView: GDMapView, didTap marker: GMSMarker) -> Bool
    
    @objc func mapView(_ mapView: GDMapView, didTapAt coordinate: CLLocationCoordinate2D)
    
    @objc func mapView(_ mapView: GDMapView, didChange position: GMSCameraPosition)
    
    @objc func mapView(_ mapView: GDMapView, idleAt position: GMSCameraPosition)
    
    @objc func mapView(_ mapView: GDMapView, willMove gesture: Bool)
    
    @objc optional func mapViewDidStartNavigation(_ mapView:GDMapView)
    
    @objc optional func mapViewDidEndNavigation(_ mapView:GDMapView)
}
