//
//  KBMessageCenterViewController.swift
//  Driver
//
//  Created by Kabu on 2017-11-24.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBMessageCenterViewController: KBViewController {
    class func controller() -> KBMessageCenterViewController{
        let controller = UIStoryboard.init(name: "MessageCenter", bundle: nil)
            .instantiateViewController(withIdentifier: "KBMessageCenterViewController") as! KBMessageCenterViewController
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = UINavigationItem.LargeTitleDisplayMode.never
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
