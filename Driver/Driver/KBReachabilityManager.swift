//
//  KBReachabilityManager.swift
//  Driver
//
//  Created by Kabu on 2017-11-28.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit
import Reachability

class KBReachabilityManager: NSObject {
    static let shared = KBReachabilityManager()
    private var testConnectionTimer:Timer?
    var isConnected = true{
        didSet{
            NotificationCenter.default.post(name: NSNotification.Name.KB_Reachability_Changed,
                                            object: nil)
        }
    }
    private let reachabilityManager = AFNetworkReachabilityManager.init(forDomain: "www.google.com")
    private var isMonitorStarted = false
    func start(){
        if(self.isMonitorStarted == true){
            return
        }
        reachabilityManager.setReachabilityStatusChange({ (_ status:AFNetworkReachabilityStatus) in
            if(status == AFNetworkReachabilityStatus.notReachable){
                self.isConnected = false
            }else if(status == AFNetworkReachabilityStatus.reachableViaWiFi){
                //self.isConnected = true
                self.testConnection()
            }else if(status == AFNetworkReachabilityStatus.reachableViaWWAN){
                //self.isConnected = true
                self.testConnection()
            }
        })
        reachabilityManager.startMonitoring()
        self.isMonitorStarted = true
        /*if(self.testConnectionTimer != nil){
            return;
        }
        self.testConnectionTimer = Timer.scheduledTimer(withTimeInterval: 30.0,
                                                        repeats: true,
                                                        block: { (timer:Timer) in
                                                            // Move to a background thread to do some long running work
                                                            self.testConnection()
        })
        self.testConnectionTimer!.fire()*/
    }
    func testConnection(){
        let url = URL(string: "https://www.google.com/")!
        let request = NSMutableURLRequest.init(url: url,
                                               cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData,
                                               timeoutInterval: 30)
        request.httpMethod = "GET" // POST ,GET, PUT What you want
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            if(error != nil){
                self.isConnected = false
            }else{
                self.isConnected = true
            }
        }
        dataTask.resume()
    }
}
