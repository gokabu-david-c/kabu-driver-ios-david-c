//
//  KBPaymentNC.swift
//  Driver
//
//  Created by Jack Du on 2018-04-11.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

import Foundation

class KBPaymentNC: UINavigationController {
    class func controller() -> KBPaymentNC{
        let controller = UIStoryboard(name: "Payment", bundle: nil)
            .instantiateViewController(withIdentifier: "KBPaymentNC") as! KBPaymentNC

        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
}
