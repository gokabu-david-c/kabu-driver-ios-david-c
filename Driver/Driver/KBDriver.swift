//
//  GDUser.swift
//  Driver
//
//  Created by ZY on 2017-04-12.
//  Copyright © 2017 Developer. All rights reserved.
//

import Foundation
enum GDDriverISOnline:Int {
    case False = 0,
    True = 1
}
enum GDDriverIsReceivingNewReservationNotification:Int {
    case False = 0,
    True = 1
}
class KBDriver : NSObject, NSCoding {
    public static let Key_UserID = "id"
    public static let Key_Username = "username"
    public static let Key_UserAccessToken = "user_access_token"
    public static let Key_LastLoginAddress = "last_login_address"
    public static let Key_LastLoginCoordinate =  "last_login_latlng"
    public static let Key_LastLoginIP = "last_login_ip"
    public static let Key_LastLoginTime = "last_login_time"
    public static let Key_NickName = "nick_name"
    public static let Key_Phone = "phone"
    public static let Key_AreaID = "area_id"
    public static let Key_CarDescription = "car_desc"
    public static let Key_CarCapacity = "car_capacity"
    public static let Key_Range = "range"
    public static let KEY_IS_ONLINE = "is_online" //"status"
    public static let Key_DeviceType = "device_type"
    public static let Key_DeviceToken = "device_token"
    public static let Key_Score = "score"
    public static let Key_Photo = "photo"
    public static let Key_IS_BUSY = "is_busy"
    public static let Key_PARENT_AREA_ID = "parent_area_id"
    public static let Key_PARENT_CITY_NAME = "parent_city_name"
    public static let Key_SUB_CITY_NAME = "sub_city_name"
    public static let Key_SKIP_CHECK_ORDER_COUNT_TILL = "skip_check_order_count_till"
    public static let KEY_IS_RECEIVING_NEW_RESERVATION_NOTIFICATION = "is_receiving_new_reservation_notification"
    public static let Key_Waiver_Agree_Time = "waiver_agree_time"
    public static let Range_5 = 5
    public static let Range_8 = 8
    public static let Range_12 = 12
    public static let Range_NoLimit = 0
    public var id:Int = 0
    public var username:String = ""
    var userAccessToken:String = ""
    var last_login_latlng:String = ""
    var last_login_time:Int64 = 0
    var skip_check_order_count_till:Int64 = 0
    var nick_name:String = ""
    var phone:String = ""
    var car_desc:String = ""
    var car_capacity:Int = 0
    var range:Int = 0
    var is_online = false
    var score:Double = 0.0
    var photo:String = ""
    var is_busy = false
    var parentAreaID = 0
    var parentCityName = ""
    var subCityName = ""
    var isReceivingNewReservationNotification = false
    var waiverAgreeTime:String?
    static func rangeDisplayText(_ range:Int) -> String{
        if(range == KBDriver.Range_NoLimit){
            return NSLocalizedString("No Limit", comment: "")
        }
        return String(range) + " " + NSLocalizedString("Km", comment: "")
    }
    init(json:[String:Any]) {
        if let id = json[KBDriver.Key_UserID] as? Int{
            self.id = id
        }
        if let username = json[KBDriver.Key_Username] as? String{
            self.username = username
        }
        if let userAccessToken = json[KBDriver.Key_UserAccessToken] as? String{
            self.userAccessToken = userAccessToken
        }
        if let last_login_latlng = json[KBDriver.Key_LastLoginCoordinate] as? String{
            self.last_login_latlng = last_login_latlng
        }
        if let last_login_time = json[KBDriver.Key_LastLoginTime] as? Int64{
            self.last_login_time = last_login_time
        }
        if let nick_name = json[KBDriver.Key_NickName] as? String{
            self.nick_name = nick_name
        }
        if let phone = json[KBDriver.Key_Phone] as? String{
            self.phone = phone
        }
        if let car_desc = json[KBDriver.Key_CarDescription] as? String{
            self.car_desc = car_desc
        }
        if let car_capacity = json[KBDriver.Key_CarCapacity] as? Int{
            self.car_capacity = car_capacity
        }
        if let range = json[KBDriver.Key_Range] as? Int{
            self.range = range
        }
        if let score = json[KBDriver.Key_Score] as? String{
            self.score = Double(score.floatValue)
        }
        if let photo = json[KBDriver.Key_Photo] as? String{
            self.photo = photo
        }
        if let is_busy = json[KBDriver.Key_IS_BUSY] as? Int{
            if(is_busy == 0){
                self.is_busy = false
            }else{
                self.is_busy = true
            }
        }
        if let is_online = json[KBDriver.KEY_IS_ONLINE] as? Int{
            if(is_online == 0){
                self.is_online = false
            }else{
                self.is_online = true
            }
        }
        if let parentAreaID = json[KBDriver.Key_PARENT_AREA_ID] as? Int{
            self.parentAreaID = parentAreaID
        }
        if let parentCityName = json[KBDriver.Key_PARENT_CITY_NAME] as? String{
            self.parentCityName = parentCityName
        }
        if let subCityName = json[KBDriver.Key_SUB_CITY_NAME] as? String{
            self.subCityName = subCityName
        }
        if let isReceivingNewReservationNotification = json[KBDriver.KEY_IS_RECEIVING_NEW_RESERVATION_NOTIFICATION] as? Int{
            if(isReceivingNewReservationNotification == 0){
                self.isReceivingNewReservationNotification = false
            }else{
                self.isReceivingNewReservationNotification = true
            }
        }
        if let waiverAgreeTimeString = json[KBDriver.Key_Waiver_Agree_Time] as? String{
            self.waiverAgreeTime = waiverAgreeTimeString
        }
    }
    required init(coder decoder: NSCoder) {
        self.id = decoder.decodeInteger(forKey: KBDriver.Key_UserID)
        if let username = decoder.decodeObject(forKey: KBDriver.Key_Username) as? String{
            self.username = username
        }
        if let userAccessToken = decoder.decodeObject(forKey: KBDriver.Key_UserAccessToken) as? String{
            self.userAccessToken = userAccessToken
        }
        if let last_login_latlng = decoder.decodeObject(forKey: KBDriver.Key_LastLoginCoordinate) as? String{
            self.last_login_latlng = last_login_latlng
        }
        self.last_login_time = decoder.decodeInt64(forKey: KBDriver.Key_LastLoginTime)
        if let nick_name = decoder.decodeObject(forKey: KBDriver.Key_NickName) as? String{
            self.nick_name = nick_name
        }
        if let phone = decoder.decodeObject(forKey: KBDriver.Key_Phone) as? String{
            self.phone = phone
        }
        if let car_desc = decoder.decodeObject(forKey: KBDriver.Key_CarDescription) as? String{
            self.car_desc = car_desc
        }
        self.car_capacity = decoder.decodeInteger(forKey: KBDriver.Key_CarCapacity)
        self.range = decoder.decodeInteger(forKey: KBDriver.Key_Range)
        self.score = decoder.decodeDouble(forKey: KBDriver.Key_Score)
        if let photo = decoder.decodeObject(forKey:KBDriver.Key_Photo) as? String{
            self.photo = photo
        }
        self.is_busy = decoder.decodeBool(forKey: KBDriver.Key_IS_BUSY)
        self.is_online = decoder.decodeBool(forKey: KBDriver.KEY_IS_ONLINE)
        if let parentCityName = decoder.decodeObject(forKey: KBDriver.Key_PARENT_CITY_NAME) as? String{
            self.parentCityName = parentCityName
        }
        if let subCityName = decoder.decodeObject(forKey: KBDriver.Key_SUB_CITY_NAME) as? String{
            self.subCityName = subCityName
        }
        self.isReceivingNewReservationNotification = decoder.decodeBool(forKey: KBDriver.KEY_IS_RECEIVING_NEW_RESERVATION_NOTIFICATION)
        if let waiverAgreeTimeString = decoder.decodeObject(forKey: KBDriver.Key_Waiver_Agree_Time) as? String{
            self.waiverAgreeTime = waiverAgreeTimeString
        }
    }
    func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: KBDriver.Key_UserID)
        coder.encode(self.username, forKey: KBDriver.Key_Username)
        coder.encode(self.userAccessToken, forKey: KBDriver.Key_UserAccessToken)
        coder.encode(self.last_login_latlng, forKey:KBDriver.Key_LastLoginCoordinate)
        coder.encode(self.last_login_time, forKey:KBDriver.Key_LastLoginTime)
        coder.encode(self.nick_name, forKey:KBDriver.Key_NickName)
        coder.encode(self.phone, forKey:KBDriver.Key_Phone)
        coder.encode(self.car_desc, forKey:KBDriver.Key_CarDescription)
        coder.encode(self.car_capacity, forKey:KBDriver.Key_CarCapacity)
        coder.encode(self.range, forKey: KBDriver.Key_Range)
        coder.encode(self.score, forKey:KBDriver.Key_Score)
        coder.encode(self.photo, forKey: KBDriver.Key_Photo)
        coder.encode(self.is_busy, forKey: KBDriver.Key_IS_BUSY)
        coder.encode(self.is_online, forKey: KBDriver.KEY_IS_ONLINE)
        coder.encode(self.parentCityName, forKey: KBDriver.Key_PARENT_CITY_NAME)
        coder.encode(self.subCityName, forKey: KBDriver.Key_SUB_CITY_NAME)
        coder.encode(self.isReceivingNewReservationNotification, forKey: KBDriver.KEY_IS_RECEIVING_NEW_RESERVATION_NOTIFICATION)
        coder.encode(self.waiverAgreeTime, forKey: KBDriver.Key_Waiver_Agree_Time)
    }
    func rangeDisplayText() -> String {
        return KBDriver.rangeDisplayText(self.range)
    }
    func lastLocation() -> CLLocation? {
        if(self.last_login_latlng.count == 0){
            return nil
        }
        if(self.last_login_latlng.contains(",") == false){
            return nil
        }
        let coordinateData = self.last_login_latlng.split(separator: ",").map(String.init)
        if(coordinateData.count <= 1){
            return nil
        }
        let latitude = coordinateData[0].floatValue
        let longitude = coordinateData[1].floatValue
        return CLLocation(latitude: CLLocationDegrees(latitude),
                          longitude: CLLocationDegrees(longitude))
    }
    var photoURL:URL?{
        if(self.photo.count == 0){
            return nil
        }
        if self.photo.starts(with: "http://") == false && self.photo.starts(with: "https://") == false{
            let urlString = KBConfig.driverWebDomain + "images/" + self.photo
            return URL(string: urlString)
        }
        return URL(string:self.photo)
    }
    var isAccountAgreedWaiver:Bool{
        if(self.waiverAgreeTime == nil || self.waiverAgreeTime == ""){
            return false;
        }
        return true;
    }
    override func isEqual(_ object: Any?) -> Bool {
        if let right = object as? KBDriver{
            if(self.id != right.id){
                return false
            }
            if(self.username != right.username){
                return false
            }
            if(self.userAccessToken != right.userAccessToken){
                return false
            }
            if(self.last_login_latlng != right.last_login_latlng){
                return false
            }
            if(self.last_login_time != right.last_login_time){
                return false
            }
            if(self.nick_name != right.nick_name){
                return false
            }
            if(self.phone != right.phone){
                return false
            }
            if(self.car_desc != right.car_desc){
                return false
            }
            if(self.car_capacity != right.car_capacity){
                return false
            }
            if(self.range != right.range){
                return false
            }
            if(self.is_online != right.is_online){
                return false;
            }
            if(self.is_busy != right.is_busy){
                return false;
            }
            if(self.score != right.score){
                return false
            }
            if(self.photo != right.photo){
                return false
            }
            return true
        }
        return false
    }
}
