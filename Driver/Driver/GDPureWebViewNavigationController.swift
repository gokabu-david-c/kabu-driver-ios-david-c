//
//  GDPureWebViewNavigationController.swift
//  Driver
//
//  Created by ZY on 2017-07-28.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDPureWebViewNavigationController: UINavigationController {
    //weak var webViewNavigationControllerDelegate:GDPureWebViewNavigationControllerDelegate?
    var urlString:String?
    var controllerTitle:String?
    var showCloseButton = true
    var closeButtonAction:(() -> ())?
    class func controller(urlString aURLString:String?,
                          title aTitle:String?) -> GDPureWebViewNavigationController{
        let controller = UIStoryboard(name: "GDPureWebViewController", bundle: nil)
            .instantiateViewController(withIdentifier: "GDPureWebViewNavigationController") as! GDPureWebViewNavigationController
        controller.urlString = aURLString
        if aTitle != nil{
            controller.controllerTitle = aTitle
        }
        return controller
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if let childController = self.childViewControllers.first as? GDPureWebViewController{
            childController.urlString = self.urlString
            childController.title = self.controllerTitle
            if(self.showCloseButton){
                let closeButton = UIBarButtonItem()
                closeButton.title = ""
                closeButton.image = UIImage.init(named: "ic-close")
                closeButton.target = self
                closeButton.action = #selector(closeButtonClicked)
                childController.navigationItem.leftBarButtonItem = closeButton
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func childController() -> GDPureWebViewController? {
        if let controller = self.childViewControllers.first as? GDPureWebViewController{
            return controller;
        }
        return nil
    }
    @objc func closeButtonClicked() {
        self.closeButtonAction?()
    }
    /*func dismissController(){
        if let childController = self.childViewControllers.first{
            childController.dismiss(animated: true, completion: {
                self.webViewNavigationControllerDelegate?.GDPureWebViewNavigationControllerDidDismiss?(sender:self)
            })
        }
    }*/
}

/*@objc protocol GDPureWebViewNavigationControllerDelegate : class {
    @objc optional func GDPureWebViewNavigationControllerDidDismiss(sender aSender:GDPureWebViewNavigationController)
}*/
