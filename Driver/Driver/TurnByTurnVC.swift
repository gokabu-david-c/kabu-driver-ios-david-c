//
//  TurnByTurnVC.swift
//  Driver
//
//  Created by Jack Du on 2018-05-31.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

//import MapboxDirections
//import MapboxCoreNavigation
//import MapboxNavigation
//
//class TurnByTurnVC: UIViewController {
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        let origin = Waypoint(coordinate: CLLocationCoordinate2D(latitude: 49.261669, longitude: -123.105782), name: "Mapbox")
////        let origin = Waypoint(coordinate: CLLocationCoordinate2D(latitude: 38.9131752, longitude: -77.0324047), name: "Mapbox")
//        let destination = Waypoint(coordinate: CLLocationCoordinate2D(latitude: 49.281669, longitude: -123.125782), name: "White House")
//        
//        let options = NavigationRouteOptions(waypoints: [origin, destination])
//        
//        Directions.shared.calculate(options) { (waypoints, routes, error) in
//            guard let route = routes?.first else { return }
//            
//            let viewController = MapBoxNavigationController(for: route)
//            self.present(viewController, animated: true, completion: nil)
//        }
//    }
//    
//}
