//
//  KBOpenOrderNotificationVC.swift
//  Driver
//
//  Created by Jack Du on 2018-04-16.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

class KBOpenOrderNotifcationVC : UIViewController {
    
    var currentOrderView : KBOpenOrderNotificationView?
    var currentOrders : [KBOrder] = []
    let reservationPoolVM = KBReservationPoolVM()
    var currentView : KBOpenOrderNotificationView?
    let ordersHelper : KBOrdersHelper = KBOrdersHelper()
    var racedPickupTimeList : [Int64] = [Int64]()
    let systemVM = KBSystemSettingsVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateTimestamps()
        self.setupViews()
        self.showOpenOrderView(currentOrders.first!)
        NotificationCenter.addObserver(self, action: .loadNewOrders, name: NSNotification.Name.KB_ReservationPool_OrderChanged)
    }
    
    func showOpenOrderView(_ order:KBOrder){
        let newOrderView = UINib(nibName: "KBOpenOrderNotificationView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! KBOpenOrderNotificationView
        newOrderView.closeButton.addTarget(self, action: .closeButtonAction, for: .touchUpInside)
        newOrderView.countdownButton.addTarget(self, action: .raceForOrder, for: .touchUpInside)
        newOrderView.setOpenOrder(openOrder: order, parentVC: self)
        newOrderView.frame.origin.x = 600
        self.currentView = newOrderView
        self.view.addSubview(self.currentView!)
        self.animateEaseInOrder()
    }
    
}

//New Reservation Orders Functions
extension KBOpenOrderNotifcationVC{
    
    @objc func loadNewOrders(){
        let newOrders = reservationPoolVM.getNewOrders()
        //Setup when the VC loaded
        if(currentOrders.count < 1){
            currentOrders = newOrders
            if(currentOrders.count>0){
                self.showOpenOrderView(currentOrders.first!)
            }
            else{
                self.dismissView()
            }
        }
        else{
            currentOrders = ordersHelper.addNewReserveOrders(oldOrders: currentOrders, newOrders: newOrders)
        }
        updateTimestamps()
    }
    
    @objc func raceForOrder(){
        self.showLoading("")
        currentView?.stopTimer()

        guard let orderNumber = currentOrders[0].orderNumber else{
            return
        }
        self.currentView?.closeButton.isEnabled = false
        KBReservationPoolVC().reservationPoolVM.raceForOrder(orderNumber: orderNumber,
                                                             start: {()
        }, success: {
            self.dismissLoading()
            self.currentView?.raceSuccessful()
            self.racedPickupTimeList.append(self.currentOrders[0].reservationPickupTime)
            Timer.scheduledTimer(timeInterval: 2, target: self, selector: .dismissView, userInfo: nil, repeats: false)
        }, failure: { (error:NSError) in
            self.dismissLoading()
            self.showError(error.message)
            Timer.scheduledTimer(timeInterval: 1, target: self, selector: .dismissView, userInfo: nil, repeats: false)
        })
    }
    
    func processReservedOrders(){
        let orderHelper = KBOrdersHelper.init()
        var processOrders = [KBOrder]()
        for order in currentOrders{
            if(orderHelper.validateReserveOrder(reservedOrder: order, openPickupTimeList: racedPickupTimeList)){
                processOrders.append(order)
            }
        }
        currentOrders = processOrders
    }
    
    func updateTimestamps(){
        let lastTimeStamp = GDAppManager.shared.lastNewReservationTime ?? 0
        for order in currentOrders{
            if(order.timeCreated > lastTimeStamp){
                GDAppManager.shared.lastNewReservationTime = order.timeCreated
            }
        }
        GDAppManager.shared.lastReservationPopupTime = Int64(Date().timeIntervalSince1970)
    }
    
}


//UI Functions
extension KBOpenOrderNotifcationVC{
    func setupViews(){
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: .dismissAllAlert)
        dismissGesture.numberOfTapsRequired = 1
        dismissGesture.numberOfTouchesRequired = 1
        currentView?.dismissView1.addGestureRecognizer(dismissGesture)
        currentView?.dismissView2.addGestureRecognizer(dismissGesture)

        currentView?.dismissView1.isUserInteractionEnabled = true
        currentView?.dismissView2.isUserInteractionEnabled = true

    }
    
    @objc func dismissView(){
        updateTimestamps()
        
        if(currentOrders.count>0){
            currentOrders.remove(at: 0)
        }
        processReservedOrders()
        self.animateEaseOutOrder()
        if(currentOrders.count < 1){
            GDAppManager.shared.lastReservationPopupTime = Int64(Date().timeIntervalSince1970)
            self.dismiss(animated: true, completion: {})
        }
        else{
            self.showOpenOrderView(currentOrders[0])
        }
    }
    
    @objc func dismissAll(){
        systemVM.pauseReservationPopup()
        self.dismiss(animated: true, completion: {})
    }
    @objc func dismissAllAlert(){
        let alert = AlertViewHelper().dismissPopupTemporary(self, okAction: .dismissAll)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func animateEaseInOrder(){
        UIView.animate(withDuration: 0.4, animations: {
            self.currentView?.frame.origin.x = 0
        }, completion: {(finished:Bool) in
            ()
        })
    }
    
    func animateEaseOutOrder(){
        UIView.animate(withDuration: 0.4, animations: {
            self.currentView?.frame.origin.x = -600
            
        }, completion: {(finished:Bool) in
            ()
        })
    }
 
    @objc func closeButtonAction(){
         if(currentOrders.count>5){
            let alert = AlertViewHelper.init().makeRaceOrderTooManyAlert(self, okAction: .dismissAll, cancelAction: .dismissView)
            self.present(alert, animated: true, completion: nil)
         }
         else{
            dismissView()
        }

    }
    
}

extension  KBOpenOrderNotifcationVC : KBOpenOrderNotificationViewDelegate{
    func onFinishedNotificationCountdown() {
        self.dismissView()
    }
}

fileprivate extension Selector {
    static let dismissView = #selector(KBOpenOrderNotifcationVC.dismissView)
    static let dismissAll = #selector(KBOpenOrderNotifcationVC.dismissAll)
    static let dismissAllAlert = #selector(KBOpenOrderNotifcationVC.dismissAllAlert)
    static let raceForOrder = #selector(KBOpenOrderNotifcationVC.raceForOrder)
    static let easeInOrder = #selector(KBOpenOrderNotifcationVC.animateEaseInOrder)
    static let loadNewOrders = #selector(KBOpenOrderNotifcationVC.loadNewOrders)
    static let closeButtonAction = #selector(KBOpenOrderNotifcationVC.closeButtonAction)

}
