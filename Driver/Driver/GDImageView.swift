//
//  GDImageView.swift
//  Driver
//
//  Created by ZY on 2017-04-09.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class GDImageView: UIImageView {
    var cornerRadius:CGFloat = 0.0
    override func layoutSubviews() {
        superview?.layoutSubviews()
        self.layer.cornerRadius = self.cornerRadius
    }
}
