//
//  KBReservationPoolVM.swift
//  Driver
//
//  Created by Kabu on 2017-11-22.
//  Copyright © 2017 Developer. All rights reserved.
//

import UIKit

class KBReservationPoolVM: NSObject {
    
    var isDownloadingReservationPool:Bool{
        didSet{
            if(self.isDownloadingReservationPool != oldValue){
                self.isDownloadingReservationPoolChanged?()
            }
        }
    }
    var lastUpdateTime:Date?{
        didSet{
            if(self.lastUpdateTime != oldValue){
                self.lastUpdateTimeChanged?()
            }
        }
    }
    var orders:[KBOrder] {
        didSet{
            var oldOrderIDs = ""
            for currentValue in oldValue{
                oldOrderIDs += currentValue.orderNumber!
                if(currentValue !== oldValue.last){
                    oldOrderIDs += ","
                }
            }
            var newOrderIDs = ""
            for currentOrder in self.orders{
                newOrderIDs += currentOrder.orderNumber!
                if(currentOrder !== self.orders.last){
                    newOrderIDs += ","
                }
            }
            if(oldOrderIDs != newOrderIDs){
                self.ordersChanged?()
            }
        }
    }
    var reservationPoolError:String?{
        didSet{
            if(self.reservationPoolError != oldValue){
                self.reservationPoolErrorChanged?();
            }
        }
    }
    override init() {
        self.isDownloadingReservationPool = true
        self.reservationPoolError = KBReservationPoolManager.shared.reservationPoolError
        self.lastUpdateTime = KBReservationPoolManager.shared.reservationPoolLastUpdateTime
        self.orders = KBReservationPoolManager.shared.reservationOrders
        super.init()
        NotificationCenter.addObserver(self,
                                       action: #selector(performOrdersChanged),
                                       name: NSNotification.Name.KB_ReservationPool_OrderChanged)
        NotificationCenter.addObserver(self,
                                       action: #selector(performOrdersDataChanged),
                                       name: NSNotification.Name.KB_ReservationPool_OrderDataChanged)
        NotificationCenter.addObserver(self,
                                       action: #selector(performDownloadingStatusChanged),
                                       name: NSNotification.Name.KB_ReservationPool_DownloadingStatusChanged)
        NotificationCenter.addObserver(self,
                                       action: #selector(performErrorChanged),
                                       name: NSNotification.Name.KB_ReservationPool_ErrorChanged)
        NotificationCenter.addObserver(self,
                                       action: #selector(performLastUpdateTimeChanged),
                                       name: NSNotification.Name.KB_ReservationPool_LastUpdateTimeChanged)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    var ordersChanged:(() -> ())?
    //var ordersDataChanged:(() -> ())?
    var isDownloadingReservationPoolChanged:(() -> ())?
    var reservationPoolErrorChanged:(() -> ())?
    var lastUpdateTimeChanged:(() -> ())?
    @objc private func performOrdersChanged(){
        self.orders = KBReservationPoolManager.shared.reservationOrders
    }
    @objc private func performOrdersDataChanged(){
        self.orders = KBReservationPoolManager.shared.reservationOrders
    }
    @objc private func performLastUpdateTimeChanged(){
        self.lastUpdateTime = KBReservationPoolManager.shared.reservationPoolLastUpdateTime
    }
    @objc private func performErrorChanged(){
        self.reservationPoolError = KBReservationPoolManager.shared.reservationPoolError
    }
    @objc private func performDownloadingStatusChanged(){
        self.isDownloadingReservationPool = KBReservationPoolManager.shared.isDownloadingReservationPool
    }
    func downloadReservationPool(start aStart:(() -> ())?,
                                 success aSuccess:@escaping(() -> ()),
                                 failure aFailure:@escaping((_ aError:NSError) -> ())){
        KBReservationPoolManager.shared.downloadReservationPool(start: {
            aStart?()
        },success: {
            aSuccess()
        }) { (error:NSError) in
            aFailure(error)
        }
    }
    func raceForOrder(orderNumber aOrderNumber:String,
                      start aStart:(() -> ())?,
                      success aSuccess:@escaping(() -> ()),
                      failure aFailure:@escaping((_ aError:NSError) -> ())){
        KBReservationPoolManager.shared.raceForOrder(orderNumber: aOrderNumber,
                                                     start: {
                                                        aStart?()
        }, success: {
            aSuccess()
        }) { (error:NSError) in
            aFailure(error)
        }
    }
    func getOrder(orderNumber aOrderNumber:String) -> KBOrder?{
        for currentOrder in self.orders{
            if currentOrder.orderNumber == aOrderNumber{
                return currentOrder
            }
        }
        return nil
    }
    

    func getNewOrders() -> [KBOrder]{
        var newOrders : [KBOrder] = [KBOrder]()
        
        let currentTime = GDAppManager.shared.lastReservationPopupTime ?? Int64(Date().timeIntervalSince1970)
        let lastNewReservationTime = GDAppManager.shared.lastNewReservationTime ?? 0
        
        for currentOrder in self.orders{
            if(currentOrder.timeCreated>=currentTime && currentOrder.timeCreated > lastNewReservationTime){
                newOrders.append(currentOrder)
            }
            else if(currentTime - currentOrder.timeCreated < 30 && currentOrder.timeCreated > lastNewReservationTime){
                newOrders.append(currentOrder)
            }
        }
        print("Number of New Orders: \(String(newOrders.count))")

        return newOrders
    }
}
