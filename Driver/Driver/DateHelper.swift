//
//  DateHelper.swift
//  Driver
//
//  Created by Jack Du on 2018-04-20.
//  Copyright © 2018 Gokabu Technologies Inc. All rights reserved.
//

import Foundation

class DateHelper: NSObject {
    
    func dayDifference(_ date : Date) -> String
    {
        
        let calendar = NSCalendar.current
        if calendar.isDateInToday(date) { return NSLocalizedString("Today", comment: "") }
        else if calendar.isDateInTomorrow(date) { return NSLocalizedString("Tomorrow", comment: "") }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 0 { return "\(day) " + NSLocalizedString("days before", comment: "")}
            else { return "\(day) " + NSLocalizedString("days after", comment: "")}
        }
    }

    
}
